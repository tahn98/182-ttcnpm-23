INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(5,'chapter 1','Amamiya Hiroto has been reincarnated inLambda!

Amamiya Hiroto has been cursed by the god of transmigration, Rodcorte!

I think I heard this kind of announcement inside my head, but I dont really remember too well. In a state of being neither awake nor asleep, my consciousness gradually begins to take shape.

Where is this place? What kind of situation am I in now? Though I get the feeling that Im alive.

But I dont have a good idea of what kind of circumstances Im in.

Even if I open my eyes, only darkness fills my vision and my arms and legs arent moving well, as if Im in the middle of a dream.

My entire body is submerged in a warm fluid, and I cant breathe. But I dont feel any discomfort from this. Its like Ive become a fish.

Surely its not possible that Ive been reincarnated as something other than a human?

Its possible, since Ive been cursed, after all. Things might work out if Im still a human, at least, but if Ive become an animal or a fish then I have no idea what Im supposed to do.

But fortunately, my suspicions are proven to be wrong.



I can hear a voice. Its not crying or shouting its a gentle, singing voice.

The sound is strangely muffled, so I cant make out the lyrics properly. But I can feel the emotion in the song.
Love.

I see, Im inside the womb. Im a fetus right now.

It seems that the gods curse isnt able to influence me before Im reborn, at least.

From the fragments of information about the situation that Rodcorte put in my mind before I was reincarnated, Lambda seems to be a world with swords and magic. I might be some other species like an Elf or a Dwarf, but as long as Im a sentient being, I dont have a problem with that.

Is it just my imagination? This song sounds kind of like Japanese

And then my consciousness melts away again.

The next time my consciousness returns, Ive already been born.

Youre always quiet, arent you, Vandalieu? Its good that you are, but youre allowed to cry from time to time, you know?

I quietly look up at the woman whos holding me and speaking to me.

I suppose Vandalieu is my new name. Its a lot better than the serial number I was assigned in Origin. And this person must be my third mother. She seems much better than the one I had in Origin as well.

By the time I was selfaware in Origin, Id already been sold, so I have no complaints about this beginning to my life. Considering I have a curse in place of a gods blessing, this could be called a miracle.

Well then, what kind of environment have I been born into? It seems my mother is a Dark Elf.

The mother reflected in Vandalieus eyes was a woman in her early twenties with golden hair and dark brown skin. She was a beautiful woman with pleasant features Vandalieu could have high hopes for his own appearance if this person was his mother.

The tips of her ears were pointed.

Even Vandalieu, who had only just been reborn into this world, could guess that she was a member of the darkskinned Dark Elf race.

Well, it was not entirely impossible that she was simply a tanned Elf.

Well, if thats the case, I suppose it makes sense that she lives in a cave, huh.

Indeed, the two of them were not in a cavelike house, but an actual cave.

There was a door and furs are spread out across the floor in place of a carpet, but it wasnt a particularly civilized home.

But the Elves in fantasy stories on Earth were generally people that live in harmony with nature, so perhaps this kind of thing was normal for them.

Well, the more important problem is my own body.

His mothers warm arms were wrapped around his fragile body. He looked at her soft skin that reminded him of chocolate, and then at his own plump, useless hands.

His hands were as white as silk.

Why is my color different? Though my ears do seem to be pointed.

His mother was a Dark Elf, but his skin was white. Could it be that he wasnt a Dark Elf himself? He didnt know how Dark Elves bodies change over the course of their lives. Could it be that they were born with white skin that turned darker as they grew into adults?

Or could it be that this person was not biologically his mother?

Hmm? Are you curious as to why your mommys skin is a different color? Youre clever, arent you, Vandalieu? To notice that already. But dont worry. Vandalieu, you just look a lot like your daddy, but you are definitely your mommy, Darcias, son.(Darcia)

As Darcia, Vandalieus mother, smiled gently, her words dispelled his doubts. He realized that he was a child of mixed blood, born to parents of different races.

There was a chance that Darcia was lying, but he had no intention of doubting her.

He wanted to simply enjoy the love that he was being given and feel at ease, rather than spend his time and emotional endurance on doubting her words.

And Sleep

And so Darcia lulled Vandalieu to sleep.

As a threemonthold, Vandalieu was a good son who didnt cause any trouble for Darcia, who was going through her first experience raising a child.

Ah(Vandalieu)

When he was hungry, he made a noise like this and patted his stomach or pointed at Darcias chest and complained. When his diaper needed changing, he made a similar noise and patted his own hips.

Yes here is Mommys breast Youre so wellbehaved, Vandalieu(Darcia)

As Darcia raised Vandalieu up in her arms and exposed her nipple to feed him her milk, she thought,what a good boy.

Of course, she knew that this wasnt normal. That he was too clever.

But she didnt do anything to make her own son uncomfortable.

Maybe its because he takes after his father after that person.

She suspected that the intelligence of her threemonthold son was due to his fathers blood.

That and the fact that she had recently often been feeling some strange Mana coming from Vandalieu only gave her more reason to believe that this was the case. His fathers race was more proficient at using magic than Dark Elves.

What bothers me more is the fact that he doesnt laugh or cry I wonder if he feels anxious because he doesnt have a father?

What bothered Darcia was the fact that Vandalieu didnt laugh or cry like a normal infant.

When her beloved son was hungry or even when she tickled him, he was always as expressionless as a doll. She had thought that he was just in a bad mood at first, but it seemed that wasnt the case.

It also didnt seem to be the case that his emotions were simply underdeveloped. She had once spotted him silently crying with an empty expression.

On that occasion she had thought that he might be sick and applied all the healing magic that she knew, but it seemed that he had simply been crying after having a scary nightmare.

But the thing that bothers me the most is when I give you my breast. Does Mommys breast not taste nice?(Darcia)

What bothered Darcia the most about her sons behavior was that when she went to breastfeed him, he didnt suck on her breast straight away, but instead let his eyes wander without moving for a while first.

He drank the milk in the end, but Darcia was worried that there was something wrong with her own milk.

Vandalieu, who was in the middle of the important task of sucking milk from his mothers nipple, was filled with feelings of embarrassment and guilt.

Its not easy being an infant, huh.

One would normally be happy and proud to have such a young and beautiful mother. However, on the inside, he was a man who had lived about thirtyseven years over his two previous lives, so he had mixed feelings about it.

Though his body is that of an infant, his mind is that of an adult so he feels overly conscious about it. He has been especially conscious of members of the opposite sex since he was a highschooler on Earth.

I wonder how the others felt? In Origin, I was about ten months old when my memories returned.

Had the others who had been reborn in Origin felt embarrassment when their mother changed their diapers?

But I cant make my mother worry about it forever. Ill just have to get used to it as soon as possible.

Vandalieu had already imprinted in his mind that Darcia, his third mother, was his mother. He had no discomfort with this fact. In any case, despite this being his third life, this was the first time he was experiencing a mothers love. It would be impossible for him to reject it.

And so, at first, he tried his best to act like a normal infant But it was impossible.

It was impossible for him to act like an infant when he was a teenager on the inside. And for some reason, he found himself unable to laugh or cry. He couldnt change his facial expression at all.

At first, he had thought his facial muscles were paralyzed, but he found that his mouth and eyelids moved normally. But they didnt move naturally. He could only move them when he put conscious effort into it.

Was this because of the curse?

Well, Ill ask Mother about my expression not changing when Im able to speak Theres all kinds of problems, but Ive learned quite a lot during the past two months as well.

Darcia wasnt aware that Vandalieu understood her words, so she hadnt explained her past or current situation in detail, but he had a general idea from having lived with her for two months.

First, it seemed that Darcias husband, Vandalieus father, was a member of a greatlyfeared race. As a mixed child of that race, Vandalieu knew that he would likely be the target of discrimination and persecution.

This was the reason why Darcia was living in this cave that she had dug out using spiritual magic in a forest away from where other people lived.

It was no wonder that he had never seen a single person in the two months he had been here other than his own mother.

I know that shes planning to return the Dark Elf village once Ive grown up to some extent. And that this place is the country of Mirg, a part of the Amid Empire in the northwest part of the Bahn Gaia continent.

It seemed that something would work out if she returned to the Dark Elf Village, so for now, her objective was probably to gather strength for that purpose.

For that reason, even though Vandalieu wasnt even able to crawl yet, he began training himself in using magic.

The deathattribute magic that he had become able to use at will during his previous life. It would surely be helpful for his journey with his mother, or so Vandalieu thought. But for some reason, he couldnt use it very well.

Is this because my bodys still that of an infant? Or is it because of the curse?

He considered this possibility. But giving up now because he couldnt use his magic properly wasnt an option. Because for Vandalieu, who didnt have any cheatlike powers or aptitude for any other types of magic, this deathattribute magic was his only weapon.

It would be nice if I could see Vandalieus status.(Darcia)

Vandalieu was about a month and a half old when he heard these words from Darcia.

Status?

Vandalieu was unsuccessfully trying to use his deathattribute magic and was wishing that he could at least be able to crawl across the floor while keeping his head raised when he heard these from his mother. This isnt a game, he thought, but he was surprised when his own status was displayed inside his head.

        Name Vandalieu

 

        Race Dhampir (Dark Elf)

 

        Age 0 years old

 

        Nickname None

 

        Job None

 

        Level 0

 

        Job history None

 

        Attributes

 

                Vitality 12
                Mana 100,000,000
                Strength 10
                Agility 1
                Endurance 25
                Intelligence 20

 

        Passive skills
                Superhuman Strength Level 1
                Rapid Healing Level 1

 

        Active skills
                None

 

        Curses
                Experience gained in previous life not carried over
                Cannot learn existing jobs
                Unable to gain experience independently

 

 

Wow, my Mana is no joke. Also, it seems my father was a Vampire. And I have three curses, just how badly does that god want me to kill myself?

There were several things displayed on Vandalieus status that surprised him.

To start with, his race had been set as Dhampir. If that meant the same thing in Lambda as it did on Earth, that meant that he was a halfVampire.

I see, that explains why she has to live in this hidden place. I dont know what religions there are in Lambda, but I doubt theyd be very welcoming to a mixedblood child of a Vampire.

Isnt it actually quite likely that theyll treat me like a monster and try to exterminate me? At the very least, this is something that might endanger my life.

The possibility of facing discrimination and prejudice due to being a Dhampir would always be there. Vandalieus life had been set to a hard mode even more extreme that he had expected.

The next surprising thing was his Mana. There was no MP stat on the status screen, so Mana must be the MP stat in the world of Lambda. But the number next to this stat was 100,000,000.

He didnt know what the average for this stat was, but this was surely an abnormal number.

Was this what Rodcorte had mentioned earlier, the Mana that was housed in theempty frameleft by his lack of cheatlike abilities, fortune and destiny? He had been unable to see anything like a status screen in Origin so he had no indication of how vast it was other than the words of the researchers who had saidThis is over ten thousand times greater than a firstclass Mage!But now that he sees the number with his own eyes, it really is amazing.

Well, Im a powerless infant right now, though.

No matter how much MP he had, there was no point if he couldnt use magic. He had to relearn how to use his deathattribute magic as soon as possible.

As for the rest, I seem to have normal strength and my agility is, well, like that. Endurance is like stamina, but intelligence doesnt seem to be as simple as just a measure of how smart I am. Its probably a stat that affects how fast I learn magic, the power of its effects, simultaneous casting and how much incantation Ill need to perform it. It could even be related to my willpower.

As Vandalieu deciphered his status, he understood why his agility was lower than every other stat. He was a month and a half old, but he couldnt even keep his head raised. He couldnt even roll over in bed by himself, let alone crawl. Theres no way this kind of infant could be considered agile.

But on the other hand, considering that he was a level 0 baby, his other stats were pretty high, though not as high as his Mana.

As expected of a Dhampir, he thought.

Next were his skills. From Vandalieus knowledge of games, he knew that passive skills were ones that exhibited their effects without requiring conscious effort while active skills required conscious use.

Superhuman Strength and Rapid Healing, huh. These are probably skills that I have because Im a Dhampir. Something like racial traits that you see in games, I suppose. But as I thought, deathattribute magic isnt listed on there.

His list of skills was mostly empty because he was still a baby, but Vandalieu was someone who had experienced thirtyseven years of life on Earth and in Origin. At least the skill of using elementarylevel deathattribute magic that he had managed to learn in his previous life should have been there.

The reason it wasnt was probably the three curses listed next.

The curse that prevents my experience from my previous life from being carried over, its probably because of that. Thats why I dont have my skills from my previous life or my life before that. The other two curses arent exactly great news for me, either. Those will make it really hard for me to learn a job or level up.

After all, these were curses that Rodcorte had placed on him in order to encourage him to kill himself. Knowing that they were extremely troublesome curses, Vandalieu let out a great sigh.

But choosing to just die quietly isnt an option. As long as thats the case, I have to keep gathering strength so that I can survive. Ill keep trying to learn to use my deathattribute magic today as well, and Ah Its no use Im sleepy.

Vandalieu, like any other sixweekold infant, was defeated by his sleepiness.

You have acquired the DeathAttribute Magic skill!

This announcement echoed inside Vandalieus head.

Three months after his birth, around the time he was finally able to support the weight of his own head with his neck, Vandalieu was finally successful in learning deathattribute magic.

Ive spent about two months failing to get my Mana to take the form of a spell. I guess this is the result of my hard work.

With that said, the only spells he could use wasSterilization, a spell that exterminated microscopic organisms within its range, andBug Killer, a version of the same spell that did the same thing to insects. The other spell he could use wasMagic Absorption Barrier, a spell that created a barrier that absorbed Mana that it came into contact with, something that had no immediately no visible effects.

He wanted to show Darcia, his mother, that he could use magic, but that proved difficult. It would be simple to show her that he couldabsorbher spells, but she never used her spiritual magic in his sight.

She probably used spiritual magic to at least create fire to use for cooking, but

With this body thats only just become able to sit up, I cant exactly get a good look around the room.

As Vandalieu exhales to sit up on his bed, his mother simply laughs, thinkingHes kind of like an old man, but pays him no further attention.

But Im grateful that shes at least taking me outside. I was about to die of boredom.

Perhaps it was because he was now three months old Darcia had started taking him outside in her arms on sunny days.

The reason for this was that she had decided that it would be a good stimulus for a growing child, and her stores of food were also getting dangerously low, so she was going outside to gather more.

But Vandalieu remembers the way she had been strangely cautious the first time she had taken him outside.

She had opened the door to the cave to allow just a little sunlight through and let it shine lightly on Vandalieus fingertip.

What a relief! You didnt inherit the Vampires weakness!(Darcia)

As Vandalieu watched Darcia cry out in joy, he was reminded that his father was a Vampire. It would have been really troublesome if, as a Dhampir, he had even inherited his fathers inability to tolerate sunlight.

Well then, lets go outside with Mommy from now on, okay?(Darcia)

As Darcia said that and took Vandalieu outside, the sight of the world beyond the cave moved Vandalieu to the point that he was at a loss for words (not that he could speak words in the first place).

Oh The world Nature Its so huge!

The outside of the cave was a forest. The air was fresh.

The sun was bright, the sky was a transparent blue, the clouds were pure white and the thickly growing trees were bright green.

It was the scenery of a forest with no particular features, but Vandalieu had spent twenty years imprisoned in a small room before being finished off by Amemiya Hiroto and Narumi. In his eyes, everything seemed to shine beautifully.

Fufu, it seems youve taken a liking to being outside.(Darcia)

His face was expressionless as usual, but Darcia understood from the fact that he was gazing at their surroundings as if he was in a gaze that he was very happy. She began walking around and gathering food.

Of course, she didnt do dangerous things like hunting animals using her bow and spiritual magic. She gathered edible grasses, fruits and mushrooms and laid some traps to catch animals later.

Most of the food she gathered went into her own mouth, while a little was set aside to be made into baby food for Vandalieu.

I have mixed feelings about this.

Once a day, his young, beautiful Dark Elf mother saidsay ahand fed him a spoonful of food. And with that, the amount of time she spent breastfeeding him decreased.

In order to survive from now on and make the journey from this area that was dangerous for a Dhampir to live to the Dark Elf village, weaning was necessary for Vandalieu. But even so, growing was a difficult task for him.

Incidentally, he enjoyed the taste of his mothers milk more than the baby food.

The DeathAttribute Magic skill has been upgraded to level 2! The following new skills have been added Status Effect Resistance, Magic Resistance, Night Vision, Bloodsucking!

Once Vandalieu reached five months of age, Darcia began leaving him in the cave for hours at a time to go out and gather food from the forest.

In a little while, well be going to where Mommy was born. I have to make preparations for that, so even if you get lonely, bear with it, okay?(Darcia)

Saying that, she left to hunt animals and went to the village, pretending to be a passing adventurer to buy supplies. There were times she didnt come home for half a day at a time, but it was necessary to get the supplies they needed to survive, so there was no helping it.

Leaving an infant alone at home for half a day would have been questionable on Earth, but Vandalieu himself had no complaints about it. Because everything was being done for his own protection.

With no help from anyone else, Darcia was raising an infant by herself. A troublesome halfVampire child, no less.

Judging from the fact that Vandalieu had not seen his father even once, it seemed that there was no help coming from the Vampire community either. Vampires looked down on halfVampires just as much as humans did, if not more. HalfVampires were often the targets of persecution by Vampires as well. Even in Japan, where people had a sense of values as human beings, people of mixed foreign blood were discriminated against. It was no surprise that halfbloods of different races in Lambda experienced the same.

At this rate, it doesnt seem like Ill be able to see my fathers face.

The reason being that he was probably no longer alive.

Thinking logically without taking emotions into account, it would be a much better choice for Darcia to abandon Vandalieu. Then she would be able to live a much more peaceful life.

That would make her life more carefree, free of responsibility. Once some time passed and her feelings died down, she would be able to live on as if nothing had happened. If she wanted a child, she could easily go to another country or back to the Dark Elf village and find a new man to create one with.

Despite this, the likely reason why Darcia had not abandoned Vandalieu was because she loved his father and Vandalieu himself.

This is a bit of a clich, but being loved is a truly happy thing.

With this happiness driving him on, Vandalieu continued his hard work.

He spent his time awake practicing magic, strengthening his body by exercising his arms and legs and practicing using his voice. The result of this was that his DeathAttribute Magic skill had increased and he was able to cast with visible effects that Darcia could see.

Wow, to think that you can use magic even though youre still a baby! Vandalieu, youre a genius, arent you
How happy it made him for someone to be proud of his increase in skill, for someone to praise him.

Though his ability to use other skills seemed to be not the result of his hard work, but his growing up.

His Status Effect Resistance was a skill that granted him resistance against poison, sickness, fatigue due to lack of sleep, hunger and other various detrimental or fatal conditions that could be applied to him through magic. This was likely something he had inherited from his Vampire father.

His Magic Resistance, a skill that mitigated the damage and other effects that he took from magical attacks, was a racial characteristic of Dark Elves. His mother, Darcia, also had this skill.

The Night Vision was a characteristic that both of his parents possessed, which allowed him to see even on completely starless nights as if it were daytime.

And finally, Bloodsucking was selfexplanatory. His canine teeth had grown unusually quickly compared to his other teeth It was a skill that he had acquired when fangs had grown on both the top and bottom side of his mouth.

Theyve appeared, after all. Though it might be better for you not to, Vandalieu(Darcia)

As Darcia said this upon noticing that her son had grown fangs, she cut the head of the rabbit she had caught with a knife. And then she caught the dripping blood on a wooden plate.

Here, try drinking this.(Darcia)

Mother, are you insane?

In response to the ironlike smell coming from the plate that had been brought close to his mouth, Vandalieu looked up at Darcia with halfclosed eyes.

On Earth, blood is sometimes used as an ingredient in sauces and eel or turtle blood is sometimes distilled into wine. I know that, but Wouldnt feeding an infant the raw blood of an animal be considered child abuse? I think that would be the case, but she doesnt seem to want to change her mind.

Well, I guess Ill try it.

It would surely be disgusting. As he thought this, Vandalieu extended his tongue and drank a little of the rabbits blood. Surprisingly, he didnt think it tasted bad.

Huh? I can drink this. It tastes like iron, but its not as bad as I thought In fact, its delicious?

It hadnt been distilled into wine or had spices added to it to mask the smell, but for Vandalieu, the rabbits blood was as easy to drink as his mothers milk.

He was astonished, but Darcia explained as she stroked his growing hair.

Vandalieu, you can drink blood like your daddy. But it doesnt mean you have to drink blood, so even if youre hungry, only drink it if Mommy isnt around, okay?(Darcia)

I see, so Im a halfVampire after all. That explains why Dhampirs are shunned.

Well, for now, Ill just think of it as an increase in the variety of my baby food.

As Vandalieu approached six months of age, he gained the ability to crawl. That day, Darcia had left Vandalieu at home and made a long journey to a nearby town.

Im glad Mother has taken my unusual intelligence for an infant to be a good thing.

Including the fact that he could use magic, Darcias only response to Vandalieus abnormality wasAmazing!and she had not questioned it or expressed any bewilderment about it.

Dhampirs are amazing, after all.(Darcia)

Because she often said that, it was likely that she thought that all of Vandalieus unusual characteristics are due to the fact that he was a Dhampir. He was very thankful that she had not investigated it any further.

After all, even if he wanted to explain it, he couldnt speak yet as he was only six months old. He was continuing his practice on using his voice, but he was frustrated that he couldnt form proper words yet.
If that wasnt the case, he would want to explain his situation.

Id explain everything about Rodcorte, my previous life and Amemiya Hiroto.

In light novels or manga that Vandalieu had read on Earth, it was common for characters that had been reincarnated in another world to keep that fact a secret. But he thought he should break that common pattern. That he should at least tell Darcia, as soon as possible.

Because she was his own mother.

If this was a normal reincarnation or trip to another world, Id consider keeping it a secret too. But my situation is different. Because soon, a hundred people from the same world as me will be reincarnated here with cheatlike powers.

Amemiya Hiroto and the others, the ones who had abandoned him without even searching for him on Origin, the ones who had killed him. Once they died in Origin, they would surely be reincarnated here in Lambda.

Vandalieu didnt know when that would happen. When Vandalieu had died in Origin, they looked to be around twenty years old. So unless they were involved in an accident or something, it would take at least fifty years. But it wasnt certain that time flowed at the same rate in both Origin and Lambda.

It could even be that for each day that passed in Lambda, a year passed in Origin.

Well, it probably wasnt that extreme, but they would definitely be reincarnated in Lambda one day. Even Rodcorte himself couldnt prevent that from happening.

The problem is what Rodcorte will say to them before theyre reincarnated. I screamed to him that Id kill them. So if they were to be reincarnated here before I died, hed at least warn them about me.

After all, Rodcortes goal was to have them develop this world. It would be problematic if they died before that happened, so he would definitely warn them about Vandalieu.

In that case, they would see Vandalieu as a threat and be cautious of him.

In their first lives, they were Japanese people raised in the peaceful country of Japan, so it would be fine if they wanted to talk about things or apologize for what happened in Origin.

But Vandalieu couldnt say for sure that there wouldnt be any of them who would kill him upon finding him, to simply eliminate the threat. Just as Vandalieu himself had led an unimaginably wretched life, they might have experienced something like that as well.

Even if they were heroes there, if they had to deal with evil terrorists or criminal organizations for extended periods of time, who knew what kind of state they would be in.

Yes, heroes. These were unlucky.

I heard them just before I died, but if Im not mistaken, they were notified that an Undead had appeared and come to kill me. If thats the case, they were probably putting their cheatlike powers to use and being international heroes or something. Like the ones in American superhero comics.

And Vandalieu himself was a halfVampire, a Dhampir.

If they displayed the pacifism, philanthropism and respect for human rights that they did as Japanese people, there wouldnt be a problem. But if they were affected by the antiVampire, antiDhampir values of Lambda, that would be dangerous.

Facing a hundred people with cheatlike powers as enemies wouldnt be an easy task.

The one getting caught up in all of this would be Darcia. It would be too dangerous and unreasonable to expect her to face that without knowing anything. This was why Vandalieu thought that he had to explain the situation to her as soon as possible.

If Mother separates herself from me because of that reason, then theres nothing I can do about that.

It was only for half a year, but Darcia was the first mother that Vandalieu had known. He had never been loved as much as she had loved him.

Well, if possible, I dont want to be separate from her.

For that purpose, my revenge on them Well, its impossible to forgive them or reconcile at this point, but Id be content if they just kept their distance from me.

This was the extent to which Vandalieu was attached to Darcia. It was also partially because his thoughts had become strangely clear after he had been reborn, but he thought that he could give up on his revenge for the sake of his mother.

After I grow up a little more, Ill explain everything to Darcia. And then if I can use the knowledge and deathattribute magic that I have from my time on Earth and in Origin to live a fulfilled life, Ill be happy with that.

And if I can just watch those guys with cheatlike abilities put all their effort into this world, then thatll be fine.

Vandalieu thought this as he continued crawling across the floor to continue his physical training, but he felt suddenly hungry.

I guess Ill just drink some blood.

He lifted the rabbit that Darcia had captured alive from its cage. Though he was only six months old, he was still a Dhampir with the Superhuman Strength skill, so it was simpler than he thought.

He usedSterilizationandBug Killeron the struggling rabbit to sanitize it and then bit into it.

Blood is delicious, but Mothers milk is still better.

Sating his hunger by mercilessly sucking the blood from the convulsing rabbit, Vandalieu longed for his mothers breast.

That day, the time that Darcia was supposed to come back passed, but she did not return.

 

 

        Name Vandalieu

 

        Race Dhampir (Dark Elf)

 

        Age 0.5 years old

 

        Nickname None

 

        Job None

 

        Level 0

 

        Job history None

 

        Attributes
                Vitality 18
                Mana 100,000,600
                Strength 27
                Agility 2
                Endurance 33
                Intelligence 25

 

        Passive skills
                Superhuman Strength Level 1
                Rapid Healing Level 1
                DeathAttribute Magic Level 2 (NEW)
                Status Effect Resistance Level 1 (NEW)
                Magic Resistance Level 1 (NEW)
                Night Vision (NEW)

 

        Active skills
                Bloodsucking Level 1 (NEW)

 

        Curses
                Experience gained in previous life not carried over
                Cannot learn existing jobs
                Unable to gain experience

 
',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(5,'chapter 2','Even when Vandalieu opened his eyes the next morning, there was no sign of Darcia.

This is strange I have a bad feeling about this. Maybe I should go out to find her.

Normally, it would be reckless for a sixmonthold infant to go outside by himself, but if Darcia didnt come back by the end of the day, Vandalieu would have to venture outside either way.

There was still the one rabbit that Darcia had caught alive, but its blood would only last him until the end of today. He would have to make food suitable for an infant to eat by himself, but he was still only six months old. His stomach found it difficult to cope with food other than blood and his mothers milk.

Infants eat a lot more than youd expect. I need to eat five or six times a day, since Im doing magic practice and physical exercises, so my energy expenditure is quite high.

Because of this, Vandalieu needed to eat considerably more than a normal infant. If he simply stayed here like the infant that he was, it was possible that he would starve to death in a few days time.

Because he was a Dhampir rather than a human, he wasnt sure how long it would take for that to happen.

Well, I guess Ill start getting ready.

Though he wanted to go out to find Darcia, his body was still unable to walk, let alone run, so he had to make various preparations. First, he had to secure a method of moving around.

The first thing to do is Get up.

As Vandalieu gave this command in his head, the rabbit that he had killed by consuming its blood twitched and began to move.

He had not granted it life. He had used his magic to have the various spirits floating around nearby to possess the rabbits corpse, turning it into an Undead.

It was a deathattribute spell that created Undead.

Looks like it worked. I wasnt sure if I could do it or not, since my skill is still only level 2, but it seems it works fine if I use a bit more Mana to compensate. I guess its because my skill level is low I can only make a moving corpse. But I can still use it to move around.

The rabbit didnt have any of the agility that it had when it was still alive it simply moved. In Origin, he had planned to use this spell to escape. But he could only create these weak servants and they would be of no use against the strict security that he had been under, so he had given up.

The amount of Mana that he had needed to create this single Undead rabbit was far greater than the amount he needed for spells like Sterilization and Bug Killer. When Vandalieu checked his status, he found that his Mana had decreased by 10,000.

Using Sterilization once only cost him a single point of Mana, so the Mana cost of 10,000 was enormous in comparison.

But one rabbit isnt enough. More of you, get up. You, you and you, get up. You as well, hurry and get up!

The ivy that Darcia had used instead of rope began to crawl around like a snake.

The small knife that she had used for cooking began lightly floating in the air.

The shabby bed made of wood rattled as it began to shake.

Vandalieu could have spirits possess more than just corpses. He could make them possess inorganic substances such as ivy, wood and metal. The idea came from fantasy and occult works that he had seen on Earth  evil spirits that possessed weapons to move on their own without a bearer, cursed gems or vases and Poltergeists that that caused furniture to move on its own.

However, having spirits possess inorganic substances required several times more Mana than having them possess a corpse.

I used nearly 1,000,000 Mana just now. My head hurts a little Ill leave after I replenish myself.

Vandalieu sucked the blood from the last remaining rabbit to replenish his Mana, turned that into an Undead as well and then left his home, riding on the bed.

Get up, get up, get up, get up, get up! Which way is the town? Where can I find somewhere with lots of people?

The bones of a deer, a mummified bear, a wild boar missing its head and internal organs and a humans corpse that for some reason only had a skeleton remaining. These servants carried Vandalieus bed as he continued onward.

If anyone other than an adventurer or soldier were to witness this sight, they would surely tremble with fear.

An infant with silver hair and oddcolored eyes, one deep crimson and the other a bluishpurple, leading an army of Undead. A terrifying sight indeed.

But he looked more fearsome than he actually was.

The Undead corpses and bones of animals. The tools and furniture that had been cursed to be made Undead Any one of these could be repelled by a single farmer wielding a hoe, and any adventurer would only consider them a group of small fry.

Knowing this, Vandalieu continued increasing the number of Undead at his command as he hurried forward.

Mom isnt in this forest.

He had commanded a winged insect fly around to search the forest, but there was no sign of Darcia. He asked the spirits that were floating amongst the trees, but they couldnt give him the information he wanted.

His head felt hot, as if it was steaming, and he felt a dizziness that would not subside. He no longer knew how many millions of Mana he had spent. But he couldnt shake off this bad feeling.

I should have tested making Undead earlier! I should have had an Undead follow Mom around!

If he had done that, he would know where she was. If something happened, he would be able to sense it straight away.

In fact, if Darcia had known that he was capable of doing these things, she might have decided to leave the forest on the journey for her birthplace earlier.

But he didnt test it. He had lived in peace, thinking that he would test it the next time his skill level increased. He had been spoiled by Darcia, his mother.

Faster, to the town!

He had around 50,000,000 Mana remaining. By the time his group of Undead reached Evbejia, the town closest to the forest, night had already fallen.

Evbejia was a small town built in between two villages, with a population of around 2,000. It was governed by Bestero, the Baronet.

It was surrounded by a wall to ward off monsters and there was no way to enter except through the four gates to the north, south, east and west. Unless one could fly through the air.

You guys wait here. Bone Monkey, you carry me.

He grabbed onto the back of an Undead he had made from the skeleton of a large monkey he had found in the forest, whom he had named Bone Monkey, and quietly approached the towns outer wall.

It was a town with a population of only 2,000, so it wasnt at particular risk of monster or enemy nation attack. Vandalieu could see holes in their guard system. If the person in charge of the security at the military facility in Origin could see this, he might feel sorry for these people.

Even so, the gates were closed and guards stood watch after nightfall.

There would be chaos if a humansized skeleton monster approached them. Thats why Vandalieu ignored the gates and headed for the walls.

And then he embedded some Mana and a suitable number of spirits into the stone wall.

Make a hole, big enough for us to pass through.

The portion of the outer wall, now an Undead wall, wouldnt disobey its masters command. Making small, grinding noises, it changed shape to create a hole to allow passage through.

49,000,000 Mana remained.

The DeathAttribute Magic skill has leveled up!

He was barely aware of this announcement inside his head his dizziness was very severe now. His head felt as if there was a hammer continuously striking his temple.

Bone Monkey, find an alleyway where theres no sign of any people.

Bone Monkey continued forward, his bones rattling as they moved against each other. His movements were unrefined and showed no sign of consideration for Vandalieu who was clinging to his back, but it wasnt a problem. Vandalieu wasnt able to walk on his own yet, but his arm strength already exceeded that of a grown man. Holding onto Bone Monkey with both hands, he could easily support his own body weight of less than ten kilograms.

He looked around the dark, almost lightless town. Thanks to his Dark Vision, he could see as clearly as if it were in the middle of the afternoon.

Find Mom.

Sending out the Undead insects to gather information, he listened to information from the spirits floating around the city that are conscious.

Normal spirits that hadnt been turned into Undead normally dissipated quickly after death unless they had an exceptional force of will. That was why Vandalieu thought there wouldnt be very many of them.

 Shes in the town square.(Bugs)

Therefore, he was so surprised that his heart almost stopped when he heard this answer given to him.

To the town square!(Vandalieu)

Bones rattling again, Bone Monkey moved quickly.

And in the town square, Vandalieu saw Darcia.

She was there.

Vandalieu Im sorry(Darcia)

As a spirit that looked as if it would vanish at any moment.

Mom!(Vandalieu)

Im sorry. Mommy died. But I didnt say anything about you, Vandalieu.(Darcia)

What happened?(Vandalieu)

Darcias spirit, covered in countless whipmarks, began her explanation.

She had come to here, to Evbejia, to gather information to prepare for the journey to the Dark Elf village, where the murder of even a Dhampir would not be allowed.

After all, Vandalieu, you have one eye the same crimson color as your daddys, and one bluishpurple like your mommys. People would know that youre a Dhampir with one look at your face.(Darcia)

Thats why using the highway wasnt an option. So there was a need to find a relatively safe alternate road where monsters wouldnt appear.

But she had been betrayed. The Church of the god Alda knew her name and appearance, and one of their members reported that there was a female adventurer who sometimes came to the town, matching her description exactly.

Darcia had unsuspectingly entered the town only to be surrounded by hired adventurers and ambushed by the knights of Baronet Bestero and the monks of the Church. She had resisted, but eventually captured.

I fought desperately, but there were adventurers that were even better with a bow and spiritual magic than me, so it was hopeless. After that, they whipped me, asking me, where is the Dhampir that you birthed, you damn witch?(Darcia)

The Church had tortured Darcia severely. They had whipped her, broken her fingers and pressed hot irons into her.

The pain must have reached all the way into her soul even after becoming a spirit, the scars of the torture remained visible on Darcias body.

Even so, I kept quiet, Im amazing, arent I? I did well, didnt I? But the Church realized that torturing me was useless, so they burned me at the stake earlier this evening.(Darcia)

In order to spread the news of their achievement, the news that they were ending the life of the woman who had succumbed to a vampires seduction, they had executed Darcia publicly.

So I was burned alive. After that, I wanted to at least go to see you after becoming a ghost, but the Churchs High Priest Gordan sprinkled holy water over my ashes. Because of that, I could barely keep myself from disappearing, so I couldnt come to see you.(Darcia)

As Vandalieu listened to Darcias words, his vision blurred and he felt a sense of despair and powerlessness, as if his body were rotting away.

If he had left to look for her yesterday, he would have made it in time. If he had come here sooner, he would have made it in time.

While he had been carefree, doing nothing, they were whipping his mothers beautiful, chocolatecolored skin. While he had been satisfying his stomach with the blood of the rabbit, hot irons were being pressed into her.

And while he had been awkwardly wandering about the forest, she was burned alive in public as a criminal.

All that remained of her now was a smile pile of ash on the stone pavement of the town square.

You have acquired the Mental Corruption skill!

If I had grown up faster, if I had trained myself in using magic earlier, if I had taken the time to gather information beforehand If I had become stronger, Mom wouldnt have had to die!(Vandalieu)

Maddening emotions swirled around Vandalieus mind. His expression didnt change much, but his body was shaking in grief and tears of regret and anger flowed endlessly from his eyes.

The level of the Mental Corruption skill has increased to level 2!

Darcia watched him sadly.

In truth, she had not wanted to tell him about the horrible things she had endured before her death. But she hadnt been able to stay silent about it.

As a weak spirit on the verge of being extinguished, it was impossible for her to go against the wishes of Vandalieu, a deathattribute mage with an enormous amount of Mana.

And in front of her eyes, Vandalieus Undead insects returned to him. They had brought back information about Darcia from all over Evbejia.

My Lord, todays public execution was exciting, was it not?(Steward)

Indeed. Even if we did not catch the vampire itself, catching the Dark Elf who opened her legs to him is still a satisfactory achievement. My reputation with His Majesty and the Church grows better, and the day of my appointment to a higher position draws near.(Feudal Lord)

The day you can receive the Characters of Correspondence from Baronet Besterosama is also near.(Steward)
TLN Im not entirely sure what this means, nor am I sure on the translation. I think its some kind of honoraward. If it becomes clearer later Ill update this.

A feudal lord and his steward were happily envisioning a bright future for themselves while enjoying their dinner.

The level of the Mental Corruption skill has increased to level 3!

That damn witch, she didnt say anything about the Dhampir in the end.(Feudal Lord)

Hmph, I suppose she was trying to show her motherly love. She should have at least pleaded for her life when we burned her at the stake. She did not show any signs of repentance, right up until the end she was truly a witch.(Steward)

She is surely burning in the flames of Hell now. So from tomorrow, we will be hunting the Dhampir using the Fivecolored Blades?(Feudal Lord)

Would it not be fine to leave the infant? It has already been three days since the baby who still requires his mothers milk has been separated from her. I am sure it is dead by now.(Steward)

Do not be so complacent! Even if it is a baby, half of its blood is that of a Vampire we do not know what kind of powers it may have inherited! If his father had been of lower breed it would be another matter, but if he was a noble or a purebreed, what would happen if you left him!(Feudal Lord)

Mmy apologies, High Priest Gordan.(Steward)

But we do not have the funds to hire the Fivecolored Blades for long, and it would be unwise to add any more to our current achievements. I am sure that they are also not planning to associate themselves with us for much longer. They are adventurers, after all, different from us apostles of the God of Law and Fate, Alda. Have Ebvejias hunters guide them on the hunt in the mountains and work together with the Baronetdonos knights.(Gordan)

Yes, Sir.(Steward)

In the Church, the High Priest and holy knights were calling Darcia a witch and cursing her while discussing how to dispose of Vandalieu.

The level of the Mental Corruption skill has increased to level 4!

Todays work was quite simple, huh. That Dark Elf was decently skilled with a bow and spiritual magic, but Id say she was a Dclass at most she didnt stand a chance against us. Isnt that right, Blueflamed Sword Heinz?

Whats wrong, Heinz? Youre acting a bit strange.

 No, its nothing. I just have a really bad aftertaste about it.(Heinz)

You feel sorry for that Dark Elf? Youre the leader of us Bclass adventurers, the Fivecolored Blades. Dont be going saying nave stuff like that.

I dont know whether she succumbed to the Vampires seduction or wanted to become immortal, but that Dark Elf was paying for her own mistakes. Its nothing to be concerned about.(Female adventurer)

Thats true, but I was shown a letter of introduction from the earl. I suppose it couldnt be helped.(Heinz)

Well, lets use the gold we earned to get some drinks and food. Its just like eating the meat of the monsters we defeated, you know? Thats how we can honor her memory.

The adventurer group who had been hired to catch Darcia, theFivecolored Blades, were saying that they would use the gold they earned from killing her to treat themselves to a meal.

The level of the Mental Corruption skill has increased to level 5!

Cheers! A toast to the Baronetsama and HighPriestsama!

Hahaha, a toast to Orbies moodiness as well!

Oi, who are you calling moody?!(Orbie)

You, of course, you right here. Im proud to have a companion who would fall in love at first sight with a beautiful Dark Elf coming to the town and then follow her like a hunter after being rejected.

What, its because of that that that we figured out that she was the witch on the wanted poster!(Orbie)

It was really a waste, though, huh. Burning a beauty like that at the stake. And now well be earning some extra money by going on a hunt for the Dhampir. The knights will probably be in command, but Im sure we can get paid a bit for guiding them through.

What are you talking about, were going to be the ones to catch the Dhampir. That way well be getting a big payout, not just a little bit.

You didnt tell them where the Dark Elfs hiding place was?!

Good job, Orbie! If we catch the Dhampir, we can sell him to the Church, the Mage Guild, slave merchants, wherever we like!

The hunters who had betrayed Darcia were now laughing loudly as they planned to capture Vandalieu and sell him.

The level of the Mental Corruption skill has increased to level 6!

If you dont behave, youre going to be burned at the stake like the witch.

We cant find the Dhampir, huh. Im a bit anxious, they should hurry and exterminate him.

Is there no chance the Vampire will come back for revenge?

It would be a big problem if our villages wine became known as the wine of the village the Vampire came from. I wish this problem would be taken care of already.

And finally, the words of the townspeople the people who led very ordinary lives. None of them had any sympathy for Darcia.

The level of the Mental Corruption skill has increased to level 7!

What a turn of events.

In all of his three lifetimes, Vandalieu had not thought even once that the world revolved around him, or that it was full of good will.

But he had never thought that it would be this cruel, this vicious.

Earth, Origin and finally Lambda.

This was his third life, but he had not experienced happiness even once. Would he forever be forced to endure the torturous feeling of having things taken from him?

In every one of his lives he had done nothing wrong, but had irreplaceable things taken from him and received nothing in return.

Even the god that he had discovered really existed had made no effort to help him.

Im sorry, Vandalieu(Darcia)

And then, right before Vandalieus eyes, Darcia was about to be taken from him as well.

It seems Mommys at her limit now.(Darcia)

Darcias form, which was already faint to begin with, began to blur and her voice began to waver. Her spiritual form was reaching its limits on how long it could remain in this world.

Wait! Mom, you cant go!(Vandalieu)

Darcia would be going to the afterlife, into the hands of that god of transmigration, Rodcorte. If he found out that Vandalieu had lost his mother on Lambda, he would surely be laughing. Rodcorte would laugh at Vandalieu, hoping that he would feel more despair, hoping that he would give up. Vandalieu did not want this, he couldnt allow it to happen, he couldnt bear it.

Im sorry. I wanted to talk to you like this a lot more. I wanted to see you grow big, turn into an adult. I wanted to see you find a wife, have children and become happy.(Darcia)

The level of the Mental Corruption skill has increased to level 8  9!

I got it, Mom.(Vandalieu)

Vandalieu had 45,000,000 Mana remaining. He poured all of it into Darcias spirit.

Eh, ah, ah, ah? Vanda  aaaah!(Darcia)

In the same way as when he created Undead, he poured his Mana into Darcia, whose fleeting spiritual form looked as if it could disappear at any moment. It was like pouring water into a bucket with a hole in the bottom, but even with a hole, the bucket would be filled if a vast amount of water was poured into it.

Next, I need a vessel that Mom can possess(Vandalieu)

He couldnt use a random stone, tool or bone. If he used something like that, it would be difficult for it to preserve Darcias personality for long.

If it were possible, her original body would have been best, but her body was now a pile of ash.

Isnt there something I can use?(Vandalieu)

He had Bone Monkey search the pile of ash and found one of Darcias scorched bones. It was a fragment small enough to fit into Vandalieus tiny hand.

This will do. Mom, inside here.(Vandalieu)

Darcia, who was having an enormous amount of Mana poured into her, tried to defy her sons request.

This was for his sake.

It was tragic, but she was already dead. Even if she was turned into an Undead, it would be different from when she was alive. Being by her sons side in such a form wouldnt be of benefit to him. In fact, it would have a negative influence.

That was why, even though she wanted to be by his side longer, she tried to separate herself from him as she fought back her tears, but 

You have acquired the DeathAttribute Charm skill!

I want to be by his side, I want to be by his side, I want to be by his side, I want to be with him forever! I dont want to separate myself from this child!

I understand. Ill be with you forever.(Darcia)

Darcias mind was filled with one thought, and she willingly inhabited her own remains.

With this, Ive recovered Mom, even though its just her spirit. Ill create a new body for her one day.(Vandalieu)

Vandalieu had gathered the Mana needed to maintain Darcias spirit form and a vessel for her to inhabit. But it was just a bone fragment, after all. But she couldnt move of her own will and could do nothing but sleep until Vandalieu decided to speak to her.

That was why he definitely had to increase his skill with DeathAttribute Magic and create a vessel for her with which she could live a normal life, just as she had when she was alive.

But before that, I have to get revenge on these guys.

The Baronet and his servants, the Church, the adventurers, the hunters, the townspeople  Vandalieu would take revenge on all of them.

He would never be satisfied until he did so it made no sense that they would suffer no punishment whatsoever. It would be far too absurd.

But right now Im at my limit.

He had less than a tenth of his Mana left. His head was hurting, he was thirsty, hungry and sleepy He had demanded too much from his sixmonthold body.

Im sorry, Mom. Ill avenge you later Bone Monkey, go back the way we came.

Bone Monkey picked up Vandalieu in his arms and left Evbejia, bones rattling as he walked. The outer wall that had opened a hole for Vandalieu and Bone Monkey to pass through returned back to normal and the group of Undead that had been waiting followed after Bone Monkey.

The people were completely unaware that they had stirred the hatred of someone they shouldnt have.

The level of the Rapid Healing skill has increased to level 2! The level of the Surpass Limits skill has increased to level 2! The level of the Status Effect Resistance skill has increased to level 2!

        Name Vandalieu

 

        Race Dhampir (Dark Elf)

 

        Age 0.5 years old

 

        Title None

 

        Job Ordinary person

 

        Level 0

 

        Job history None

 

        Attributes
                Vitality 18
                Mana 100,000,600
                Strength 27
                Agility 2
                Stamina 33
                Intelligence 25

 

        Passive skills
                Superhuman Strength Level 1
                Rapid Healing Level 2 (LEVEL UP!)
                DeathAttribute Magic Level 3 (LEVEL UP!)
                Status Effect Resistance Level 2 (LEVEL UP!)
                Magic Resistance Level 1
                Dark Vision
                Mental Corruption Level 10 (NEW!)
                DeathAttribute Charm Level 1 (LEVEL UP!)

 

        Active skills
                Bloodsucking Level 1
                Surpass Limits Level 2 (NEW!)

 

        Curses
                Experience gained in previous life not carried over
                Cannot learn existing Jobs
                Unable to gain Experience Points independently

',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(5,'chapter 3','Struggling to stay conscious, Vandalieu returned to his home in the forest with the Undead while clutching the fragment of his mothers bone that contained her spirit.

He felt as if he would die of hunger, but fortunately, a raccoon had been caught in a trap set by Darcia, so he drank its blood before passing out.

By the time he woke up, the sun had already risen.

Good morning, Mom, everyone.

As Vandalieu opened his eyes, he quietly greeted his new companions in the house though he only managed to actually let out an unintelligible noise that sounded likeAueuh.

So, have the hunters come after me?

Bone Monkey, who was standing next to the bed, shook his head. It seemed that his good fortune had not yet arrived.

First, if I gather up all the information I learned yesterday 

The knights of Baronet Bestero who governs Evbejia, High Priest Gordan and the holy knights under his command, the ones who believe in Alda, the God of Law and Fate, would begin searching the forest today to hunt Vandalieu down.

However, the hunter Orbie had not told them where this house was, so they would take some time before they found this place.

But the hunter Orbie himself, along with his companions, would come here first in order to obtain this rare Dhampir baby for themselves.

Against them, my combat ability is enough to take on the three of them, I guess.

He had created several hundred Undead yesterday. However, the majority of them were small animals such as mice and insects there were only around thirty of them that would be useful in combat, including Bone Monkey, but they were fundamentally weak.

When he was alive, Bone Monkey probably had the strength of an orangutan, enough to be able to easily tear off a humans arm. But that wasnt the case now. In fact, when Vandalieu had tested Bone Monkeys strength, he found that Bone Monkey was even weaker than himself. Though in this case, perhaps the abnormal thing would be Vandalieus own strength, not Bone Monkeys weakness.

Bone Monkey was even less agile than a normal human. As for endurance, he would be resistant to arrows and knives due to being made only of bones and dead flesh, but if he were to be hit a few times with a hoe, he would fall apart.

On top of that, when Vandalieu somehow managed to check Bone Monkeys status screen, he had no passive skills or Undead skills.

The Undead that Vandalieu had at hand right now were all Rank 1. After all, they were all monsters that would be defeated by any untrained villager in Lambda in oneonone combat.

However, he had thirty of them. With that being the case, he could make something work out with a little ingenuity.

For now, the insects will find the enemy. Bone Monkey and four others can guard me inside the house. The others 

Vandalieu decided to turn the tables on Orbie and the other hunters first. He had wanted to hear about various things from Darcia, including information about his father, but it would be problematic if the hunters arrived while he was doing that. So he would catch them first before listening to what Darcia had to say.

 

 

 

Orbie continued through the forest, leading two of his hunter companions with whom he had been drinking yesterday. No matter how elite the holy knights were, they wouldnt be able to find the hiding place the Dark Elf had built right away.

They would definitely be able to catch the baby first. He was sure of it.

Oi, Ill just make it clear that 

I know. I dont plan on turning into bear or wolf food before we get rich.

Being professional hunters, they were confident. This forest wasnt some Devils Nest where monsters were prevalent, but it was inhabited by dangerous beasts such as bears and wolves, and even some weak monsters like Goblins also lived here.

Being cautious on the lookout for threats like these, they continued making their way through the forest.

This is strange. Arent these the footsteps of a big monkey? And the ones here are a bear?

Fortunately, their caution had allowed them to spot the footsteps left by the Undead as they made their way back from Evbejia.

You think so? Arent they a bit shallow for the footsteps of a monkey and a bear?

However, in Orbies eyes, the footsteps looked too shallow. Footsteps left in the earth would be deeper the heavier its owner was, but these footsteps appeared to belong to a very light creature.

The shape is weird, too. They look a bit small and the bears ones are missing some toes.

The reason they appeared that way is because the creature who had produced them was an Undead made only of bones its body weight was less than half that of when it was alive, it was missing the pad of its paw and several of its digits had been broken off.

The adventurers would take precautions if they were facing this strange monster as their main enemy, but 

Im sure they just look like that because there are multiple beasts footprints overlapping each other.

Orbie and his companions had only experienced hunting monsters from time to time to earn a little extra money, so they didnt think much of it. The knowledge that this forest didnt have many monsters helped them push the footprints from their minds, thinking it was just their imagination.

Were almost there. Theres a cave dug out in a small cliff that was her hiding place.

Alright, lets go catch that baby while its still alive.

And so Orbie and his companions reached Darcias home.

The trees were sparse in front of the house, with small spaces in between them. There were traces of fire that had been used for cooking and other signs that someone had been living here.

Hmm? There are quite a lot of things lying around.

As the hunters looked around, they saw many tracks that looked as if they had been dug up, as well as ivy and animal bones scattered around.

Shit, lets go inside.

Clicking his tongue, Orbie opened the door to check whether the baby was still here.

Uoooon

At that moment, the earth rose up with a resentfulsounding groan.

Wwhat is this?! A golem?!

Yeah, its an Earth Golem!

The Earth Golem with a body made of dirt that had been lying in wait was now standing up.

Hyiih?! There are Undead beneath the Golem!

Underneath its body were the Undead skeletons of a wolf and a bear.

Rrun!

Where?! Were surrounded!

Orbie and his companions who had approached the door were now surrounded by the Golem and Undead.

The weapons available to them were their short swords and their bows and arrows. Even if the rank of the Golems and Undead were the lowest possible, these were the worst weapons to fight them with.

Uwah?!

A ssnake! No, its ivy, the ivy is moving!

And then the ivy on the ground that should have been dead began moving like a snake and winding itself around the hunters.

Shshit! Let go, let go of me!

Even as they drew their short swords to try and cut the ivy, the Golem and Undead pressed onto them and various pots began flying through the air and throwing themselves at the hunters heads.

They couldnt escape. Orbie and his companions were captured alive.

I guess it went well.

Seeing the hunters bound by the Undead ivy, unable to speak, Vandalieu gave a sigh of relief.

He had spirits inhabit the ground outside the house to create an Earth Golem and then buried the Undead beneath it. The Undead and the Golem didnt breathe, and they simply smelled of dirt like the rest of the forest, so the hunters couldnt notice them if they were completely still.

The same applied to the ivy and pots. They wouldnt suspect that the discarded items scattered around the ground would turn out to be Undead.

Well then, I want to hear what these guys have to say 

Orbie and his companions opened their eyes wide in shock at the sight of Vandalieu riding on Bone Monkeys back, looking down at them. They had never imagined that the baby they had been planning to capture would be commanding Undead at this age.

This was the difference between Vandalieu and the High Priest Gordan, who reprimanded his subordinates.

But first, I have to establish a way to have a conversation.

Vandalieu, who was still only able to mumble unintelligible sounds that sounded likeauaucouldnt converse with the hunters.

And so Vandalieu created a Sand Golem out of the gravel and sand.

Mogomoga!

The hunters began making fearful noises at the sight of the humansized golem rising up from the gravel and sand. They were likely assuming that they were about to be killed.

However, the Sand Golem did not raise its heavy fist above their heads parts of it instead crumbled off in front of them to form words in sand on its body.

Stop making so much noise, be quiet. Answer my questions.

Nice, it worked.

He had thought that if it was possible to make turn part of Evbejias outer wall into a Golem to change its shape, it would be possible to change the shape of the Sand Golem to form letters. This idea seemed to be a great success.

Well, he had to supply the Golem with a considerable amount of Mana, which took its toll on his mental endurance.

But the problem was whether the hunters would be able to read Japanese characters.

In order to test this, he unbound the hunters mouths, who had now fallen quiet.

 Wwhat do you want to know, you can ask whatever you want. But in exchange, spare our lives

Yyeah, well tell you anything we know.

Well keep quiet about this place. So please let us go.

And then they began pleading for their lives. It seemed that in Lambda, the spoken and written language was Japanese.

Thats convenient, but why is that the case? Now that I think about it, according to the knowledge Rodcorte shared with me, he summoned champions from another world to Lambda in the past. Is it because of that?

Well, he would figure that out when he had time later.

Vandalieu decided to question the hunters through writing and gather information from them. While they could read hiragana and katakana, it seemed that they were mostly incapable of reading kanji (their initial pleading for their lives had been done by guessing the situation rather than being able to read Vandalieus writing). Since this meant that Vandalieu would need to write more characters with the Sand Golem, it increased the amount of Mana he needed to spend and took a greater toll on him.

But not all of his questions would have favorable answers.

What is a Job?

Huh? What, you want us to tell you our Jobs? Were hunters, of course.

What are Experience Points?

Eh? Experience Points are Experience Points, right?

Who is Alda, the God of Law and Fate?

Ah, thats a god.

What is a Job and what are Experience Points in Lambda? Vandalieu was asking simple questions, but the hunters were not giving helpful answers.

From the hunters point of view, Vandalieus questions were too simple. It was like asking on Earth,What is air?orWhat is water?Even so, if the hunters were educated and had enough vocabulary, they would be able to give useful answers, but Vandalieu understood that he couldnt expect this from these hunters.

Ill ask Mom about stuff like that later.

Darcia would answer any of Vandalieus questions with an easytounderstand explanation. After all, she was a mother raising her child.

Tell me about Baronet Bestero and his knights.

Vandalieu decided to change gears and ask information that they would know about the other enemies.

The feudal lord who took over about ten years ago is a person with a lot of ambition. He doesnt have any other special characteristics hes your typical nobleman. I havent heard about anything that makes him different from the other nobles.

There are five knights he uses them as soldiers as theyre stronger than the rest of us. Though it seems they undergo training as well.

Ththats why theyre not that strong. In fact, why dont we get rid of them together? If we team up with you itll be simple.

Vandalieu decided to ignore that last part. It seemed that Baronet Bestero did not have many pawns and they were apparently not particularly skilled. This area was likely considered a peaceful region by its population.

At least, its population considered it as such.

Just answer my questions. Tell me everything you know about High Priest Gordan and his holy knights.

The high priest and his holy knights are never in the town. They came to the town about half a month ago with wanted posters featuring a Dhampir and the witch that birthed it that is to say, you and your honorable mother Err, hes a clergyman who is famous as a Vampireslayer, and with the holy knights, theyre like a collection of elite individuals.

Yeah, for a clergyman, hes a ridiculously strong freak who used that strength to climb all the way to the position of High Priest. Hes already killed multiple Vampires and their subordinates Vampiresamas and their dedicated subordinates. He has already dealt with Vampiresamas that had lived for hundreds of years, and rumors have it that hes comparable to a Bclass adventurer.

Bbut if you team up with us, were familiar with the terrain so outwitting him will be easy. See, so please make us your subordinates!

I see. That priest is a capable Vampirehunter, huh Next I want to know about the Fivecolored Blades, but before that This is a good time, so lets have a meal first.

He had the Undead bring one of the three male hunters forward.

Wwhat is this? Youre making me your subordinate? Well definitely be helpful to you, our skills with a bow are wellknown around Evbejia Hyiih! Well really be helpful, well do anything for you, so please spare me!

Bone Monkey had grabbed a hold of the mans head so that he cannot move his neck. He was begging for his life in a highpitched voice, but Vandalieu wasnt listening.

His fangs plunged into the mans neck.

Kyaaaah!

Johann!

Ignoring the screams of the man called Johann and the other hunters, he greedily devoured the blood, filling his throat with it.

It feels greasier than rabbits blood, and it tastes a bit saltier as well.

Of course, he would have preferred to be held by Darcia and drink her milk. But this was his first meal of the day, so he simply continued drinking Johanns blood until his screaming stopped and he fell limp.

Fu Ah, Bone Monkey, give my back a little pat. Yeah, right there Burp

As Vandalieu burped, he showed the now deathlypale body of Johann to the other hunters.

And then he directed words of sand at them once more.

Didnt I tell you to just answer my questions?

Seeing the dead Johann whose blood had been completely consumed and Vandalieus expression that had not changed in the slightest after killing him, Orbie and the other remaining hunter simply nodded their heads.

They were facing a mere baby, but this baby had no childish naivety or pity. They finally realized that Vandalieu would not hesitate to kill them if they showed the slightest sign of disobedience.

Now then, what do you know about the Fivecolored blades and Heinz, the Blueflame Sword?

From that point, his interrogation of Orbie and the other hunter proceeded smoothly. The example he had made of Johann was incredibly terrifying.

The Fivecolored Blades were a group of five adventurers and Heinz was its leader, a highly successful Bclass adventurer even though he was still in his teenage years.

The other members were Cclass, so they were a group of capable individuals.

As I thought, he and High Priest Gordan are enemies I cant defeat in my current state.

The next thing he asked them about was the geography of this region  the area surrounding Baronet Besteros territory and the places that High Priest Gordan and his knights would be searching right now.

The answers to these questions were quite helpful. He had asked the animal spirits inhabiting Bone Monkey and the other Undead as well, but he hadnt managed to get a good idea of the situation from them.

The spirits were more talkative than they had been yesterday, but they were originally animals and insects, after all. They didnt have the sense of distance and direction that a human could use as a reference.

There was no problem when they guided him around, but trying to make a map out of their information inevitably resulted in chaos.

Well then, I should stock up on food and water while I have the chance.

High Priest Gordan was searching a different place right now, but this forest was not that large. They would be able to search the whole forest within ten days.

With that said, trying to make a hasty escape would be a poor choice.

Due to the mismatched eye colors that Vandalieu possessed as a special Dhampir characteristic, he couldnt conceal his true nature so he couldnt approach any areas inhabited by humans. However, without somewhere safe to hide like the house in the cave, living outdoors would be too dangerous.

The reason for that being 

Im only six months old, so I need a lot of sleep. In fact, Im quite sleepy right now.

Despite the fact that one of his parents was a Vampire, no matter how high his attribute point values were, no matter how many skills he had acquired, he was still a baby. Though it was considerably better than when he was only a month old, he was still frequently sleepy and found it difficult to stay up for long periods of time.

He could resist the effects of sleep deprivation with his Status Effect Resistance skill, but that would have negative effects on his development. Darcia wouldnt want that.

Therefore, Vandalieu decided to stay hidden until High Priest Gordan gave up the search. He had never imagined that he would be living a hikikomoris lifestyle at six months of age, but this was necessary to survive.

Ooi, whats wrong? Dont you have anything more to ask?

If thats the case, let us go!

Orbie and the other hunter began to make some noise, but their role as information sources had already been fulfilled. The ivy covered their mouths once more.

Wwait! Please, let us go! I have a muguuh

I have a  what? A fianc? A wife? A young daughter? An aging mother? Considering that he sold out a mother with a young son, what was he trying to say?

Even if he had something, it made no difference to the roles the hunters were to play.

Vandalieu informed them of this through sand letters.

You are merely food for me.

 

 

 

Two muffled screams came from the hunters.

Once upon a time, there were two great gods in this world.

The vast black god, Diachmell.

The vast white god, Arazan.

The two gods fought each other. Nobody knew which of them was good and which of them was evil. However, nothing existed but the two of them, so they fought each other.

It seemed that the conflict between the two gods would go on forever, but in the end, Diachmell and Arazan defeated each other simultaneously.

The great black and white gods lay on top of each other and new gods were born from their shells.

Vida, the goddess of life and love.

Alda, the god of light and law.

Zantark, the wargod of fire and destruction.

Peria, the goddess of water and knowledge.

Shizarion, the god of wind and art.

Botin, mother of the earth and goddess of craftsmanship.

Ricklent, the genie of time and magic.

Zuruwarn, the god of space and creation.

These eight gods of the elements as well as the dragonemperor god Marduke, the Colossus god Zerno and the beastgod Ganpaplio were known as the eleven founding gods.

These eleven gods did not use their power to fight each other like the great gods that came before them they worked together and created the planet Lambda.

The eight gods of the elements created humans, modeled after themselves, and began to teaching and guiding them as their followers.

The dragonemperor god Marduke created dragons, and the Colossus god Zerno created Colossi as their followers.

To provide food for them, the beastgod Ganpaplio birthed countless birds and wild animals and released fish into the seas.

The wargod Zantark and the goddess Botin gave birth to the Dwarf race, while the goddess Peria and Shizarion gave birth to the Elves. People became a general term that applied to all sentient races, and the creatures that had been known as people up until that point came to be known as humans.
TLN The actual terms for these are humans and mankind respectfully, but having the term humans include Elves and Dwarves will be confusing so Ive made this adjustment.

 

The world created by these events was peaceful. The people believed in their gods, the dragons and Colossi were wise there was plenty of wildlife in the mountains for them to eat without fighting over territory and there were bountiful blessings in the sea.

However, the peace was shattered by the appearance of the Demon King Guduranis from the abyss beyond the stars.

After descending upon Lambda, Guduranis began a war to take over the world using the evil gods that were his servants.

The Demon Kings tainted Mana gave birth to monsters such as Orcs and Goblins that had never existed in Lambda before, and the Demon King used them to fight the other gods.

Though the people of Lambda had polished their fighting skills for competition and hunting the wildlife that sustained them, they were thrown into chaos as they had never experienced a fight to the death and the gods were driven into a corner. The wargod Zantark, the dragonemperor god Marduke, the Colossus god Zerno and the others fought bravely with their followers and the genie Ricklent bestowed magic upon the humans and gave them commands, but they could not turn the tides of the battle. The beastgod Ganpaplio put up a good fight, but was eventually destroyed.

To fight the Demon Kings forces, Zuruwarn, the god of space and creation, summoned seven champions from another world.

The seven champions taught the people techniques for fighting, gave them the knowledge to make great weapons and fought bravely at the frontlines themselves.

And so, through battle after battle, the Demon King was finally destroyed and sealed away not even a single piece of his body remained. The evil gods that were his subordinates lost their powers some were destroyed and others were sealed away into a deathlike state.

However, the final result was difficult to call a victory.

The wargod Zantark was cursed by the evil gods and fell into darkness Peria sank into the depths of the oceans and Shizarion returned to the wind. Botin was sealed deep within the earth while Ricklent and Zuruwan entered a deep sleep to regain their power.

Marduke was torn to pieces and Zernos heart was destroyed. Their followers powers weakened the number of dragons grew fewer and the weaker drakes increased in number. The Colossi became monsters that worshipped evil gods.
TLN This term is actually just dragon as well, but with different kanji (that means the same thing and is read the same way). Im translating  to dragon and  to drake to differentiate.

 

Finally, there were only three champions left and the population of the people had decreased to the point that even if the Elves and the Dwarves joined the remaining humans, who had a considerable population before the conflict, they were barely able to maintain a single city.

The remaining population was not enough to sustain civilization and culture. Even after the Demon King was defeated, the surface of Lambda was overrun by monsters that had been corrupted by his Mana during the conflict with his forces, and there were Devils Nests all across the lands. The surviving monsters began to breed and multiply endlessly.

Alda, one of the two gods who still had power left, chose to work with the champions to guide the surviving people. However, the goddess Vida believed that creating new races of people and having them join the other races would be a faster way to restore Lambda.

Vida was the goddess of life and love. Her powers were more suited to creating new races than for battle.

First, she mated with the Sun Giant Talos, who had retained his noble spirit and virtues without becoming a monster. She gave birth to a race of Titans with robust, large bodies that could barely fit in human towns.

Next, she mated with Tiamat, the most powerful of the surviving dragons that had been Mardukes followers, and gave birth to a race of Drakonids, humans with the power and horns of a dragon.

And then, with the kings of the beasts that had followed Ganpaplio, she gave birth to a great variety of beastpeople, and with Tristan, the god of the seas who had been Perias righthand man, she gave birth to the Merfolk.

Following that, she mated with an Elf that had been in her service at that time, giving birth to the Dark Elves, who had the same Mana that Elves did while also possessing outstanding physical prowess.

Alda criticized the goddesss actions, saying that they would only plunge the alreadyruined world into further chaos. As the god of law, Alda could not stand idly by as Vida gave birth to new races, one after another.

But Vida believed that her actions were correct, so they could never come to an agreement in their discussions.

Finally, Vida mated with monsters to give birth to Lamia, Scylla, Arachne, Centaurs, Harpies, Majins and other monster races.

And then she infused the power of the life attribute into one of the champions who had fallen in the battle with the Demon King, Zakkart, to turn him into an Undead. She mated with him to give birth to Vampires.

The original Vampires had powers that rivalled those of gods in every aspect. And they were able to share this power with the other races. By giving their blood to others, they could turn them into Vampires.

But Alda was outraged at the fact that Vida had mated with monsters and given birth to Vampires.

With the three remaining champions, he set out to exterminate Vida, who had given birth to a race that would overthrow the balance of the the world, and the race that she had given birth to.

Of course, to protect the new races of her children, Vida and the revived champion Zakkart fought against Alda and his followers. However, by the narrowest of margins, she was defeated and was dealt a deep blow, fell from her position as a god and disappeared into the Devils Nests with the champion Zakkart.

Alda was victorious, but he did not have the strength to eliminate the remaining Vampires. In addition, the goddess of the life attribute had now disappeared, and Alda now had to take up her role despite the fact that his own body was now exhausted.

Alda now called himself the god of light, law and life, and his followers praised him as the God of Law and Fate. But even 100,000 years after the battle with Vida, the world was still in a state of chaos.

 

 

 

This is the legend of the origins of Lambda.(Darcia)

Thanks, Mom. That was very helpful.(Vandalieu)

After Vandalieu finished preparing to hide in the house, he asked Darcia, whose spirit was residing inside the bone fragment, about how the world of Lambda came into existence.

He had continued creating Golems from the earth and rock of the cave, instructing them to stand guard there. In order to prevent the house from caving in, he had the Golems made from the earth and rock from deeper within the cave to move towards the entrance, while digging the cave deeper towards the back. And then he released the spirits from the Golems so that they sealed the entrance.

He could change the Golems shapes as long as he had Mana, so he had thought that he would be able to use them for construction, and he was right.

He had left broken furniture and vases when he sealed the entrance in order to make it appear as if the cave had collapsed. But he was just praying that High Priest Gordan wouldnt give an order like,Dig out the cave until you find that Dhampirs corpse!

He had already filled up the cave over fifty meters from the entrance while expanding it deeper, something that would normally be impossible without ordering some largescale construction or hiring an earthattribute mage.

He had left a fistsized hole to allow air to pass through, so there was no problem there. He didnt need any lighting, thanks to his Dark Vision skill.

Vandalieu was prepared for his new underground lifestyle.

And now he was talking to Darcia to pass the time.

So this is Mirg, a country in the northwest part of the Bahn Gaia continent thats a part of the Amid Empire. The Empire and its countries worship Alda, the God of Law and Fate, as the official religion. No wonder its so dangerous.

Alda was a god who had fought Vida over the races she had given birth to, saying that they would throw the worlds balance out of order. It was no wonder that a halfVampire would be subject to persecution no, extermination.

The Amid Empire and the Adventurers Guilds in its countries, where the Church of Alda had a strong influence, had extermination requests openly on display. Incidentally, the part used as proof of extermination The proof of extermination that was required was a deep crimson eyeball.

It made sense to hide in the forest in such a situation.

It seemed that Dark Elves like Darcia, as well as beastpeople, Drakonids and the other races that Vida had given birth to following the battle with the Demon King were also targets of persecution.

In the Amid Empire and its countries, only humans, Elves and Dwarves were accepted as people Titans, Dark Elves and beastpeople were discriminated against as demihumans. The most conspicuous discriminatory behaviour was that people were only used as slaves if they were criminals, while there were no restrictions on the buying, selling, ownership and use of demihuman slaves.

Including Vampires, the races that Vida had given birth to by mating with monsters were, of course, simply exterminated as monsters. Though the reality was that for every Vampire and Lamia that the adventurers and soldiers of the Amid Empire killed, there was a dead adventurer or soldier killed in retaliation, and even civilians suffered casualties.

The followers of Alda claimed that this was proof that they were evil, that their god was right. Meanwhile, Vidas followers believed that the reason they had become monsters that harmed humans was because they had lost the guidance of the goddess. In other words, it was all Aldas fault. This argument had apparently continued for many tens of thousands of years.

Next, tell me about my father, Vampires and Dhampirs.(Vandalieu)

Alright. But its time for your afternoon nap, so Ill continue when you wake up.(Darcia)

Okay(Vandalieu)

 

 

 

You have acquired the Golem Transmutation skill!

 

 

 

    Name Vandalieu
    Race Dhampir (Dark Elf)
    Age 0.5 years old
    Title None
    Job Ordinary person
    Level 0
    Job history None
    Attributes
        Vitality 18
        Mana 100,000,600
        Strength 27
        Agility 2
        Stamina 33
        Intelligence 25
    Passive skills
        Superhuman Strength Level 1
        Rapid Healing Level 2
        DeathAttribute Magic Level 3
        Status Effect Resistance Level 2
        Magic Resistance Level 1
        Dark Vision
        Mental Corruption Level 10
        DeathAttribute Charm Level 1
    Active skills
        Bloodsucking Level 1
        Surpass Limits Level 2
        Golem Transmutation Level 1 (NEW!)
    Curses
        Experience gained in previous life not carried over
        Cannot learn existing Jobs
        Unable to gain Experience Points independently

',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(5,'chapter 4','The Goblin collapsed as its skull was caved in with a dull noise.

Hmph. Are Goblins the only enemies that are going to come out?

The High Priest of Alda, the God of Law and Fate, hurled abuse at the Goblins as he beat them to death with his favorite mace.

High Priest Gordan, is it not likely that the Dhampir either escaped with his Vampire father or was buried alive when that cave collapsed?

So said one of the holy knights under his command, but Gordan was not satisfied.

Bormack Gordan was originally a commoner, but he was now a great man who had climbed to the position of High Priest by his thirties using his faith, physical strength and his magic of the life and light attributes.

Now he had a burning obsession with exterminating Vampires, Lamias, Scylla and the other monsters that Vida had given birth to.

Alda, the God of Law and Fate is the only one with authority in the world of Lambda and he has determined that the mere existence of these creatures is evil. It is our duty as his followers to destroy them!

Like the other zealous followers of Alda, Gordan earnestly believed this with no doubts. He felt no shame in having killed the Dark Elf or in his current task of hunting down her Dhampir son he was proud, believing that his actions were those of justice.

For that reason, he had been conducting a very thorough search of this small forest to find the Dhampir, but the search had run into problems.

Unable to contact the hunters who had sold out the Dark Elf to lead them through the forest, he had hired other hunters and joined forces with the feudal lords knights, but they had only encountered wild animals like wolves, bears and boars and the occasional weak monsters like Goblins that came out to attack them.

Five days after they had began their search, they discovered a cave that someone appeared to have been living in that had collapsed, but they hadnt been able to find the Dhampirs corpse.

Gordan had thought to dig out the whole cave, but had no choice but to give up on that idea because of the risk of it collapsing on top of them. If they had a construction specialist or a Dwarf miner at hand, it would be a different story, but there was no demand for a construction specialist in this area right now, so they were unable to hire such a person.

The reason they continued searching the forest for two more months despite this was that the hunter that had sold out the Dark Elf and two of his friends had simply gone missing after setting out to hunt the Dhampir.

Three hunters who were all familiar with the geography of this area had all gone missing. On top of that, their corpses still hadnt been found.

Gordans intuition told him that this was not the result of some coincidence or unfortunate accident, but something had taken revenge on them.

The Dhampir in question is apparently still a baby, but its not out of the question. There was a reported incident in the past where a threeyearold Dhampir slaughtered a party of Dclass adventurers.

Giving up here would mean allowing evil to take root and grow in the future. He had to hunt the Dhampir down and turn him into ash.

High Priest Gordan, it is impossible for us to make our search of the forest any more detailed than we have already done.

Baronet Bestero has withdrawn his knights already as well.

However, not everyone shared Gordans zeal or saw the impending threat that he did. The feudal lord, Baronet Bestero, had dispatched five knights and half of his soldiers to help with the search, but now he grimly refused to offer any help at all.

He could not allow the soldiers and knights that were needed to uphold public order to spend too long searching the forest.

In fact, he criticized the continuation of the search, asking,Is there no other work that the High Priest could be doing for the people?

 I know that the inhabitants of Evbejia have expressed their discontent.

This forest was an important source of resources for the people of Evbejia and the High Priest had told them,You cannot enter the forest while we are searching for the Dhampir, as it is too dangerous.It was only natural that the people of Evbejia were unhappy about being unable to enter the forest for two whole months.

No, it is not only the people. The adventurers are not happy either. Well, they are Fclass and Eclass adventurers of no importance.

They are taking their complaints to the Guild, saying that they are having their work taken away from them.

The monsters such as Goblins that the adventurers received payment to exterminate had heavily decreased in population during Gordans search of the forest. Of course, this resulted in a sharp drop in the income of those adventurers.

In addition, Evbejia was a central point of commerce between villages and towns so there were many merchants and wealthy travelers requesting escorts, but as the adventurers accepted those requests, there were less adventurers operating inside Evbejia itself. Naturally, the Adventurers Guild was not pleased by this.

Baronet Bestero didnt approve of the number of adventurers decreasing either. High Priest Gordan and his men were keeping the population of Goblins in check for now, but they were not permanent residents of this town. After they left, he had no way of knowing if enough adventurers would return to control the population of Goblins, who reproduced very quickly.

What are you saying we should do?! Surely youre not going to suggest we spare the Goblins that come and attack us!

Like I said, perhaps it is time to give up? My words are not those of the feudal lord, but there are plenty of other places that could benefit from our presence.

Muh

In reality, while they were spending their time here, the Vampires were moving in the shadows. Other monsters of Vida were also causing harm to the people.

Was it acceptable to continue spending time searching for this Dhampir with no leads to follow?

We have no choice. We will leave Evbejia the day after tomorrow.

The red evening sun shone on Gordans bitter face.

He had a strong feeling that the Dhampir would be laughing at them in the nights to come.

 

 

 

That Dhampir, Vandalieu, spent the afternoon that High Priest Gordan left sunbathing to his hearts content.

Sunlight Love you

He proclaimed his love for the sunlight. He had recently become able to speak, though it was not completely coherent yet.

Living for that long underground had been harsh on him.

He had thought that it would be for one month at most, but Gordan and his men had persisted for two whole months and Vandalieu had spent all that time living in the dark, surrounded by earth and rocks.

He had used his deathattribute magic to prolong the lives of the two hunters, but they only lasted about half a month. After that, he had Bone Monkey and the other Undead to crush the wheat grains and dried meat that Darcia had left behind and soak it with water to make baby food for him to stave his hunger off with. When he could no longer stand that, he desperately looked for insects in the dirt to eat.

His search led to the discovery of a stream of underground water he had thought the cave would flood and he would drown, but it ended up being a fortunate event. His water supply had fallen low and the discovery of this underground water had allowed him to clean his diapers.

Up until then, he had made a Golem out of the earth to change shape and dig a hole, drop in used diapers and then bury them. Having run out of the ones that Darcia had made before, he had been forced to use diapers made from the clothes taken from the dead hunters, and even those were running out.

He could use deathattribute magic to sterilize himself, but he couldnt stand having soiled himself. As a person, that was not acceptable.

He had been living on earthworms and sap from tree roots for the past few days. Thanks to a new skill he had learned,Danger Sense Death, he could find the ones that werent fatally poisonous, but as he consumed them, for some reason, his Status Effect Resistance skills level increased. There was likely something in the food he ate that was poisonous, though not enough to kill him.

I dont ever want to have to live a life where I can consider catching a mole to be a miracle ever again.

He firmly told himself that.

Vandalieu, being a Dhampir, could see in the dark just as well as he could in the middle of the day. But that didnt mean he didnt need sunlight. After all, he was halfDark Elf.

He had been worried that he was going to die of vitamin deficiencyrelated diseases.

High Priest Gordan, adventurers of the Fivecolored Blades Party, please be well.

Vandalieu earnestly prayed for the wellbeing of High Priest Gordan and his men, who had left Evbejia and Baronet Bestero today, and theFivecolored Bladesthat had left two months ago.

Please dont die until I kill you.

He earnestly prayed for them to not be killed by someone else or die by accident or illness.

The limit for a human to be in the dark is around ninety hours? If youre trapped in darkness for that long, apparently it causes some mental problems, but I seem to have endured it surprisingly well.

His sunbathing had calmed him down. Relieved that his mental condition had not changed over the past two months, he had the Undead set traps to catch rabbits and other small animals.

When he checked his status, a skill with an ominoussounding name called Mental Corruption had been upgraded all the way up until level 10 was this because he had sworn to take revenge? Well, it also seemed to serve as a mental stability skill, so he didnt think much of it.

As for his DeathAttribute Charm skill, he thought it was a skill that he had used in Origin. There were several occasions when the elderly researchers in Origin that had treated him as a guinea pig died of strokes, became spirits and then completely changed their personalities and apologized to him in tears.

He hadnt thought badly of them for it if it werent for his conversations with the spirits in Origin, his mind would have crumbled long before his body died.

However, when Orbie and his companions had approached him as spirits after they died, he had felt only disgust for them.

Vandalieu did not want to let them pass on peacefully, so he had forced them to inhabit a stone pillar that was supporting the cave, and they were still there now.

For now, I guess I have to stay quiet and hidden until I grow a bit bigger.

Over the past two months, Vandalieu had grown teeth other than his fangs. He was now also able to walk, though with poor balance due to the relatively large size of his head.

But he still needed to sleep for long hours and wasnt able to run. He was capable of only an unsteady waddle, so even if he spent a whole day walking he wouldnt cover much distance.

If he wanted to leave this place, he still needed more time to either grow up or make preparations.

Because of that, he had to postpone his revenge on Baronet Bestero, his knights and the people of Evbejia.

He had already thought of how he was going to do it and had the confidence that he could pull it off, so he was feeling impatient, but it was something that he had to endure.

Its already winter, huh. Now that I think about it, I still havent asked Mom about this worlds history. But before that can I catch some prey whose blood I can drink?

Though he had finally come outside, it seemed that he would die of starvation in the cold of winter. Vandalieu let out a sigh at the harshness of this world.

 

 

 

In Lambda, Vampires were divided into four different categories.

First, there was the Vampire born between Vida and the Undead champion Zakkart, who had long since perished and had his name forgotten, theTrue Ancestor.

The ones who had directly received the blessing from the True Ancestor or those who were his direct descendants were known asPureBreeds.

Those descended from the PureBreeds with Vampire blood that had thinned over the generations were known as theNobleborn.

And finally, those given the blood by the PureBreeds and Nobleborn and taken as servants did not inherit the Vampires magical abilities such as their charming gaze they inherited only their physical abilities such as their superhuman strength and regenerative capabilities. They were known as theSubordinates.

Generally, the closer a Vampire was related to the True Ancestor, the stronger their powers were. But Vampires had considerable individual differences, so some Subordinates who were even more powerful than Nobleborn Vampires also existed.

The common features among all Vampires were their weakness to sunlight and the antiUndead lifeattribute magic taught among the followers of Alda. The other feature was the need to drink blood to sustain themselves.

Vandalieus father was aPureBreedsSubordinate.He wasnt even one of the exceptional ones that were stronger than the Nobleborn he was simply a normal human who had been turned into a Subordinate. His name was Valen. He had originally been a thug living in the slums.

The only thing that was not ordinary about Valen was his resistance against the sun. He had been even more resistant to the sun than most humans, so his PureBreed master valued him highly. He had ordered Valen to carry out various tasks and gather information among the people.

That was when he met Darcia.

Just like in old love stories, the two of them had fallen in love at first sight.

However, in the modern age, Vampires were also divided into two factions. There were the conservatives who continued to believe in Vida, who had been defeated by Alda, and the extremists who had converted to follow evil gods. The latter overwhelmingly outnumbered the former.

If Valens master had belonged to the conservatives, there wouldnt have been any problems with the love between him and Darcia. But as an extremist, he was strongly opposed to the dilution of the Vampire blood with that of other races, even if that race was another race created by Vida.

Making use of his special trait that allowed him to use his powers even during the day, he eloped with the pregnant Darcia, but in the end, he fought with his pursuers to protect his unborn son.

And so Darcia, who managed to escape to this forest, gave her son half of his fathers name and half of her own name.
TLN Valen is Varen, Darcia is Darushia, and Vandalieu is Vandaru. Vandalieus name in Japanese is a combination of the first and last syllables of Valens name and the first two syllables of Darcias name.

 

So, thats why Im named Vandalieu.(Vandalieu)

Thats right. Do you like it?(Darcia)

Yes, I like it. Its a very good name.(Vandalieu)

Vandalieu, who for the first time in a while was able to eat something other than insects and tree root sap, listened to her story about his father that she had already told him dozens of times.

Thats good. Im sure your father would be happy, too. Wed agreed that wed name you Vandalieu if you were a boy and Varcia if you were a girl. Err, so, where was I? Ah yes, I was telling you about the Vampires.(Darcia)

With a smile, Darcia began to repeat the story that she had just finished telling. Her memory was prone to quick failure.

After the death of her body, her spirit was deteriorating as it spent time in this world. There were cases where spirits would retain their personalities even after a hundred years if they were tenacious or harbored strong hatred and strength of will, but she felt regrets about the fact that she had been reunited with Vandalieu as she was about to pass on.

Even though she was inhabiting a piece of her own remains, her spirit could not recover.

 Even so, if I keep supplying her with Mana, shell last a hundred years.

As he listened to Darcia talk, he decided to find a way to make her a new body within that time.

Despite the few problems that she was having with her memory, Darcia could still clearly remember things from when she was alive, so she told Vandalieu everything she knew.

Lambdas time and calendar were the same as Earths, with twentyfour hours in a day, twelve months of 360 days in a year.
TLN Yes, it says 360, not 365. ()

 

The reason Japanese was spoken was because it had spread from the champions who had spoken Japanese in their original birthplaces after they survived the battle with the Demon King. Currently, only common Japanese terms were used and commoners could only read hiragana and katakana. The ability to read kanji was limited to nobles and merchants who had received a higher degree of education.

Experience Points were a quantification of the life experiences that a person had been through. The people of Lambda increased their Experience Points little by little as they lived their lives. Of course, defeating monsters would grant Experience Points. However, the amount of Experience Points gained through certain experiences depended on ones Job.

Jobs were apparently theBlessings of the Godsthat existed since before the Demon King appeared. The people were feeble when compared to gods, so they were given Jobs in the hope that one day they would grow to be able to stand alongside the gods.

Jobs determined attribute points and adjusted how new skills were acquired, and if a Jobs level was raised to 100, one could change Jobs.

For example, if someone reached level 100 as an apprentice warrior, he could change Jobs to a warrior.

As well as than warriors and mages, there were farmers, craftsmen and the like. People with Jobs like warrior and mage would gain most experience from defeating monsters and enemies in battle, while farmers would gain most experience by doing everyday farm work or doing agriculture research and craftsmen would do so by crafting things.

Well, it would be strange for someone with a warrior Job to gain a lot of experience by doing farm work. It would also be strange for a craftsmans skill at crafting things to increase by defeating monsters.

In other words, it was a system to encourage warriors to be warriors and farmers to be farmers everyone should work hard in their specialized fields.

Incidentally, monsters had no Jobs but had levels, and apparently evolved or transformed into a higherrank monster upon reaching level 100.

Therefore, it had been possible for Vandalieus father, Valen, to survive long enough and evolve from a Subordinate to a higherrank monster. If he had managed to do so, he might have survived.

Now I know just how bad the curses that Rodcorte gave me are, the ones that prevent me from gaining existing Jobs and gaining Experience Points for myself.

Cannot learn existing Jobs In other words, the curse prevented him from attaining a Job whose existence was already known, so it was troublesome. If he didnt discover some new, unknown Job, he would forever be Jobless.

The curse that prevented him from gaining Experience Points on his own was likely even more serious. While others could steadily increased their levels, no matter how hard Vandalieu worked to train himself or study, he wouldnt gain even a single point of Experience.

Eight months have actually passed since I was born. Ive gone through much harder experiences than the typical infant Ive trained myself in magic, used it in practice and finally even captured Orbie and those other hunters, but I havent leveled up.

At this rate, he would reach adulthood and still be level 0. Being a Dhampir, his attribute points were high, but it would still be impossible for him to work as an adventurer and a soldier.

With that said, it would also be difficult for him to work as an industrial worker like farmer or craftsman.

With no Job, he would not have any adjustments for how he would acquire new skills. In other words, he would have to work several times harder than regular craftsmen to compete with them.

In addition, Vandalieu was aware that he had poor communication skills.

He had no talent for communicating with people he had been born with anxiety problems.

The more I think about it, the more I get the feeling that Ive been cornered in this life Ill stop thinking about it, then.

Lets go to sleep. They say that sleeping children grow up well.

Vandalieu had Bone Monkey put out the fire that he had started by making Wood Golems out of two pieces of firewood and having them rub against each other. Wrapped in furs, he went to sleep, leaving the other Undead to keep watch over him.

He drifted off, thinking that the lullaby that Darcias spirit was singing to him sounded nice, but wishing he could feel her warmth.

Incidentally, Darcia didnt know of any Dhampirs other than Vandalieu and Dhampirs themselves were very rare. Therefore, she didnt know anything about Dhampirs and he had been unable to ask her about them.

 

 

 

    Name Vandalieu
    Race Dhampir (Dark Elf)
    Age 8 months old
    Title None
    Job Ordinary person
    Level 0
    Job history None
    Attributes
        Vitality 21
        Mana 100,001,200
        Strength 31
        Agility 4
        Stamina 30
        Intelligence 27
    Passive skills
        Superhuman Strength Level 1
        Rapid Healing Level 2
        DeathAttribute Magic Level 3
        Status Effect Resistance Level 3 (LEVEL UP!)
        Magic Resistance Level 1
        Dark Vision
        Mental Corruption Level 10
        DeathAttribute Charm Level 1
    Active skills
        Bloodsucking Level 2 (LEVEL UP!)
        Surpass Limits Level 2
        Golem Transmutation Level 2 (LEVEL UP!)
    Curses
        Experience gained in previous life not carried over
        Cannot learn existing Jobs
        Unable to gain Experience Points independently

',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(5,'chapter 5','Note from the translator

Huge shoutout to SoulSlayerAbad, who translated 90 of this chapter. Hes going to be assisting me from now on to help me cope with the sheer volume of this series.

Yoshi

 

Time for training.

Vandalieu told this to his undead servants after making them line up in front of the cave.

 

 

 

Because of Rodcortes curse, Vandalieu couldnt level up or change Jobs, thus making it impossible for him to become stronger on his own. So he tried to change his way of thinking and thoughtIf I cant become stronger, then Ill just make my servants stronger.

His reasoning was that if the undead following him were stronger, he could become capable of doing a wider range of things.

Going to the town with them might be difficult, but Mom did say that there were some adventurers that had Jobs in which monsters did their bidding, like Tamers, Lesser Mages and Summoners, so itll work out somehow.

In the future, he wanted them to have enough strength to fight monsters easily, but for now, if they could carry him and run with the same speed they had when were alive, it would be enough to get out of this forest and go on a journey.

They cant even defeat wild wolves and bear as they are now, let alone monsters. Just the other day, my Bone Goblin was broken by a living Goblin.

The Undead that he had recently created were all very weak. Well, that was to be expected since he had a group of level 1 Undead. Well then, why didnt he just create stronger Undead? That reason was that he didnt know how to make Undead above level 2.

In his previous life when he had awoken to the Death Attribute, he hadnt experienced turning anybody into Undead except himself.

The researchers at that facility would have never given me the chance to gather servants. When I awoke, my control over both my body and my magic had already been taken away long ago. If they hadnt done that, I would have found out about the various things they were researching, like immortal soldiers. Well, that facility ended up failing anyway.

He decided to change gears and check the fighting potential of his servants. Excluding the bugs that he used for gathering information, he lined up the Undead that looked like they could fight.

Bone Monkey, Bone Wolf, Bone Man, Bone Bear and Bone Bird. These were the ones that seemed suited the task. There were also Bone Boar, Bone Rabbit, Bone Goblin and so on. But the only ones who were complete, without missing or broken bones, were these five only.

Well then, let us start thetraining? Wait, what should I even make them do?

Vandalieu, who suddenly realized that he had no idea about what to make Bone Monkey and the rest do, was at a loss for words.

 

 

 

Even amateurs had an idea about what do when training in order to become strong.

One would run to increase stamina and do strength training to increase muscle strength.

One would learn the moves of martial arts or unarmed combat and then have sparring sessions to get practical experience, and so on.

But what would be an effective, ExperiencePointearning training for the Undead lined up in front of Vandalieu?

Running? It would be pointless. They have no heart or lungs, and in the first place, what would be the point in the untiring Undead aiming to gain more stamina?

Strength training? No, still no use. They had no muscles in the first place, having bodies solely made up of bones, so what exactly would they be trying exercise by doing pushups and situps?

Unarmed combat and martial arts? Vandalieu didnt know any, so there was no way to teach them.

Sparring? The mental capabilities of the Undead were too low if he had them fight, they would keep fighting until they were in pieces. It would just become a contest for one of them to crush the other.

To begin with, Vandalieu himself never received any real training for fighting. On Earth, he did a moderate amount of exercise, but it was on the level of practising judo in physical education class in middle and high school certainly not something that could be used in a real battle. In Origin, well, nothing more needed to be said.

Maybe I can ask the spirits of Orbie and those hunters No, its impossible.

Those guys knew how to use bows. And the Undead didnt yet have enough mental capacity to use bows. Thered be no point in teaching them.

Lets check their status for now.

Though Darcia had failed to check Vandaleius status when she tried, he was easily able to check the status of the Undead. What had been impossible for even a parent to do was probably possible because the Undead were in a subordinate relationship with Vandalieu. Darcia didnt know much about the Jobs that let one control monsters like Tamer or Lesser Mage, or the skills related to them, so there was no way to be sure why.

The status of the Undead was the following

 

 

 

    Name Bone MonkeyBone WolfBone BearBone ManBone Bird
    Rank 1
    Race Living Bones
    Level 0
    Passive skills
        Dark Vision
    Active Skills
        None

 

 

 

Theyre weak Too weak.

The Undead all belonged to the same race, regardless of what creatures bones had been used to make them, and they all had the same status.

Living Bones Corpses that moved despite having become only skeletons after death. They had no other special characteristics.

They did move, but their movements were clumsy and their strength was far below that of an average adult male. As for agility, they werent capable of running their movement was slow. In addition, they couldnt use the abilities and skills that they had while they were alive.

The bones themselves were hard, but it would be easy to defeat them by attacking their joints.

They had almost no intelligence or instincts, and at most they would make practice dummies for apprentice adventurers.

In short, they were undead that couldnt be relied on in battle.

So how am I supposed to make these guys stronger? I created them two months ago and theyre still level 0. I guess that means that they havent done anything that would gain them Experience Points as Living Bones. Then what would gain them Experience Points?

Swordsmen would gain Experience Points through battles and training, farmers through farming and weaponsmiths through forging weapons. So what would Undead need to do to gain Experience Points? How did monsters normally gain Experience Points?

By killing humans, maybe?

It occurred to him that they would need to harm humans as monsters would do, but Even if this would work, it would be difficult to do in practice.

Vandalieu would feel no remorse no matter how many of the people of Evbejia he killed, but if he killed people and then the Adventurers Guild sent out a request for him to be exterminated, he would be forced back to his underground lifestyle.

He might be able to escape using the Undead as a decoy, but complete skeletons of large animals like Bone Monkeys werent easy to find. They werent servants that he could use carelessly.

Well, lets try killing things like rabbits and mice.

Because Vandalieu had needed fresh blood as a replacement for his mothers milk, the Undead had captured animals like rabbits in traps, but kept them alive. Vandalieu would then bite them with their fangs to suck out their blood.

If the Undead killed the animals themselves, they might be able to gain Experience Points.

 

 

 

The next day, the Undead successfully captured a Goblin alive.

High Priest Gordans expedition had decreased their numbers, but not completely wiped them out. There was a reason the Adventurers Guild was always requesting the subjugation of Goblins.

Gyiih! Gyageeh!

There was no way to know whether the Goblin was trying to be menacing or hurling insults at them in the Goblin language, but it was shouting in an annoying voice as the Undead held it down.

So this is a Goblin

Vandalieu looked at it with excitement, though his face was expressionless. It really was a fantasy world, he thought.

Goblins. They were the typical smallfry monsters found in numerous fantasy works and it seemed like Lambda was no exception.

It was lowerrank monster that not only lived in Devils Nests that were inhabited by monsters, but also lived in normal forest and even grasslands. Its body had dark green skin with a height reaching around the chest of a grown human and it had long ears like an elf, but its face was incomparably ugly.

Its strength was about the same of an average human, if not a little less, and it wasnt particularly agile. Its mental age was the same as a threeyearold, and it armed itself with a tree branch while using the hide of an animal to cover its body for protection.

It was at Rank 1 and stronger than a single of the Living Bones Undead, but if alone, it was so weak it could be taken down by a farmer with a hoe. However, there was several superior types as well, so it was wise to be cautious.

But the thing Goblins were feared for the most were their reproductive capabilities and adaptability.

When breeding, Goblins birthed at between three to eight children at once. The newly born children became full adults in six months. The Goblins could keep on increasing in numbers regardless of if they were in the middle of a scorching desert or in the middle of winter.

Oh and theres also the setting that often occurs in fantasy works where they kidnap human women. Though apparently its not as common as Orcs doing that.

He considered the knowledge Darcia had given him, but he thought it unlikely that the Goblin in front of him could do that. Simply because it didnt have the strength to.

For now all of you, go ahead and deal the finishing blow together.

His initial excitement was over now it was time to see if the Undead could gain Experience Points or not.

Giyaahhhhh!

Bone Monkey and Bone Man hit and kicked the screaming Goblin on its head, Bone Bear and Bone Wolf bit and scratched it, while Bone Bird pecked at it. It was a gruesome method, but since the Living Bones couldnt finish off the Goblin in one hit, it couldnt be helped.
Within ten seconds, the Goblin stopped moving. Vandalieu checked the status of the Undead.

 

 

 

    Name Bone MonkeyBone WolfBone BearBone ManBone Bird
    Rank 1
    Race Living Bones
    Level 2
    Passive skills
        Dark Vision
    Active Skills
        None

 

 

 

They had leveled up from level 0 to level 2.

Oh, they leveled up! Now its clear that Undead can gain Experience Points from killing living things.

Having found out that even the Undead that he created using deathattribute magic could gain Experience Points, Vandalieu was both relieved and elated at the same time. With this, the prospect of living as an adventurer somewhere away from the influence of Amid Empire and the Church of Alda wasnt impossible. He felt really good.

Hmm? I feel a little too good status.

He was a little curious, so he tried checking his own status.

 

 

 

    Name Vandalieu
    Race Dhampir (Dark Elf)
    Age 8 months old
    Title None
    Job None
    Level 3 (Up!)
    Job history None
    Attributes
        Vitality 24
        Mana 100,001,203
        Strength 29
        Agility 4
        Endurance 31
        Intelligence 29

 

    Passive skills
        Superhuman Strength Level 1
        Rapid Healing Level 2
        DeathAttribute Magic Level 3
        Status Effect Resistance Level 3
        Magic Resistance Level 1
        Dark Vision
        Mental Corruption Level 10
        DeathAttribute Charm Level 1

 

    Active skills
        Bloodsucking Level 2
        Limit Break Level 2
        Golem Creation Level 2

 

    Curses
        Experience gained in previous life not carried over
        Cannot learn existing Jobs
        Unable to gain Experience Points independently

 

A surprising change had occurred.

My level has gone from 0 to 3!? The curse is still active, so why?

In the 8 months that he spent after being born on this planet, Lambda, his level had not increased at all no matter what he did. Even when he practised deathattribute magic, or killed rabbits by sucking them dry, or even killed Orbie and his partners in the same way.

He checked his status again there was no mistake. However, the Unable to gain Experience Points independently curse was still there.

Then why? As Vandalieu was thinking about this, an explanation came to him.

Maybe, when Bone Monkey and the others gain Experience Points, a portion of that goes to me?

On Earth, there had been games with systems where when friendly monsters or the clones of the protagonist gained Experience Points, the protagonist gained a portion of that those. Had something similar happened here?

The effect of the curse just stops me from gaining Experience Points on my own, but gaining Experience Points by having the Undead under me earn it for me is effective For curse a god cast on me, thats too big of a loophole.

To overlook that fact that Vandalieu could get around the effect of the curse that had been cast in order to drive the deathattributewielding Vandalieu to suicide by having the Undead gain Experience Points for him. It was such a big loophole that he thought it was some kind of trap for moment. Rodcorte wouldnt even have to think for a second to realize that Vandalieu could create Undead.

However, the more he thought about it, the more Rodcorte seemed like a simple fool.

Now that I think about it, what he is doing is basically just passing all the work to us. Giving strength and opportunities to us to everyone excluding me, and then offering no detailed instructions or following up with us afterwards. In that case, it would be natural to think that he wouldnt be able to predict something like this.

It was likely that Rodcorte was viewing Vandalieu and Amemiya Hiroshi from a place that was far above. It wasnt like a noblesandpeasants relationship, it wasnt even in the same dimension. It was like the relationship between a player playing a simulation game and the player character inside the game. That was the extent of the gap between them.

That was why even though he had put Vandalieu through such a terrible experience, he had merely thought That was wrong of me and nothing more. That was why, on top of that, to make Vandalieu give up on his revenge, Rodcorte thought nothing of trying to drive him to despair so that he would commit suicide.

That was why Rodcorte didnt think too deeply about Vandalieu. To him, Vandalieu was just one character among a hundred and one characters.

Thats why it was a curse like this. At this rate, I might be able to do something about my Job too.

Vandalieu felt delighted and invigorated, but at the same time, remembering all those thing made his wrath against Rodcorte flare up again. And then he renewed his vow to never commit suicide, a vow that he had made to himself many times before.

Incidentally, he went ahead and had the corpse of the Goblin buried. The blood of Goblins tasted bad, there were no raw materials to be gotten from it, and since he wasnt an adventurer, he couldnt collect a bounty on it so there was no point keeping its body parts as proof of killing it. He thought about turning the corpse into Undead, but thinking that increasing the numbers of bodies that needed training would be a hassle,  he said goodbye to it.

 

 

 

When the sun had completely sun below the horizon, a bonfire could be seen burning in front of the cave in the forest.

Five Undead with bluishwhite flames burning in their empty eye sockets were gathered around the red fire. And then there was the child that looked like a toddler with such white skin that one would shiver at the sight of him, even in the red glow of the fire.

In addition, the spirit of a beautiful, yet scarred Dark Elf appeared beside that toddler, as if cuddling him.

If a traveller happened to come across the scene, such fear would be instilled in him that he would run away as fast as he could.

The Dark Elfs spirit slowly opened her mouth.



It was an awkward song. It was said that it was the song that the champions had sung on their birthdays long ago, and it was still sung on birthdays in Lambda to this day.

Kakakakak, Kashakashakasha.

The Undead that had nothing but bones, instead of the song began making sounds with their teeth and beaks, or their hand or feet bones in rhythm with the song.

Happy first birthday, Vandalieu!

Receiving the blessings of the spirit, the toddler breathed in, and whispering a few choice words, blew in the direction of the bonfire.

Heat Leech.

The bonfire which was robbed of its heat by the magic spell imbued in the breath promptly went out. The surroundings became pitch black, but the hearts of the ones present were shining like the sun.

Happy first birthday, Vandalieu. Your Mother is really happy.

Thank you, Mother.

The humidity of June in the country of Mirg was less than that of early summer in Japan. Vandalieu had turned exactly one year old today. They were gathered here to celebrate his birthday, but he couldnt deny that it was less than glamorous compared to the birthday parties in typical households in both Earth and Origin.

There was no cake and no presents. Their feast consisted of soup, with stock made from the bones of a raccoon with its meat finely minced and cooked with some spices.

And, of course, the fresh blood of a raccoon.

Though he had forcibly made the hard, smelly meat of the racoon into something edible, it was in no way an excellent meal. For a Dhampir like Vandalieu, it could be said that the blood was the only saving grace.

Im really sorry. If your Mother was still alive, Id have made you a proper feast.

Nonetheless, Vandalieu was happy.

Theres no need to apologize, Mother. Im very happy.

Vandalieu had lived on both Earth and Origin, but this was the first time he was experiencing someone celebrating his birthday. In Origin, no researcher would celebrate the birthday of a guinea pig. To them, it was probably nothing more than increasing the number in the age field of their data records by one.

On Earth, he had lived with his uncles family, but was toldYoure going to becoming a very responsible adult in the future, so if we start celebrating your birthday or christmas while youre a kid youll become spoiledand thus received no presents or cake. He had never even been congratulated once, even though the uncle did celebrate his own childrens birthdays.

For Vandalieu, this was his first birthday party.

Vandalieu

Besides, today is a very memorable day when a lot of happy things happened.

Vandalieu said to Darcia, who had become teary eyed and reminded her about the other memorable things that had happened.

First of all, he had become able to speak properly. This was a surprisingly important thing, because by being able to speak words, he was finally able to chant the spells he was casting.
Up until now, he had been unable to chant the spells, and had been forcibly activating them by using an absurd amount of Mana. Therefore, each spell used Mana several times more Mana than it normally would and its potency was less than half of what it would normally be. If he hadnt possessed such an abnormally huge amount of Mana, he would not have been to come as far as he did.

However, since he did come this far without reciting the spells at all, as soon as he was able to speak, he gained theChant Revocationskill, which allowed him to cast spells without chanting them.

Apparently, it was such a rare skill that even Darcia was shocked, although she was a little bit depressed by the timing.
TLN I think its because her baby just learned to speak, and now he doesnt have to.

 

In addition, Bone Monkey and the other Undead had reached Level 100 and increased their rank to 2.

 

 

 

    Name Bone MonkeyBone WolfBone BearBone Bird
    Rank 2
    Race Bone Animal
    Level 07
    Passive skills
        Dark Vision
    Active Skills
        None

 

 

 

    Name Bone Man
    Rank 2
    Race Skeleton
    Level 4
    Passive skills
        Dark Vision
    Active Skills
        None

 

 

 

As usual, there were no skills except Dark Vision, but there were great benefits that had come with increasing their rank.

The overall attributes of Bone Monkey and the other Undead, which had now become Bone Animals, had increased to match the values they had as when they were alive. In addition, they now not only obeyed Vandalieus commands, but had their own intelligence of normal beasts.

Bone Man was the only one that had changed into the Skeleton race after the rank up, but that was a given since he was made of the bones of a human. He had the strength and agility of an average human, and from the viewpoint of adventurers it was still a small fry, but for Vandalieu it was a reassuring addition to his forces.

If he made him wear the leather armor that he had obtained from Orbie and the other hunters and then equip him with a dagger and wooden shield that he had made by creating a Wood Golem, he could prepare a fine skeleton warrior. Right now, it was practising using a bow.

Incidentally, Vandalieu had also reached level 100. However, since it was the level 100 of None Job, his attributes did not increase that much. Although he had technically fulfilled the requirements for a Job change, he didnt have access to the facilities required for a Job change, namely the special rooms that could be found in various guild or temples, so it was meaningless.

Finally, even though his forces had come together somewhat, Vandalieu thought that it was still too early to exact his revenge on Evbejia. His power was still far too little. Even now, starting and completing his revenge would be possible. But he wasnt sure that after that he would be able to run away from Evbejia and reach a place far away from the Mirg country where a Dhampir could live peacefully in a town. He must get his revenge on Evbejia and feel satisfied afterwards, so for now he was amassing power and supplies.

Im thinking of starting to hunt bandits from tomorrow.

Bandits. They were not monsters, but rather armed factions of criminals that worked as robbers in the outskirts of towns.

In other words, they were humans.

How was the goal of amassing power and supplies related to taking the initiative to kill humans? There was very logical answer to this.

Recently, no matter how many hares or raccoons the Undead kill, their levels dont go up, and there no monsters stronger than Goblins in this forest. But Evbejia is the center of the path that leads to other villages, and if we search for some bandits, Im sure well find some. If we kill the bandits, thatll become Experience Points for the Undead, while the supplies they have hoarded will become ours. Two birds with one stone, I think.

And on top of that, in this world, there was no law against killing bandits and taking their loot for yourself. On Earth and Origin, killing and taking their stolen good made you a fullfledged criminal. However, in Lambda, even if a normal citizen killed a bandit, he would not be treated as a criminal. On the contrary, you were praised as having done a good job and no one called you akilleror told you that youtook away their human rights.The stolen goods were treated as the rightful property of the one who took down the bandits. If the original owner really wanted to regain his possessions, it was thought that he should independently negotiate with the one who took down the bandits.

But, isnt taking down bandits dangerous? Even Dclass adventurers do it in a group.

Darcia spoke to Vandalieu, worried about him. He smiled at her reassuringly.

Ill prepare a proper ambush, so its okay. Besides, I already have an idea of where I can find some bandits.

And so, from the next day, at the age of one, Vandalieu began to do work by himself that even Dclass adventurers did in a group.

 

 

 

    Name Vandalieu
    Race Dhampir (Dark Elf)
    Age 1 year old
    Title None
    Job None
    Level 100 (LEVEL UP!)
    Job history None
    Attributes
        Vitality 34
        Mana 100,001,223
        Strength 32
        Agility 7
        Stamina 33
        Intelligence 45

 

    Passive skills
        Superhuman Strength Level 1
        Rapid Healing Level 2
        DeathAttribute Magic Level 3
        Status Effect Resistance Level 3
        Magic Resistance Level 1
        Dark Vision
        Mental Corruption Level 10
        DeathAttribute Charm Level 2 (LEVEL UP!)
        Chant Revocation (NEW!)

 

    Active skills
        Bloodsucking Level 3 (LEVEL UP!)
        Surpass Limits Level 2
        Golem Transmutation Level 2

 

    Curses
        Experience gained in previous life not carried over
        Cannot learn existing Jobs
        Unable to gain Experience Points independently

',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(5,'chapter 6','The seven bandits sat in their simple hideout, drinking the alcohol that theyd stolen from their victims today.

Todays haul was pretty good, wasnt it?

Yeah, if we take it to the village everyone else will be happy as well.

They talked as if they were farmers sharing a drink after finishing their work in the fields, but they were definitely bandits.

The proof of that was the fact that the wine that they were drinking was taken from merchants who had been too stingy to hire adventurers to escort them, along with their lives.

Their equipment was old their leather armor had numerous patches where they had been previously repaired and their weapons consisted of handmade spears, woodcutting hatchets, bows and arrows  they were incredibly poorly equipped. They hadnt received any formal training, either. If you looked at it one way, their method of fighting came from experience in real battle and brawls. If you looked at it another way, they were just a group of people who swung their weapons around with no technique.

In fact, most of their jobs were Farmer, so they hadnt received training for battle. The reason that they were bandits was their poverty.

The young men from a village that had become poor for various reasons had left the village to commit crimes. Such cases were not rare in Lambda.

Well then, I suppose someone should stand watch so we can sleep.

The bandits base simply consisted of several tents put up in a meadow with long grass, in a spot where they had cleared some of the grass away. They did have a lookout tower, but they didnt seem to have any intentions of using it.

In fact, there was nothing in this meadow but wild deer, so they were safe as long as they kept a fire lit. The reason they still kept watch is because sometimes a stray Goblin came to attack.

But even such a Goblin would scream loudly as it approached them, so they had little motivation to stand guard.

That was why they had no way of knowing if any enemy were to approach them quietly, concealing the sound of their footsteps.

Hic, Ive drank a little too much  UOH?

The bandit on watch who had apparently drank too much wine was snatched up by Bone Bears paw that had risen up from the wall of grass and disappeared.

Muffled sounds came from inside the tents, but nobody came out. Instead, Bone Man, Bone Monkey and Bone Wolf with Bone Bird on his back emerged from the long grass.

And behind them was Vandalieu.

There are six left. Finish them quickly.

The air was filled with the sound of rattling bones.

The bandits never returned to their village.

For Vandalieu, finding the hiding place of bandits was easier the more atrocious their crimes were, as he could use the spirits of their dead victims as sources of information.

The victims who were attacked by their bandits and had not only their belongings taken, but also their lives. Their regret and hatred towards their assailants was fierce, and having bugs find the spirits was a simple task.

Even if the spirits were far away, he could use theSpirit Communicationtechnique to summon them to hear what they had to say.

And then he would send out the Undead using the information he got from the spirits, and the hideouts would be found within a few days.

With this method, Vandalieu had already found four groups of bandits operating in the area around Baronet Besteros territory.

After finding them, he continued to gather information such as how many of them there were and how they were armed. The villager bandits that he had attacked today were the smallest, most poorly equipped group out of the four.

Nice, the trainings successful. Good work, everyone.

Vandalieu was emotionless as he confirmed that the bandit group had been exterminated, but he was in a good mood as he praised the Undead.

Of course, the Undead, being made of only bones, were unable to smile, but the blue glow in their sockets seemed to flicker in happiness. Their MVP was Bone Bear, who had killed the one on lookout duty with a bear hug and then buried another bandit with his claws.

Bone Man had stabbed two of them to death with his dagger, and Bone Monkey and Bone Wolf had taken one each, strangling and biting them to death.

Err, itll be fine. Youll be able to fly soon!

Bone Bird was the only one to be depressed. Vandalieu had made a Golem of the earth and buried one of the bandits from the neck down, and Bone Bird had defeated this bandit by diligently stabbing him with its beak repeatedly.

With only one wing, it was just a burden, but there was no helping it. Bone Bird couldnt fly.

It was obvious that wings made of only bones and no feathers would not be able to fly.

If you keep gathering experience and rank up again, youll definitely be able to fly. Believe in yourself!

As Vandalieu encouraged Bone Bird, it flapped its wings with a rattle to express its happiness, as if to say that it would try its best.

After looking at Bone Bird happily and giving it a pat on its skull, Vandalieu examined the treasure that the bandits had gathered.

The first thing that caught his eye was the wagon with three barrels of wine still left in it. They were the leftovers of what the bandits had been drinking.

Evbejia prided itself on its wine, so it was probably of reasonably high quality.

 I wonder if theres anything else.

But to a oneyearold, it held less value than water. He had no way of selling it for money and his companions were all made of bones, so it was completely useless. At best, he could perhaps use it for cooking.

No, it was probably possible for him to drink it. Even as a oneyearold, Vandalieu possessed the Status Effect Resistance skill at level 3, so he would likely not become drunk unless he consumed a very large quantity of it.

No You cant drink alcohol at the age of one. What if you become a drunkard later in life?

However, his guardian would not allow it, so he decided not to drink it.

Well, I might as well take at least one barrel back. Theres even a wagon for it. And next is 

Vandalieu searched around the wagon and inside the tents, but concluded that they werecompletely brokecompared to normal adventurers or soldiers. They had apparently stolen cloth purses from the merchants who had normally stored copper and silver coins in them, but there was a whole pile of pouches filled with only seeds and oats that would normally be fed to pets and livestock on Earth. There were some dried river fish. The rest were items for the bandits daily necessities.

The special feature was perhaps the vase filled with about five kilograms of salt.

It was a considerably meagre treasure, but perhaps that was to be expected. They were merely seven men that were normally farmers.

They would have been unable to aim for targets with a larger payout  the kind that would hire adventurers as escorts.

But for Vandalieu, it was a decent harvest in its own way.

This is a great result. I was getting sick of eating only meat and blood, and the salt that Mom had ran out ages ago. And Im glad I got my hands on some cloth.

Vandalieus lifestyle up until now had been sustained through hunting the deer in the forest. Darcias emergency stores of wheat, cheese, vegetables and salt had been used up relatively quickly after her death. In the first place, it was only one persons worth and it was for the journey that she had been planning, so it was only an amount that she would have been able to carry.

And as Vandalieu developed, the amount that he ate also increased.

It would be problematic to use blood for all of his dietary needs, and if he became too much like a vampire, he would become weak to the sun so that would be problematic. That was why he wanted to eat normal food as well. But meat that was simply cooked after having its blood extracted was dull as well and there were times when he could only get his hands on the meat of raccoons and foxes and such, whose meat was not tasty. Though it was much better than the times he only had Goblin meat available.

The clothes he was wearing were not really clothes, but furs that he had wrapped around himself.

He was like a barbarian child.

If I use the bandits spare clothes, I might be able to make proper clothes for myself. Theres also money, but well, will I get a chance to use it?

The Amid was the currency of the Amid Empire and its countries. One Amid was worth about a hundred yen on Earth. There was a halfAmid copper coin, a oneAmid copper coin, a tenamid copper coin, a 100Amid silver coin, a 1,000Amid gold coin and a 10,000Amid platinum coin.
TLN 1 Amid  0.87USD

In addition, there was apparently also paper money used only by wealthy merchants and nobles, notes worth 100,000 Amid and 1,000,000 Amid. To be more accurate, they may not be actual notes, but something like handwritten government bonds.

The bandits had saved up about 1,000 Amid, about twice the monthly income of a worker living in an urban area.

But if Vandalieu were to enter a town, guards or adventurers would kill him without any questions asked, so it was doubtful whether he would get a chance to use it.

However, it might be possible to exchange it after moving to another country, so he decided to keep it.

Hmm? Whats this pouch?

He realized that there was another leather pouch beneath the pouches containing the money. It was light and something clinked around inside softly when he moved it.

He untied the string and looked inside to see two transparent, colored stones inside. They looked like some kind of gem, but they were not very pretty.

Mom, do you know what this is?

These are Magic Stones. You can collect them from monsters, and from this size and color, I think theyre from Goblins. If I remember correctly, they sell for ten Amid each.

The Mana in monsters bodies crystallized in the moment they died, and the Magic Stones were apparently the result of that. They were used as ingredients in various magical items and spiritual medicines such as potions, or used as sources of power for even normal people with low Mana to use magical items.

If used as sources of power in this way, they became normal stones after the Mana they contained was depleted, so they were consumable items. But with alchemical refining, Mana could be imbued into the stones again, so they could be used over and over.

Nobody would bother refining the Magic Stones of Goblins and they would be used as disposable items, which was why their value was low.

Incidentally, its more common for lowrank monsters to not even drop Magic Stones when they die. The probability for a Rank 1 monster to drop one is about one in a hundred. On the other hand, higherrank monsters are almost certain to drop Magic Stones when they die. Rank 5 monsters and above will definitely drop them.

Incidentally, Darcia had apparently thought it pointless to tell Vandalieu about Magic Stones earlier because he had been fighting nothing but a few Rank 1 Goblins at a time.

Also, it seemed that there were some lowrank monsters that easily dropped Magic Stones, and also monsters that dropped Magic Stones of far higher quality than their original rank. But to Vandalieu, who was focusing on hunting bandits for now, this was all just information for future reference.

He wanted to register with an Adventurers Guild and become an adventurer as soon as possible.

Finally, lets check the status of Bone Man Oh, just by killing two of those weak Bandits, youve gained thirty levels. You too, Bone Bear. The rest of you too. Just by killing a single man each, youve gained more than 10 levels!

Just by ambushing and killing one or two bandits that had failed as farmers and couldnt even fight properly, their levels had risen this much. The amount of experience they got from Goblins or the monsters in the forest couldnt even be compared to this amount.
If this applied to not just the Undead but all monsters, than that meant that humans were a very good source of experience of the monsters. It would also explain why monsters were so aggressive against humans.

Alright, Ill wipe out all the Bandits in this area and increase everyones levels.

After seeing this explosive increase in level, he couldnt go back to hunting small monsters.

There were still many Bandit groups left in the area around Baronet Besteros territory. One of them had the same number of people as the one they had killed today, one of them had more than ten people, while another one had around twenty.

The Undead would surely reach Rank 3 after they were all exterminated.

Since you were able to kill us, the guys to the south will be a piece of cake.

The guys west of here are from three villages over. They have a lot of people, but it should still be easy.

But the guys up north are professional bandits. Their leader was apparently a guard in some city, and Ive heard that hes gathered himself a bunch of strong underlings. They extorted some money out of us as well.

While learning some information from the spirits of the dead bandits, Vandalieu used the Earth Golem to bury their bodies. After loading up their loot on the wagon, Vandalieu left, leaving the tragic scene behind him.

Around a month had passed since Vandalieus first Birthday. It was the season in which he had to get rid of mosquitoes every evening usingBug Killer, and he was finally ready to take on the largest and most highlytrained bandit group in Baronet Besteros territory. They moved while making sure no travelers or guard patrols on the highway found them and trained in forests or in areas with high, thick grass to polish their skills.

Our opponents are in a different league from the bandits weve been killing up till now. Their leader is a exsoldier, and his subordinates have been trained under him to some extent. In other words, we cant just raise our weapons and scare the enemy, we actually have tofightthem.

Oooooooooh

Gurururuuuuu

The Undead listening to Vandalieu literally had fires of determination burning in their eye sockets. Because of the experience they had earned from killing all the bandits, Bone Man had increased his rank to become a Skeleton Solider, while Bone Monkey, Bone Bear and Bone Wolf had become Bone Beasts.

They had also gained the skillSuperhuman Strengthand, perhaps because they had been killing bandits by ambushing them, they had also gainedSilent Steps.When this skill was used, the sounds of their bones hitting and grinding against each other completely disappeared. In addition, Bone Man had gained the skills Swordsmanship, Archery and Shield Technique.

They were all still level 1 skills, but the results were pretty good considering that they had obtained them after fighting in practice battles against golems for a month.



Bone Bird, however, was still at Rank 2. But it was at Level 90, and if todays plan went well, it would also rank up.

Thats why we need to be extra careful today. We have a lot of spare bones for you, but make sure your skulls are not damaged or broken. Also, if there are any humans that dont look like bandits For example, humans that are tied up with rope or humans that are in cages, dont kill them. Lastly, well begin the plan as soon as I cast the support magic.

Speaking in his usual indifferent tone and expressionless face, Vandalieu didnt show his internal nervousness at all as he cast the spells.

First,Bloodshed Enhancementto their weapons, fangs and claws. ThenEnergy Absorptionto their armor and bones.

Bloodshed Enhancement was a magic spell that boosted the attack power of whatever it was cast on even shields and armor could damage the enemy if they came in contact with them.

Energy Absorption was a defensive spell that could absorb magic, heat, electricity and even motion energy, and in Lambda it was effective against both magical and physical attacks.

Both of these spells had very strong effects, but since both were not that hard to use with the Death Attribute, mastering them was easy for Vandalieu.

Ooooh

The first to move among the Undead surrounded by dark blue magic was Bone Man. He headed towards observation tower the bandits base  a small gathering of simple huts surrounded by a wooden fence  loading an arrow into the bow taken from Orbie and his companions and pulling back the string.

The bandits in the observation tower were holding bows with quivers on their backs and seemed completely unmotivated.

Man, were really unlucky. Getting guard duty on the day before were supposed to withdraw.

The bandits were planning to take all that they had earned up till now and move to a new location. They were planning to pick up the ransom money for the hostages along the way and then go on to a new territory or even a new country, and start their jobs as bandits all over again.

Other bandit groups had been disappearing lately and they had heard rumours that a Lord had become fed up with the deterioration of public order on the highway and put together a subjugation team. If that was the case, then it really was time to leave.
They had decided to get rid of things that would get in the way of traveling, and were in the middle of a party to consume all of the excess food and liquor they had in one go.

These bandits were the unlucky ones that were unable to participate in the party, and they were too distracted by their own misfortune to notice anything.

Gahhh!?

Bone Mans arrow cut through the night and buried itself in one of their throats. Letting out a short scream, he lost his balance and fell from the tower.

Seeing one of their own guards fall down with a arrow in his neck, the bandits that had been drunk and enjoying themselves woke up from their stupor.

Eenemy attack!

Wake up, you bastards! Get your weapons!

As they bandits tried pick up their axes, maces and spears

Guwoooon!

Uooooooooh, uooooooh!

Destroying the wooden fence and scattering splinters all over the place, Bone Monkey and Bone Bear charged in.

Iits the Undead! Monsters! Were being attacked by monsters!

Calm down, you idiots! Those of you specializing in axes and maces, move forward! Those who have swords and spears, fall back! Archers as well!

The leader of the bandits calmly gave his subordinates clear instructions, a halberd in his own hands.

This man had experience fighting with Skeletons and Zombies from when he was still a soldier. He knew that his subordinates could deal with Undead effectively if they used blunt weapons like clubs or axes rather than bladed weapons like swords or spears, even if they werent very skilled at fighting.

A few Rank 1 or 2 monsters are no match for twenty of us! Get em!

The bandits morale that had been withering because of the sudden attack and death of one of their own was recovered thanks to the instructions from their boss. They rushed at the boorish Undead who had rained over their festivities.

Guooooo!

Ghyaa!?

A bandit that had raised his axe went flying as Bone Bear stood up on its hind legs and delivered an attack with its front paws.

With a crunch, Bone Monkey crushed a bandits skull like an egg.

The fangs of Bone Wolf pierced the legs of the bandits, and when they fell down it ripped out their throats.

Oooh Oaoooooh

Bone Man put away his bow and drew the long sword that he had taken from the other bandits they had defeated to cut his way into the bandits ranks.

Hiiii!? Gyaaah!

Bone Man against the bandits. Both sides had never received any formal training in swordsmanship, and soon Bone Man came out as the clear winner.

Their fighting skill was about the same, and the longsword that Bone Man was using was an iron sword with a crude casting. In fact, the enemys weapons were of better quality.
However, the strength of a Rank 3 Skeleton Solider was far above that of an average human, and especially because of the Superhuman Strength skill, Bone Man was far stronger than the bandits.

As humans lacked physical strength and special abilities, they honed their skills and learned martial arts and magic in order to fight against monsters. A human going up against a monster while possessing the same amount of skill as the monster had no chance of winning.

Ooooh

Bathed in the blood of his enemies, Bone Man shivered from the feeling of getting experience by taking a life, and wanting to earn even more, jumped at his next prey.

Boss! These arent Rank 2 monsters!

We cant handle them! Help us, boss!

Hearing the pitiful screams of his subordinates as their number decreased, the leader clicked his tongue.

Those useless fools! If its come to this, I have to get away by myself.

The bandit leader chose to run without hesitation. He didnt think about fighting the Undead for his men for even a second.

He didnt even have enough skills to even fight oneonone with a Rank 3 or higher monster in the first place.

He had certainly been a soldier at point in his life, and even had a level 2 skillHalberd Techniquethat allowed to him to use the Halberd. But in the end, he was just an exsecurity guard. According to the Adventurers Guild, his fighting prowess was at most a Eclass.
It was said that to win a fight oneonone with a Rank 3 monster, you needed to be at least Dclass.

If he made well use of his subordinates, he could defeat maybe one of the Undead. But there were four Undead. No, five.



The leader noticed Bone Bird, who was finishing off the bandits that had fallen unconsciousness by stabbing them with its beak, but considered that to be an exception.

Even if he did manage to defeat one of them, there would be no point as they would be finished off by the rest. There was no point in retaliating if there was no hope of winning.

All of you! Dont back down, push forward!

Giving that unreasonable order to his men, the leader himself began backing off carefully so as not to be noticed by them. He would just jump in the carriage he had taken from the merchants and run away. If he could get away, he form another bandit group.

Get up.

His hopes of getting away were shattered as a wooden wall grew from the ground behind him.

Uoh?! Wwhat?! An alchemist?!

He had heard a shrill voice like a small girls through the dying screams of his men and the resentment filled roars of the Undead. He realized that this was the doing of the owner of the voice, and looked around for its owner.

He found him almost immediately.

A small child stood some ways away, wearing a rag. It was easy to tell that he was a baby.

Are you telling me this is all this kids doing!?

The leaders eyes opened wide, but there was no denying the childs  Vandalieus  strangeness.

The white hair and oddcolored deep crimson and purpleblue eyes. And even on this bloodstained battlefield, the weak, ghostlike presence. If the child hadnt said anything, he could have approached the bandit leader all the way up to his feet without the leader noticing.

Aare you the one controlling these Undead? Iin that case, I give up. I surrender. Ill let you have all the treasure, go ahead and hand me over to the Adventurers Guild or wherever.

Throwing down his Halberd, the leader put up both hands and gave up.

If he couldnt win, he would run. If he couldnt run, hed live by giving up. Stubbornness and pride wouldnt get you even one Amid.

Surrender?

Yeah, thats right.

Forcing a smile to his face, the leader replied to Vandalieu who spoke in a flat tone.

Theres a bounty on my head. And I also have information about the other bandit groups in this area. And half the money made from selling criminal slaves goes to your pocket. Dont you see? If you capture us alive, its a lot more profitable for you.

The things that the leader said were all true. This was a man that was willing to sell off other people in the same trade if he was able to live.

Also, depending on the severity of their crimes, the bandits that were captured alive were sold off as criminal slaves. Most of them were overworked in mines and the army, doing odd jobs and labor. Half of the profits earned from them were not to be underestimated, and depending on the number of bandits captured alive, it could be worth even more than all the treasure that the bandits had gathered.

 Are you an idiot, perhaps?

But what came was an insulting reply a baby asking about his mental capacity.

Whwhat did you say!?

As you can see, I am a Dhampir. If I took you to the Adventurers Guild to hand you over, I would just be killed. I would be dead before you could be sold as a criminal slave.

In the Amid Empire and its countries, where the people worshipped Alda, the God of Law and Fate, Dhampirs were treated as monsters rather than people. Even if he entered the city with the intention of handing over the bandits, the guards or adventurers would prioritize dealing with the Dhampir over the bandits.

The bandit leader was not an adventurer and he was unaware of the Dhampir panic that had happened a while ago, so he was late in realizing that he was talking to a dhampir.

Ththen make me one of your subordinates! Ill be really useful. Your Undead are pretty strong, but you do need one human subordinate as well, right?!

In that moment, Vanadalieus impression of the bandit increased slightly as he thought that he was more clever than he looked.

In fact, Vandalieu had been been feeling the same thing from quite a while back. The Undead that followed his every command. The spirits of Darcia and the others. Their presence alone could not solve the small inconveniences of his daytoday life.

However, using the man in front of him to solve them was unthinkable.

While it is true that I need a living companion, I dont need a bandit who would sacrifice his men to run away. But I can make you one of my subordinates after youve died.

Pointing at the leader that had first expressed hope and then despair, Vandalieu asked Bone Bear and the others.

Do it.

Pplease wait! II dont want to die!

? Neither did the people that you killed, right?

This man was oblivious to many things. By the time this thought occurred to Vandalieu and he turned around to talk to the leader again, Bone Bear had already crushed his neck.

Gu, guru?

Crap, did I kill him too early? Bone Bear seemed to ask, but Vandalieu waved a hand to sayIts fine, its fineand exhaled heavily.

Ahh, that was nervewracking. It was my first time talking to anyone besides Mom, so I was pretty nervous.

He had suffered from stranger anxiety in all three worlds Earth, Origin and Lambda. He wished that he could have avoided having this kind of facetoface conversation in this bloodstained place.

The smell of blood is so strong that Im getting hungry. But I dont want to get closer to being a vampire by sucking the blood of the bandits, so I have resist. Resist For now, Ill check if there are any survivors.Sense Life.

To distract himself from his hunger caused by the smell of blood, he used a deathattribute spell that detected all the life in the vicinity.

Ignoring the reaction from bugs, weeds, germs and fungi, he searched for reactions from animals, humans containing magic or large monsters.

There were three large animals behind the large hut, probably horses. There was also a reaction from the basement of the hut. From the reaction, it was human.

He had heard from the spirits that the number of bandits had been nineteen. The number of corpses was also nineteen.

Theres someone in the basement of the hut, is it a new guy?

NNo, that one is not my subordinate. Its a peddler I caught a few days ago. Ugeh. I let him live because it seems like he has a family business or something in a town over in Viscount Maggios domain, and ransom money can be eexpected. Mmy nneck.

Because of the shock and fear he had received at the time of his death, the neck of bandit leaders spirit was still twisted. Vandalieu wrinkled his eyebrows as he processed the information he had given.

Things had become a little troublesome.

        Name Bone Man

 

        Rank 3

 

        Race Skeleton Soldier

 

        Level 39

 

        Passive skills
                Dark Vision
                Superhuman Strength Level 1 (NEW!)

 

        Active Skills
                Swordsmanship Level 1 (NEW!)
                Shield Technique Level 1 (NEW!)
                Archery Level 1 (NEW!)
                Silent Steps Level 1 (NEW!)

 

        Name Bone MonkeyBone WolfBone BearBone Bird

 

        Rank 3

 

        Race Bone Beast

 

        Level 2432

 

        Passive skills
                Dark Vision
                Superhuman Strength Level 1 (NEW!)

 

        Active Skills
                Silent Steps Level 1 (NEW!)

',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(5,'chapter 7','The merchant Rudy considered himself to be blessed.

As the thirdborn son of Viscount Maggios noble merchant family, he couldnt inherit the household, but he had been raised without suffering discrimination at the hands of his parents and two older brothers. He had received the same highlevel education as the first and second sons and had been prepared to become independent from an early age, as he couldnt inherit the household.

And so once he became an adult, Rudy began to gather experience as a merchant. He had received a oneoff support fund from his family to become independent and had experienced various hardships to get to where he was today.

This was now his third year as a merchant. His business had succeeded to a point where he was deciding whether he should use the money he had saved to buy a carriage and invite other merchants to form a caravan, or settle down somewhere and open his own store.

However, Rudy had been captured several days ago and thought his luck had run out.

He had hired Eclass adventurers as escorts, but they had been killed by the sheer numbers of a bandit group with twenty members. He regretted not hiring more escorts, but it was too late now.

He had his goods and money taken, and when his life was next, the words he had shouted wereMy family will pay a ransom for me!It was the worst way to beg for his life.

But dying proudly was not a merchants job. He would cause trouble for his family, but he had to do whatever he could to survive and rebuild his business. He could repay the ransom his family had paid for him by earning more money through that business.

It would be best if a group of adventurers or knights appeared gallantly to exterminate the bandits, rescue him and return his stolen goods and money. But the world was apparently not such a nice place.

As he thought that, a sound reached the ears of Rudy, who had been living a simple lifestyle in a dark, damp dungeon.

Has someone come underground?

Rudy, becoming aware of the soft sounds of someone coming down the ladder, lifted his body from the crude, slightly smelly blanket to peer through the wooden bars, but it was pitchblack and he couldnt see anything.

If it were one of the bandits coming down, there should be a candle and signs of living magic. He had thought it was just his imagination, but he could hear the footsteps of someone steadily approaching.

Those footsteps stopped in front of his prison. However, in the darkness, he still couldnt see anything.

Wwho is it?

In response to his highpitched, fearful question, a voice came from towards the ground.

Ah, I apologize. I had not realized that it is dark down here.Demon Fire.

Whoosh. A blue, fistsized flame lit the dungeon.

?! A gghost?!

Standing in front of Rudys eyes was a small infant with disheveled white hair and candlewaxlike skin, wearing raglike clothes.

Rudy had assumed that this lifelesslooking figure must be an Undead child that had been killed by the bandits and come back to haunt this place. He clutched his blanket tightly and began to tremble.

 No, I am alive.
TLN Vandalieu is speaking rather politely to Rudy, compared to the tone of his selfmonologue.

 

When Rudy screamed, Vandalieu had wonderedCan this person see spirits too?But when Vandalieu realized that it was he himself that had scared Rudy, he lowered his shoulders. He thought that Rudy shouldnt be so afraid of him when he had come down here to help, and sullenly put his own appearance out of his mind. He gathered his thoughts again and continued.

I have taken care of all the bandits that captured you. And as you can see, I am not an adventurer, but a Dhampir.

As Vandalieu spoke with a disinterested tone, Rudy began to gradually calm down. But he realized that his position hadnt changed for the better and his face turned pale.

A Dhampir so does that mean youre going to kill me?

Dhampirs. In the country that Rudy was born and raised in, they were treated as monsters and targets of extermination. The priests of Alda in particular did not hesitate to label them as special kinds of Undead.

Therefore, it was not just a matter of Vandalieu not feeling any obligation to rescue him like an adventurer or knight would. It was highly likely that he would be silenced now that he had seen the Dhampirs face.

However, the Dhampir child before him shook his head.

No. If you are willing to stay quiet about me, I have no intention of killing you.

Rreally?

Yes. You were captured once by bandits, but you succeeded in escaping during the conflict with another bandit group. Please spread that story. Please do not say a word about me to anyone.

Rudy was only halfconvinced. Though it was understandable, as from his point of view it was as if he had met a virtuous devil.

Vandalieu himself actually knew that it would be better to silence the merchant. But he had a reason for not killing Rudy if he could help it.

I want to make my normal activity as good as possible. For the future.

That was his simple reason.

It was fine to kill bandits for his own protection, as nobody would consider it a crime even if he were to kill every last one in all of Lambda. However, killing a pitiful victim like Rudy for his own protection would be the act of a fiend.

Vandalieu thought that this kind of behavior, even if nobody found out about it, would have a terrible influence on his own personality.

His uncles family who had mistreated him on Earth, the terrorists who had used a bomb to blow up both themselves and the ferry, the researchers in Origin who had performed human experiments on countless humans including himself  all of these people were simply evil lunatics in Vandalieus eyes.

He couldnt become an evil lunatic like them.

Because he hated them, he felt that he didnt want to be like them. But even more importantly, Vandalieu wished to live the happy life that he had never had a taste of on Earth or in Origin. This was to make that wish come true.

A prosperous, comfortable lifestyle and warm relations with the family and friends around him. Could an evil lunatic build and maintain such a lifestyle?

And though it was still about half a century away, one day Amemiya Hiroto and the others with cheatlike abilities would be reincarnated here. If he turned to evil, it was likely that they would purge him in the name of justice.

Because of their cheatlike abilities, Vandalieu had the feeling that even if he left no witnesses, they would somehow dig up and expose the crimes of his past. There would probably be some of them who could read a persons mind or see into the past or otherwise expose his crimes directly, and they would be very skilled with magic.

Therefore, Vandalieu wanted to avoid having to kill victims like Rudy.

If that is all, then I understand. Since I am not an adventurer or anything.

And fortunately, Rudy wasnt a fanatical worshipper of Alda, so he accepted Vandalieus conditions and chose to live.

The other wise decision that Rudy made was not to look down on Vandalieu because of his age. He was fearful and careful of this child whose height did not even reach his own waist.

With that said, it was the natural response for Rudy.

Vandalieus mature tone that didnt match his appearance and the ominous aura that he was giving off. It was clear that he was not just an infant.

With a clank, the padlock was removed with the key that the bandit leader had been holding. Now that Rudy was free, he gave a sigh of relief, but 

Ah, if you tell anyone about me, I will send evil spirits after you. So please do not change your mind.

With a pale face and chattering teeth, Rudy nodded in response to the threat that Vandalieu made just in case.

Rudy climbed the rope ladder to the surface and shuddered violently as he saw the bandits corpses on the ground, the bloodcovered Undead and Bone Bird who was now covered in pale, bluishwhite light after increasing in rank, letting out ominous cries of joy.

And then he swore that he would definitely keep this a secret, even if he was offered a mountain of platinum coins.

 

 

 

Rudy drank the alcohol that the bandits had been drinking to mask his fear as he retrieved his belongings that the bandits had stolen. In addition, he had been toldI cant use living horsesso he had been given the bandits horses. And then he tied the wagon taken from other bandits to the horses and left.

He was traveling alone with no escorts, but Vandalieu had exterminated all of the bandits that had made this area their territory, so as long as he wasnt unfortunate enough to encounter a group of Goblins, he would be able to reach his destination.

He had unexpectedly received horses, so now he would be buying a carriage, forming a caravan of merchants and expand his business, but this was irrelevant to Vandalieu.

Vandalieu spent the remaining time until daybreak to replace the chipped bones of the Undead with spare parts the animal and human bones that he had been collecting up until now. He also played with Bone Bird, who had increased in rank and gained the ability to fly with its new spirit wings and buried the dead bandits. And then he slept soundly until noon.

He left Bone Man and the others to see Rudy off.

No matter how much of a stranger Rudy was to Vandalieu, he thought he should at least do that.

And then Vandalieu woke up and ate the bandits leftovers for lunch.

On his menu was salted meat, bread and cheese. There were dried vegetables in place of a salad, and a soup with salted fish. For dessert, he ate fruit that he had obtained from the forest.

Even bandits eat better than I do

As he ate, Vandalieu realized this and became a little depressed.

After having attacking bandits and taking their food, his dietary situation had improved significantly he no longer had to eat the hard, smelly meat of raccoons and foxes. However, when he had tried to use the Cursed Tools and Undead cookware to cook the food, it hadnt gone well.

The Cursed Tools had been made by evil spirits possessing them and were monsters that moved on their own, but they had no strength or dexterity. Because of that, they werent suited for cooking.

In that case, it was possible to have Bone Man do it, but the spirits inhabiting him were not that of humans, but of small animals such as insects and mice, so he didnt understand the concept of cooking. When ordered tocut the vegetables, he had swung his sword at the vegetables with full force, cutting straight through the chopping board.

The fact that he had learned to use a sword and bow was more than enough development for him.

As for Vandalieu himself 

No! To be cooking when youre only one year old! What will you do if you burn yourself?!

But Mom, I think directly eating dried meat and burnt bread is bad for a oneyearold childs digestive system. Though I have several times the jaw strength of a regular oneyearold.

No means no! What will you do if you burn yourself?!

 Ill use water andHeat Leechto cool it down.

Since the Death Attribute was the opposite of the Life Attribute, he was bad with things like healing. He did have an ability that could heal, but it was a strange one that only worked forhealing fatal wounds and serious illnesses, but not injuries that do not endanger ones life.

If he burned his whole body, he could heal it, but if he was burned just enough to form a scar on his hand, it would be hard for Vandalieu to heal it with his current abilities.

Then you cant!

Alright

This was probably because Darcia had thoughtI have to be careful not to let him get burned while hes still a small childwhile she was alive. This notion had possibly gone a little wild after her death.

But Vandalieu had only just turned one year old and his arms and legs were short. There was definitely the risk of an unexpected injury if he tried to use a pot, so Darcia wasnt wrong to warn him.

As a result, Vandalieu was now heating water using embers created by having pieces of firewood rubbing against each other, and having the Undead add broken bread and torn pieces of dried meat into it. This had become his daily meal.

The taste was well, better than raccoon and fox meatballs.

Its fine. Once I grow up, Ill earn a lot and hire a skilled chef to make me delicious meals every day. Err, more importantly, todays harvest is

First of all, there were the bandits weapons. All of the other bandit groups that he had defeated earlier had been using handmade spears made by attaching knives to the ends of wooden sticks, and their clubs, bows and arrows were all of low quality. But this group wasnt called the biggest bandit group just for show. Every one of them had been using good weapons.

They were made with normal iron, but they werent the inferior quality made by just pouring molten metal into a cast, no. These had been properly forged by a craftsman. And although some of of them had become chipped over time or broken by Bone Man and others in the fight, some of the bandits had been killed swiftly without the time to retaliate,  so their weapons were still spotless.

The same applied to the leather armor the bandits had been wearing. Compared to the leather armor of other bandit groups, which had been repaired over and over, this was still in very good condition.

They had surely attacked the wagon of a weapons merchant previously.

There was also the treasure in the carriages. He had returned all of Rudys belongings, but there was still a considerable amount left in the threehorse carriage.

There was about 50,000 Amid in money and a small amount of accessories, though it was hard to discern their exact value. There was also a small pile of a highquality cloth with a pretty color and two barrels of seemingly highquality wine. In addition, there was the sugar, which was a valuable ingredient as it was not produced in the country of Mirg.

Finally, there were the bandits spare weapons and food.

Including the carriage, all of this was likely worth more than 200,000 Amid. It would be about 20,000,000 Japanese yen if converted, and although it was a lot of money, it was not quite enough to be called a fortune. However, looting that much money in this area where the only town around was Evbejia was quite a feat.
TLN Roughly 175,000 USD

 

Though in this case, instead of praising the bandits for their skills, it might be more appropriate to blame the ineptitude of the guards.

With that said, everything other than the food is useless for me.

Vandalieu would still have no opportunity to spend the money, and trying his best to make clothes out of the cloth had ended in the rags he was wearing right now. As he was unable to fully utilize the treasure the bandits had gathered, his situation hadnt changed.

But Bone Man could use the weapons and armor and the leather armor could be taken apart to make defensive gear for Bone Monkey and Bone Bear.

However, with this amount of treasure, the problem was how to transport it. He had given all of the horses to Rudy and only the carriage itself remained. Even if he wanted to have his Undead to pull it, the carriage would have to be altered.

Vandalieu had an idea.

Now then, who should I get to do it?

As Vandalieu murmured and looked around at the spirits around him, there were not only the spirits of the bandits who had just died. To his surprise, the spirit of a thin middleaged man whose appearance from when he was alive was preserved stepped forward.

Please allow me, Sam! In my lifetime I held the posts of caretaker of horses and coachman for a nobleman. My skills in handling a carriage are second to none!

It was fortunate for Vandalieu that the spirit of Sam, who had apparently handled carriages during his lifetime, had not moved on yet. The fact that his spirit had maintained his appearance from while he was alive without needing Mana supplied to him meant that he had considerable emotional strength as well.

In a way, Sams spirit was the greatest find out of all the treasure here.

Vandalieusama, you have avenged myself and my daughters! My daughters, who were treated as playthings and then killed! In our gratitude, my daughters and I will serve you for the rest of your life!

The two spirits standing behind Sam with their heads bowed were likely those of Sams daughters. Their burnt, black bodies were barely recognizable as those of women, so Vandalieu couldnt be certain.

The bandits had probably gone too far using them as playthings and they had become unsellable, or perhaps the bandits had no connections with any slave traders. They were likely killed for one of these two reasons.

From the fact that the spirits of the bandits were trembling as Vandalieu looked at them, this was probably correct.

Well then, Ill leave it to you, Sam.

Firmly deciding that he would use the bandits spirits until they were no more, he sent Sams spirit to the carriage.

Get up.

As Vandalieu gave this command and poured Mana into the carriage, it began to creak.

 Go forth.

And with those words, the carriages wheels began to turn, even without the horse that was normally essential in its function. Seeing this, Vandalieu gave a satisfied nod.

Making a Cursed Carriage with Sam, success. With this, Ive gained a method of transport as well. Well then, Ill have it run Goblins over to level up on the way back to the forest.

Two days later, the highway patrol stepped into the bandits base after receiving the information from Rudy, but all they saw were the piles of dirt where the bandits had been buried and the beasts and Goblins that had dug them back up greedily.

Tracks made by the wheels of a carriage continued off outside, but they assumed that these were those of the bandits that had been victorious in this conflict and didnt bother investigating any further.

The commanding officer of the patrol tilted his head when he realized that there were no hoofprints accompanying the carriages tracks, but it wasnt something worth mentioning in his report.

 

 

 

Baronet Bestero had been in a good mood for the past year.

After that witch had been caught and executed, only good things had been happening. Yes, the High Priest and his men had occupied the forest for the next two months and three hunters had gone missing, but those were trivial things.

Though the Dhampirs corpse had not been discovered, its death had been accepted and Bestero had received a medal from the king of Mirg. WIth that, the scorn from the feudal lords of the surrounding areas that he was merely the feudal lord of a countryside with no redeeming features but its winehad been drowned out.

Even this years wine and the grapes that were used as ingredients for it had turned out well, and the bandits that had caused headaches and threatened the public order had been dealt with near the beginning of summer.

He was a little dissatisfied that it wasnt resolved by the highway patrol, protege knights or even adventurers. But the bandit problem had been a serious one, so he was happy that it was dealt with, even though it had been done through disputes amongst the bandits themselves.

The best thing was that he had been promoted.

Though it had not been formally decided, he had received private word that he would soon be summoned to the the Amid Empires imperial capital.

The position of Baron that his greatgrandfather, the firstgeneration Baronet Bestero, had longed for. That would be his soon.

Knowing this, even if the liquid in his glass was not wine but vinegar, he would surely drink it happily. That was how joyful he was now.

 

 

 

On a humid summer night, there was a shadow prowling around outside Evbejias outer walls.

Go in, go in, go in.

Vandalieu was followed by countless spirits, and he began to have them possess the outer wall one by one.

Even if someone were to see Vandalieu now, they would have no idea what he was doing. Unless one had an aptitude for deathattribute magic or was a Necromancer, they wouldnt be able to see spirits that had not turned into monsters and Undead.

The guards might have shot arrows at him if they saw him, but their job was to prevent monsters and criminals trying to enter the town over the outer walls. It was not their job to deal with monsters and criminals that appeared outside the walls at night. Their attention was focused on the gates and the areas surrounding them.

The chances of them spotting an infant moving around some distance away from the outer walls without even holding any source of light were incredibly low.

It would be possible for adventurers or experienced knights with a Mana Detection skill to notice him. However, though Vandalieu himself was not aware of this, his Mana was difficult for others to sense due to his aptitude for the deathattribute magic.

Therefore, a person with a level 1 or 2 Mana Detection skill wouldnt be able to sense his magic, even if he were to cast a magic right in front of their eyes.

Even with a level 3 or higher skill, it would be impossible without great concentration, but because there were no Devils Nests with strong monsters around Evbejia, its Adventurers Guilds members were Dclass at best, so there were none with a level 3 Mana Detection skill.

After making a halfcircle around Evbejias outer wall, Vandalieu exhaled heavily.

With this, Im done Its taken me two days, but the preparations are finished. Now I can achieve my revenge with one word. But lets leave it until tomorrow morning.

 

 

 

The next day, the morning sun shone upon the people of Evbejia just as it had yesterday and they were completely oblivious.

Oblivious to the fact that today was the day that Evbejias name would be known not only in all of Mirg, but across the entire Amid Empire.

Known as thetown of the mysterious incident.

Collapse.

A sound came from the fivemeterhigh walls that had protected the town from monsters such as Goblins, dangerous wild animals and bandits.

As the guards showed puzzled expressions, the outer wall crumbled with a loud noise.

UOOOOON!

It seemed that the outer wall had collapsed, but one by one, large, humanshaped creatures began to form and let out evilsounding roars of malice that echoed out into the blue sky.

And then, with thundering footsteps, they began to walk away.

Wwhat?! What just happened?!

Commanding officer! The outer wall has turned into a Golem!

I can see that!

Even as the commanding officer of the guards was shouting, the gate that the guards had been watching became part of another Golem and began to walk off.

Dont just stand there, stop those Golems!

But commanding officer, theyre rampaging around, just walking away. It would be a different story if they were walking towards the town, but at this rate, even if we dont do anything

The bewildered commanding officers subordinates did not seem eager to obey his command. But that was to be expected who would want to stand before Golems made from fivemeterhigh stone walls?

And how would the guards even fight the Stone Golems with their iron spears? Looking at it from the guards point of view, it wasnt surprising that they were questioning the sanity of their commanding officer.

You fool! Thats the towns outer wall! Without that, how do you plan to protect the town from now on?!

However, the guards were taken aback as the commanding officer pointed out this big problem.

Without those outer walls, the wild boar, wolves and bears that came out at night would enter the city, eat the crops and livestock and attack people. Goblins and bandits would be able to enter the town as they pleased.

Though there were enough guards to watch over the few gates of the town, they definitely lacked the manpower to guard the entire circumference of the town.

They had now become aware of this problem, but there was nothing that they could do about it.

Bbut

The guards showed their uneasiness, but no matter how grave the situation was, they couldnt simply increase their manpower.

They couldnt do anything but watch the backs of the Stone Golems that had once been their towns outer wall as they left.

 

 

 

The outer walls suddenly turned into Golems and walked off. Everyone in Evbejia noticed this, but they couldnt immediately do anything about it.

Baronet Bestero froze in terror, the knights and soldiers were in a panic, the Adventurers Guilds personnel were running around in a hurry and shouting to send out an emergency request and the townspeople were simply dumbfounded.

However, that wasnt the end of it.

The dirt, the dirt has turned into a Golem!

Thats my vineyard! Thats the dirt from my grape vineyard!

Wait, my wheat field! Wait!

The earth of the fields, along with the crops and trees that were growing in them, turned into Golems and followed in the footsteps of the Stone Golems that had left the town just a little earlier.

The farmers saw this and desperately chased after them.

Even though the Golems were just made of dirt, the farmers were quite brave to try to stop the Golems that were larger than themselves. But because they were farmers, they couldnt simply allow the Golems to leave.

Not to try to recover the crops and trees sprouting from the Golems backs, but because the dirt itself that made up the Golems bodies was most important to them.

The earth was important in agriculture. They had spread fertilizer and ploughed the fields to ripen their produce for many years it was essential to them.

If they lost it, they would have to rebuild that earth from scratch. On Earth and Origin, one could get their hands on fertilizer immediately, but in Lambda it would take time.

For the farmers who owned vineyards, it was even more serious, as they would have to raise their trees from scratch as well.

And not only the fields, but 

Uwah! The feudallordsamas house! The house is collapsing from the second floor and becoming a Golem?!

The Adventurers Guild as well! My friends are in there!

The feudal lords house and the Adventurers Guild building became Golems and began walking outside the town as well.

The Golems movements were slow, but their their bodies were large so their strides were long. The townspeople, knights and adventurers who wanted to stop the Golems were now on a chase to recover the rubble of their buildings.

 

 

 

Vandalieu watched all of this, feeling satisfied.

The outer walls, the fields, the Adventurers Guild and the feudal lords house. Having all of the various spirits possess these and wait for his signal had been difficult. The outer walls alone had taken two days and the fields and the Adventurers Guild had taken a day each.

But once it came to starting it, he only needed to say one word. Like dominoes that had been painstakingly arranged finally falling down, it was a very satisfying revenge.

Look, Mom. The guys who laughed at you are all crying with pathetic looks on their faces now.

With its outer walls and structures turning into Golems, Evbejia was now in a situation from which it couldnt recover. Even if they defeated the Golems and retrieved the rubble that had once been the buildings, they would still need to do repairs.

Much of the rubble would be unusable the only thing that could somewhat be gathered up and returned to normal would be the earth of the fields.

Evbejia would have to rebuild those tall, thick walls. Unless this was done, the town would not function, not in the world of Lambda in which dangerous monsters roamed.

Of course, the guards would have to be prepared while the repairs were ongoing. Hiring more guards, putting out more requests for adventurers, ordering materials from stonemasons and arranging for the workers would take a great amount of time and, most importantly, money.

So much that the reconstruction of Baronet Besteros house would have to wait, no matter how much he tried to use for bribery.

Of course, he would beg for help from Mirgs government, and then his position of Baron would disappear.

In addition, Vandalieu had even extended his reach into the wine warehouse.

He had appliedDecompositionmagic to the barrels of wine in the warehouse and caused it all to spoil. And then he had usedSterilizationto kill every single particle of yeast that was preserving it.

Nobody had noticed it yet, but Evbejias entire industry had been obliterated. It would take literally decades to rebuild it.

Mhmm, thank you for what youve done for me, Vandalieu. It was hard, wasnt it? To do this kind of gentle revenge.

Darcias spirit looked lovingly at her son who possessed a vast amount of Mana. There would be many who would demand what was gentle about this revenge. But considering Vandalieus magic and Mana pool, it could be called gentle, just as Darcia had said.

If he had the Golems that he had made from the outer walls go into the town rather than outside, he could have caused large amounts of damage to the town and many would have died. Including the Golems made from the feudal lords house and the Adventurers Guild building, catastrophic harm could have been caused.

Because High Priest Gordan and the Fivecolored Blades, led by Heinz, had left this town long ago.

He could have usedDeadly Poisonto contaminate the well water or simply usedDiseasemagic to start an epidemic.

If he had wanted to, he could have killed every single person in Evbejia.

Darcia had thought it kind of him to not do this, and he had even waited until morning before causing the outer walls to crumble.

I just did to them what they did to me.

Vandalieu didnt confirm or deny Darcias words. But he hadnt intended to show compassion for the townspeople. In his mind, there was not a single guiltless person in Evbejia. Every single one of them were criminals who had watched Darcias execution and laughed as if it were some kind of show.

That was why he had done the same to them.

From now on Im going to spend years, decades to get you back, Mom. Im going to make a new body that will match your spirit. Ive made it so that these people have to work for decades to return to their original lifestyle, thats all.

There wouldnt be a single casualty directly caused by this event. He had even thought about the order in which the Golems began to move, with the walls first and then the buildings, so that they wouldnt collapse partway through.

He had only destroyed things that could be repaired.

Youre right, you gave them a chance. Vandalieu is a gentle child, after all.

If one thought of it one way, he had inflicted damage that would take decades to repair, but Darcia, who had been charmed by the DeathAttribute Charm skill, had no intention of pointing that out.

As Darcia patted his head with her hand that he could not feel, his eyes narrowed a little.

Well then, lets get going. Bone Bird, Im counting on you.

Geeeeh

Letting out a cry that sounded like a person being strangled, Bone Bird, who had become a Phantom Bird, spread its bone wings.

Putting away the fragment of Darcias bone near his chest, he took hold of both of Bone Birds legs.

The bone wings, covered in shining, bluewhite spirit feathers, began to flap and Vandalieu flew through the air to return to the Cursed Carriage where Bone Man and the others were waiting.

 

 

 

    Name Vandalieu
    Race Dhampir (Dark Elf)
    Age 1 year old
    Title None
    Job None
    Level 100
    Job history None
    Attributes
        Vitality 34
        Mana 100,001,247
        Strength 32
        Agility 7
        Stamina 33
        Intelligence 45

 

    Passive skills
        Superhuman Strength Level 1
        Rapid Healing Level 2
        DeathAttribute Magic Level 3
        Status Effect Resistance Level 3
        Magic Resistance Level 1
        Dark Vision
        Mental Corruption Level 10
        DeathAttribute Charm Level 2
        Chant Revocation

 

    Active skills
        Bloodsucking Level 3
        Surpass Limits Level 2
        Golem Transmutation Level 3 (LEVEL UP!)

 

    Curses
        Experience gained in previous life not carried over
        Cannot learn existing Jobs
        Unable to gain Experience Points independently

 

 

 

    Name Bone Bird
    Rank 3
    Race Phantom Bird
    Level 17
    Passive skills
        Dark Vision
        Spirit Feathers Level 1 (NEW!)
        Superhuman Strength Level 1 (NEW!)
    Active Skills
        Silent Steps Level 1 (NEW!)
        Swift Flight Level 1 (NEW!)

',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(5,'chapter 8','After the mysterious incident in which Evbejias outer walls turned into Golems, the people of the city began working on the large scale construction to rebuild it with heavy hearts. The Adventurers Guild set Eclass adventurers to carry out human wave tactics to defend the city and the countrys government requested for renowned mages and adventurers to be dispatched in order to investigate the incident. Meanwhile, the mastermind behind all of this was in the midst of an enjoyable journey by carriage.

He was sitting on the hide of a beast spread out inside the carriage in place of a cushion, listening to the cries of insects as he murmured to himself about how it would be autumn soon.

Now that I think about it, this is the first time Ive been on a journey.

Technically, he had gone on a school trip while on Earth, but the ferry had blown up partway through and he had drowned, so it didnt count. In any case, he remembered that it wasnt exactly enjoyable.

Speaking of journeys 

A different atmosphere from your everyday life, new experiences, rare sights, delicious food, talking with family or friends No, thats not a journey, its a trip.

How is a trip different from a journey?

If your goal is to have fun, its a trip. If not, its a journey.

Dictionaries back on Earth probably defined it differently.

So thats how it is. Vandalieu is much more knowledgeable than his mother.

Darcia gave a small, happysounding laugh. Hearing only this conversation, one might think that it was the start of a happy family trip.

However, the mother was the ghost of a Dark Elf, with traces of gruesome torture visible on her slightly glowing body. And the child that was sitting in the carriage had shining red and bluishpurple eyes he was a oneyearold Dhampir with even less of a presence than the ghost.

The carriage that they were riding on was moving without a driver or horses to pull it. It was a Rank 2 Undead monster, the Cursed Carriage. Flying in circles high up in the sky and keeping watch was the Rank 3 Phantom Bird, a bird with a body of bones surrounded by a bluish glow. And protecting the carriages on all sides were the Rank 3 Skeleton Warrior, and the Bear, Monkey and Wolf Bone Beasts.

This group that was proceeding forward in the middle of the night looked like a monster parade, a smallscale pandemonium. If there were any adventurers nearby, the only two responses they would have to them would be to either attack or run away.

I think Ill go to sleep now Before the sky gets brighter, I need to hide somewhere.

Knowing this, Vandalieu and his party only traveled on the highway at night. They were heading to the Orbaume Kingdom, which had been in conflict with the Amid Empire for centuries.

The original plan had been to head to the village where Darcia had been born and raised, the place she had been living when she was still alive, but Darcia herself stopped them.

I think its better not to go to my birthplace. Since I went and died, there is no way to prove that Vandalieu is my child, and Im not sure that everyone would believe my words as I am now.

Vandalieus skin was as white as a dead mans. His ears were pointed, but because of his skin color, one would not think that one of his parents was a Dark Elf.

According to Darcia, the relationship between Elves and Dark Elves of Lambda was not a good one. There were some Elf clans that had friendly relations with Dark Elves, but they were a minority most Elves hated Dark Elves and regarded them as the filthy Elves birthed by Vida.

And there was no need to even question what the Dark Elves thought about the people that hated them. If Darica herself had been alive, they would have believed her, but unfortunately, she had died. She did exist as a spirit, but the only ones who would be able to see her other than Vandalieu would be those who possessed rare jobs such as Spiritualist.

Also, I dont know if theyll forgive your father or not. Your father was the subordinate of a Vampire that believed in evil gods.

The Dark Elf race had been birthed by the goddess Vida and most of them lived while worshipping Vida and the Spirits while also intermingling with the other races created by Vida. Therefore it made perfect sense that they were also friendly with Vampires and Dhampirs, but only those that believed in gods other than Alda or the evil spirits.

There are also races of monsters such as Lamias and Scyllas that share a parent with the Dark Elf race, but if they were made obedient by tamers, it wasnt a problem. But the evil gods that were once the Demon Kings subordinates are still enemies. Those who worshipped them, even if they were of races given birth to by Vida, were considered traitors.

Its a tough world we live in, mother.

Yes, thats why I think it would be better if we headed towards the Orbaume Kingdom.

The Orbaume Kingdom and the Amid Empire made up the two halves of the Bahn Gaia continent.

The continent of Bahn Gaia was sometimes called Warhammer of Zantark because of the fact that it was shaped like the letter T. The warhammers handle part was thought to be the place where the fallen champion Zakkart had faced his end for the second time, an area filled with high mountains and countless Devils Nests. It was unexplored territory where even adventurers had never set foot.

The head part of the warhammer was divided into two In the west was the Amid Empire, and in the east was the Orbaume Kingdom. They had been fighting one another for centuries to take over the other and unite the continent. The Orbaume Kingdom had formed from the alliance of many smaller countries that rebelled against the Amid Empire around five hundred years ago.

But since it was a union of countries now, it needed to be managed properly. For this purpose, they had made an electoral system called selective monarchy where the royal families of the participating nations became dukes and then a king was selected from among these. The term of the king was ten years, and reappointment was possible only once. However, since the reappointment of a king was common, the effective term period was usually twenty years.

Politics in the Orbaume Kingdom changed depending on the personality and policies of the new king, but whatever the case, Aldas teachings, the state religion of the Amid Empire that was the enemy nation, should be weak.

It seems like there are Beastmen and Titans among those who can be selected as kings, so a Dhampir such as Vandalieu should have no problem.

Darcia had never been to the Orbaume Kingdom herself, so she couldnt be sure. But since there were Beastmen and Titans among the candidates for kings position, it was certain that the Vidas offspring there were treated very differently compared to the ones in the Amid Empire.

And so they were headed towards the Orbaume Kingdom, but they were unable to follow the straightest, shortest path there. They had heard that a war between the Orbaume Kingdom and Amid Empire was close.

Uhehe, this is a secret okay? Its special, nationalsecretlevel information, okay?

The spirit of a spy of the Amid Empire who had died several months ago and gone slightly insane told them this.

Vandalieu actually wondered whether he should believe him or not, but it would be terrible if he ignored him only to find that the war had already started when he reached border between Mirg and Orbaume. Of course, there would be increased security, and even if he managed to get through it to sneak out of this country, he wasnt sure if he could sneak into the next.

Convincing a suspicious soldier that they werent spies of the enemy would be no easy task. In fact, the soldiers would immediately assume that they were a small group of Undead. There would be no Monster Tamer that they could spot with a single glance, so instead ofLets try talking to them firstthey would surely thinkLets get rid of them first.

If Im lucky, theyd take me into custody, but I dont want the Undead that I took the time to raise to be destroyed, and who knows what theyd do if they found Mom.

Vandalieu was just a fourteenmonthold child, so if they didnt pulverize everything including the carriage from long range using offensive magic, he would probably be taken into custody, but that was impossible for the Undead.

As for Darcia, she was unlikely to be discovered as there was probably no soldier with the Necromancer job, but if they took the bone she was residing in and buried it, she might just disappear this time.

For that reason, they were not heading northeast to the Mirg country that was closest to the Orbaume Kingdoms border, but were rather heading southeast towards the southern part of the continent, the area covered in high mountains and Devils Nests.

Crossing high mountains and Devils Nests will be difficult, but its still better than coming into contact with humans.

This was also often true on Earth too. Humans were the most fearsome creatures of all, and this was probably true for his situation right now.

Incidentally, the reason why Vandalieu hadnt trained any Golems or Zombies till now was because he had been planning for this journey.

The movements of Golems were sluggish, and if he made them big to cover for that, they would be conspicuous, even in the middle of the night. Zombies were the same their movements were also very sluggish. In movies on Earth, he had seen Zombies that could run at the same speed as athletes, but you shouldnt expect fictionlike things in reality. Yes, this world was a fantasy world, but it was also reality.

Also, in the case of Zombies, he would have to use the corpses of the bandits or Orbie and his companions, and they were hardly faces that he wanted to look at all day. This was another very important factor.

And so, Vandalieus little group was composed of just bones.

 

 

 

Vandalieus travels, except for the fact that they couldnt move around while the sun was up, was progressing smoothly. During the day, they hid in forests away from the highway, and resumed their travels once night fell. If there were some peddlers or adventurers that were travelling along at night, they would go off the highway and continue on, and do the same when there were guard stations or checkpoints in the way

Even though they were making steady progress, their pace was less than half that of a normal traveler, but that couldnt be helped.

I want to hurry and be able to travel boldly in the middle of the highway.

He hadnt done anything wrong (except for his revenge) it was such an unfair world.

He had to worry about the risk of beasts, monsters or even bandits attacking them if they travelled off the highway, though that was quite rare. Just from seeing Bone Man or Bone Monkey protecting the carriage from far away, beasts and bandits would be discouraged from approaching.

The wild beasts such as wolves and bears instinctively feared monsters except in cases where the monster was plainly weaker than them. Bandits were the same. Their targets were merchants and travelers, not monsters.

Of course, by defeating monsters you could expect some gains like ingredients and experience points. But if they had the skills to take on monsters, they would be working as adventurers, not as bandits.

And as for monsters themselves, there werent many monsters that attacked other Undead monsters, especially skeleton types that were made of just bones.

The primary reason for a monster attacking another monster was to feed, but skeletons didnt have any edible parts.

There were other reasons such as territorial disputes and selfdefense, but Vandalieu and the others never invaded the habitats or entered too far into the territory of other monsters, so the monsters would not do much other than wait and see.

Gigyaaah!

Well, from time to time monsters with low judgment capabilities decided to attack them.

Bone Wolf dragged a twitching, almostdead Goblin in front of the carriages wheels.

The Goblin was crushed with a loud crunch.

We havent been able to gain levels these days by killing monsters like Goblins, huh.

Vandalieu didnt pay the Goblin any attention as he left it behind. He didnt bother searching for a Magic Stone, let alone the right ear that adventurers took to prove that they had killed the monster.

Darcia had told him that because the chances of finding a Magic Stone on a Rank 1 monster was less than one in a hundred, even many higherrank adventurers didnt bother searching monsters like Goblins for them. Since Vandalieu had no way of even exchanging the Magic Stones for money, he decided to follow their example.

What concerned him more was the difficulty he was having in raising the levels of his Undead.

When they were Rank 1, their levels increased even when they killed a single monster together, even sharing the experience between all of them. But now, killing numerous monsters without sharing experience didnt even yield one level.

The Rank 2 carriage still increased in levels after running over some Goblins, but the levels of the Rank 3 Undead werent increasing at all.

I suppose I have to kill more bandits after all. Though I want the carriage to be Rank 3 before we cross the mountain range.

Vandalieu, Rank 3 is normally considered quite strong, you know? It would take several normal Rank 2 people to drive away one Rank 3 monster.

A Rank 3 monster was so threatening that it could still trample onesidedly over a group of normal people. At the very least, even normal citizens who were proud of their fighting skills or physically strong town guards wouldnt stand a chance.

But Mom, there arent going to be any normal people in the place were going to, and since I cant get any stronger myself right now, I want everyone to get stronger instead.

I suppose thats true though youre able to make Golems quickly, so youll be fine.

This mother of Vandalieus was an easygoing person, even after death.

 

 

 

The Amid Empire had been founded by the descendants of Bellwood, a champion who was guided to this world long ago.

The champion Bellwood had stood on the frontlines with the people who were forced to do battle against the Demon Kings forces, and fought bravely. Within ten days of coming to this world, he had defeated a Dragon and rescued a noblemans daughter and the First Lady who had been captured.

Within a month, he had repelled a large army of Giants together with the Second Lady who was a warrior. Within half a year he had fought the Knights of Darkness that the Demon King had sent while protecting the Third Lady who was a Priestess and prevailed. And finally, after ten years, he defeated the Demon King, even as Zakkart and the other champions were lost, bringing peace to the world. It is said that he and the other surviving champions offered their wisdom to help rebuild what had been destroyed.

Much of the champions wisdom had been lost, but what remained was the hiragana, katakana and kanji that was the commonlyused writing in this world, and the Amid Village that eventually became the Amid Empire.

And Balschmidt, the great First Emperor of the Amid Empire who appeared tens of thousands of years later, was a direct descendant of the champion Bellwood.

 

 

 

It sounds madeup and unpleasant.

Vandalieu closed the book that had been mixed in with the bandits treasure and threw it away violently.

In Lambda, where the technology for printing presses didnt exist, every book was a written copy. Therefore, they were quite highclass items.

But Vandalieu didnt see even a single copper coins worth of value in the tales written about the actions of Bellwood, the champion who had founded the Amid Empire, and the First Emperor Balschmidt.

Firstly, the whole thing sounded like nonsense.

The proof that the First Emperor of the Amid Empire was a direct descendant of the champion was incredibly questionable. Surely there was no way to prove a relationship with an ancestor who had lived a hundred thousand years ago. The book had covered this up with stories about destiny, prophetic dreams and an illusion of the champion himself appearing and speaking to the emperor.

Secondly, it was unpleasant because it made Vandalieu feel envious.

The champion Bellwood He was probably known as Suzuki in Japan. He had manifested cheatlike abilities or something as soon as he arrived in Lambda, and within ten days he had become known as a Dragonslaying champion who had rescued the princess. Within a month, six months, he had continued piling up achievements and connections and after ten years, he was known as a great hero.
TLN The kanji for the name Suzuki is literally bell and treewood

 

What was Vandalieu compared to that? He couldnt help but to ask this question to himself.

Fifteen months had passed since he was born. The only member of the other sex that he had talked to was his own mother. It seemed impossible that he could become a great hero within ten years.

Of course, the champion Suzuki probably endured hardships and suffering of his own he had survived the battle with his life on the line, after all. Even so, Vandalieu is all too aware of the difference between receiving a gods blessing and being cursed by the god instead.

This is all Rodcortes fault as well.

Youre right, its all this god Rodcortes fault. But even so, Im a little thankful. It was that god who made you my child, Vandalieu.

Mom

His splintered heart began to heal a little. Vandalieu reconsidered, thinking that perhaps he should at least be thankful to Rodcorte for making Darcia his mother.

But if it werent for Rodcortes curse, he would be able to use deathattribute magic at the level that he had been using it just before he died for the second time in Origin. He would be able to employ countless Golems freely, manipulate many spirits simultaneously, turn an entire lake of water into deadly poison or reverse the process, turning deadly poison into pure water. If he had been able to do all these things, perhaps Darcia wouldnt have had to die. When he thought about that, any gratitude that he might have felt towards Rodcorte was blown away.

The carriage that had been proceeding along a side road to avoid being spotted suddenly stopped.

What is it?

Due to hisDanger Sense Deathmagic, he knew that they werent in an emergency situation such as being surrounded by enemies or attacked by a powerful monster. That magic detected not only poisonous mushrooms and plants, but also the bloodlust given off by monsters and humans.

Therefore, he shouldnt be in a dangerous situation or so Vandalieu thought as he poked his head out from inside the carriage to see a gate that led underground.

A gate? A base used by bandits its a little too wellbuilt to be something like that. Is it some kind of ruins?

As Vandalieu tilted his head sideways, it was Sam who told him what the gate was.

Goshujinsama, I believe this may be a Dungeon.

A Dungeon?

Yes, it is a Dungeon.

Because Vandalieu was inside the carriage, he couldnt see Sam, but he could hear his voice clearly.

Dungeons. They were originally breeding facilities created by the Demon King to multiply his monster subordinates and increase their individual strength. Afterwards, Ricklent, the genie of time and magic, used his secret magic to turn them into places that humans could use to train and obtain materials.

The basic principle behind their appearance was that when a certain place was corrupted with a certain amount of magic, there was a corresponding probability that a Dungeon would form. Their shape, size and degree of danger varied greatly, and some of the first Dungeons that had appeared over a hundred thousand years ago still hadnt been discovered yet.

There were Dungeons that only consisted of one floor that couldnt be distinguished from a normal cave, home to only weak monsters like Goblins, but there were also enormous fortresslike Dungeons with over a hundred floors, crawling with highrank monsters that even Aclass adventurers would have a tough time getting through.

And as the requirement for the formation of a Dungeon was for magical power to contaminate the earth, most Dungeons existed only in Devils Nests

Is this place a Devils Nest?

Probably not. If this were a Devils Nest, we would be seeing a lot more monsters coming out. This is likely a rare Dungeon that has appeared outside a Devils Nest.

As Darcia said, Dungeons forming in normal forests and fields were rare, but not unheard of. But in such cases, the Dungeons were often not very dangerous.

But if left alone for too long, the monsters multiplying inside the Dungeon would eventually emerge from it and turn the surrounding area into a Devils Nest.

There were apparently continents in Lambda whose entire land surface and surrounding oceans had become Devils Nests due to this process repeating numerous times.

So then there should be a lot of monsters in here that arent too strong. Judging from the grass growing thickly in front of the gate, it seems that adventurers havent been here in a while Alright, lets stop by and go inside.

Dungeons. The fantasylike sound to the word and the sense of adventure excited Vandalieu.

Though the ones who would actually do the adventuring and fighting the monsters would be Bone Man and the other Undead.

That couldnt be helped, since he was still only fifteen months old.

 

 

 

Around the time that Vandalieu was preparing to enter his first Dungeon, Degan, the guildmaster of the Adventurers Guild in the town of Terow was watching three adventurers glaring at each other with a troubled look on his face.

The three people here were each a leader of an adventurer party that was well known here in Terow.

Cashew, the leader of the Dclass adventurers party, theWind Chasers.Barn, the leader of a similar party called theSteel Wings.And Miranda, the leader of theWhite Stars.

It was safe to say that none of them were famous on the national level in the country of Mirg, and none of their abilities surpassed the Dclass level. However, in this modestlysized commercial town of Terow where there were no Devils Nests nearby, they were topclass adventurers.

In fact, they had repelled bandit attacks numerous times and overcome the explosive increase in the Kobold population. They were known as heroes here in Terow.

They had accepted a request to exterminate a lost Ogre together and searched the forest near Terow, and had discovered a Dungeon on the way back.

Only the members here, the towns feudal lord and his aide knew about this, but Degan was ecstatic as this was a big opportunity to develop the town.

Indeed, the Dungeon was dangerous. If left alone, monsters might emerge from it and advance onto the town.

But if adventurers were to venture into it from time to time to thin out the monster numbers, it was essentially a chicken that laid an unlimited amount of golden eggs. The fact that monsters spawned without limit meant that there was a limitless amount of raw materials to be obtained from them, as well as precious plants and ores that could only be found inside Dungeons. Not to mention the treasure chests that appeared in Dungeons that could yield valuable magic items.

Adventurers seeking this Dungeon would gather in the town, and in turn, merchants seeking the gold that the adventurers would spend would also gather here, and thus the town would develop.

And if the Dungeon was cleared in this way, it would become a resource with greater returns than the risk.

With that said, there were also Dungeons in Lambda with only monsters such as Goblins that yielded only small amounts of raw materials and treasure boxes with poor contents, the kind of Dungeons that only caused trouble just by existing.

Would this be one of those failures, or a great success? Most importantly, adventurers would need to be deployed into the Dungeon to determine how dangerous it is, and there was an unspoken understanding that the adventurers who had discovered the Dungeon would have the first rights to it.

Of course, in cases where adventurers that were not hunters or otherwise not confident in their abilities discovered the Dungeon, they could sell the rights to explore the Dungeon first through the Adventurers Guild.

In cases where multiple adventurer parties discovered the Dungeon together, they often negotiated with money or performed the first exploration together.

WeWind Chaserswill explore the Dungeon first!(Cashew)

What are you saying! WeSteel Wingsare more suitable for the task!(Barn)

Leave it to us, theWhite Stars!The rest of you can go and collect some Goblin ears or something!(Miranda)

However, it seemed that these adventurers had no intention of coming to a compromise, and expecting them to join forces seemed hopeless.

This is a problem. Though I suppose that they were competing against each other since the very start(Degan)

For a long time, these three adventurer parties had been competing with one another for the title of the best adventurer group in the town. However, they had a proper sense of morals as adventurers, so they had worked together to overcome the Kobold population problem in the past, but that was all.

The Dungeon that had been discovered had no evidence that any monsters had wandered outside from within, so there was no hurry to explore it. Also, Dungeons that appeared outside of Devils Nests were generally not very dangerous and small in size.

Therefore, there was no need to cooperate any one party could take the Dungeon on without a problem.

In fact, if the first exploration of the Dungeon was successful, the party that completed it would be viewed favorably by the Adventurers Guild and may even be able to increase their rank to Cclass. It was a chance for one party to surpass the other two.

WeWind Chasershave members that are good at disarming traps! The best your partys Thieves can do is conceal their presence and scout, hardly suitable for exploring a Dungeon!(Cashew)

What the hell are you saying, Cashew?! Your party has no Healers exploring a Dungeon relying only on potions is too dangerous! In that regard, weSteel Wingshave two Healers who can heal injuries, remove poison and cure diseases on the spot!(Barn)

What are you talking about? It seems that in short, you have a bad balance in members. Ill tell you this, weWhite Starsare a perfectly balanced party with one person on attack and one on defense, an Archer and a Thief, an offensive Mage and a Healer! You should leave the first exploration of the Dungeon to us!(Miranda)

Seeing these three giving each other angry looks, with a fistfight that could break out at any moment, Degan pressed his fingertips against his eyebrows.

You three weve been having this conversation for three days now.(Degan)

Perhaps this would be settled faster if a fistfight really did break out. It took everything in Degan to resist the temptation to suggest this.
',0,'2019-05-01 23:00:01.000000');