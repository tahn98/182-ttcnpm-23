INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(11,'chapter 1','Underhanded, repulsive and detestable I feel sick.

Every time I look around, each and every person is the same.

They betray while smiling, trample on good intentions while grinning, and administer poison while laughing loudly.

I feel like an idiot for trying desperately to save people like them, but without being able to rewind the time, I can only feel depressed.

It could be said that I am responsible for getting deceived, thats why this conclusion is only natural. However, if someone says I should accept it, then heck no.

?. We did it.?

?Geez, youre a monster, really.?

?Its finally over. All evil is purified in accordance to Gods will.?

While the source of life is spilling on the sword stabbed in my chest, my former friends, no, people I thought as friends surround me.

This is without a doubt a punishment, punishment to the me who blindly clung to the word believe.

Therefore, if there is a second time, I wont make any mistakes then for sure.

Therefore, if there is a next time, Ill definitely kill.

Ill kill the princess, Ill kill the knights, Ill kill the villagers, Ill kill the sorcerers, Ill kill the soldiers, Ill kill the Saints, Ill kill the martial artists, Ill kill the assassins, Ill kill the dancers, Ill kill the merchants, Ill kill the King, Ill kill the Queen, Ill kill the nobles.

I will massacre them all, in the most cruel way in the most sadistic way.

I will keep it engraved in the deepest, deepest, deepest part of my heart, so I wouldnt forget it in the next life.

? System Message Holy Sword of Vengeance unlocked. ?

As my consciousness starts to fade, such a voice was heard. But my body wont budge even a little bit, I cant do anything.

?Hey, I wonder where did we make a blunder What should we have done, to live in that gamelike time forever, or maybe, there was nothing we couldve done I wish it turned out differently?

And then, I remember at the end of the killing, one woman killed another. She too was pierced the same way I was, laughing at her own helplessness, that woman was referred to as the Demon Lord.

?kukuku ahahahaugguukku bwahahahahaha!!?

A mouthful of blood mixed with her laughter. It truly is a hilarious story, the enemy of the world, the enemy they told us to fight who was considered the heros true nemesis was the only person that didnt tell a lie. The pierrot was dancing a bit more longer.

?Tch, You still havent died yet!?

?No, he doesnt have any power to stand anymore. But, the purification of evil will take a little time.?

?Thats right, he cant even glare at us anymore.?

While feeling like they were about to laugh again, thoughts were suddenly surging.

Since he already lost too much blood, pretty thoughts were not settled there. Therefore, words that were engraved only by instinct came out.

?Ah all of you, Ill absolutely kill all of you by all means ?

Kachick The sound confirmed the total depletion of his HP, and his consciousness sank into the bottom of the dark abyss.

I, Ukei Kaito died.

?

? System Message Tutorial Mode has ended.

Time Elapsed 4 years, 112 days, 17 hours, 52 minutes and 35 seconds.

Experience(level) is decreasing over time

Since the retrogression amount exceeds the accumulated experience points, resetting level to initial value, a deficit of 20,000 is pooled.

Setting experience point to be locked at level 10.

Retrogression amount exceeded the deficit limit.

Revoking skills which fit to the retrogression amount it exceeds retrogression process executed.

Retrogression of skills, revoking success. Skills were fully initialized.

Because retrogression amount exceeds skill, attempting retrogression process by revoking the special skill ?Spirit Sword?.
 Attempt failed due to the effects of ?Holy Sword of Vengeance?.

Revocation process cancelled, moving on to attempt sealing the key amount of experience points.

Sealing process success. 53 of 58 forms of ?Spirit Sword? was sealed.

Offsetting of the excess retrogression amount has ended.

Preparing retrogression to start mode



 Complete.

Commencing retrogression to start mode.?
',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(11,'chapter 2','?Welcome, thank you for coming, Herosa gafu!?

As I opened my eyes, the object of hatred right before me took a full hit.

My fist intuitively punched out and hit the pit of her stomach with my full might. Princess Alesia Aurelias silver hair swayed and she staggered a few steps back, crouching down and holding her stomach.

I actually wanted to aim at the face, but because I was lying down, my fist didnt reach it, and the brunt of the blow wasnt fully delivered.

?HER HIGHNESS!!??PRINCESS!!?

Unable to understand the situation at hand, the knights were momentarily dumbfounded and rushed to the princess aid in panic. Some immediately chanted lowclass healing spells and a faint light wrapped around the princess.

Seeing that spectacle, I wasnt satisfied at all.

Even though I didnt use any weapons, though it was a partial hit without any strength, reinforcement magic nor blessings, I couldnt understand how she took my blow directly without any equipment and got off with just this.

When a question pops out, more questions about the current situation instantly follow.

?Uhh oh? Where the heck am I? Is this a dream? A revolving lantern??

Though I shouldve died, I didnt see anything wrong with my body when I looked down.

Neither the trace nor mark of the sword ?Deus Slayer? that shouldve been struck in my chest can be found, not even on the clothes I was wearing.

It was more than 4 years ago, this black clothing I had when I went on a journey in this world for the first time.

It was the uniform of the high school, I, Ukei Kaito was attending.

?You bastard, what the heck did you do!!?

?Even though you are the hero, you cant harm the princess!!?

Though the knights quickly pointed their swords at me, their pressure didnt bare any killing intent. They mustve thought I was still inexperienced at fighting, so the pressure they emit was like a gentle breeze.

I ignored it and promptly tried to grasp the current situation.

I should be at the summoning area inside the castle in the royal capital based on my surroundings.

Just a while ago, I was in the deepest part of Ryuudouden in the depths of the unexplored region, The Dragons Tomb.

It should be so far away that the linear distance is 10,000 kilometers. Assuming transfer magic was used, youd have to do a longdistance transfer more than 10 times, itll be so impossible that even the Demon Lord who took pride in her absurd amount of magical power cant do it in one go.

Then it must be my life flashing before me like a revolving lantern Nah, illusions which could be considered as reliving ones life are definitely impossible.

Since that dream, it hits me and this hostility that I sensed gradually resurfaced.

If this wasnt a dream or a revolving lantern, I unfortunately still couldnt come up with an explanation for the current situation.

?Oi, are you listening!??

?Not really.?

?Wh!? You bastard!!?

Like a slap in their face, the annoying knights cheap pride got damaged with a halfhearted answer. His hostility turned into real bloodlust and pointed his sword.

On the contrary, the serious bloodlust that turns to me becomes like soft warmth for the moment.

While theyre thinking that I wasnt really that strong because of that halfhearted answer softening them up, my body reacted and rushed over to them.

?Eh? Wh!??

I stepped on his foot and I struck my elbow into his throat, carrying all my weight. I didnt hold back in any of my movements.

I was summoned as the hero for three years.

After defeating the Demon Lord, I was chased for a year, being pursued by the world as a sacrifice in the postwar recovery.

If one is not accustomed to dealing fatal blows and hesitates, then they wouldnt have that long to live.

Furthermore, the knights had stopped moving at the unexpected scene. The guy who came at me slammed into the wall, with a partially crushed windpipe, and was now foaming at the mouth. His body loosened and he was stumbling.

?Ah? The neck was not blown off. Errr Was his neck protected by spirit strengthening? Nah, I didnt feel that kind of magic used though Rather, my body feels heavy? Hmmmm??

A dead silence fills the room and only my voice was heard.

No matter how skilled or strong he was, even though he seemed strong or an expert. Though I didnt use any sort of weapon, its difficult to think that it was ineffective.

Supposedly, his neck shouldve been twisted and turned with a kruryuk sound. But in reality, that didnt happen.

?LLauren!!?

In a few seconds, the knights broke out of their petrified state and gathered near the fallen knight. They hastily chanted a recovery spell and doused an intermediate healing potion on the fallen knights throat.

?Wwas there something that was unsatisfactory to you, Herosa ma ?

The Princess barely got those words out, as she was recovering from vomiting with a pale blue face that reached her ears. She was intimidated when the bloodlust that I leaked involuntarily filled the room.
With a pale face and somewhat recovered, the princess barely muttered those words. Hearing her caused my bloodlust to unintentionally leak out, intimidating every person in here.

?You can call it something like that, Alesia. As expected of a princess. I dont like just about everything about you, that voice, those eyes, that figure, that attitude, I dont like it at all. I felt sick just from hearing those words from your mouth.?

A knight felt an overwhelming sense of danger and whipped his trembling body to action. He swapped positions with the princess in order to protect her, but little did he know that it was useless.

Because, I who can move with inhuman speed, was no longer there.

?Kyaarghhh! Uggu Kku?

I slipped pass through the knights and dashed beside someone, then I grabbed its neck with one hand and smashed it against a wall.

?I was but an innocent man who was selfishly summoned, forced into the role of a hero. If I defeat the Demon Lord again, all thats waiting for me is to be blamed for crimes I did not do, and be betrayed and laughed at.?

?Wh what do you mean kuh huff ?

Its very clear to me, I will never forget it.

As soon as the Demon Lord was defeated, the world will turn inside out.

The Saints recognized me as the worlds enemy, and the kingdom confirmed it. And all of the crimes committed behind the scenes were blamed unto me.

Companions who had fought alongside me, friends who I thought shared a close bond with, all of them will betray me without exception.

Without a doubt, the same guys I saved with just a single cry for help would betray me, will throw stones at me, insult me and spit on me.

This princess was also one of those people.

After I defeat the Demon Lord, the world would become my enemy, and all my allies will disappear.

Among them, the princess will pretend to be an ally. She approaches me saying Ill help you and Ill give you shelter.

Life as a fugitive can get tiring. These dramatic changes can get so exhausting that I easily believed those words of help. Only to then get betrayed and laughed at.

While saying that it was a safe place, I was taken to a teleportation gem, where I can use magic to get in, but not to get out. It was a trap room inside a dungeon.

And when I had barely escaped with my life, I had serious injuries that took a considerable time to heal.

?Ah, when you set me up, your ally, that time you told me I never betrayed you, from the beginning I was never your ally and it shouldve been obvious. In the first place, a person from a different world is not a human, didnt you tell me that??

?Really, I dont know what youre?

They treated me like an idiot. And the truth was that I was an idiot.

I wouldve noticed if I had assessed her properly, that she was hiding her enmity of me. If I had just abandoned the word believe, then this never would have happened.

Anyhow, I can clearly feel the malice hidden within the princesss heart now. It is clear even though she seems in pain and confused.

Slight gestures, eyecontact, the breathing pattern, the change in expressions.

These are the ways I gain information to predict the thoughts and moves of my opponents in combat. You can never conceal illwill from me.

?Sigh, you truly have thick mask under your skin. Well, although I dont understand the situation, this doesnt seem to be a dream or a revolving lantern, but either is fine. Ill leave the difficult things for later.?

Ah, I let out my voice unintentionally.

?I dont know how long will this bonus time that I have last I swear?

My voice started to overflow with delight, my expression broke out into euphoria very quickly, then my heart started to beat faster causing my arm to brim with excitement and act rashly.

?Aa Ugguhk?

Then, the animosity from the princess withered rapidly.

I released my hold and the grip I had on her neck loosened. She landed on her rear, looking at me with eyes filled with fear. My figure reflected in her eyes and my expression was certainly distorted and disturbing.

But, its good. Its all good.

I wanted to live in this beautiful world forever. Because its a world where oneself can become a hero.

But, the ending result of this world was to be betrayed and framed as a criminal. Becoming a laughing stock.

I was of course pure, but not anymore. I was already broken a long time ago.

I swear, I will have revenge.

The face I mustve shown to the others must be one filled with insanity, it must counter their idea of honor.

?Pplease Hhelp?

?Detestable. Suffer as much as possible, Alesia?

?GYAU!!?

Left, right, left, right I hit her face until her conscious withered and her face numbed. I hit her so she can suffer as much pain as possible.

?You bastard!, Gya!??

?Gguuhkk!!?

?Hora! Hora! Hora! Hora!! Your important princess is being beaten to a pulp, cant you do anything about it? Huh!??
Caught in the moment where the princess was being clobbered, The knights finally moved and sprung into action, I was encircled by 5, no, 6 knights, this much doesnt
matter to me though.

I drove my elbow into the joint in the leg of a knight, destroying his center of gravity. I then applied pressure to inflict as much pain as possible, the bone was then twisted and broken. I then proceeded to gouge his eyes, crush his ear, and rip his nose into pieces.

?Ahaha AHAHAHAHA!!?

As usual, my body felt heavy, sluggish, but there is no need to use the Spirit Sword.

It is unfortunate but theyre unworthy to be killed by my Spirit Sword. Also, I didt want to kill.

I want revenge.

I want them to suffer.

I want them to suffer as much as they possibly can.

If I cant, my heart will never know peace.

?AHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHA!!!?

Their screams of pain and fear were like music to my ears, this sensation cant be stopped it feels too good.

The voice doesnt stop, it wants to torment them until the point of death. If they ever faint, I can just inflict more pain until they wake up again.

It certainly was hell for the knights.
It certainly was hell for the princess.

For me, this was the paradise where all my wishes came true.

The laughter never ends. It will never end.

The screaming never stops. It will never stop.
',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(11,'chapter 3','?Aa ugh, gaah?

?Higguu Gguukk, Aagh?

After a few minutes, the room wasnt filled with words but moans. When her reaction didnt please me anymore no matter what I did, I stopped.

Her face was so disfigured that you wouldnt know who it was upfront. All of her fingers were broken, pointing at odd directions.

Finally, when she couldnt handle anymore beating, she curled up and foamed from her mouth.

?Well then, shall we do it again?

My desire for revenge isnt going to be quelled with just this, but if shes not showing any reaction or whatsoever from the torture, itll be meaningless.

When I regained my composure after acting violently, I used a healing magic on Princess Alesia while enduring to impulsively stab the short sword in her.

A small amount of magic was absorbed through my hand, and a faint light welled up. That light was from the ?Verdant Green Crystal Sword?.

I obtained this one form of the Spirit Sword when I fulfilled the requirements in the Forest of the Elves.

The short sword is approximately 15 centimeters long and its like a green crystal. And if some magic is poured in, there will be a healing effect on the target.

However, its not an instant recovery. So in the meantime, I will recollect my thoughts.

?I thought it was just my imagination, but why does my body feel heavy?

What I understood during the battle was that my physical ability has evidently deteriorated. Why that happened, I have a few ideas.

?Hmm Maybe its a curse?

What pierced me before, the ?Deus Slayer? was a sword that has the ability to steal attributes every time you inflict a wound on the opponent, and that debuff effect is why the church treasure it. That time when I had equipped it, the effect of my Spirit Sword was nullified.

My inherent skill ?Spirit Sword? has the ability to acquire numerous forms every time I meet a certain requirement for my exclusive use.

Those four years werent for naught, among the many forms that I obtained, theres one that can cure any ailments. So first of all, I have to investigate about how to deal with the sword.

?Status Open?

As if adhering to my voice, a pale semitransparent board the status window, emerged.

In there was a list of the attributes of the person who called it. Without using some type of appraisal, skill or magic, they wouldnt be able to see it except me.

In there was amazing information written inside.

Ukei Kaito  17 years old  Male
HP 531545 MP 347412
Level 1
Strength 224
Stamina 324
Endurance 545
Agility 587
Magic Power 117
Magic Resistance 497
Inherent Skills ?Spirit Sword?? ?Language Comprehension?
Skills ?Punch Lv1?
Condition Excellent
?What on the earth is this?

I rubbed my eyes and closed the status window thinking that it was a bug. I shook my head and I opened it again.

?Status Open.?

Ukei Kaito  17 years old  Male
HP 531545 MP 347412
Level 1
Strength 224
Stamina 324
Endurance 545
Agility 587
Magic Power 117
Magic Resistance 497
Inherent Skills ?Spirit Sword?? ?Language Comprehension?
Skills ?Punch Lv1?
Condition Excellent
?Why?

There were many strange attributes so I unintentionally muttered that question.

Firstly, my age.

I was summoned about 4 years ago, when I was in my second year of highschool and I was 17 years old, now Im 21 years old.

What couldve happened Had some secret technique been activated Or not

Well, I dont have any problem with this.

Next was level.

I defeated the Demon Lord, and the experience I gained after fighting her was bountiful. It exceeded above 300 levels, and it rose to nearly level 400. Before departing, the leader of the knights, which was considered to be the Kingdoms strongest person was about level 121, there were also approximately 270 participants during the Demon Lord subjugation, that was also something to consider to know what extent it has risen.

Of course, if my level goes up, the attributes are affected too.

But Ive never heard stories where your level decreases, the only case was when you continue to skip training for years.

But Ive never heard of a rumor where your level decreases in one go from level 300.

Or rather, in this world only infants are level 1.

Even when I first came to this world, I was at level 3.

And to match my level 1, my attributes had decreased extremely.

And lastly, my skills.

My Perfectionist trait, its something like a hero correction skill and unlike the inherent skill. If youre suitable and you fulfilled the conditions, then with enough time you can get it.

The skills have proficiency and level. If you train several times they level up, and then you can master it.

Basically you dont have any other choice but to spam the skills until you level up, in other words, my skill ?Punch? which I had spammed relentlessly was the fruit of my hard work.



?Ahh! Silly me The ?Sky Walk?!!?

The princess consciousness hasnt come back yet, but most of her wounds were already cured so I interrupted the treatment and cancelled my Spirit Sword.

I kicked the ground, and then kick the ground one more time using magic as my foothold, I jumped to the air.

The skill I hurriedly acquire is ?Sky Walk?. With this skill, I can make a foothold beneath my feet, making aerial battles possible.

This skill has helped me on the battlefield. It was certainly activated. It had activated, but

?Is this a joke?

He gasped as he was dumbfounded at the excessively crude activation of the skill. The consumption of magic power and the activation speed, it cant be compared to my own senses.

?Status Open!!?

Ukei Kaito  17 years old  Male
HP 531545 MP 297412
Level 1
Strength 224
Stamina 324
Endurance 545
Agility 587
Magic Power 117
Magic Resistance 497
Inherent Skills ?Spirit Sword?? ?Language Comprehension?
Skills ?Punch Lv1? ?Magic Manipulation Lv1??Sky Walk Lv1?
Condition Excellent
When I opened the status window, the skill that I used just now, ?Magic Manipulation Lv1? and ?Sky Walk Lv1? were registered.

When I tapped the word ?Sky Walk? on the status window, I sighed.

?Sigh, just as I feared ?

?Sky Walk? Skill Level 1
Skill Proficiency 110000

A skill to make footholds in the air with magic.

That was written in the status window.
In other words, the skill was treated as newly obtained.

My drained MP proves it.

Like I said before and to put it briefly, the skill level indicates how much you are used to the skill.

When you successfully activate the skill, your proficiency will rise up. And with that, you can use it more efficiently.

To elaborate, when Sky Walk levels up, the MP cost and casting time decreases.

Frankly speaking, a level 1 skill is garbage. Most of them have a consumption rate so bad that you cant use them in actual combat.

Just in case, I checked the other skills
but they were level 1 too.

?It doesnt seem to be an effect of a curse. If there were a tremendous effect like this, I wouldnt have lasted that long on the battlefield.?

Actually, before I had awoken here, if my memory serves me right, I had every skill on advanced level, making it possible to use ?Sky Walk? and ?Magic Manipulation? while chattering, and fight alone for a year or more.

If there was a sword that has a skill effect that can reset ones level and skills, then there is no need to rely on a hero, because you could just defeat the Demon Lord in one hit

Nah, although thats impossible, you could defeat her with ease.

?That moment, before being killed Nah, Im going to assume that I survived because Im still alive. And while I was unconscious, they afflicted me with a new kind of status effect?

In the status window, its stated that my condition was excellent, but the status window can be full of flaws so I can only use it as a reference. And my status window couldve been deceived by an obstructive and high level status effect.

Thats why, to recognize what type of effect I have on me, I opened my status window again and used the appraisal ability that the Spirit Sword has to obtain a more detailed information of my condition.

If this is really a new type of effect, then I have to grasp the effect without fail, otherwise I wont be able to perceive the requirements to cancel it.

?Well then, Appraisal!





Apprai sal?

And when I was about to chant the appraisal ability that the sword has and use it on my status window, I stopped my movement.

?It, cant, be?

While making a wry face because of an awful premonition, I tapped the ??? icon next to the Spirit Sword.

Then, there listed were numerous names of the Spirit Sword where I had overcome innumerable hardships until now.

However, most of them were displayed in dim gray, and next to them was a mark of a padlock. By touching the mark, there indicates the necessary amount of experience required to release that type of Spirit Sword.

?This is a joke, right?

I tried to bring out a few of the Spirit Swords that are colored in dim gray, but none of them worked.

It appears like its only the grey ones, because I can use the ones colored in white.

Again, to the point of dropping tears, this time, she had passed what she can tolerate, together with the irritation and anxiety, her emotions were going in circles.

While pretending to be unconscious, she attempted to chant a spell from her mouth, so I kicked her to interrupt it.

?Gyaaaaaa!?

?You really do like surprise attacks, Alesia. For what purpose do you think did I heal you Dont die before I give you a beating.?

It seems like that was a lowgrade fireball, but because it was in an incomplete state, the fireball exploded inside the mouth of the princess.

Surely, the inside of her mouth had become something very serious, so serious that it feels great.

Although I wont say it, I want you to oppose me with every being that you have except killing yourself.

Seeing Princess Alesiasama selfdestructing, its very fun. Its very pleasant.

As I thought, taking vengeance for myself is refreshing. While thinking that, I suddenly found a notification on the upperleft of the status window, something that I never saw.

Until now, I never saw that icon once, so I dont know what type of effect has.

?Hm, what to do?

?Guahkk Gauvuaahh?

For now, Im pressing my bored feet against her stomach while she glares me intensely. Enjoying the disfigured appearance and voice of Princess Alesiasama.

Suppressing my shock for losing many forms of the Spirit Sword, I slowly pressed my righthand finger on the email icon.
',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(11,'chapter 4','Chapter 4  The Wedding Procession

Floating Cloud City was the smallest city of the Blue Wind Empire. It was so small that it wasnt even suitable to be called a city perhaps calling it a town would be more appropriate. Floating Cloud City was not only the smallest city but was also the most geographically remote in terms of location. The population, economy, and even the average profound strength was the lowest of the low. These days, Floating Cloud Citys residents often mock themselves for being a forgotten corner in the Blue Wind Empire.

Floating Cloud City was particularly lively today for it was Xiao Che and Xia Qingyues big wedding day. Nobody would care if it was only Xiao Ches wedding but Xia Qingyues marriage was Floating Cloud Citys biggest sensational event.

The Xia Clan was not a clan that solely trained in the arts of the profound. It was a clan that specialized in business for generations. Although they could not be said to be wealthy among others of the Blue Wind Empire on Floating Cloud Citys list of the most prosperous clans, Xia Clan was at the top. However, this did not mean that the Xia Clan was weak. With their abundant wealth, they could naturally afford to hire experts to protect their vast fortune. The leader of the Xia Clan had two children Xia Yuanba and Xia Qingyue. Both his son and daughter had no interest in the family business. They solely focused on training in the ways of the profound. Xia Hongyi had never opposed their decision and instead allowed them to continue their path. After Xia Qingyue surprised Floating Cloud City with her talent, it was even more unlikely that he would prevent her growth. Due to Xia Qingyues amazing godgiven gift, Floating Cloud Citys major families were on their best behavior in their presence. After all, i

However, that Xia Clan had let the citys most brilliant girl marry Xiao Che, a goodfornothing with no possible future. Who knows how many people regret that decision. Of course, there were more people with feelings of envy and jealous hate.

Since it was the Xia Clan marrying off a daughter, the spectacle was indeed not too shabby. As soon as Xiao Che went out, he saw a long line of red carpet that began from his doors entrance. This red carpet was the Xiao Clans starting point and it extended in twists and turns towards the direction of the Xia Clan.

As soon as the Xiao Clans wedding team appeared, Floating Cloud Citys street started to bustle with noise. The street was full with spectators on both sides of the street. As Xiao Che kept pace with the team, various whispers from the crowd entered his ears.

Look! That is the grandson of Xiao Clans Fifth Elder. I heard rumors that his profound veins are damaged and he would never in his life be able to breakthrough into even the first level of the Elementary Profound Realm.

Oh, this is the first time Ive seen him in person.

Its normal if you havent seen him before. With such a stubborn bull for a grandfather and the fact that he is a goodfornothing himself, would you even have the face to go out? Oh, for Xia Qingyue to actually be married to such a person, the heavens must really be blind!

It is said that his father Xiao Ying and Xia Hongyi became sworn brothers when Xiao Ying saved Xia Qingyues life after using most of his profound strength. Xia Hongyi then promised that on his daughter, Xia Qingyues sixteenth birthday, she would become Xiao Yings daughterinlaw. Not long after, Xiao Ying fell to an assassin. Due to his previous exertion, he was unable to fight back. The news hit Xia Hongyi like a meteorite and he felt extremely guilty. Now that Qingyue had finally turned sixteen today and although Xiao Yings son is a moron, Xia Hongyi was not willing to break his promise because of his heavy feelings of remorse and gratitude. If not for that, how could that guy even marry Xia Qingyue.

What! Xia Qingyue is our Floating Cloud Citys greatest treasure. If he did not have his status of being the Fifth Elders grandson, he could not even measure up to the mud on the floor. I am probably one hundred times stronger than him! This world is so unfair!

The goddess of my dreams is about to marry such trash, Id rather die than accept this fact! Ahhhhh!

The Xiao Che on horseback had bright serene eyes that conveyed a deep character. He had on an elegant expression and exhibited a light grace. His long hair fluttered behind his dazzling red wedding robe and his entire body emitted an air of elegance. The sounds of murmuring from the crowd contained all kinds of malicious undertones. However the envy, resentment, prejudice, ridicule, scorn, and disdain from the crowd seemed to not faze him at all. He seemed to be unconscious of the crowd as a beautiful smile was still plastered on his face. It was not known how many girls lost their hearts as their eyes misted over in adoration.

Although Xiao Ches profound strength was the lowest of the low, his looks were actually not that bad. He could even be said to be beyond Xiao Yulong. Coupled with the fact that he rarely went out due to his low profound strength, he looked very white and delicate. Just like a living doll!

So even if the countless youths hated Xiao Che enough to make their teeth itch, in the depths of their heart, they also had to admit that he looked as if he was truly worthy of Xia Qingyue.

I thought that Xiao Che would ride in the carriage today but unexpectedly he is riding horseback. With that temperament. it seems as if the rumors were not true.

Tch! The trash that people typically look down on is about to marry our Floating Cloud Citys greatest treasure. Of course he should be proud! How could he be afraid of losing face? rang a bitter voice.

I heard that Yuwen Clans young master as well as other young masters from other families are taken with Xia Qingyue. Do you think they will come to stop this procession?

Yeah right! Xiao Che is nothing but his grandfather is Xiao Lie. The experts of our Floating Cloud City all have to respect him in his presence. His son is already dead and he only has one grandson left. He tended to his grandson from the very beginning of his life. If anyone were to cause trouble, they would meet Xiao Lies wrath! Whoever dares to do that will lose his head! Moreover this is not a forced marriage, who would dare to cause trouble to incur the wrath of the Xia Clan? I estimate that right now, all the lovesick youths who would actually disrupt the procession are firmly locked in their own house.

The wedding procession went on in a casual manner, not too fast but not too slow. The journey of over five kilometers took nearly one and a half hours.

Brotherinlaw!!

As soon as he saw the Xia Clans main entrance, he heard a wild cry. A tall sturdy figure ran over to Xiao Che. This person was not that old, but he was at least two meters tall. His body was as strong as a bull and the ground trembled when he came over. As Xiao Che watched him approach, he gulped bitterly and spoke with wide eyes, Yuanba! I have not seen you for only a month, how could you already have grown that much taller yet again!!

This man. it was more accurate to call him a boy for he is Xia Qingyues little brother, Xia Yuanba. He became fifteen this year yes he was really only fifteen years old! However, if you take a look at his body, nobody would even imagine that he had just turned fifteen! Two meters tall. Xiao Che on his horse was the same height as Xia Yuanba standing up. Yuanba weighed over one hundred seventy five kilograms. This number was definitely not that high because Yuanba was fat, but was instead because of his big strong muscles. His muscles were of a dark tan metallic luster and broadcasted his astonishing strength. His profound strength was only average, at the fourth level of the Elementary Profound Realm, but his physical strength was a force to be reckoned with. He could fight on par with those at the sixth level of the Elementary Profound Realm.

Xia Yuanba was Xiao Ches best friendhis only friend. From a young age he had always called Xiao Che his brotherinlaw, and they often played together when they were children. The days before he turned eight, Xia Yuanba was actually a dark and skinny child. He was often bullied but ever since he turned eight, it was like he ate something wrong and had an enormous growth spurt. His height, weight, and appetite all soared and the increase in his strength was absolutely stunning. Now at the age of fifteen though his face had not fully matured since it was still quite childish, his size. was of a monstrous proportion!

After hearing Xiao Ches exclamation, Xia Yuanba rubbed his head in embarrassment, This. even I cannot help it. My father tells me to go on a diet everyday. However, letting myself starve and go hungry would be far worse than the feeling of being killed by another.

 Xiao Che became speechless. At that moment he was only fifteen years old. Once he became an adult he didnt dare to imagine that!

Xiao Che knew that Xia Yuanba had a huge appetite. Fortunately he was born in the Xia Clan. If he was born in an ordinary family, his consumption would dissipate their entire fortune.

Hehe, brotherinlaw, today you can finally become my brotherinlaw. Xia Yuanbas smile was honest because he had been happily looking forward to this day. In his opinion, with such a strong sister for a wife, nobody would dare look down on Xiao Che.

Hurry up and come in, my sister is already ready. He slapped his head, Oh, Ill go open the door.

After that, Xia Yuanba turned in the direction of the Xia Clan entrance and ran like a mobile mountain of meat.

The procession came in the Xia Clans entrance. At the door, Xiao Che saw a smiling Xia Hongyi. He quickly dismounted and stood before Xia Hongyi. He respectfully greeted, Uncle Xia.

Haha, after all this time you still call me uncle? Xia Hongyi laughed. His stature was not tall for he looked like normal slightly overweight middleaged man. Although if one were to look at him and the words simple and honest came to mind, nobody in the entire city of Floating Cloud would dare look down upon him.

Xiao Ches eyes brightened and politely replied, Fatherinlaw.

He had always been respectful towards Xia Hongyi for Hongyi was the sworn brother of his father. From a young age, he suffered through the scornful glances of many people but Xia Hongyi had always tenderly cared about him. Even though he was born with a crippled profound veins, Xia Hongyi had never violated the agreement he made with his father the agreement that Xia Qingyue would marry Xiao Che once she became sixteen.

Haha, good! Xia Hongyi nodded. He reached out his hand and patted Xiao Ches shoulder, Cheer, starting today, I will give Xia Qingyue to you. Although you are not a big hero, you are Xiao Yings son so I can have a peace of mind when I give you my daughter. Your father Xiao Ying was an amazing man. Becoming his sworn brother is something I would never regret in this lifetime for he was a passionate and righteous man. You are Xiao Yings son. Even though your profound veins are damaged, I do not believe that you will stay an ordinary person in the future.

Treat my daughter well. For those who talk nonsense and can only flap their mouths in malice, screw them all.

Xiao Ches eyes smoldered in anger. He firmly nodded his head slowly, Fatherinlaw, have no worries. Although people hold me in contempt now, once my profound veins are fixed, a sleeping dragon will be awakened from the abyss. I will make those people who look down on me and those who think that the Xia Clan took in a wastrel for their soninlaw obediently shut their traps.

Xia Hongyi was surprised at his outburst. He had always known Xiao Che to be weakminded with a mild temper and an unconscious inferiority complex. Xiao Ches fearless utterance, sharp eyes and calmness made him see Xiao Che in a brand new light one that was completely different from his previous attitude.

Good! Xia Hongyi nodded and patted Xiao Che on the shoulder again, I knew that Xiao Yings son would not be an ordinary child. I will wait for the day this dragon surfaces. Alright now, Qingyue is waiting for you go on.
',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(11,'chapter 5','Chapter 5  The Wedding Ceremony

Xia Qingyue appeared between the arms of two bridesmaids. She wore a red phoenix coronet on top of her head. A fine curtain of beads hung down from the phoenix coronet to completely cover her entire face while also hiding her current expression. Her soft and shiny black hair fell gently behind her shoulders. Her straight scarlet robe was decorated in the four happiness cloud pattern and the belt displayed her slender narrow waist nicely. On her belt hung a charm made of exquisite jade while pearls dangled at its tassels, matching her golden shoes. These significant details made her magnificent outfit even more dazzling than ever.

Xia Qingyue slowly came to Xiao Ches side in the arms of the bridesmaids and every step she made was light and elegant, as if she were walking across the clouds. An ordinary person would look like they were walking but if they were in her body, they would look like a fairy riding on top of a cloud. Her usual posture was already that beautiful and Xiao Che saw such a feast for the eyes.

Xia Qingyue finally came to the front of the carriage and the two bridesmaid walked away bowing backwards. In accordance to the Blue Wind Empires wedding tradition, the groom would bring the bride onto the bridal chair. Xiao Che stepped forward and stretched out a helping hand at Xia Qingyue. Xia Qingyue elegantly lifted her hand However, as Xiao Che took Xia Qingyues hands in his palm, a piercing cold energy unraveled onto Xiao Ches hand and his entire right arm stiffened in pain, halfimmobilized.

The biting cold sensation slowly disappeared as Xiao Che put his arm down with a silent indifferent expression. Aside from a frown between his eyebrows when the icy cold hit him, he did not make any other expression nor the slightest sound.

If one opened Xia Qingyues curtain of fine beads, one would see her beautiful eyes flash in surprise and then hastily become cold once more.

Xiao Che sat on the horse and the wedding procession went on with great strength and vigor. The Xia Clans wedding team followed in the direction of the Xiao Clan shortly after.

After another hour and a half, the procession returned to the Xiao Clans main entrance. This long journey was smooth and calm, to the disappointment of those who looked forward to the development of any drama.

Xiao Lie was already at the doorway and stood to welcome their guests. Sadly, the number of people who came for Xiao Che can be counted with one hand. Most of the guests came for Xiao Lie and the Xia Clan. With Xiao Lies fame and Xia Clans connection, many guests were seen. Outside the entrance of Xiao Clan house, people who came to see the event was next to infinite, the streets were so filled up that not even a water droplet could get through. All these people came for the marriage of the number one beauty in Floating Cloud City.

Xia Qingyues bridal carriage slowly stopped in the middle of the noise. A corner of the curtain was opened as her maid Xia Dongling gently spoke Miss, we have arrived.

Afterwards, a hand reached out and Xia Dongling gently lowered her arms. As soon as she got out of the carriage, the deafening atmosphere suddenly quieted to replaced by deep breathes that followed one after another.

It was almost noon. The soft sunshine reflected against her phoenix coronet while her robe charmingly glittered against the breeze making ones eyes blur if they look too long. Her hair was wrapped up high into a bun on top of her head and the phoenix coronet that framed her head was that of four layers. The top layer was adorned with fine golden hairpins while the bottom had several golden phoenix engravings. The satin red golden phoenix coronet extended into a fringe of swaying pearl tassels. Although her face was not laid bare, with her eyes and lips partly hidden yet partly exposed, her beauty was exquisitely flawless.

The sound of irrepressible breathing overlapped against one another as many people stared straight ahead, unable to snap back into reality. That is the power of Xia Qingyues charisma, for this all happened without the revealment of her face! Based on her aura and posture alone, she still seemed like a fairy that had walked out straight from a painting. Her beauty was so immense that none could take their eyes off her.

Xia Dongling wrapped a red silk sash around Xia Qingyues hand. Naturally, the other end of the sash was tied to Xiao Ches hand. As he got off the saddle, Xiao Che smiled as he walked over to lead Xia Qingyue across the brazier. They cruised over the doorstep of the Xiao clan and stepped directly into the hall.

As they entered through the main entrance of the Xiao clan, the noise did not abate. Xiao Ches expression did not change as he kept pace but he naturally wanted the wedding to end as soon as possible.

This was Xiao Clans center hall for important meetings. The only people who were allowed to use this place for a wedding are the leader of the Xiao Clan and its Elders. For this wedding the room underwent a large scale transformation. It was truly a vision that should be seen. As far as one could see, the pillars were all embedded with yellow topaz and the walls had been painted with dragons that were inlaid with rare precious pearls. A red carpet spread across the center of the hall in a straight line and stopped at a short golden staircase. A subtle golden light filled the atmosphere, making the already beautifully decorated hall more dazzlingly eyecatching. The Xiao Clan was not willing to invest such a large amount of money for Xiao Che and mostly came from the Xia Clan. Xia Hongyi was willing to spend as much as needed for his darling daughters wedding.

Xiao Lie and Xia Hongyi sat on the highest seat of the hall, smiles plastered all over their face as they watched Xiao Che and Xia Qingyue enter. On both sides of the red carpet were three rows of rose sandalwood chairs, all already filled with people. Xiao clans leader, Lord Xiao Yunhai was there as well along with the other four Elders of the Xiao clan. As Xiao Che walked in with a happy smile, their expressions stilled on the surface but in the depths of their heart, they sneered in disdain.

The Xiao Clan was a clan that practiced in the ways of the Profound for many generations. To have Xiao Che with a crippled Profound Vein born in the clan was a shame for the Xiao Clan. If he were not the grandson of the Fifth Elder, Xiao Lie, he would have already been expelled instead of remaining inside like he was now. And if it werent for the fact that he was about to wed the highprofile daughter of the Xia Clan, they would not even have bothered to show up in person let alone be present at the scene.

Regarding Xiao Che, if they hear his name, they could only think of the word trash and not pay attention for they do not even remember how he looked like. In the Profound Sky Continent, one did not deserve dignity if one does not have the skill to back it up. It was a harsh reality and a universal truth followed by many, even towards members of the same clan.

The expressions on the faces of the younger Xiao generation were unsurprisingly consistent as well. They all had their sights on Xiao Qingyue and their eyes belied their insuppressible infatuation. As their eyes transferred towards Xiao Che, their eyes almost erupted with naked envy. The Xiao Clans outer disciples had always looked down on this permanently disabled person and never in their dreams thought that he would actually marry Floating Cloud Citys unattainable number one treasure. The uncomfortable feeling that those two people in the marriage hall evoked was similar to eating dead flies raw.

The person in charge of the wedding was logistics manager Xiao De. The wedding ceremony began at the cry of his voice.

The master of ceremonies first introduced the bride and groom and then read names from a list of distinguished guests that have came to visit. As he went on, Xiao Ches expression remained neutral but in his heart were clashing waves of water. Whatever else the master of ceremonies said after that was something Xiao Che could not bother to listen to for he was repeatedly ruminating over a problem that he actually care about.

What was that sudden weird feeling he felt at Xia Clan when his hands met with Xia Qingyues? Was it some kind of Profound power? But he had never heard of such a mysterious power in Floating Cloud City. For Xia Qingyue to reach the 10th level of the Elementary Profound Realm at the age of sixteen was truly an amazing feat. but she was still at the lowest point of the 10th Elementary Profound Realm how was it possible that she could soundlessly release such an ice cold aura that actually completely immobilized his entire arm? What was that astonishing mystifying power which was displayed at such a level?

Or. was this the hidden strength of the Xia Qingyue who had reached the pinnacle 10th level of the Elementary Profound Realm?

The voice of the master of ceremonies stopped. After a brief pause, his voice became eight octaves higher

First bow to heaven and earth!

Xiao Ches mind rapidly rotated as he glanced at Xia Qingyue through the corner of his eye while bowing in the direction of the doorway, to the heavens and earth.

Second bow to the elders!

The two people made an 180 degree turn towards the seated Xia Hongyi and Xiao Lie and bowed. Xiao Lie firmly nodded and smiled lovingly at Xiao Che and his new granddaughterinlaw. Xia Hongyi also smiled and beamed in content.

Husband and wife, exchange bows!

Xiao Ches body turned towards Xia Qingyue and at the same time Xia Qingyue also turned to face him. This swift action that did not withhold any hesitation nor delay made all members of the younger generation of the Xiao Clan clench their teeth. In their thoughts, they believed that Xia Qingyue did not willingly enter this marriage with that crippled bastard but was instead forced to come by the Xia clan. To their disappointment, even up to this moment, there was no resistance at all from Xia Qingyue. There werent even rope lines of her supposedly failed escape that they imagined to have happened.

The two people bowed and as they both bent towards one another, Xiao Che saw cold eyes peeking through the gaps of the bead curtain. cool icy eyes that revealed no traces of emotion.

Usually at moments like this, the crowd would burst into enthusiastic applause, loud laughter and cheers. But only a few people authentically clapped for the married couple, it was quite awkward.

Indeed the Fifth Elder should be congratulated. The First Elder Xiao Li who was sitting next to Xiao Yunhai said with a cynical strangeness in his words.

I congratulate you in representation of all the Five Elders well wishes. Even the First Elder Xiao Li who was sitting next to Xiao Yunhai heard the strange cynical scorn in his words.

Second Elder Xiao Bo laughed with the same strangeness that came from the leader of the Xiao clan earlier and slowly continued Fifth Elder, getting such a talented granddaughterinlaw has added glory to the Xiao Clan. As for the Xia Clan getting such a soninlaw, haha, is also quite acceptable. Congratulations.

The atmosphere in the hall immediately cooled. If one were not an idiot, they would clearly hear the apparent irony in the words congratulations that came out of their mouths.
',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(11,'chapter 6','Chapter 6  Peerless Beauty

When Xiao Ying was alive, Xiao Lies position in the Xiao Clan was second to none for even the leader of the Xiao Clan at that time was respectful towards him. There was a clear reason Xiao Yings talent at that time gave him the potential to be one of Xiao Clans strongest experts in the future. In this world that respected those with power, as Xiao Yings father, Xiao Lie was held in high regard. However, after Xiao Yings death, Xiao Lies only grandson was born with a damaged Profound Vein. Although he was the strongest in Floating Cloud City who would fear him? His son was dead, his grandson disabled, and he had no other successors. His position in the Xiao Clan was now suffering a disastrous decline.

Xiao Lie did not get angry for he had already become accustomed to these cynical jabs of wordplay. With an indifferent smile, he spoke Thank you all for personally coming today. Be sure to drink more than a few cups of wine to celebrate this event.

I have already given you face by personally coming here today so I do not need any wine. My grandson Xiao Chengzhi has now reached the 7th level of the Elementary Profound Realm. I have been here for quite a while now, I must personally go stabilize him. Third Elder said as he stood up.

(TL Give face  give somebody respect especially when in public)

Chengzhi has already reached the 7th level of the Elementary Profound Ream? To be only seventeen and have such success, his future is truly limitless. No wonder you were positively glowing today, that is surely gratifying! The other four Elders rose to congratulate him with a surprised look on their face.

Although well disciplined, Xiao Lies face had a look of condensed anger. His four brothers had always showered him with respect but ever since Xiao Yings death and the confirmation of Xiao Ches damaged Profound Vein, their attitude changed for the worse. Basically, they do not bother to show respect for him anymore. They typically bragged about their own grandsons in front of him but now, in his own grandsons wedding hall, they still fearlessly flaunted their own grandsons achievements. His brothers used their own grandsons success to rip out the deepest scars in his heart.

All of a sudden, the atmosphere that could make one sweat bullets shifted. The master of ceremonies, Xiao De, hurriedly tried to speed up the wedding process and exclaimed in a squeaky high voice The bride and groom, go into the bridal chamber. distinguished guests, please go to the banquet.

In the midst of the sound of gongs and joyful drumbeats, the couple who many were watching finished their rituals of worship and started to walk towards Xiao Ches small courtyard. The bridal chamber was the room where Xiao Che usually lived in. It had been decorated entirely in red. The carpet on the floor was finely embroidered with a dragon and phoenix that flew upon clouds, an auspicious symbol for a blissful marriage. The room full of red silk also contained a large double happiness sign and two red candles were shining brightly between a golden lamp. The dragon and phoenix engraved on the candle seem to sway in the twinkling light. The flickering candle touched upon golden glaze curtain and seemed to fill the room with a dreamy hazy color. It was isolated from the outside world and gleamed in such a way that would make ones eyes soften.

Xia Qingyues maid, Xia Dongling, escorted Xia Qingyue to sit in her bed and then soundlessly stepped out of the room while closing the door. The room became silent and they could only vaguely hear the soft sound of their breathing.

Xia Qingyue sat there quietly, soundless and motionless. Xiao Che did not approach her but instead stood near the doorway and stared past it with a shadowy look in his eyes.

Your grandfather was disrespected during your own wedding, you must be really upset, right?

A soft clear voice entered Xiao Ches ear and his expression changed. Although Xia Qingyues words stung his ears, she left him in awe for she took the initiative to talk to him.

Xiao Che glanced to his side and hesitantly spoke You can take down that phoenix coronet now. That thing is too heavy and if you wear it for too long, it would become uncomfortable.

According to the wedding traditions of the Profound Sky Continent, the groom must take off the coronet for the bride. A while back, as he was trying help her off the carriage, she stung his hand with her mysterious cold aura. Xiao Che was too prideful to touch her again because he was fearful of that happening again. Besides, he did not think the cold Xia Qingyue would even be willing to accept his offer if he were to actually try to help her with her coronet.

After a slight pause, Xia Qingyue raised her hands and silently took off the phoenix coronet. At that moment, a beautiful breathtaking face appeared in Xiao Ches line of sight. As she raised her charming eyes and meet Xiao Ches in contact, he immediately became stunned. A pair of indescribable magnificent eyes met his. It was as if the worlds essence laid deep within her eyes. Not even the worlds most brilliant painter nor the most precious of words could accurately portray her beauty. Her jadelike skin and creamy face was as white as snow under the dim lights in the room. Her lips were like the worlds most delicate petals and her nose was of the most beautiful of sculpted white jade, high and proud with an innate nobility.

You reputation precedes you. Xiao Che murmured, measuring her with his unblinking eyes. A pair of beautiful eyes peered back at him. An endless gravitational abyss drew his every attention and thought, making it difficult for him to move his eyes elsewhere.

Although this was an arranged marriage decided since they were born, aside from the occasional glimpses he took when he was young, this was actually the first time he truly saw Xia Qingyue since the age of ten. This was because Xia Qingyue rarely left her home and the disabled Xiao Che had low selfesteem that evolved into an inferiority complex. He only heard about Xia Qingyue from the hearsay of others and after hearing that Xia Qingyue had grown up to become a peerless beauty, he pictured her in his heart.

The shadowy figure in his illusions faded as he looked at Xia Qingyue realizing that he was in the face of true beauty. Xia Qingyues allure was beyond his imagination for he could not describe this peerless beauty. The Xiao Che with clear memories of two worlds could not help but forget his thoughts when faced with such a sight.

Xia Qingyue was called the number one beauty in Floating Cloud City but if anyone were to say that she was the one number beauty in Blue Wind Empire, Xiao Che would not dispute that claim. He could not think of anything that would surpass this beautiful spectacle before his eyes. The girl in front of his eyes was yet only sixteen girls at that age have not fully blossomed but it is impossible to imagine what would happen to Xia Qingyue after a few years.. perhaps she would reach a transcendent level by that time.

A girl whose every movement and smile that could move the world was born in little Floating Cloud City. And this girl has become his wife today. Xiao Che could not help but question the authenticity of his reality.

And you are not what the rumors say nor what I had imagined as well.

Xia Qingyue rose and her touching curve of her body was exposed as she approached Xiao Che. Her eyes shone like water as she slightly parted her lips Rumors say that your vein was disabled so you can only stay at the 1st level of the Elementary Profound Realm. Because of that, you have a weak physique and low self esteem since you only stay at home. Your only friends are your little aunt Xiao Lingxi and my younger brother Yuanba. The only thing on your body that can be regarded as an advantage would be your appearance.

Your Profound aura is not only weak and murky but your Profound Vein is indeed disabled. However, your personality is completely different from what the rumors claimed you to be.

Xia Qingyue stopped at a distance of three steps before Xiao Che as she stared straight at Xiao Che with her beautiful eyes Although you deliberately hide and pretend your true character, I sensed your arrogance ever since I laid my eyes upon you. You are completely the opposite of those rumors. Earlier at my place, I froze your hand with my Profound power but your calm reaction surprised me. If your hand wasnt so still, I would have doubted that I failed to use my power. In the wedding hall you and your grandfather Xiao Lie were mocked during such an important occasion but it only looked like you were angry for a quick second and then your anger disappeared. Your expression and heart rate showed no signs of abnormal fluctuations at all. It is difficult to achieve that mental state of mind even for a middle aged Profound Spirit practitioner!

When you look at me, there is an obsessed glint in your eye that has not diminished even now.

Your disabled Profound Vein is fact but your true personality and state of mind has fooled everyone. Xia Qingyue whispered, as her boundlessly deep eyes stared at Xiao Che.

Xiao Che startled in surprise.

With Xia Qingyue that close to him, one would usually be in ecstasy when in the presence of the perfume of her floral scent but Xiao Che could not be bothered to be moved by that. His heart had completely went in shock as he stared back at her in awe. It was true, he was indeed a proud person regardless of who he was in the presence of because back when he was in the Azure Cloud Continent, he was one of the proud warlords there. Anyone living in Floating Cloud City at the moment was not on par with those legendary warlords of his time. Although his strength went through an extreme decline, his state of mind stayed the same. He did not deliberately set out to appear that way but it was the natural attitude of his previous life. However, he had to keep his prideful self hidden because of his current lack of strength and situation.

What Xia Qingyue said had perfectly described his personality in one go!

On the road from Xia Clan back to Xiao Clan, he always thought that she held him in contemptuous disregard for she was a heavenly treasure and he was the mud on the floor that people despised. It was a typical common thought. But he had never been aware that she had been watching him during their journey back. Xiao Che suddenly saw Xia Qingyue in a new light as he looked into her extremely beautiful eyes. Those clear eyes that saw through his mind and heart in meticulously fine detail left him in shock.

You know! This Xiao Che with two memories of experience was a person that could not be moved in the face of all the worlds heroes and even in the face of death! Yet she already saw him through!

Was she really only a girl who had just turned sixteen!?

How can this sixteen year old girl possess such a monstrously keen eye and mind!

Xiao Che had a suspicion that maybe Xia Qingyue was like him, a person reincarnated into this world!

Are you enduring it? Xia Qingyue asked after a pause.

Enduring it? Xiao Che let out a seemingly selfdeprecating laugh Perhaps. The truth of my damaged Profound Vein wont change. In the Profound Sky Continent, living a life with a disabled Profound Vein has made people look down on me like I am the scum on the bottom of their shoe. Is there a difference between a cowardly selfblame and a silent endurance?

Enduring it? More like he was tolerating it! Yesterdays Xiao Che was exactly as what the rumors had said he was! No matter how clever Xia Qingyue was, she could not have thought that todays Xiao Che would have another lifetimes worth of memory. His temperament and mental state had also changed along with it.

Xia Qingyues beautiful eyes narrowed as she suddenly lifted the jadelike palm of her hand and stretched out two fingers to touch his chest. Suddenly, a cool but not entirely cold aura hit Xiao Che in the chest and spread throughout his body. As Xiao Che was about to ask her what she was doing, the cold feeling instantly disappeared and Xia Qingyue opened her lips that were like the soft petals of a flower Your Profound Vein is indeed disabled, but it is not a congenital deformity. You were probably attacked when you were very young and someone directly destroyed your Profound Vein!


',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(11,'chapter 7','Chapter 7  Frozen Cloud Asgard

Was destroyed? Xiao Che frowned. Ever since he was a child, his grandfather and everybody else all said that he was born with a damaged Profound Vein. He even believed that himself when he got his memories of rebirth back since his memories included the mastering of all the medical books in during his lifetime.

But Xia Qingyue said he wasnt born with a damage Profound Vein but instead it was damaged by an external force.

In truth, no one in the Xiao Clan had been able to see through this guise. Xia Qingyue had only looked at him within a few breaths of time and to her, that fact was as clear as crystal.

This woman.

Yes. Xiao Qingyue creased her eyebrows and gently spoke It was heavily damaged during your childhood and your family did not notice. Because of that, it was never repaired and now that you have already grown up the damaged Profound Vein has fully formed into its disabled state and lost all hopes of repairment!!

Xia Qingyue said that last statement with absolute confidence. If an adults Profound Vein became damaged, their power would leak but there were a variety of methods to repair that.

However, if the Profound Vein broke in the early stages of infancy, the growth of the vein would start from a bad foundation and will only become worse. At Xiao Ches age, the vein had already become badly shaped so it was impossible for it to be repaired back to normal.

Xiao Ches expression did not change and simply said That may not necessarily be true.

Xia Qingyue gently inclined her head to glance at him Looks like you always had the notion of repairing your Profound Vein?

Ill definitely fix it. Xiao Che said blankly.

Xia Qingyue looked at him deeply. She saw not only confidence and arrogance but a deep layer of coldness in his eyes. She sighed in her heart and softly spoke The Profound Sky Continent is a big place with numerous amounts of talented people, perhaps there really may exist a person who can repair your damaged vein. I should not have said what I had said earlier with such assertion, you can just take it as my ignorance.

In those few words, Xiao Ches impression of her improved dramatically. He hesitated, then asked The icy cold power you used earlier, what was that? I have never heard of anyone in Floating Cloud City with that skill. Your master is not a person from Floating Cloud City, am I right? Of course, its up to you whether or not you want to tell me.

Xia Qingyue was silent for a while and right when Xiao Che thought she would not answer, she calmly replied Frozen Cloud Arts.

Frozen Cloud Arts? Xiao Che slightly shivered at the name as he felt a vague sense of familiarity. When he suddenly remembered the name of the concept, his paled and words uncontrollably escaped from his mouth Frozen Cloud Asgard !?!?

Xia Qingyue tilted her beautiful head and looked at Xiao Che with a surprised expression. When he shouted out Frozen Cloud Asgard, Xiao Che had already lost his cool but in her eyes, he was still too calm. Even if the Lord of Floating Cloud City heard that name, his body will weaken and his legs would tremble in fright. She lightly said My master is indeed from Frozen Cloud Asgard so I can be regarded as a disciple of Frozen Cloud Asgard. This fact, the only person in Floating Cloud City who knows of that is my father. You are the now the second. I tell you this. because you are my husband now and this is the most basic of my respect for you.

 Xiao Ches racing heartbeat could not calm down. The words Frozen Cloud Asgard burst into his heart like a huge bomb. Because it was the most powerful out of Blue Wind Empires four major Sects and also the holy land of that many yearn and long to be in. Even the Blue Wind Empires royalty must worship their transcendent existence annually!

Heavenly Sword Villa, Frozen Cloud Asgard, Xiao Sect, Burning Heaven Clan.

Out of the seven countries in Profound Sky Continent, the Blue Wind Empire was currently the smallest. Although they had the lowest overall strength, they have never been annexed by the other countries. A large part of the reason why was because of those four major sects. Those powers that be are not afraid of the Blue Wind Empires strength but instead fear the four major sects.

There is no doubt about the strength of those four major sects. The process in which they recruit disciples was very strict and they did not care about background in their selections. The most important thing they want in the end was talent. Everyone living in the Blue Wind Empire dream and aspire for that kind of great fortune. If you get into one of these major sects, even the lowest of disciples will bring honor and prosperity to their family. They would even be the guests of honor at court and be presented with the rank of a marquis.

No one of this little Floating Cloud City had ever heard of anyone being admitted into one of those major sects and no one would even dare dream about it. If the previous Xiao Che heard the names of the four major sects, it would be like hearing the existence of the heavenly sky kingdom. He would never have expected to be touched by those words. but he did not expect that the wife he had just married was one of the disciples of the top ranked sect, behind Heavenly Sword Villa, Frozen Cloud Asgard!

Xiao Che quickly calmed himself down and asked  Since you are one of Frozen Cloud Asgards disciple, why dont your family reveal it to the public? With your identity, Xia clan can freely walk through anywhere in Floating Cloud City and nobody would dare offend them. All the rich people, including the citys main clan will seek to curry favor from you. The growth of your Xia clan will also develop at a rapid pace.

Because of you. Xia Qingyue answered.

Because of me? Xiao Che stilled in silence Those three words had reminded him why.

I married you with the identity of a girl from Xia clan and the entire city of Floating Cloud is in an uproar. If I married you with an Frozen Cloud Asgards disciples status, not only our small city of Floating Clouds would be in an uproar. No matter what, there would be a lot of unpredictable consequences between you and my Xia clan. After all, the difference between you and I is far too vast. Xiao Qingyue said in a light voice with clear eyes. Although she only stood there silently, she was already brilliantly dazzling.

Xiao Che slowly let out a sigh Then why did you marry me?

You know precisely why. My life was saved my Uncle Xiao Ying. I am the reason why he died during an assassination attempt soon after. Since I was born, my father arranged a marriage between us in order to repay Uncle Xiao Yings kindness. Although it was my fathers promise, I have no reason to disobey him and thus I have no reason to not marry you.

Xia Qingyue raised her glittering cold eyes The reason why I told you that I belonged to Frozen Cloud Asgard was to let you know that in order for me to continue to practice the Frozen Cloud arts, my heart must be like a frozen cloud. Frozen Cloud Asgard only accept women and they must be pure and uncontaminated for life. Although I did marry you, I cannot love anyone in this lifetime. You must understand my commitment.

Even if you arent a disciple of the Frozen Cloud Asgard, I dont think you would fall in love with me anyway. Xiao Che said with a selfdeprecating smile.

Xia Qingyue slowly shook her head Maybe you have misunderstood me. I have never despised you nor have looked down on you. My master had told me many times that people of a higher level should never overlook someone else. Nor underestimate them. Besides, I have just reached the starting point of my journey up. The Profound Sky Continent respects the strong so there are countless doctors around. Just because your Profound Vein was broken does not mean that your life is completely ruined.

Xiao Che was moved. Floating Cloud City bragged about Xia Qingyues beauty and talent but perhaps nobody knew about the elegance in her state of mind that not even the countless number of middleaged people in this world can achieve.

And she was really only sixteenafter a few years, one could not imagine how she would be. No wonder she caught the fancy those from Frozen Cloud Asgard !

This woman with such beauty, talent, and mind of a fairy had really just became his wife not too long ago! It felt like that was only just a dream!

If he did not have two lives worth of experience and memory, he would feel a sense of inferiority and could probably not even muster up the courage to look her in the eye.

Thank you for telling me this Xiao Che said with a passionate sigh. His eyes then became focused as his voice changed Then, can you tell me the level of your current profound strength?

Entering the tenth level of the Elementary Profound Realm at the age of sixteen was talent amazingly enough to stir the entire city of Floating Cloud. However, Xiao Che did not believe that Xia Qingyue was only at the tenth level of the Elementary Profound Realm anymore. Because this unrivaled level of talent in Floating Cloud City should not catch the eyes of those of Frozen Cloud Asgard.

Xia Qingyue became silent and did not answer Xiao Ches sudden question. However, her silence had indicated that her strength was higher than that of the tenth level of the Elementary Profound Realm.

You should go to the toast. Xia Qingyue slowly replied with a glint in her eye.

As her voice fell, the sound of slow footsteps came from outside the door. Xiao Hongs old gentle voice came from the door Young master, it is time for you to propose the toast.

Grandpa Hong, Ill be right there. Xiao Che promised. With one last look at Xia Qingyue and after patting his clothes of any dust, he walked out of the room.

As soon as Xiao Che left, a bright ice aura surrounded the room and a white figured dreamily appeared in front of Xia Qingyue. Xia Qingyue lightly stepped forward and spoke with a gentle and respectful voice Master.

Qingyue, do you wish to return to Frozen Cloud Asgard with me?

The beautiful gentle voice was as misty as a cloud and as gentle as a breeze. It was enough to make even the worlds coldest heart completely melt.

Xia Qingyue gently shook her head Master, Qingyue intends to stay a while. If I leave as soon as I got married, he will suffer even harsher of endless ridicules. I ask master to give me a months time so I wont hurt him nor allow others to make fun of him by the time I leave.

The woman dressed in white looked at her. After a while she nodded slowly and smiled faintly Alright. It is the first time in hundreds of years that a Frozen Cloud Asgard disciple was allowed to be married. Since it had been an exception, another months worth of wait is nothing.

Thank you for fulfilling my wish, Master. Xia Qingyues body lowered again as she hesitantly whispered softly Master, is it really impossible to repair his Profound Vein?

The woman in white shook her head without delay There are no such things as absolutes in this world. but at least to me, it seems to not be possible. Qingyue, it is good that you are a kind and compassionate person with a giving heart. However, I really cannot help you with this issue.
',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(11,'chapter 8','Chapter 8  The Wedding Night

Xia Qingyue did not ask again. Since even this whiterobed woman with such high pedigree and exalted status was sure that it was not possible, then there should not be even the smallest chance of it being possible.

Qingyue, I know you are eager in repaying your debt of gratitude since your life was saved at a young age, to even go as far as delaying your return to Frozen Cloud Asgard, but you marrying him should already be enough to repay that obligation. When you return to Frozen Cloud Asgard, your identity will be revealed. Although he may suffer more ridicule after your departure, his status of being the husband of a Frozen Cloud Asgard disciple would still be intact. At least in this little Floating Cloud City, with that kind of prestigious status, no one would dare to cause him physical harm. The lady in white said with a comforting tone of voice.

Xia Qingyue softly nodded I hope so.

His Profound Vein is disabled and he also has no other strengths. He will never be able to achieve any accomplishments perhaps in his entire life. But you are beautiful and smart. Talents of your nature are born once every hundreds of years. Otherwise, our Mistress would not have let you break the rules like that and get married. Marrying you was his greatest fortune and luck in his entire life. You taking this step is justice enough. If his father was still alive and smart enough, he would have cancelled this wedding I have to go. I will pick you up a month later. During this period, I will not go anywhere far. If you encounter any unresolvable problems, write me a letter to keep me informed.

I bid teacher farewell.

The white robed womans chin turned around. Suddenly, a beautiful face with a hint of coldness on it was seen. She wore no make up for her skin was as smooth as a snow white piece of jade. People cannot help but think of the terms a beauty with flesh of ice and bones of jade and a face of snow with lips of pearls to describe her as they laid their eyes upon her. Her facial features were as exquisite as they were perfect. She was charming in a way that made people unable to even dare look at her. One would think that she was as holy as she was lofty when gazing into her eyes. She was like a fairy who had ascended above Nirvana, untainted by anyone of the human realm.

She opened the window and her body slightly trembled. As if accompanied by an invisible cold ice spirit, she looked as if she had dissolved right at the place she once stood.

Xiao Clan Main Hall, a full Guest House.

Seventh Uncle Liu, please have a drink. Xiao Che respectfully presented a cup in front of a gentle looking middleaged man.

The person who was called Seventh Uncle Liu stood up as he started to laugh. He raised his cup and drained it of its contents. He spoke with laughter My nephew, I was best friends with your father, now that I see you have formed your own family and have married to such a good wife, my heart is happy for you.

Thank you Seventh Uncle Liu.

First Elder, please have a drink.

Xiao Clans First Elder Xiao Li took the cup and gulped the entire mouthful. He then heavily slammed the cup of wine onto the table. During this entire process, aside from saying hmph through his nose, he did not say a single word nor look Xiao Che in the eye. Even with that attitude, drinking Xiao Ches cup of wine had already expressed the large amount of face he gave Xiao Che.

Xiao Che did not speak as well and moved to the next table. Just as he walked two steps, Xiao Li spit on the floor and opened his mouth as he spoke in a cold harsh tone of voice within Xiao Ches hearing range Such a delicate flower has now been inserted into poop. Bah!

Xiao Ches expression remain unchanged. His consistent footsteps had not paused and it was as if he had not heard him. That was only if one did not look closely, for his eyes hardened and a deep cold condensation hid behind them.

He came over to the Second Elder, Xiao Bos side. Xiao Che bowed slightly Second Elder, Xiao Che offers you a cup.

Xiao Bo did not bother to look Xiao Che in the eye but he opened his mouth to lightly say Yang dear, help me drink it.

Yes, Grandpa. Xiao Yang said without hesitation. He took Xiao Ches offer of wine and drank it all, only making a guru sound.

A cup of wine presented to an elder but drank by his children did not show only contempt anymore. It was a sort of public humiliation. After drinking the wine, Xiao Yang put the cup down and sat back on his seat, his eyes filled with undisguised scorn and ridicule.

Xiao Che did not say anything again. He only nodded slightly and moved on to the next table. Just like before, as soon as he took two steps, a cold grunt was heard Hmph, trash is trash. Even if trash climbed into Xia Clan, it is still trash. Xiao Lie that old bastard is actually going to depend on his granddaughterinlaw? Bah!

The voice contained deep disdain, sarcasm, and of course jealousy. Even if one did not mention the wealth of the Xia Clan, if one can still brag about Xia Qingyues amazing talent. If she had not been married to Xiao Che but instead to his grandson Xiao Yang, his laughter that came from his dreams may actually become audible.

Xiao Che pretended to not have heard and walked away smiling.

Xiao Che finished his toast and sent out the guests. The long night of banquet was over. During this entire process, the people who displayed genuine heartfelt congratulations and best wishes were so few that Xiao Che could count them on his ten little fingers. Countless people were very polite towards him, after all today was his wedding day, but he clearly saw contempt in almost all of their eyes. Some people sighed, some people were jealous with anger. The rest had unconcealed scorn and other negative thoughts like trash and worthless written all over their faces.

His Profound Vein was disabled so it was a fact that he would not achieve any greatness during his entire lifetime because of that. So they dont have to be friends or be polite with Xiao Che. They did not care if they offend him because even if he got offended, he could not do anything against them with his disabled Profound Vein. Before this wastrel they could be unscrupulous and not care, readily dripping in the show of their superiority. They felt strong as they look down on this person who would never become anything but one who was weaker than them.

That is the ugly reality of human nature.

Take an early rest. Xiao Lie patted Xiao Che on the shoulder with a gentle smile on his face.

Xiao Che did not know what was hidden under his grandpas smile at this time.

As Xiao Lie became older and older, his temper had become more and more gentle. But when he was young, he was like a candle that could easily be ignited. If someone made him upset, he would make that person ten times angrier and no one dared to provoke him. Xiao Che knew all too well that his grandfathers temper did not become soft due to old age, but was instead because of him.

In order to protect his useless grandson, he had to become kind and soft. Even if they were looked down upon, as long as the bottom line had not been crossed, he would endure it as best as he could. This way, there wouldnt be any enemies that would come back to get revenge on his grandson after he had passed away.

As the most powerful person in the Floating Cloud City, the Fifth Elder that everyone used to be afraid of, was now not respected nor feared by the other Elders anymore. This was true in the case of the younger generation as well.

Watching Xiao Lies back, images of disrespectful and laughing faces appeared in Xiao Ches mind. Xiao Che clenched his fists slowly as they gradually became pale. His eyes sharpened and radiated an image of an icecold blade. Subsequently, the corner of his mouth slowly opened and revealed a smile that would make ones hair stand on its end.

Xiao Che was definitely a vengeful person. As someone who held long grudges, in the last six years in Azure Cloud Continent, his heart that was filled with hatred remembered everything. He remembered everyone who had been nice to him but also all the people who had not been nice as well. He kept it in deep in his heart until it was time to seek revenge for even the smallest of grievances.

You guys will. be sorry.

A deep guttural noise slowly overflowed from Xiao Ches mouth like a vicious curse.

Since God gave me this chance to be another person, how could I let my Grandfather and I suffer through this bullying!

Back in his little courtyard, the moon was still hanging high against the sky. Xiao Che walked to the corner of his yard and stretched out his left hand. Suddenly, water arrows shot out from his palm.

During todays wedding, he could not avoid drinking a lot of wine. In the end, he drank so much that it looked as if he could barely stand on his own. In reality, he was actually clearly awake. This was not him having a high capacity to hold his liquor but was instead because of the Sky Poison Pearl. All the wine he had drunk transferred into the Sky Poison Pearl. Since the pearl had become one with his body, he manipulated it as if it was like his own body.

The sound of hua lala was heard for a long time until all the wine had been removed from the Sky Poison Pearl. Xiao Che lifted his winecovered left hand and smirked. He rubbed the wine directly onto his face and held his breath until his face turned red. Stumbling, he pushed his bedroom door wide open as he wobbled to his left and right, as if he was drunk.

The door was pushed open and the smell of wine followed Xiao Che as he staggered into the room as if he were about to trip any second. He awkwardly lifted his head and looked at Xia Qingyue. Xia Qingyue sat on the bed with her beautiful eyes closed. It was extremely quiet. The dim candlelight flickered across her beautiful soft face, adding a bewitching mystical feel that one could not possibly resist.

Xiao Ches eyes brightened and his feet shook as he walked towards Xia Qingyue Hehehehe, my wife I have let you wait for a long time lets go we can now use the bridal chamber

Xia Qingyue suddenly opened her eyes and casually waved her right hand.

An irresistible cold force suddenly swept up Xiao Che and pushed him out of the door. Xiao Che fell on his bottom and had almost knocked over the stone table in the courtyard.

Xiao Che was in pain and rubbed his butt. It took him a lot of effort to get up and then he angrily roared Damn! I was only joking, you dont have to be so ruthless! I am so feeble yet you hit me as hard as you could anyone would think that you are planning to murder your husband.

The door slammed.

Xiao Che pushed forward but discovered that the bedroom door was firmly shut.

Xiao Che suddenly became depressed This woman, dont even even speak of flirting, even jokes are taken so seriously! Can I really live happily this way?

I was really only joking Besides, I am only at the lowest 1st level of the Elementary Profound Realm. Even if I wanted to do something to you, that would be impossible.

Xia Qingyue did not respond.

Xiao Che stood at the door for a long time but the door showed not the slightest sign of opening. Xiao Ches small courtyard only had one housing. There was no point in mentioning if he had other rooms for he did not even own a work house nor a stable. If it was a normal day, he could sneakily slip into his little aunts place to sleep. But tonight was his wedding night, so it was not appropriate for him to sleep anywhere else.

As a cold night wind passed by, Xiao Che shivered and it looked as if he had become smaller. He knocked the door again and weak spoke Hey, youre not really letting me sleep outside, are you? You should know that there are many people in the Xiao Clan who would want to lay their hands on you. They are very upset that tonight is our wedding night. They have realized that someone as talented as you are, would not let me touch you even if we are married, so they must be waiting for something to happen for an excuse to laugh at me. If they come over and see that I was locked outside, I would forever become a laughing stock.

No matter what, I am still your husband. Do you really have the heart to helplessly watch me get laughed at?

The room was still completely silent. Right when Xiao Che had the notion of kicking the door, the closed door finally slowly opened.

Xiao Che rushed in as quick as lightning and shut the door with a bang.

Xia Qingyue remained in bed with the previous attitude from before. Although she was just sitting on the bed, she emitted a hazy noble elegance. Her beautiful eyes slightly turned, looked at the flustered Xiao Che and spoke in a faint voice You are not allowed to be within five steps from me.

.. Then where would you let me sleep? Xiao Che rubbed his chin. The room was small it only had a bed, a reading table, an eating table, and two cabinets. If one walked from east to west across the room, at most there would only be the size of seven of eight paces of distance in between.

You sleep on the bed. Xia Qingyue stood up from the bed.

No need! Xiao Che flatly refused and sat in the corner furthest away from Xia Qingyue and closed his eyes. Although Xia Qingyue was perhaps a hundred times stronger than him, his dignity as a man refused to let a girl sleep on anywhere but a bed if he had a choice.


',0,'2019-05-01 23:00:01.000000');