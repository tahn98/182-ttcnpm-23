INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(12,'chapter 1','Chapter 1 The Downfall of the Central Plains

Wang Chong knew that this was his mission in this world!

If a barbarian were to become a king, then it would be better that the Central Plains fell. This was an analect of Confucius. Wang Chong had never thought that in this distant timespace continuum, Central Plains would actually be destroyed!

Furthermore, he himself would be the final witness of the scene!

The skies burned and the earth tremored. Countless corpses laid everywhere, forming mountains and filling oceans. The fresh blood that flowed from them converged to form a crimson river. Wang Chong could even see dense death aura rising from the corpses of tens of million of citizens of the Central Plains lying all around him.

On the other hand, countless cavalries of a foreign race were slowly closing in.

No one knew where these foreign cavalries came from. Neither did anyone knew why they were determined to destroy this world. They only knew that ten years ago, these death aurafilled foreign cavalries appeared from nowhere and within the short span of a few years, they tore through all of the empires!

Along with the appearance of these foreign cavalries, the earth collapsed inwards and space shook! Destruction! Tens of million of living beings were reduced to withered bones!

At this moment, the group that Wang Chong was leading was the final fighting force of this world!

In this center of this vast lands, Wang Chong led the final remaining army of the Central Plains. As though a duckweed floating in a pond, he waited patiently for the end to come.

After going through so many years of hardship, Wang Chong thought that his heart had already steeled. Yet, when the destined moment was about to set upon him, Wang Chong cant help but shiver.

Sorrow, pain, and despair surged through him, but those werent selfpity. Those were for his brothers and the ultimate end that was awaiting this homeland of his, the Central Plains!

General, please pardon my early departure!

Youre not at fault that we came to this point! General, you have already done your best!

There is no need to feel sorrow for us! We are already prepared for this. At least, at the very end, we didnt bring shame to the Great Tang Dynasty! To be able to accompany general in this life, I have not lived in vain!

General, lets meet again in our next life!

Come, you foreign rascals! Lets fight one last round! Hahaha



Familiar figures passed by him one by one, roaring heartily in laughter. They charged forward resolutely toward the boundless army of foreign race, as though moths attracted to a flame.

Theres no need, my dear brothers. We will reunite soon enough!

Looking at those familiar figures disappearing as though an epiphyllum in the midnight, tears finally gushed out of Wang Chongs eyes, flowing steadily down his cheeks.
Epiphyllum is a flower that blooms only for an instant before disappearing.

Wang Chong wasnt a soul from this world. In fact, if not for that accident, he should have been in another timespace continuum enjoying the sunlight and rain, completing his university education and living the rest of his life in peace.

Yet, thirty years ago, a mysterious shooting star suddenly appeared, bringing him into this world which was similar to the Great Tang of the Central Plains in Chinas history. Yet, it was an entirely different world and in it, he became the fifteenyearold son of a clan of generals.

Upon arriving, he experienced betrayal and fear. He felt completely out of place and that nothing here had anything to do with him.

But a catastrophe struck and those who loved him and those that he loved died one after another. At that moment, Wang Chong awoke to reality and his fighting spirit lit up!

It was a pity that it was already too late.

In this world, Wang Chong had experienced many different things. Ten years of aimless wandering caused him to miss the ideal window to cultivate in his lifetime. Eventually, due to the doing of certain coincidences and the army commanding that he had gained through playing strategic games in his previous life, he was able to earn the acknowledgement of various seniors of the empire.

They infused all of their Origin Energy into him, allowing him to become the final Grand Marshal of the empire, the person who carried the final hopes of the Central Plains on his shoulders.

Yet, it was already too late. He had missed out on too much, way too much. Even though he gave his all, he still ended in failure.

With sorrow gripping at his heart, Wang Chong slowly closed his eyes.

He did not fear death, just that it wasnt time yet. He was still waiting. There was someone that if he did not kill, he would not be able to rest even upon death!

He was the culprit for this all! If it wasnt for him, the empire wouldnt have weakened to this point!

Wang Chong felt hatred!

Only fresh blood could wash away the endless hatred in his heart!

But the other party was too cunning. He never showed himself easily and Wang Chong could never find an opportunity to strike. Only this time, when he came to this barren valley to bait him out, Wang Chong knew that he would definitely be unable to suppress his urge to come.

He had already hidden for thirty years. This time, when victory was just at hand for him, he would definitely step out from the shadows!

Wang Chong, give up. I have already spoken to the king and as long as you are willing to surrender, he can spare you from death!

Suddenly, a voice echoed from afar.

Behind the endless sea of foreign cavalries, a fat tottering figure revealed the upper half of his body. Gleefulness reflected in his sight, but fear and apprehension could be seen in it as well.

He was in no way a cowardly person. However, it was a mystery why the fellow before him was that incredible. He did not hold much military might in his hands, yet he was able to defeat opponents that outnumbered them tenfolds.

He had only taken over control of the army of the Central Plains for a short few years, but the number of foreign warriors who had fallen under his army was equivalent to the that of dozens of years.

If not for his fear of this fellow, he wouldnt have hidden for that long a time.

Traitor!

Wang Chong looked at that figure as hatred burned in his eyes. If not for someone allying with them and guiding them, how could the foreign cavalries cause so much damage and conquer massive plots of land within a short period of time?

This was all his doing!

Hehe, Wang Chong, as expected of the Central Plains God of Warfare! To think that a scion of the Wang Clan who lived off his family while awaiting for death would become the worlds Grand Marshal! Reality is truly an enigma! If those old fogeys chose you thirty years earlier to become their successor, the Wang Clan might not have fallen back then and the Central Plains would still have a hope! However, all is already done!

The figure gleefully says.

Wang Chong, let me give you an advice. You are a talented person and the emperor has already said that as long as you pledge him your allegiance, he is willing to spare you! Furthermore, he is willing to convert you into one of them! How do you think about it?

But Wang Chong didnt seem to have heard a word he said.

Kangya Luoshan!

Wang Chong shouted out his name. His resentment was so powerful that it seemed to shoot out from his eyes. After so long, the opportunity had finally come. In the end, this despicable fellow couldnt resist the urge to appear before him.

You shall be buried with me and Great Tang!

Amid the sound of the trembling of the earth, majestic radiance shot out from Wang Chongs spear. In that instant, there seemed to be another brilliant sun in the sky, blinding the eyes of all beneath it.

Retreat! Retreat!



The wind blew frenziedly and upon seeing Wang Chong, fear ignited in the hearts of the thousands of foreign cavalries. They immediately retreated backward as though an ocean wave.

Protect Lord Oracle!

Some of the foreign experts immediately reacted. They gathered around Kangya Luoshan and incredible rings of light and black flames burst forth from them. However, it was too late.

Boom! A ray of light with radiance comparable to a supernova shot down from the heavens and the diffused light from it warped the colors in the sky. In an instant, the light enveloped the hundreds of foreign experts and figures that were between them.

You!

A piercing but momentary anguished shout could be heard. That chubby face warped in fear before the brilliant flames and swiftly, Kangya Luoshan was reduced to ashes.

Even at his moment of death, he never imagined that Wang Chong would pit all of his strength to kill him despite being forced to a corner!

At that instant, he struggled angrily. However, he was unable to match up to the unmatchable spear!

Finally, I have succeeded!

At this moment, an unexplainable surge of exhilaration overwhelmed Wang Chong!

Father, mother, and the countless souls of the Central Plains, you all can rest in peace now!

Death charged straight at him, but in its face, Wang Chong only smiled faintly as he stared calmly at the countless flamed spears stabbing towards him.

Boom! At the very last moment, Wang Chong ignited his dantian, causing the thousands of foreign cavalries to be pulled down to their graves along with him

It was said that a humans final moments would seem as though an eternity. To think that the myth wasnt true!

Wang Chong smiles sorrowfully, but his heart was tranquil.

He was finally relieved of his duties after so many years. Yet, he felt an aching pain deep in his heart. In that instant, Wang Chong thought of his grandfather, his granduncle, his parents, his eldest brother, his second brother, his cousins and

If only he wasnt that obstinate then!

If only he was able to awaken in time, stand up and protect his family and homeland with his military talents!

It was all too late!

All that he loved and all he loved had left him!

All those that loved deeply and all that he loved dearly had left him.

If only he could do it all over again, he would have been a better person. However, it was all too late!

From this moment onward, the Central Plains of China will become the hunting grounds of the foreign race. Thousand years from now, will no one know of the existence of the race known as Yanhuang and the dynasty known as Great Tang?

Wang Chong felt regret, sorrow, and indignance.

This isnt how it should be! 

Tears of regret flowed down Wang Chongs eyes. If only he could restart, in order to make up for all the regrets he was feeling now, he was willing to give his all! The entirety of him!

Boom!

When this thought flashed across Wang Chongs head, in the depths of the heavens, the rumbling of lightning could be heard. At the last moment when the final flicker of his life was going to extinguish, when everything was dark, Wang Chong suddenly saw a shooting star.

This Isnt this the shooting star which brought him to this world?

?The user has awakened. Activating the Power of Destiny!?

Seemingly coming from nowhere, a mechanical voice devoid of any emotions rang by his ear.

The Son of Destiny! He is the Son of Destiny! Stop him! 

In the darkness, the frightened screams of countless foreign cavalries suddenly echoed. What could that made these foreign lifeforms which feared not of death to experience such terror!

However, Wang Chong didnt know of these. Surrounded completely by darkness, Wang Chong sunk into it.



Why are you called a transcender?

It might have been an instant or countless long eras, but Wang Chong was suddenly awakened by a curious voice by his ear. That voice seemed distant yet near, crisp as though silver bells, with a tinge of innocence and immaturity to it.

As though a rock that had been thrown into the surface of a lake, ripples diffused in Wang Chongs consciousness.

Who is it? Whose voice is this?

Didnt they say that a person is reduced to nothing upon death? Why was he still able to hear? Could this be an illusion?

Hmph!

Just when Wang Chong was still deep in contemplation, a displeased harrumph sounded by his ear. Before Wang Chong could react, he something violently jabbing at his body.

It was a finger!

Wang Chong immediately came to a realization.

Thats not right! How could ones consciousness still possess a body upon death?

Unless he wasnt dead!

Weng! The moment this thought flashed across his head, Wang Chong felt as though billowing waves had risen in his head. He struggled to open his eyes and soon, a brilliant radiance spilled onto his eyes.

The darkness before him was illuminated. Not too far away from him, Wang Chong saw a pouting tenyearold girl staring at him in displeasure.

I told you not to ignore me!

The little girl used her slim finger to jab Wang Chong once more.

Little sister?!

Wang Chong couldnt believe what he was seeing. That little girl had eyebrows of a crescent moon, bright eyes and white skin with a tinge of red. Dressed in silverred leather pants, she looked like a jade sculpture.

However, the two braids on her head that faced the heavens exposed her mischievous nature. Who else could she be other than his youngest sister?

However, wasnt his younger sister already

Wang Chong stared at her blankly. For a moment, his head was unable to react.

Wasnt he already dead? He remembered clearly that at the very last moment, in order to kill Kangya Luoshan, he charged into the army of countless foreign cavalries. How was it possible for him to see his youngest sister here?

Furthermore, his youngest sister was so tiny. This was obviously how she looked when she was ten, and he was only five years older than her. If his youngest sister was only tenyearold, didnt that mean that he was

Wang Chong lifted his arms and soon, he saw a pair of skinny and white arms. This was different from what he remembered.

In that instant, Wang Chong was speechless. Could it be that He had come back to life?

Joy and anxiety gripped Wang Chongs heart. More than that, he felt apprehensive.

Little sister, pinch me.

Wang Chong suddenly said.

Right after he said those words, Wang Chong saw a soft and white tiny hand stretching over toward him. Around that tiny hand, there were faint white ripples.

That faint ripples seem to congregate around her hand instead of dissipating into the surroundings, as though steel. It felt impressive.

Origin Energy Tier 9!

Wang Chongs felt a tight squeeze in his heart. That faint white layer was symbolic of a Origin Energy Tier 9 expert. How could he have forgotten that his younger sister possessed superior talent since young and was known to be a herculean warrior with unmatched strength?

He was simply bringing himself pain by having her pinch him to wake him up!

Little sister, dont

Wang Chongs complexion warped. He hurriedly tried to stop her, but it was too late. Kacha. Wang Chong felt as though his radial bone had cracked.

Aiyo, little sister, let go!

Upon hearing Wang Chongs cry of agony, the little girl looked a little embarrassed. Sticking out her tongue, she retracted her fingers sheepishly.

Brother, you cant blame me for this. You were the one who asked me to do it.

The little girl said while sticking out her tongue. There wasnt the slightest hint of apology in her words.

Wang Chong smiles bitterly. As expected of that youngest sister of his in his memories. That extraordinary talent and mountainsmashing strength werent something average people could bear.

Even so, while rubbing his aching arm, Wang Chong felt incomparably happy. He was able to see, feel and experience pain This meant that this wasnt an illusion.

He was really still alive!

Can it be that the heavens had answered my prayers?

At this moment, Wang Chongs heart was filled with many different emotions.

3rd Brother, its not that I want to nag at you, but you shouldnt hang out with that bastard Ma Zhou. That fellow isnt a good person. He caused 3rd Brother to be lectured by father and others were saying that you had raped female villagers. Does my 3rd Brother need to rape female villagers? That bastard! I will definitely teach him a lesson if I see him. I will beat him up whenever I catch sight of him.

The little girl seemed to have thought of something and a frown appeared on her face. Kacha kacha, cracking sounds could be heard from her fearsome hands. It was clear that the resentment she felt towards him wasnt something trifling.

Little sister

Hearing the sincere tone his little sister spoke with, Wang Chong felt his heart ached. Moved, he hugged his younger sister, Wang Yaoer, tightly.

This was his younger sister, a younger sister who loved her elder brother dearly. However, it was a pity that he was too much of a jerk back then. He wasnt able to understand her sentiments until he lost her and he deeply regretted it.

Since the heavens had given him another chance, he was determined to prevent his younger sister from experiencing what she had in this life.

Little sister, thank you. However, theres no need for it. I will deal with that bastard Ma Zhou personally.

Wang Chong replies her softly.

Wang Yaoer was taken aback. In Wang Chong embrace, she raised her head and her large eyes reflected in Wang Chongs eyes. She was surprised. This 3rd Brother of hers seemed to be slightly different today.

He usually lived sloppily and hung out with bad company. He wasnt the type to say such words.

Alright, 3rd Brother, you havent told me yet. What is a transcender? What does transcender mean? Why have I never heard of it?

Wang Yaoer suddenly recalled something. The large round eyes she stared at Wang Chong with had a pair of large question marks in them. After talking for so long, she still hadnt received the answer to the question she was concerned about.

Towards this, Wang Yaoer felt quite dissatisfied towards Wang Chong.

This

Despite his thickskinned, upon hearing the words of his youngest sister, Wang Chong cant help but fondle his nose.

About the matter of transcender, that was when he first came over to this world. At that time, his heart was filled with resentment. Everything was foreign to him and he didnt know a single person here. He only felt like a passerby in this busy world, as though a fleeting bubble.

It just happened that this obstinate and interesting little sister with a twin braids came to look for him, calling him 3rd Brother. Back then, his inner youth exploded and he jokingly got her to call him transcender.

However, his youngest sister took this joke of his seriously. Again and again, she chased him asking what transcender was. Thinking about it, this was probably it.

Remembering this joke of his, Wang Chong felt so embarrassed that he could die.

Hmmm, transcender means a suave man.

Suave man? Little sisters eyes widened further in confusion.

It means a handsome guy!

Wang Chong laughed heartily.

Brother, you lied to me!

Little sister immediately erupted in anger. She might be young, but she wasnt gullible.

Little sister, I just remembered that father is about to be home soon. You should hurry back. Otherwise, if you are caught here, you will be in deep trouble!

Sweating profusely, Wang Chong hurriedly changed the topic. His little sister was innocent and believed him easily. However, if she were to realize that he had lied to her, her frightening strength would put him in place.

Hmph!!

With her red cheeks puffed up, it was clear that she was still angry. She might be young but she wasnt that easily fooled. It was clear that her brother wasnt telling the truth.

Through cajolery and deception, Wang Chong finally managed to dupe her to leave. Even so, she was still enraged and discontent then.

Father will be home in awhile. Mother told me to remind you to head to the grand hall to have your meal later!

Boom!

Upon hearing those words, a thunderous roar seemed to have flashed across his head. Wang Chong suddenly shuddered.

After saying those words, little sister stomped out of the room.

Wang Chong fondled his forehead and realized that his hand was full of cold sweat. He really thought that she had managed to sneak in without anyones notice. Even if she managed to get past their father, she was unable to escape their mothers allseeing eye!

However, it wasnt a surprise. Given his little sisters capability, how was it possible for her to escape the Buddhas Five Finger Mountain?

After his little sister left, Wang Chong closed the door and leaned against the wall. His face tilted slightly upward as he stared at the high roof of the room. His face slowly regained its calmness.

Todays happenings were simply too bizarre. He needed some time to muse over them.

Everything that happened during his moment of death appeared in his mind and eventually, the shooting star he saw slowly clarified. Some distant memories that he thought that he had forgotten through time abruptly emerged clearly in his head.

Wang Chong could remember clearly that in AD2022, on Earth of another parallel universe, it was a scorching day of summer. When he was walking on the street, a shooting star suddenly fell from the sky onto him. After which, he was brought into this foreign world.

When he transcended over, he thought that he would obtain some transcenders privilege. However, he lived an ordinary life, even up to the moment of his death. Other than his identity as the son of a general, he was no different from any other person.

That shooting star was an enigma. Other than bringing him into this foreign and distant land, it hadnt showered any miracles upon him.

He didnt think that it would appear at the moment of his death.

Was it resentment? Or indignance?

Wang Chong pondered.

No matter what, he had returned. He had truly returned to this day, thirty years ago! This year, he was fifteen while his little sister was ten!

The era of prosperity and strength for the Central Plains!

Regardless of whether it is the Qin Dynasty or the Han Dynasty, there wasnt a single dynasty which territory had expanded as massively as the current one. The eastern sea, the western ridges, the southern prefectures and the northern Yin Mountains. They were all part of the sphere of influence of the empire.

With its 600 000 large army, the Great Tang ruled over the entire Central Plains, dominating all of the foreign countries and tribe by its orders. Furthermore, its military was filled with countless talents of general caliber and it was reputed to possess The Radiance of the Hundred Generals. Even the Hu had to submit to this massive empire.

In short dozens of years, the territories of the empire expanded continuously, resulting in its current size.

Without doubt, this was the center of the world.

The era where the Central Plains was at its greatest.

As such, those of the Central Plains term the one in the palace as the Sage Emperor. In this Central Plains, everyone was proud of their identity and complacency lingered in the air.

However, oblivious to the others, Wang Chong knew that under its powerful exterior, this prosperous empire was on its way to its downfall.

Under the illusion of prosperity and peacefulness, the empire was surrounded by countless threats.

In the highlands at the west of Great Tang, the Tsang was swiftly growing stronger. Going slightly further, the Umayyad Caliphate had fallen and replacing it was the empire that brought the Arabian world to the peak of its strength, the Abbasid Caliphate.

In the northeast, Yeon Gaesomun was training troops and purchasing steeds. In the south, the Erhai was also moving in the shadows.

All kinds of dangers were present, just waiting to burst.

On the other hand, the Great Tang of the Central Plains was still immersed in the illusion of prosperity. They were completely ignorant of the potential dangers that were lying around them. In fact, when the barbarians propped up their military might and were viewing at Great Tang menacingly, those Confucian scholars were trying to bring about a new line of thought by persuading the royal court to extract their army and return others their territories. They hope to move them through virtues so as to earn the Central Plains and the barbarians everlasting peace.

This was an unprecedented occurrence of one weakening their own army

A tiger was crippling its claws, a wolf was plucking its sharp teeth!

Four years later, when the avaricious barbarians came conquering its territories and a destructive catastrophe swept across it, the Central Plains found itself lacking the strength to retaliate.

After four years, all of these hidden threats came surging one after another from all directions! Eventually, this massive and prosperous empire fell completely.

His beloved homeland fell into decline within this four years and fragmented. As such, the powerful clan of generals fell to the point of struggling in mud.

He lived his previous life groggily, only awakening when everything was already fixed into place. However, this life, with the memories and experience of his previous, he will not allow the same situation to reenact!

He had contemplated many times over why this massive empire would fall apart in a short period of time. If he was able to put in place the plans he had thought of thirty years later, the fall of the Tang Dynasty could still be reversed.

But before so, he had to prevent the major event that was going to happen in this household first. His little sister, his eldest brother, his father, his mother and the entire Wang Clan Everyone would be affected by this matter.

It was due to this matter that the Wang Clan fell further and further into decline, unable to rise up once more.

After a series of events after the happening of this matter, those that he loved and those that loved him slowly left him.

This all happened not long after he transcended into this world.

Just that, he was still ignorant then. However, he was determined to change it all this life!

How can one sweep across the world without cleaning their own household first? How can any egg remain intact under a ruined nest? If he doesnt salvage his own home, how can he save the world?

No matter what, he had to stop it from happening!

With such thoughts in his mind, Wang Chong pushed the door open and walked out.
',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(12,'chapter 2','Chapter 2 Reincarnated to a Human

The autumn wind rustled.

The closer Wang Chong got to the grand hall, the more nervous he felt. Perhaps only when you lose someone will you know how dear they were to you.

He lived his previous life in a daze, caring for nothing at all and not allowing anything to bother him in the least. It was hard to imagine that just having a meal would make him so nervous.

This should be what they call fear of returning to ones homeland.

Wang Chong mumbled. Lifting his head, he saw a massive gate with two lion heads on it, one on the left, one on the right. His familys dining hall was just in front.

The Wang Family wasnt a great clan of nobility and as such, it wasnt tightly restricted by traditions and decorum. However, it was still a clan of generals. His mother might not have created many rules to limit their actions, but they still had to maintain the etiquette befitting of a great clan.

The Wang Clan had many offsprings. However, regardless of who it was, and this was including his father as well, as long as they were in the capital, they had to return home for the weekly family gathering. Everyone would gather around a huge round table and share a harmonious meal.

This was the last day of Wang Chongs grounding. This was also his first family meal gathering in the last seven days. However, what Wang Chong was bothered with wasnt this.

If things progressed as how it did in his previous life, at this time, his father should have returned home by now. Due to his fathers official duties, he often left the home early in the morning and returned only late at night. Even as his son, he wasnt able to meet him whenever he pleased.

After that incident, his father would swiftly leave the capital to the military barracks. Most likely, for the next half a year, he wouldnt be able to meet him.

If he wants to prevent that incident from happening to change the destiny of his clan, this family gathering will be the best opportunity for him to do so, as well as the final one.

However, will his father believe him?

Recalling how he was then, Wang Chong went silent.

One reaps what he sows. In his previous life, he always thought of himself as a transcender and acted like a hedonist. Taking life as a mere game, he committed many different ridiculous actions.

In the very beginning, he obstinately wanted to wander this world leisurely and fool about. As such, he stayed out day and night and made a lot of bad company.

The Ma Zhou which his little sister spoke of was one of them.

In his previous life, Wang Chong was a straightforward and honest person. He didnt think deeply into things. He often thought that since they were friends, they should treat each other with sincerity. Never would he thought that the other scions were so scheming. On the surface, they treated you as their brothers but behind your back, they threw multiple daggers at you.

These fellows made use of his name to fool about outside. In the end, he was even labeled with the sin of raping a female villager in the daylight.

The other matters could be forgiven, but raping a female villager was truly overboard. Even his father who was often out in the fields and rarely interfered in his affairs, upon knowing of this matter, rushed back in the middle of a night.

Wang Chong was then grounded for a week due to the affair.

He had completely disappointed his father due to this matter. In the period after his transcension, even though he was rebellious and often committed inappropriate actions, he hadnt fallen to such a point.

But raping a female villager

This was already challenging the moral boundaries of his father. From then on, his father gave up all hopes on this son of his and never bothered with him again.

Wang Chong only realized a long time after the incident that he was done in by Ma Zhou and the other bastards and he was extremely depressed for a long period of time.

Even though he knew of these, he probably wouldnt be able to explain it all to his parents clearly. Besides, if it wasnt for his inability to see through the true colors of others, he wouldnt have been betrayed like that. Given the current emotions his father and mother were experiencing, there was no way they would listen to his words.

After all, what proper things could an ignorant playboy possibly do or say?

At this point, Wang Chong felt intense bitterness in his heart. He had no choice but to swallow his bitter gourd that he had sowed himself.

No matter what, I cant continue acting like a jerk. I have to change fathers impression of me by hook or by crook.

Wang Chong was fully aware that he only had this family meal gathering to change the notion that his parents had of him. He had to make them understand that he was no longer the same person as he was.

He had to slowly regain their trust.

Wang Chong took a deep breath. At this point, he already knew what he had to do.

Young master!

The gate was tightly shut. Upon seeing Wang Chong, the two sturdybuilt guards dressed in uniforms bowed their head to greet him.

The two men looked had broad shoulders and a towering stature. Their presence felt like an indomitable heavenly tower and with a single look, it was clear that they had undergone numerous wars in the battlefield.

It has been tough on you two.

Wang Chong paused for a moment beside them and sincerely thanked them.

He remembered these two guards. They were specially chosen by his father, Wang Yan, from the military barracks to guard the residence.

Only with a long journey will one know the stamina of his horse, and only with time will a persons heart show through. In his previous life, Wang Chong didnt care much about these guards, not knowing their names even. After the incident which caused a pandemonium in the family, when all of the others guards and housekeepers had left, only these two guards and a few other housekeepers stayed closely by their side, protecting and serving them.

Until the arrival of the great catastrophe which killed the two guards just like how it did with countless other people, they carried out their responsibilities loyally until the very end of their lives.

Only at that moment did their names etched deeply into Wang Chongs mind. One of them was called Shen Hai and the other Meng Long. They were the two most loyal guards of the entire residence.

Young master?

The two guards stared at Wang Chong in astonishment. In the past, this young master often acted haughtily and arrogantly, thinking that it was unbefitting for him to speak with such lowly guards.

To think that he would make the initiative to greet them as well, this was the very first time he had did so!

The two of them could see the amazement in the others eyes!

Wang Chong knew what they were thinking of, but he simply smiled silently. The solidification of a river doesnt happen in a single day of frost. The impression everyone had of him in the past was simply too poor. It would be difficult to change the conception they had of him in a short period of time.

However, after making the first step, he will continue to make the second and the third. One day, they will comprehend that he had truly changed.

Placing his hands on the lion head knocker, Wang Chong pushed with great force. Jiya, the door creaked open, echoing loudly in the grand hall. After which, he stepped in.

How fragrant!

Before Wang Chong could clearly make out anything, the deep aroma of various delicacies that made one salivate reached his nose. In the massive room, a large table that was sufficient for more than a dozen people to sit by it was placed in the middle. There were more than twenty sumptuous dishes placed on it.

It has been long since I have eaten something so luxurious.

With a single whiff, his appetite was hooked. Wang Chong felt his stomach protesting in hunger. Thinking about it, in the seven days which he was grounded, the food he ate was mostly bland, far from the current spread that laid out before him.

However, despite the lavish spread before him, something was amiss about the atmosphere before him.

Wang Chong felt a chill in his heart. Lifting his head to take a glance, he saw the grim expressions on the faces of his father and his mother. None of them were looking at him.

Even though the aroma of the food was lingering around the dining table, there were two people who werent moving at all. On the other hand, his voracious little sister was burying her head in the food. A hand of her held a pair chopsticks while the other cupped a bowl. Her mouth moved nonstop as food disappeared swiftly into her mouth. From his angle, he could only see the two braids on her head bouncing up and down.

This little sister of his only had two hobbies, one was to eat and the other was to play.

Wang Chong almost died from shock the first time he saw her eating. How could this be a little girl? It was obviously a ravenous beast!

However, when one considered her astounding strength, it all made sense.

In his family, his little sister was the only one who was allowed eat outside of official mealtimes. His voracious little sister usually made a lot of clanging sound with her bowls and utensils while eating, yet this time, her mouth was wide opened but not a single sound was produced. It was clear that there was something wrong with the atmosphere.

The air in the grand hall felt so thick that it could suffocate.

You! Are! Done! For!

While grabbing her bowl and eating frenziedly, she shot a look of sympathy towards Wang Chong. She could already see the tragic fate that was about to befall her elder brother.

This little girl may be innocent, but she was exceptionally vindictive. She hasnt forgotten how her elder brother lied to her just now!

Wang Chong didnt have the time to bother with this infuriating little sister of his. He knew that even though the punishment had ended, but this matter was far from over.

Father, mother!

Unlike previously, Wang Chong didnt walk straight to his seat and bury himself in the food like an ostrich. Instead, he circled around the dining table over to his father and mother and stopped before them.

Upon seeing Wang Chongs actions, his little sisters mouth opened wide.

What was her brother doing? Didnt he know that father and mother were still at the peak of their anger? It was suicidal to run over to them at such a moment!

However, something that left the little girl even more shocked occurred

I am at fault for this matter. I will sever my relationships with them and stay away from them in the future.

Wang Chong apologized with a lowered head.

Padah!

The little girl raised her chopsticks and stared at her brother intently. She was so astounded that her lower jaw was about to fall to the ground. What happened? To think that her brother would take the initiative to confess to his mistakes.

Surely she had heard wrongly?

Rubbing her eyes, she confirmed that she wasnt hearing things.

In the grand hall, the heavy and still atmosphere lightened slightly.

Seated in the main seat of the dining table was a middleaged lady dressed in an emerald silk robe with her hair combed into a bun. At this moment, disbelief flashed across the eyes of the beautiful lady as her face twitched slightly.

This child actually admitted his faults?

Zhao Shu Hua couldnt believe her ears. She had lectured him over this matter innumerable times, even resorting to caning and grounding to change his mind, but he had no intentions of listening to her words at all.

Sometimes, Zhao Shu Hua thought of herself as a failure of a mother. This made her extremely depressed, just that she had never expressed it before her children.

But this time, he took the initiative to admit his faults. Did this child really change for the better?

At that moment, Zhao Shu Hua was a little afraid.

She wished dearly that her child had truly changed for the better, but she was afraid that it would turn out to be just her wishful thinking. After all, his performance in the past was simply too abhorrent.

You unfilial son! You still know how to admit your mistakes?

It was an icecold voice. It came from Wang Chongs father, Wang Yan. He had a stern face and he had a piercing gaze that exerted intense pressure onto one, as though one was faced with a spear stabbing straight toward them.

In the Book of Rites, it is stated that A father should be loving and a son should be filial. Although Wang Chong felt pressured by Wang Yans gaze, he knew that his father was already keeping his might in check so as to not injure him.

What are you saying? Do you think that it is impossible for Chonger to repent on his actions? Didnt you hear him admit his faults?

In the beginning, Zhao Shu Hua was worried that Wang Chong was simply saying those words to console her. Even so, upon hearing Father Wangs words, she immediately became displeased. It was a rule in the royal court that ladies mustnt interfere in politics and as such, Zhao Shu Hua never interfered in Father Wangs military and politic work.

However, as Father Wang was often out commanding the army, the residence and the four children were mainly supervised by Madam Wang. In the matter of educating the children, Madam Wang held absolute authority in the family.

Father Wang might have total authority over commanding the army in wars, but at home, his authority was beneath that of Madam Wang.

Wang Chongs head was lowered, but he could discern their expressions clearly. His father still had a steeled face and he refused to look at him faceon. However, his complexion had soothed slightly and it was no longer as tightlyknitted and stern as before.

Clearly, his apology wasnt completely useless.

I will listen to fathers reprimand. Chonger was too stubborn and foolish in this past, thus causing father and mother sorrow. From today onwards, Chonger will change anew.

Wang Chong said with his head still lowered.

That single sentence caused Father Wang and Mother Wang to raise their heads. Both of them could see astonishment in the others eyes. To think that he wouldnt retort upon being lectured, perhaps the sun had risen from the west today!

His apology might just have been on the spur of the moment, but his response clearly wasnt so. Could this unfilial son have truly changed for the better?

Chonger, dont listen to your fathers words. Hurry up and sit down. As a family, we should be eating together harmoniously, it isnt appropriate for you to have that stiff expression on you.

Mother Wang hurriedly says.

Un, Wang Chong answered. He walked obediently to his seat and sat down. His face was still lowered and he sat there quietly. Father Wang and Mother Wang hadnt started eating yet, so Wang Chong also remained motionless.

This action of his caused another wave of astonishment to Father Wang and Mother Wang.

This child has really changed.

The one who felt the happiest and relieved at this moment was Mother Wang, Zhao Shu Hua.

Who doesnt hope for their own son to soar to the heavens as though a dragon?

Yet, the behavior of this child of hers broke her heart time and time again. Could it be that her prayers had been answered, and this child finally became sensible?

At this moment, Zhao Shu Hua almost burst into tears from the happiness gushing through her heart.

s
',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(12,'chapter 3','Chapter 3 Changing Myself

Wang Chongs heart was filled with guilt.

Wang Chong saw the reactions of his own mother clearly. Just simple actions like apologizing and sitting properly by the dining table could make her so delighted. Through this, it was clear to him how much of a jerk he was in his previous life.

In his previous life, he was forced to transcend here from another world and thus, he rejected everything. Even though they had always treated him as their son, deep in Wang Chongs heart, there was always a lingering thought that they werent his true parents.

Thus, Wang Chong always felt distant from them.

This was also why in his previous life, albeit the caning and lectures, he refused to listen to their teachings. Wang Chong had always thought that this wasnt his world and that he was just a passerby here.

Everyone and everything felt just like fleeting bubbles in his life. However, reality had proven him wrong.

Only when one lost what they possessed will they learn to treasure it only those who are left with nothing will know how precious everything is!

In his previous life, after that incident which caused his clan to fall, he thought that the behavior he had shown previously would cause his father, his mother and his relatives to give up on him.

Yet, it was them who, during his arduous and hardest moments, in those days that he lived fleetingly, stayed by his side and took care of him.

If there was a mouthful of rice, he would be the first to receive it.

Remembering his mother, who wasnt even fifty yet then, with a head full of white hair as though a seventy year old elderly, Wang Chong was filled with guilt.

His mother spent the final moments of her life in his embrace. When that seemingly indomitable figure fell, Wang Chong was shocked to realize how fragile and weak her body was.

Wang Chongs heart bled.

It was that moment that Wang Chongs heart shattered and died. In the thirty years after that, the living Wang Chong was merely a walking zombie.

Mother! Why did you have to treat me that well?

At that moment, Wang Chong wept. In that heavy downpour, he roared in agony. That was his first time crying so painfully after coming to this world. At that moment, the entire world collapsed!

Wang Chong awoke, but it was all too late. Nothing could be changed anymore!

Perhaps, the heavens had heard his voice and decided to grant him a chance to start anew. Looking at his mother, Wang Chong felt his heart ache.

Mother, dont worry. In this life, I will not make you sad. I will not allow anyone to hurt you, no one at all!

Beneath the table, Wang Chongs fists were tightly clenched.

Come, eat, eat! M0ve your chopsticks. We can talk after the meal.

Madam Wang, Zhao Shu Hua, was in high spirits. Picking up her chopsticks, she placed a large piece of roasted chicken onto Wang Chongs plate.

Mother, you eat as well!

Wang Chong grabbed his chopsticks and placed a large piece of meat onto his mothers plate as well.

Madam Wang was overjoyed and relieved. Even for the steelyfaced Father Wang, who was extremely dissatisfied with Wang Chong, his face toned down significantly upon seeing his actions. In the Three Cardinal Guides and the Five Constant Virtues, filial piety was the very first value. For this fellow to know to grab food for his mother, it was a huge improvement.

The big ruckus that had occurred this time probably awakened his senses. When he thought of this, Wang Yan nodded his head in approval.

Father, mother. I would take to make use of this opportunity to inform you of something, and I hope that the both of you will able to agree to it.

Just when everyone was about to dig in, Wang Chong put down his chopsticks. His eyes rippled and he seemed to be contemplating whether he should speak of the other matter he had in mind.

What do you want this time?

Upon saying those words, Father Wangs complexion darkened and his gaze turned cold. This beast! After doing something like that, I thought that he would have changed for the better. Truly, a leopard never changes its spots.

After saying so much and behaving so obediently, it turned out that it was all in preparation to negotiate with them. He would like to see what this unfilial son of his was up to now.

Chonger, what is it that you would like to say?

Unlike Father Wang, Mother Wang was greatly interested in what Wang Chong had to say and her tone carried a tinge of expectation. Perhaps it was within a mothers nature to believe in her son unconditionally. On this point, Mother Wang did not think into the matter as deeply as Father Wang did.

Father, mother, I have considered this matter for a very long time

With his head lowered, a look of contemplation appeared on Wang Chongs face. This would be the key, as well as the only way to regain his parents trust. Wang Chong knew that what he was going to say would change his life.

I want to join the army!

Wang Chong said.

The moment his words rung out, the entire grand hall seemed to have tremored. At that split moment, everyone was dumbfounded. Even when putting everything Wang Chong had said earlier together, it still wasnt as impactful as this one.

Madam Wangs mouth quivered. She stared blankly at her son and words seemed to have jammed in her throat. This news was too shocking to her, she needed time to digest it.

Even Father Wang, who had been treating Wang Chong coldly all this while, not even viewing him straight in the eye, had an astonished expression on his previously steeled face.

He was a general of an army and he had led troops to war. He was someone who wouldnt even blink an eye even if mountains were to collapse. However, this single matter that Wang Chong spoke of was simply too shocking to him.

This son of his was simply too mischievous. He had no motivation whatsoever and idled his time away. He even befriended bad company and the rape incident this time put the entire Wang Clan to shame, making it a laughingstock of the city.

After the ache in his heart had settled, he finally made up his mind to send Wang Chong to the military in advance. The military barracks challenged a persons physical and mental limits. Perhaps it was the only place which could bring this unfilial son of his to the right path.

Even if Wang Chong did not speak of this matter, he was prepared to bring this up over the meal. His will was resolute and regardless of whether Mother Wang and Wang Chong were agreeable to it or not, this matter would be done by hook or by crook.

He never thought that Wang Chong would bring up the matter before he did.

It was precisely because he knew what kind of person this unfilial son of his was that Father Wangs poker face crumbled. He knew that it was difficult for Wang Chong to garner such determination.

Joining the military wasnt a joke. It was filled with dangers and threats. If he had thought of Wang Chongs previous actions as an attempt to maneuver around them, then the matter of him joining the military crushed all doubts that he had. After all, this matter wasnt one to be joked about.

A prodigal who returns is more precious than gold. Could it be that, after going through so much, this unfilial son of his had finally repented and decided to start anew?

At this instant, Father Wang was ecstatic. He started to believe that his son had truly changed.

Looking at their expressions, Wang Chong knew that his words had managed to move them.

Given his knowledge of his previous life, Wang Chong knew that even if he didnt bring this matter up, his father would bring up the matter of enlisting him in the army over this meal as a punishment for his actions.

Back then, he protested for a very long time but it was all in vain. His fathers hardened determination and will would not be moved. Since he knew of it in advance in this life, he might as well bring it up himself.

This way, he could change the impression his father had of him and earn his trust!

Furthermore, thinking back on it, this wasnt really anything bad. Just that, it was worth thinking how he should go about doing that.

Chonger, you are still a little too young to join the army. However, it isnt really a huge problem. Have you thought about where you want to enlist at? I will go and greet them in advance.

Father Wang said.

In the past, he often addressed Wang Chong as an unfilial son. However, this time, he chose to address him as Chonger. From this fact itself, it was clear to see that Wang Chongs decision had delighted him.

Father, I have thought about the matter. I would like to start from the training camps. Before metal can be molded into a sword, it has to be sufficiently tough. I would like to enter Kunwu Training Camp to train up my martial arts first before joining a division.

Wang Chong said the words that he had contemplated over for a long time.

Kunwu Training Camp?

This time, Father Wang was truly shocked. He had just received news from the royal court about the emperors decision to establish the Three Great Training Camps, Kunwu, Shenwei and Longwei. They would become the camps where the youths of Great Tang would train at.

This matter was just confirmed not long ago. Given how confidential the matter was, how did this unfilial son learn of it?

However, thinking about it again, this unfilial son had made quite a few bad company, so perhaps he might have heard the news from them.

Why would you choose Kunwu Training Camp?

Father Wang swiftly regained his composure and asked

There is Shenwei and Longwei Training Camps too. Kunwu was specially created for sons of ordinary military officers. On the other hand, Longwei and Shenwei are of much higher level than Kunwu. It is specially targeted to the sons of nobility and authoritative officials. The training that one will receive there should be much more complete and of a higher level than Kunwu.

Perhaps it will prove to be useful to your future if you go there. If you wish to, I can make use of your grandfathers influence to enlist you into Shenwei or Longwei Training Camp.

Father Wang was just a general by the borders and he wasnt a member of nobility. As such, Wang Chong was just an ordinary son of a general.

However, Wang Chongs grandfather was different. He was a meritorious subject who assisted the current emperor to the throne. After which, he was even promoted to become the Left Premier and his students and old buddies were throughout the entire Tang Dynasty.

Even though he had already retired, he still held considerable influence.

By using Wang Chongs grandfathers influence, there wasnt any problem to enlist Wang Chong into Shenwei or Longwei Training Camp.

Wang Chong became silent. His father had good intentions in mind, but Wang Chong knew that the truth was opposite of what Father Wang said. Of the Three Great Training Camps, Kunwu was the best.

In his previous life, at this period, the wise emperor wanted to pick out capable personnel from the sons of nobility and officials so as to organize a juvenile army. As such, he established Kunwu, Shenwei and Longwei Training Camp.

At the very beginning, when everything was still at the primary phase yet, everyone thought that Shenwei and Longwei were of higher standing than Kunwu and they were better choices.

Afterward, for a long period of time, there would constantly be many people trying all means to squeeze their way into these two training camps. However, Wang Chong knew that time would eventually prove that Kunwu was the best of the three.

In the days that followed, when that catastrophe struck, the country plunged into chaos and all of the older generals fell one after another, nearly 90 of the highly capable generals that appeared in the Great Tang Empire afterward came from Kunwu Training Camp

However, it would be difficult to explain this to his father.

Father, I have given some thought to the matter and I still think that Kunwu is more suitable for me. I know most of the scions in the capital. Father wants me to go to Longwei or Shenwei, but the others are definitely thinking of the same as well. If I were to enter Longwei or Shenwei Training Camp, wouldnt I meet with Ma Zhou and the others?

Wang Chong could only find other reasons to explain the situation to his father.

Initially, Father Wang was still opposed to Wang Chongs decision. However, after giving it some thoughts, Wang Chong had just barely ended things with those bad company of his. It was hard for him to repent and change for the better. If he were to hang out with Ma Zhou and the others at this point and revert back to how he was at the start, wouldnt he regret it for life? It would go against his intentions of sending him to the military.

Since your mind is made up, then well just do it your way.

Father Wang said with a frown.

He didnt agree with Wang Chongs decision, but what Wang Chong said made sense as well. He could grudgingly accept Wang Chongs explanation.

Regardless of what you father and son have to say, say it after the meal. Chonger, ignore your father. Come, eat!

Madam Wang had heard everything clearly by the side as she was in exceptionally good moods.

Initially, when she heard that Wang Chong wanted to join the military, she got the scare of her life and her worry for her son gripped her heart. However, upon hearing that it was only the training camp, she heaved a sigh of relief.

The Wang Clan was a family of generals and Wang Chong was already fifteenyearold. Going to a training camp first before officially joining an army was something Madam Wang could accept.

This was the case with most of the general clans in the capital. As progenies of such a family, they had to step onto the battlefield sooner or later.

This was something the madams in the capital had to comprehend and accept from the very start.

Wang Chong said no more. Picking up his chopsticks, he started to dig in.

I wonder if father would be able to accept the words that I am going to say later on.

Wang Chong was slightly anxious.

Even though he has successfully changed his parents impression of him, Wang Chong knew that this was only the first step. The next one was what was crucial!
',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(12,'chapter 4','Chapter 4 Crisis of the Wang Clan

Without a doubt, this was one of the happiest meal he has had in awhile.

The laughter over the meal never stopped. Madam Wangs face was filled with delight and she kept placing food on Wang Chongs plate. The food on Wang Chongs plate piled high as though a mountain, making it impossible to put anything else on it.

On the other hand, Father Wang wasnt as stern as he was before. Under Madam Wangs piercing glare, he placed some food on Wang Chongs plate as well.

Third Brother, incredible!

Upon seeing this sight, the little sister of the Wang Family was surprised. She shot her elder brother a glance. At this moment, she couldnt be any more impressed with him.

She thought that her brother would fall under this calamity and she had already mentally prepared herself to witness a tragedy of the mortal world. She didnt expect that with just a few words, not only did her parents not blame him, they were even chuckling and complimenting him. Even her father, which was an austere man, took the initiative to place food on his plate.

The Wang Familys little sister couldnt help but feel envious. She had been in this household for such a long time but never did she receive such treatment.

Father, I dont care, I want it too!

Her face puffed up and she pushed her bowl to Father Wang.

You are a lady, look at how you are behaving!

Father Wangs face was stiff and his words caused the Wang Familys little sister to feel indignant. Tears welled up in her eyes. Taking the scene in sight, Madam Wang didnt know whether she should reprimand her or laugh

Here! Mother will give you some food!

Brother will give you some food too!

Chuckling within, Wang Chong also grabbed some food for his little sister.

Thank you, brother.

A smile finally appeared on the little sisters teary face. She began to dig in happily once more, seemingly having forgotten all of the indignance she had felt previously.

The family dug into the food happily.

Father, I heard that you are going to meet Lord Yao?

In the midst of the meal, Wang Chong casually spoke.

In an instant, the atmosphere over the dining table changed and became slightly stiff. Madam Wang hurriedly shot Wang Chong a glance and little sisters chopsticks stopped in midair in fright.

Everyone in the family knew that Father Wang didnt like talking about work at home, and neither did he like anyone interfering in his job.

Where did you hear that from?

Father Wang lifted his head and said without a change in his expression. However, Wang Chong clearly saw a slight frown flashing across his fathers forehead. It was clear that he was displeased by Wang Chongs action of bringing up this topic.

Gedeng, Wang Chongs heart skipped a beat. Even so, he could only continue speaking of the matter. After all, this matter was extremely important to him. If he could not change this course of events, all of his previous hard work would have gone to vain.

I just happened to overhear it when father was speaking about of this matter to mother.

Wang Chong said. His heart was beating furiously out of nervousness. Whether it would be a success or failure depended on what he was going to say next.

Oh.

Father Wangs forehead twitched slightly. At this point, he suddenly recalled that he seemed to have informed his wife, Zhao Shu Hua, of this matter. However, he had only spoken of the matter at home once. To think that Wang Chong would happen to overhear their conversation.

Indeed, there is such a matter. Why are you bringing this up?

Wang Chongs good performance previously had worked its wonders. Father Wang did not get mad instantly and instead, he asked him to continue speaking of the matter. Clearly, he was treating him as an adult now.

Indeed, it wasnt appropriate to treat a person who was about to join a military training camp to prepare for war as a child.

Lord Yao had never been in good relationship with father. Furthermore, the both of you dont have much contact. Yet, he took the initiative to arrange a meeting with father this time. I fear that he might have bad intentions.

Wang Chong slowly explained.

Wang Chong knew that father didnt like his family interfering in his work affairs. These words shouldnt have come from the mouth of a fifteenyearold child, but Wang Chong didnt have a choice.

In his previous life, that Lord Yao, Yao Guang Yi, under the name of work, invited his father, who he never interacted with, for a meeting.

It wasnt that his father was not guarded against him. If Yao Guang Yi had tried to pull him over to his side during the meeting, his father would have flatly rejected him. Yet, this Yao Guang Yi was extremely cunning. He didnt say anything during the meeting and instead, he pulled father for a drink and chatted about miscellaneous matters.

After which, Yao Guang Yi intentionally pointed out the matter to King Song.

King Song is one of the relatives of the royal family and he is involved in military affairs of the country. He is one of the few people who wield great authority among the clan of the royal family. Due to Wang Chongs grandfather, King Song trusted Father Wang deeply.

On the other hand, the reason why Wang Yan was able to become a general who wielded actual authority at such a young age was highly due to King Songs contribution as well.

Father had a secret meeting with the King Qis loyal servant, Yao Guang Yi, and given the hostile relationship between King Song and King Qi, how could King Song not get angry at the situation?

If it was under usual circumstances, it would be nothing much.

However, King Song and King Qi was current vying with one another in the royal court, both on the surface and in the shadows. The current situation was volatile and a lot of King Songs students and old friends had been poached by King Qi. This caused King Song to be isolated and his authority in the royal court had fallen tremendously.

This incident had agitated King Song, causing him to become suspicious of his subordinates. At such a moment, his father had a private meeting with Yao Guang Yi. It was clear what King Song would think of him.

Furthermore, his father had an extremely straightforward and inflexible personality. He knew that King Song was suspicious of him, yet he claimed that he did not speak of anything with Yao Guang Yi, that the two of them only drank together.

How could two opposing authoritative officials in the royal court meet privately for just a drink? How could King Song possibly believe such a story?

Not only did his fathers words not dispel the suspicions King Song had towards him, it made King Song believe that his father had betrayed him and King Qi was intentionally sending him over to humiliate him in his face.

After which, Yao Guang Yi intentionally misled King Song and created a series of incidents by the border, causing the misunderstanding between King Song and his father to deepen.

He thought that the entire Wang Clan had decided to side with King Qi upon seeing that he wasnt in a good position.

As the saying goes, the deeper the love, the greater the hate. Given their close relationship, King Song found the betrayal of the Wang Clan unacceptable.

This incident had struck a heavy blow to him. He found it even harder to accept than when dozens of his students and old acquaintances had abandoned him for the enemy. King Song was completely disappointed with the Wang Clan.

When Wang Chongs grandfather was still alive, King Song, on accounts of their past fellowship, only stripped Wang Yan of his authority over the army. However, when his grandfather passed away, King Qi started to oppress the Wang Clan, which had already lost King Songs protection.

In a few short years, the prestigious Wang Clan was forced out of the royal court of the Great Tang Empire.

King Song was the head of the faction which supported an aggressive stand against those who were standing up against the Great Tang. After his fall, no one was able to oppose King Qi in the court. As such, the Great Tang Empires stand towards those against them became weaker and weaker. Eventually, it resulted in the catastrophe that caused its downfall.

One could say that this matter wasnt only detrimental to the Wang Clan and King Song, it was a catastrophe for the entire royal court!

All three factions were the losers in this battle. Even King Qi himself wasnt the final winner.

Wang Chong was well aware of how deep the impact of this matter was.

The entire Wang Clan and the destiny of the royal court changed from that moment. Even at his fathers moment of death, he was unable to put this matter down. He lamented that the greatest mistake in his life was to accept Yao Guang Yis invitation and not explaining the matter clearly to King Song.

Wang Chong remembered all of these clearly.

In his previous life, Wang Chong lived in a daze, pushing away everything from him. He thought that he didnt hold any emotions to this family of his. Eventually, when he awoke and started to cherish all that he had, it was already too late to change anything.

This was one of the deepest regrets that Wang Chong had.

Since he knew how things were going to advance, Wang Chong was determined not to watch by the side as his family and homeland were destroyed.

Thus, he had to stop this matter!

Just that, it wouldnt be easy for Wang Chong to explain this matter to his father.

Theres no need for a child for you to interfere in this matter. I know what I should do.

Father Wang said flatly. His face was still impassive.

The Yao Clan and Wang Clan were hostile toward each other, but this was a matter of the previous dynasty. A long period of time had passed and Wang Yan himself didnt really have a grudge with Yao Guang Yi.

It wasnt that Father Wang was unaware of the conflict between King Song and King Wang. He tried act as though he was oblivious to the matter, but he was afraid that by doing so, he would he would fall out with either side. Thus, he decided to meet him privately.

After all, there wasnt much of a grudge between the two of them.

At most, if Yao Guang Yi tried to pull him over to his side, he would just have to reiterate his stand firmly and reject it firmly. This matter would then be settled. If Yao Guang Yi chose to continue pestering him after he rejected the invitation to this meeting, that could pose as a potential trouble as well.

Studying the expression on his fathers face, Wang Chong was panicking.

His father was a classic military man. In terms of leading an army to slay the enemies, his father was in no way inferior to Yao Guang Yi. However, in terms of political fights and scheming, Yao Guang Yi far beyond his fathers league.

Both sides werent even of the same level!

Yao Guang Yi knew his fathers character and set this trap up for him, knowing that he would fall for it. If father still embraced his attitude of As long as I operate righteously and transparently, there is nothing to fear, he would be caught offguard and fall under Yao Guang Yis hands.

By then, it would be too late to regret.

Chonger, since your father had said so, you shouldnt touch on the matter any further. Hurry up and eat.

No one could know a person more than his own mother. Upon looking at Wang Chong, she instantly knew what Wang Chong was up to. Thus, she shot him a glance to urge him to stop.

She understood her husbands character too well. He hated people discussing work over the dining table. It was already a blessing for him to tolerate Wang Chong so far.

A single phrase I know what I should do had displayed his attitude toward this matter clearly. This matter was already decided and all discussion should stop here. If Wang Chong were to continue on, Father Wang would really get furious.

Wang Chong panicked within. Naturally, he was able to tell the intentions of his mother, but this matter was of grave importance. If things didnt go well, everything here, this dining hall, the entire Wang Clan and even his uncle would all be reduced to nothing.

The entire Wang Clan would be expelled from the political scene of Great Tang. His father didnt know about Yao Guang Yis schemes, so he wasnt guarded against him yet. Wang Chong had no choice but to continue pushing the matter on.

Even if his father would be enraged, even if he would be criticized by his father, this was something he had to do.

Father, this matter is of utmost importance. I think that perhaps you should inform King Song of it in advance. At the very least, if anything goes wrong The situation wouldnt be too bad.

Wang Chong contemplated for a moment and decided to change another approach. He used a softer approach to bring up his own suggestions instead. After all, stopping his father from attending the meeting was not a viable solution. His father wasnt a child and being too obstinate on the matter would just infuriate him.

Thus, Wang Chong could only think of an alternative solution. Instead of shooting down Yao Guang Yi, he decided to bring up King Song instead.

This is an affair among adults, you need not bother yourself with it.

Father Wangs expression was cold and he stood up from the table

You all carry on eating. I still have matters to attend to and I will be leaving first.

After which, he turned around and leave without even finishing his meal.

Mother Wang stared grudgingly at Wang Chong. Wang Chong could only sigh. He knew that this one time off performance was insufficient to win his fathers trust.

At the very least, he didnt lash out.

Wang Chong thought.

Even though on the surface, this meal ended unhappily, Wang Chong knew that given his fathers personality, his action of refuting him should have infuriated him.

Yet this time, he only looked displeased. This was a huge improvement. It seemed that his words had some effects on him.

As long as his father informed King Song of his meeting with Yao Guang Yi beforehand, his efforts would have not been in vain. This matter had to be done by his father personally. Not even he could represent him and do it in his place.

Looks like I will need Ma Zhou for this matter!

Wang Chong thought worriedly.

His fathers personality was too obstinate. The moment he made a decision, he wouldnt change it easily. Changing his mind with just a few words was something impossible.

It was due to this personality that his father suffered many injustices when his opponents made use of it against him.

Wang Chongs attempt to convince his father had failed, so he could only look for another solution. No matter what, he had to prevent this matter at all cost.

After coming up with a reason, he hurriedly bid his mother and little sister farewell and left the dining hall.
',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(12,'chapter 5','Chapter 5 Number One Expert of the Wang Clan

In the flower garden at the back courtyard, Wang Chong sat on a rugged manmade mountain, deep in thoughts.

He had already been contemplating for a very long time.

His father and Yao Guang Yi was going to meet at a place called Vast Crane Pavilion.

That old fox, Yao Guang Yi, was well prepared for it. He had already passed an order to evict all customers from the Vast Crane Pavilion.

At this moment, other than Yao Guang Yi and King Qis subordinates, no one else could enter. However, from the exteriors, it seemed as though the Vast Crane Pavilion was still brimming with customers and nothing was different from the usual.

Back then, his father was duped by this illusion and ended up going along with Yao Guang Yis flow.

Wang Chong was well aware that if he didnt enter, it would be impossible for him to prevent this disaster from happening. However, the Vast Crane Pavilion was filled with experts dispatched by Yao Guang Yi. It was impossible for him to barge in with brute force. Thus, he could only resort to his wits.

I got it!

Suddenly, Wang Chongs eyes lit up as he recalled a certain person. How could he have forgotten that fellow? With that fellow by him, he would definitely be able to sneak into the Vast Crane Pavilion. However, just this much wasnt enough to foil Yao Guang Yis schemes.

No. The Vast Crane Pavilion is filled with experts, I am insufficient to accomplish this task alone. I need a powerful expert!

Upon thinking of this, Wang Chong frowned once more.

He knew himself the best. If it was in his previous life, he wouldnt need to even go through so much trouble. Regardless of how many experts Yao Guang Yi had planted, with his heavenshattering level of cultivation, he could simply charge in and pummel Yao Guang Yi.

However, he was just a fifteenyearold kid in this life. With this little strength in his hand, how could he compete with those experienced and hulking experts?

With his current strength, it would serve no purpose for him to barge in. Yao Guang Yi could simply dispatch a single subordinate to expel him from the premises.

At this point, Wang Chongs frown deepened. The delight he felt just a moment ago scattered into the winds.

Where could he find an expert all of the sudden!

Just when he was feeling frustrated, Wang Chong suddenly heard the sound of footsteps approaching. He subconsciously looked up and saw his little sister frolicking about the courtyard as a few guards stood by her.

He had seen such a scene innumerable times in the past, but for some reason, this scene had evoked a completely different emotion in Wang Chong this time.

Ah, I truly am a fool!

Wang Chong slapped his forehead and chuckled. He was fumbling around to look for a lantern when one was right in his hands! Now that Big Brother and Second Brother wasnt here, who else would be more suitable to assist in his plan other than his youngest sister?

To think that he missed out such an expert right in front of him!

At this point, Wang Chong couldnt help but chuckle happily.

In the Wang Family, his youngest sister Wang Xiao Yao was a legend.

Despite being just tenyearold, her superior talent granted her boundless strength. It was said that she had carried a heavy cauldron when she was just threeyearold.

Wang Chong didnt witness this sight with his own eyes, but since he had heard of it from his mother, it was definitely true.

Wang Chong was even willing to bet that in the entire capital, and that was including his Big Brother and Second Brother, was able to surpass her in terms of talent.

This was even truer for her potential!

As for this aspect, Wang Chong had witnessed it personally in his previous life so he couldnt be any more clear of it. He was well aware of how fearsome his little sisters strength would become in the future.

Actually, the reason why his little sister wielded such fearsome strength was simple, it was because she was born with completely cleared meridians.

Controlling ones breath to extreme smoothness, to the point of an infants, this is a phrase from the Inscriptions of Enlightenment. As everyone knew, everyones meridians were completely cleared when they were still in their mothers womb.

Only after coming to this world and breathing the tainted air of earth will ones body deteriorate and meridians be blocked. At this point, they would fall from a heavenly being to a mortal one.

This occurred instantaneously during ones first cry at birth.

However, his little sister was different. She had a unique constitution and upon birth, her meridians werent blocked along with her first cry. Such physique didnt appear even among one out of a million people.

Thus, regardless of what martial art it was that she practiced, she was able to grasp it quickly, far faster than anyone else could.

The distinguishing trait of her talent was her overwhelming strength.

However, it was a pity that his little sister was still young and she had an innocent nature. She was also extremely mischievous. Thus, she often put off training. Even so, his little sisters strength was still incredible. She was able to easily compete with those who were ten years older than her.

In the Wang Clan, his little sister was, without a doubt, the number one expert. Wouldnt he be foolish to leave this number one expert idling in the house while searching outward for an expert he probably couldnt find?

Furthermore, even though she was vindictive and hated being fooled, she was extremely trusting of him. She would definitely be willing to do as he said.

At this point, Wang Chong leaped down from the manmade mountain.

Little sister, come here!

Wang Chong beckoned her over with a treacherous smile from afar

Brother will bring you to somewhere fun!



Third Brother, where are you bringing me to?

From the carriage, little sisters head popped out from the windows. Her eyes were staring at the bustling city outside as curiosity filled her mind. Her anger had subsided completely and all that was left in her was curiosity.

After all, she was only a tenyearold girl. Usually, her movement was strictly restricted and she was not allowed to head to the streets as she pleased. It took much difficulty to sneak out with her older brother without sounding mothers radar, thus she felt extremely excited.

Hehe, dont be anxious. Youll know in awhile.

Wang Chong chuckled

Remember the promise we made. Without my permission, you arent allowed to attack anyone. Otherwise, I will not bring you out in the future.

Orh.

Little sister nodded her head obediently, agreeing to his words without even thinking about it. She had complete trust to this older brother of hers. However, she soon thought of something, raised her tiny fists and waved them menacingly

Brother, dont you dare lie to me. Otherwise, you will be done for! Hmph!

Of course not! How could I possibly dare to lie to you!

Wang Chongs heart beat furiously in fear as he recalled the fearsome strength his little sister wielded. Just a casual pinch could make him die from the pain. If she were to really go on a rampage, wouldnt he be left on the verge of death?

Oh! Isnt that Young Master Chong?

In the midst of their conversation, a crisp shout could be heard.

Were here!

Wang Chong stepped off the carriage and a hint of a smile crept onto the corners of his lip. This was a pavilion he often frequented. Ma Zhou and the gang should be here.

Leading his little sister out from the carriage, Wang Chong saw a group of slovenly people carrying birdcages in their hands and waving their fans standing in the distance.

The group of people seemed to have expected his arrival and had been waiting here for him.

Finally! How long have I not seen you for, Young Master Chong!

The young man that seemed to be the leader of the group had a bamboo fan behind him. He had the looks of a scion to him and the black mole beneath his right eyebrow was extremely conspicuous. Upon seeing Wang Chong, he immediately rushed forward to greet him passionately.

This person was Ma Zhou!

Young Master Chong, we heard that you were punished by your family members and was grounded in your residence. We brothers had wanted to visit you, but the gates of the Wang Clan blocked us out. We were forced to retreat the last few times we visited you, so we had no choice but to give up on the idea. Young Master Chong, are you alright?

Ma Zhou immediately grabbed Wang Chongs hands and showed concern for him. Those who were unaware of the situation might even think that the two shared a deep bond. Just that, the disdain and mock by the corner of Ma Zhous lip could not be concealed.

Hmph, I havent noticed it in the past, but this fellow sure can act.

Wang Chong sneered coldly inside.

Outer appearances are defined by perception, and ones perception changes with ones state of mind. Back then, he thought that Ma Zhous smile looked sincere. Now, upon seeing it once more, he found him extremely arrogant.

Somewhere in the depths of Ma Zhous heart, that fellow had definitely taken him to be a fool. It was laughable how simple he thought the world was in his previous life. He often thought that treating another sincerely would earn him sincerity in return, thus he never tried to guard himself against these scums.

Ma Zhous words were a bunch of bullshit. Wang Chong had asked the guards before leaving, and not a single fly had approached the Wang Clans gates in the last few weeks, needless to say, Ma Zhou and gang.

You have been waiting for me here?

Wang Chong asked indifferently.

Ma Zhou was taken aback. Something seemed off about Young Master Chong today. In the past, he would be extremely warmhearted to him. Why did he seem so cold today?

However, he attributed it to his delusions and didnt pay it much heed.

Young Master Chong, it is precisely because we heard that you are coming out of seclusion that we specially waited here to initiate you back to society! Brothers, dont you all agree?

As he said so, he waved to his back and the crowd behind him replied him fervently. After which, they burst into laughter, as though watching a monkey being fooled about.

How is it, Young Master Chong? Shall we go?

Ma Zhou turned back to look at Wang Chong with a bright smile on his face. At which, the disdain and mock in his gaze deepened.

Wang Chong was truly too easy to fool, he fell easily with just a few words. Furthermore, this lad believed in chivalry and was completely unaware of the fact that everyone had treated him as a fool, that everyone ganged up to scam him of his money and dumped all problems onto him for him to shoulder.

Where else can they find such a money bag and shield?

As for initiating him back to society, although it was conducted for Wang Chong, Ma Zhou wasnt going to pay up. Ever since hanging out with Wang Chong, Ma Zhou had never paid a single cent.

To tell the truth, the few brothers of his had felt restrained in the few days Wang Chong was grounded and they were starting to miss him.

Without this young master here, who would help to clean up their mess and pay their tabs?

Thinking of this, Ma Zhou became even more gleeful.

Pah!

Suddenly, a slap flew towards the gleeful Ma Zhou. Pah! Ma Zhou staggered and a fiery sensation spread across his face. Half of his face had swollen up and the palm print on it could be clearly seen.

In that instant, the entire street fell silent.

Everyone was shocked by this slap!

What was happening? How could Ma Zhou be slapped?

You slapped me?

Ma Zhou grasped his burning cheeks as he stared at Wang Chong in astonishment. His head was spinning and for a moment, he was unable to comprehend the situation.

He was unable to grasp what was happening. Wang Chong had slapped him? How could this be possible?

Even the one was slapped was in such a state, lest needed to be said about the others. The other playboys had their eyes widened in shock and their mouths were so large that an egg could be placed within.

Wang Chong actually slapped Ma Zhou?

In this world, anyone could have slapped Ma Zhou, just not Wang Chong. One had to know that the one who was closest to Wang Chong was Ma Zhou.

Otherwise, Ma Zhou wouldnt have dared to fool him like that.

However, at this very moment, Wang Chong had slapped Ma Zhou in the middle of the street! Everyone was shocked by this occurrence. No one knew what was happening.

I slapped you.

Wang Chong stared at Ma Zhou with a smiling face. The only who seemed to be unaffected by the matter was him

Ma Zhou, I treated you as my brother in the past, and yet you took me for a fool. It cant be that you think that I dont know whats going on at all?

Hua!

Another wave of astonishment gushed over everyone. Their mouth widened even further. Was this still the simpleminded and easilyfooled Wang Chong that everyone knew of?

Was this still the foolish Wang Chong who proclaimed everyone as his brother and willingly allowed them to exploit him?

No matter what, it didnt seem possible that such words came from his mouth. Wasnt this change a little too big?

Everyone was stunned by the change in Wang Chong. Even more so, they felt a pang of conscience as though their scheme had been exposed. Could it be that this Wang Chong had been playing it dumb all along?

What?! Ma Zhou, he is Ma Zhou?

Just when everyone else was feeling apprehensive of the changes in Wang Chong, a bizarre voice rang out. The little sister of the Wang Family frowned and her round eyes widened. One could feel vaguely feel the rage that was spilling out of her.

In the start, she didnt know that this fellow was the culprit that harmed her brother. At this moment, when everything was clear to her, how could she still tolerate it

You bastard! To dare to bully my brother, I will beat you to death!
',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(12,'chapter 6','Chapter 6 Vengeance of a Gentlemen

Talk wasnt sufficient for the little girl, she wanted to take action as well. Wang Chong knew of her fearsome strength and if she were to be allowed to strike a blow out of rage, Ma Zhou would probably die on the spot. If so, his plan would be screwed as well.

Little sister, cool down!

Wang Chong patted the shoulders of his youngest sister and hurriedly cajoled her. Leave this kind of little things to me. Dont forget, we have an agreement. It cant be that you are refusing to listen to my words?

Ah!

Little sister was conflicted. She knew that her brother being grounded for a week was a result of the doing of this Ma Zhou bastard.

Given her personality, a person who dared to hurt her family would be killed in a single punch of hers. However, she couldnt disobey the words of her brother.

Alright then.

Little sister lowered her head, choosing to obey her brothers words in the end.

Wang Chong smiled. This was exactly as how he remembered his youngest sister!

Ma Zhou, putting everything in the past aside, you used my name to rape a female villager in daylight. Did you think that I wouldnt know anything at all?

Wang Chong said indifferently. He glanced at Ma Zhou and the coldness of his gaze penetrated the other partys bones. For some reason, everyone felt terrified and uneasy. It was as though he had become an entirely different person.

Things have blown up!

This fellow actually knows of it all?

F, who was the one who told him?



The impression Wang Chong gave them today was one who was suddenly enlightened. To think that he would suddenly awaken from his dreams! The scions immediately retreated backward. Subconsciously, they felt that it was impossible to settle the matter today peacefully.

On the other hand, disbelief and shock reflected on Ma Zhous face. Eventually, all of it faded into calmness. Even the right hand that was grabbing onto his cheeks had loosened and fallen downward.

Truthfully, Ma Zhou didnt expect Wang Chong to suddenly become so smart. It felt as though nothing could be hidden from him anymore.

As though everything that had been done in the past had been exposed.

Wang Chong, you were the one who asked for it!

With a dark expression, Ma Zhou howled savagely.

Of all things possible, Wang Chong should have never slapped him in front of so many brothers. How could his pride allow him to take it lying?

Even more so, he shouldnt have pointed everything out. If he was truly smart, he should have known to just drop the matter instead of speaking of them. At most, he could have just chosen not to roam on the streets in the future.

Wasnt hanging out with other people, even if hypocritically, better than this?

Did he think that just because he called him Young Master Chong in the past, he had become the head of the group?

Ma Zhou stared at Wang Chong coldly and haughtily, not even trying to hide the disdain and mock in his gaze at this moment.

Damn, Ma Zhou is going to rage!

What the heck, we should hurry up and get away! Ma Zhous rampage isnt a joke!

The last time, Ma Zhou had crippled a scion who was in Imbued Bone realm. Given how Wang Chong is only at Imbued Blood realm, he is in for a difficult time now that he had angered Ma Zhou!



The surprise and dumbfounded looks on that bunch of vile young faces were replaced with a gloating one.

Ma Zhou was a bastard, but the others werent fools. If he werent capable, there was no reason for them to take him as their leader.

This fellow was never an easy figure!

Everyone could already see Wang Chong been thrown around by Ma Zhous punches with his teeth scattered all around the road.

Ma Zhou was feeling unhappy now, extremely unhappy.

Wang Chong was only a puppet beneath him, yet this puppet was trying to climb over his head now.

How could Ma Zhou stand this?

Kacha, the crisp sound of bones cracking could be heard from Ma Zhous body. Blood surged through his body steadily as though a stream and a powerful strength burst from the depths of his body.

Imbued Core realm!

Ma Zhous strength had already reached Origin Energy Tier 4. He had already reached the level of drawing Origin Energy to cleanse his bones of impurities. He was much stronger compared to the little Young Master Wang Chong, who was still at Origin Energy Tier 3!

To think that you would dare disrespect me, you are looking for it!

Ma Zhou sneered savagely.

Is that so?

Wang Chong smirked coldly. There wasnt a hint of fear in his eyes. Ma Zhou was taken aback. For some reason, the feeling Wang Chong generated in him was extremely bizarre, as though he was an entirely different person.

Putting the matter aside, Ma Zhou stepped forward, advancing as though a strike of lightning, and shot a punch straight toward Ma Zhou. Kacha. The crisp sound of bones breaking echoed in the air. Ma Zhou seemed to have heard the sound of Wang Chongs bones breaking. Before he could even cheer, he heard exclaims from the surroundings

Ma, Ma Young Master Ma, your nose!

The youngsters by the side had their eyes wide in fear as they stared intently at Ma Zhous nose, as though something it was something fearsome.

Whats wrong with my nose?

Ma Zhou was astounded by their words. The moment such a thought flashed across his mind, he immediately felt a pain that penetrated straight through his heart. A fire seemed to be burning in his nostrils and searing fresh blood with flavors of all kind, sour, bitter, spicy and sweet, seemed to gush out simultaneously.

My nose!

Ma Zhou howled in agony. That voice was sharp and distinct, causing everyone goosebumps. In this instant, Ma Zhou realized that the sound of bone breaking came from his nose breaking, not from Wang Chong.

The nose bone was the softest bone in the entire human body, as well as the weakest one.

Upon being punched in the nose, Ma Zhou felt his entire body losing its strength as he slumped into a kneeling position on the floor while clutching his nose. Clearly, he had lost all fighting will.

Ma Zhou was still unable to understand how he sustained that punch!

Given how even Ma Zhou himself was unable to comprehend the situation, the others were even more confused. In their vision, they only saw Wang Chong walking half a step to the side, causing Ma Zhous punch to hit empty air. At the same time, Wang Chongs punch struck Ma Zhous nose.

They felt their blood curdle!

It was not the first time they were hanging out with Wang Chong and they knew Wang Chongs fighting prowess well. To think that an Origin Energy Tier 3 Imbued Blood realm was able to defeat an Origin Energy Tier 4 Imbued Core realm?

This wasnt right!

He sure is vicious!

Upon seeing Ma Zhous pathetic situation, the others felt their goosebumps rising on their arms once again, and a few of them immediately ran away.

Ma Zhou, these two slaps are for the people who you have once oppressed!

Wang Chong grabbed Ma Zhou by his pair and pah pah, two slaps were bestowed upon him. Ma Zhou was the type who only possessed brute strength. In terms of technique and fighting intuition, he was far inferior to Wang Chong.

Even if you wanted to going about scamming and oppressing others, there should be a limit to it. To actually rape a married woman Dont you know that I hate this kind of action the most?

As he spoke, pah pah, another two slap rained down on him, knocking Ma Zhous teeth out of his mouth.

Brother, good slap! Good slap!

The tenyearold younger sister was cheering by the side. Watching the scene, she felt her own resentment being vented. Although she wasnt able to do it herself, watching her brother deal with this fellow was also exhilarating.

Only after sending any two fists onto him did Wang Chong feel his anger subsiding slightly. Regardless of whether it was this life or his previous one, Wang Chong hated those who bullied the weak. He found it exceptionally unbearable when Ma Zhou and the gang used his own name to commit such atrocity.

This was precisely the reason why his parents grounded him and the Wang Clan was humiliated. As such, Wang Chong struck even harder and viciously!

AHH! You bastard, you will pay for it!

Ma Zhous eyes were red with frenzy and his body trembled with rage.

Pah!

Wang Chong suddenly kicked Ma Zhous crotch and vaguely, one could hear the sound of something shattering. The pain caused the latter to howl in pain as he hurriedly grabbed his crotch.

His entire face turned pale and perspiration beads fell from his forehead profusely as though rain. Only the sound of his pained exhalation could be heard.

Ma Zhou, dont think that I do not know that you are Yao Fengs subordinate. He is making use of you to deal with me. Living off the authority of others, did you really think you are really something big?

Wang Chong walked over and glared down at Ma Zhou coldly.

Ma Zhou was a person without any background. Without anyone instigating him from the back, how could he dare to make a fool out of Wang Chong?

Upon seeing Ma Zhous flabbergasted look, Wang Chong knew that he had guessed correctly. In the entire capital, the only one who wanted to deal with him was Yao Feng.

Even though Yao Feng didnt have a grudge against him, he had some conflicts with Wang Chongs Big Brother and Second Brother. As such, he instigated Ma Zhou to deal with their younger brother.

Wang Chong, dont you dare get gleeful! Acting so haughtily in front of me, I dare you to do the same in front of Young Master Yao! Indeed! I used your name to rape a woman outside, but what about it? It was all instigated by him, go at him if you dare!

Ma Zhou stiffened his neck and roared.

Heh, Ma Zhou, did you think that I dare not to?

Wang Chong was waiting for these words. In order to enter the Vast Crane Pavilion, he needed this brother to lead the way for him.

I dare you to lead the way then. I would like to see what Yao Feng has to say about this affair.

Wang Chong sneered coldly.

Hua!

Ma Zhou seemed to have found energy from some unknown source and abruptly forced himself up. A cold gleam flashed across his eyes and from it, one could feel bonepiercing hatred

Wang Chong, follow me if you are a man! Whoever who backs out will be a cowardly bastard!

If he didnt get back at Wang Chong for this, his pride wouldnt allow him to live on!

He wasnt able to deal with Wang Chong, so he could only leave it to Yao Feng to avenge him!



Things went even smoother than Wang Chong expected, With Ma Zhou leading the way, Wang Chong soon arrived at Vast Crane Pavilion.

At the center of the bustling city, an octagonal pavilion appeared. It was built with an arched roof with majestic pillars propping it up. There were four levels to the pavilion and goldenplated red lanterns hung layer by layer down the eight corners, bringing out an elegant disposition to it.

Returning back to a familiar land and seeing a familiar infrastructure, Wang Chong couldnt help but recall the past.

In his previous life, when he returned to this Vast Crane Pavilion, it was already old and in tatters. The corners were filled with dust and spider webs, far the previous prosperity and liveliness it had once experienced.

This was the turning point for the destiny of the Wang Clan!

In his previous life, even on his deathbed, his father couldnt help but remember this Vast Crane Pavilion. As such, Wang Chong also visited this place over and over again, touring the tattered remains of the Vast Crane Pavilion to reminisce the past.

If that didnt happen, everything would probably be different!

Wang Chong thought.

Now that everything had returned back to the starting point, he finally gained the opportunity to stop everything in his tracks in the place of his father, as well as to salvage everything. Just that, his father no longer remembered anything.

Wang Chong, I dare you to enter with me!

By the side, Ma Zhou was already done conversing with the guards in the Vast Crane Pavilion. He angrily beckoned Wang Chong over. The Vast Crane Pavilion had already banned all customers from entering. Other than those from the Yao Residence and King Qis subordinates, no one was allowed to enter.

However, Ma Zhou was an exception. He was a lackey of Yao Guang Yi and he was acquainted with the guards of the Yao Residence. The only one who could allow Wang Chong in was this Ma Zhou.

Why? Are you afraid?

Ma Zhou snickered coldly in an attempt to taunt him, fearing that Wang Chong might go back on his words.

Hmph, cut the nonsense and lead the way.

Wang Chong scoffed.

Estimating the time, his father should have already entered the Vast Crane Pavilion. How everything would go from now on depend on what he did today. Taking a deep breath, Wang Chong, along with his little sister, walked into the Vast Crane Pavilion.

Only upon walking into the Vast Crane Pavilion did one realize how vibrant the tavern was. This was nonsense. In every floor, there were more than two hundred seats but there wasnt a single person on any of it.

Wang Chong could clearly tell that the people in here were Yao Guang Yi and King Qis subordinates. There were even those who once followed King Song.

In scheming against his father, those who betrayed King Song played a vital role. Yao Guang Yi had truly put a lot of effort into this matter.

However, Ma Zhou seemed to be ignorant of the matter. He couldnt tell what was different with the atmosphere and all he did was to taunt Wang Chong forward, as though afraid that he would back out and retreat at the last moment.

Hurry up! Hurry up!

Rascal, are you regretting your decision now?



After suffering such indignance, how could Ma Zhou simply let this matter slip? All he wanted now was to use Yao Fengs hands to teach Wang Chong a lesson.

What are you hurrying me for, do you think that I am afraid of Yao Feng?

Wang Chong behaved as though he had fallen for his taunting, but he was sneering coldly inside. He was thankful for his relationship with Ma Zhou. If this fellow knew what was going on in the Vast Crane Pavilion at this moment, he wouldnt dare to bring him here even if he had ten times of his guts.

Flinging his robe, Wang Chong followed Ma Zhou up the stairs.

The child and two teenagers did not catch anyones attention.
',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(12,'chapter 7','Chapter 7 Taunting Yao Feng

In a refined room in the third floor of the Vast Crane Pavilion, a group of young gentlemen who were dressed extravagantly, seemingly originating from wealthy families, were gathered together. All of them proposed a toast to the luxuriously dressed, dignified young man who sat in the main seat.

Yao gongzi, cheers!

Yao gongzi, allow me to propose a toast!



The young gentlemen chattered among themselves and their words were filled with pleasantries. They all seemed to have treated the latter as their de facto leader and acted subserviently around him. Young Master Yaos words were refined and he was a rising figure in the capital. Furthermore, he had a powerful background which was exceptional even among the entire group.

Please do not stand on ceremony. Allow me to play host today, you all should enjoy yourselves!

The luxuriously dressed young mans movements were elegant as he returned the toast to the group. His expression was calm and magnanimity exuded from his gestures. He stood out even among the group of refined gentlemen.

The Yao Residence had an imposing reputation in the capital, especially given their blood ties with King Qi. Young Master Yao was the eldest son of the Yao Residence and in the future, he would inherit the standing and influence of the Yao Clan. He was a figure that was destined for greatness.

Thus, even though this was the first time Young Master Yao Feng had invited them over, they delightfully accepted and attended this gathering.

After exchanging a few toasts, the atmosphere became vibrant.

Seated in the main seat of the room, Yao Feng inconspicuously glanced at his surroundings and delight surged through his heart. He nodded his head in approval.

Today was his joyous occasion. Using his fathers reputation and the name of the Yao Family, Yao Feng had invited all respectable and influential young masters in the capital over to the Vast Crane Pavilion.

This was the first time everyone was gathering together and to Yao Feng, this was an opportunity to win their hearts over and establish his prestige. As long as he was able to buy these young masters over to his side, he would be able to become one of the leaders of the youths in the capital.

Furthermore, his father had already promised him that if he were able to win the hearts these respectable gentlemen over to build his own following, he would gradually grant him authority and slowly pass the family business over to him.

Without doubt, such a promise was equivalent to granting him the position of the successor.

As such, this gathering was of absolute importance to Yao Feng!

The mood at the gathering was pleasant and joyful. Yao Feng could tell that everyone was intentionally fawning over him, that people were still in awe of the Yao Residence. However, that wasnt enough. Thus, with the intention to win their hearts

Everyone, those who are seated here are my brothers. No matter what you all need in the future, feel free to bring it up

Young master, young master Ma Zhou had brought two people here!

The wooden doors to the room opened and the manager of the Vast Crane Pavilion rushed in, interrupting Yao Fengs words halfway through.

Whats wrong, for you to be panicking like this?

Yao Feng frowned. He was displeased with the managers actions, but he didnt express it blatantly.

Didnt I say to allow no one to interrupt me now? Tell Ma Zhou to come later.

Yao Feng said.

I have already said it

The managers voice trailed off. After hesitating for a moment, he decided to say it, But he has already barged in!

The moment those words were spoken, silence fell over the room.

Initially, Yao Feng was just slightly displeased but after hearing those words, he couldnt help but be infuriated. Ma Zhou was just an insignificant pawn to him. If he were to wreck such an important meeting, Yao Feng would not let him off easily.

Young Master, why dont we

A young man dressed in silk tried asking.

Theres no need!

Before the other party could finish his words, Yao Feng interrupted him.

Everyone, a small matter had propped up. I apologize for my impertinence. Manager Zhang, please hand this over to him. Tell him that Ill meet him personally at the end of the gathering.

With a jerk of his hand, Yao Feng threw the golden token on his waist over.

Yao Feng rarely used his token, unless something major had occurred. If Ma Zhou was wise, he should be able to understand his intentions after looking at this token.

If he still chose to barge in even after that to ruin his opportunity, then he couldnt blame him for being merciless!

Young Master Yao, why go through so much trouble? Itll be more convenient if we just enter straight!

A cold sneer could be heard from beyond the doors. The door was kicked open and Wang Chong walked in.

Wang Chong?!

Upon recognizing the person walking in, Yao Fengs entire body trembled. He couldnt believe in his eyes. He never would have thought that this person would barge in on him.

Why? Young Master Yao, do you not welcome me?

Wang Chong smiled. He was extremely satisfied with Yao Fengs shocked expression. Before Yao Feng could even say anything, he had already pulled out a chair next to Yao Feng and sat down on it.

Seeing Wang Chongs actions, Yao Fengs eyelids twitched. He suppressed the anger in his heart and smiled in response

How could that be? All that comes are my guests, and I wont treat any of my guests rudely.

No matter what, he was still the son of a distinguished family. Every action of his exuded grace and magnanimity. On this aspect, few families were able to match up to him. At this moment, Wang Chong was rather impressed by him.

It wasnt without a cause that Yao Feng was able to become one of the leaders of the younger generation in the capital. At the very least, his attitude and disposition were something unmatched by others.

However, it was a pity that Wang Chongs purpose here was to cause trouble. Also, Yao Feng had yet to possess that distinguished position he had in his previous life.

Heh, little sister, hurry here! There is someone who wants to treat us and there is a lot of good food here! See, your brother never lies to you!

Without turning back, he beckoned the person behind him.

Really?

It was an immature and crisp voice. Everyone else stared in shock as a round face emerged from the wooden door behind Wang Chong. Her large eyes glimmered in curiosity as she looked around the room. Soon, her gaze fell upon the delicacies spread out over the table.

Waaa, you arent lying!

The young girl was awed. She stepped into the room, walked over and sat beside Wang Chong. Grabbing a fragrant chicken leg over, she started to dig in.

She completely ignored the presence of the other guests in the room. Her young mind wasnt able to process that much. Since her brother said that it was alright to dig in, she dug in.

She couldnt be bothered to think about anything else.

Un, delicious! This is really delicious!

The Wang Familys little sister was a person with voracious appetite. She might be of small stature, but the same could not be said about her stomach. With two or three chomps, she finished a giant piece of chicken legs. While eating, she complimented the food and the bones that emerged from her mouth were completely clean. In fact, teeth marks could be seen on it.

The dishes of the Vast Crane Pavilion had the fragrance, outer appearance and taste befitting of a renowned restaurant in the capital. Their dishes, regardless of whether they were steamed, fried, broiled or barbecued, they were done to the point of perfection.

Even though the food she ate at home were remarkable delicacies as well, how could they compare up to the Vast Crane Pavilion, given the amount of effort the cooks here put into the food? The little sister of the Wang Family was delighted and at this moment, the only thought that was in her mind was that her brother was a wonderful person. She unleashed her appetite and tackled the food frenziedly, filling her hands and mouth with oil

In the short amount of time since entering the room, she had already cleared two plates. Her eating mannerism and her appetite caused Yao Fengs eyelids to jump.

He wasnt bothered by the speed at which the dishes on the table was being cleared. The Vast Crane Pavilion belonged to his family and food wasnt a problem. However, these two siblings were obviously here to cause trouble.

If this Wang Familys little sister were to be allowed to continue eating like that, who would be willing to dig in? At this rate, this gathering would be ruined. In fact, Yao Feng could already feel the atmosphere cooling down.

Normally, he would just allow such a matter to slip. However, this was the first time he was approaching the various gentlemen of the capital to win their hearts, so the matter was of utmost importance to him. How could he allow these two insignificant figures ruin such a vital affair?

Your younger sister sure is cute! Young Master Wang, all that comes are my guests. How about this, I will play host and prepare a feast at another room for Wang gongzi, and all food and drinks are on me! How do you think of it?

Not a hint of displeasure could be seen on Yao Fengs face and his actions were still calm and elegant. He tried to persuade the two of them calmly, as though oblivious to the fact that the siblings were here to wreck havoc.

The other gentlemen seated in the room were impressed by Yao Fengs magnanimity. Through his actions itself, much could be said about the Yao Family and their culture. It was truly a prominent family which few could match up to.

Wang Chong had been taking in Yao Fengs expressions and the slight twitch at the corner of the latters eyes couldnt hide from his gaze. He knew that his strategy was working.

It is more enjoyable for us to have fun together! How can eating and drinking by oneself compare to the commotion here? I have heard that Yao gongzi is known for his magnanimity, so surely you wouldnt mind our addition into the room?

How could Wang Chong be driven away that easily? It was difficult for him to enter this Vast Crane Pavilion and if he were to be easily sent away by Yao Fengs words, wouldnt his efforts go to vain?

I would like to see how much longer can you endure!

Wang Chong sneered coldly within. He was here to infuriate Yao Feng. Regardless of how deeply hidden the intentions of this Yao gongzi were and how tolerant he was, Wang Chong was determined to have him erupt into anger.

Upon hearing Wang Chong words, Yao Fengs expression finally changed. He didnt expect Wang Chong to ignore his kind persuasion. It was clear that he was reluctant to leave.

Wang gongzi, if Im not wrong, I dont think that I have invited you here? Its not that I am a stingy person, but Im afraid that I would have to invite you to leave.

Anger had appeared in Yao Fengs eyes.

Heh, I know that you didnt invite me, but I cant simply skip out of this gathering! Otherwise, it would be disrespectful to Yao gongzi!

Wang Chong sneered coldly. He grabbed a bottle of alcohol before another guest, uncapped it and tried it. He complimented

Good wine!

Even though Yao Feng was wellcultured, his eyelids twitched furiously in response to Wang Chongs provocation.

What do you mean by I didnt invite you, but you couldnt skip this meeting?

Do I, Yao Feng, need your respect?

If not in consideration of the scions of authoritative officials in the room, Yao Feng wouldnt have been bothered to talk to a child like him and directly chased him out.

Ma Zhou, you bastard! To think that you brought him to me! I will skin you alive!

Yao Feng was filled with fury.

He knew clearly that the youngest son of the Wang Family had never met him before, and that it was impossible for him to know what he had done. For Wang Chong and his younger sister to come knocking, that bastard Ma Zhou must have exposed him!

However, it wasnt the time to be pursuing this matter!

Wang Chong, I will ask you one last time. Do you really want to go against me?

Yao Feng said sharply.

It was clear that he wouldnt leave no matter what he said, so it was no use acting politely to him. Everyone could tell that Wang Chong was here to mess up the situation.

Hmph, Yao Feng, this is called an eye for an eye!

Wang Chong smashed the wine cup on the floor. Pah! It shattered into innumerable fragments.

When pleasantries fail to work, ones true intentions are be revealed!

Immediately, the atmosphere grew tense. Even the most foolish of people could tell that the siblings purpose here was Yao Feng. They were intentionally trying to wreck havoc here.

Just that, there was one thing that everyone didnt understand. Who were these siblings, to actually dare to stand against Yao Feng? One must know that the Yao Residence possessed exceptional standing in the capital.

Wang Chong? Youre from the Wang Clan?

A sudden exclaim. A scion in the gathering had recognized Wang Chong.

Thats right!

Wang Chong replied frankly.

Upon hearing this answer, the expressions of everyone here changed. It was no wonder that the siblings dared to stand against Yao Feng, not even buying the Yao Familys tab. They were from the Wang Clan!

If it was just Wang Yan, there were also many other generals in the militaristic Great Tang Empire who had positions equal to him. But in this capital, everyone knew that there was another powerful figure in the Wang Clan.

That person was the one who was truly fearsome!

So its the descendant of Duke Jiu! Please pardon my insolence.

Is your grandfather still doing well?

I am from the Wei Family in the capital. Your grandfather had once helped my father, Wei Li, and he had been thinking about the matter. Young Master Wang, if possible, please help me convey the message to your grandfather that the Wei Li hopes to pay respects to him when hes free!



At this moment, their attitudes toward the Wang siblings changed and they became incomparably respectful toward them. The words Duke Jiu held distinct standing in the capital, changing the standing of the Wang siblings in an instant.

They thought that this was only a farce, but if Duke Jiu, who was in seclusion now, wanted to deal with the Yao Family, then everything would be different.

This wasnt something any ordinary families could get involved in!
',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(12,'chapter 8','Chapter 8 Wrecking Havoc at Vast Crane Pavilion

Young Master Yao, I suddenly recalled that I still have things to attend to. I will first take my departure!

Young Master Yao, lets gather together again some other time!



After some pleasantries, a scion stood up and left. Soon, more and more people followed suit.

Yao Fengs face steeled. Soon, his face became as dark as the bottom of a pot. The reason why he had been keeping his anger in check was to persuade Brother Wang Chong to leave peacefully without alarming the others.

Yet, his efforts still ended in failure. This gathering that he had put together with much effort was forcefully destroyed by this sibling pair.

Even though these people said that they were interested in gathering together another time, Yao Feng knew that they were all talk. They have already caught the scent of something deeper and for a period of time, they would try all means to steer clear of him.

Wang Chong, since you two dont want to leave, then stay here for all eternity! Men, take them down! I would like to see what the Wang Clan has to say about this!

Wrath shot forth from Yao Fengs eyes. He slammed the table and stood up.

Boom!

With a gust of wind, the two guards of Yao Family, who were already awaiting commands from beyond the door, barged in. They had towering physiques and a savage expression hung on their faces. Without saying a word, their hands assumed the shape of an eagles claws and they charged right as Wang Chong and Wang Xiao Yao!

The movements of the two were agile and fluid. It was clear that they were from the army their moves were from the standard techniques imparted in the barracks for capturing an enemy alive.

This technique was aimed at locking ones shoulders. If Wang Chong and his sister were to be caught in their attack, they would definitely suffer greatly.

Little sister, do it!

Sensing danger, Wang Chong howled loudly.

He had made his youngster sister promise not to attack anyone without his permission. Even when dealing with Ma Zhou, he didnt allow her to make a move. However, he didnt hesitate at this moment and gave her the permission to act as she pleased.

The two guards of the Yao Family were extremely swift. In the blink of an eye, right after Wang Chongs voice sounded, the little sister of the Wang Family moved!

Kacha! No one knew how she did it, but with the crisp sound of a bone breaking, the first guard of the Yao Family screamed in agony and knelt to the floor. His left palm was bent to a bizarre angle backward.

The fate of the second guard was even worse. Spitting out a mouthful of blood, he was sent flying backward as though a kite whose string was cut. Boom, his body crashed through the wall and he flew out of the building, leaving a giant hole in his wake.

In a blink of an eye, the battle had already ended! The Wang Familys little sister was still standing on the table with a chicken leg in her oily hands.

Silence!

The entire room was silent!

Shock showed through Yao Fengs eyes, and the others took it even worse. No one could have imagined that such massive strength could be harnessed within the small body.

The two welltrained guards of the Yao Family were unable to withstand a single blow from her. It was a complete defeat!

Deng deng deng!

The few guests who sat close to the little sister of the Wang Family subconsciously retreated and fear could be seen in their eyes.

Too scary! How can she possess such fearsome strength?

I probably will die if that fist were to land on me. How can a tenyearold girl be so scary?

The guards of the Yao Family are all elites! If I didnt witness this sight myself, I would have never believed it possible!



Everyone left in the room was dumbfounded. Countless people sparred in the capital every day, but none of which was as shocking as the strength which that tenyearold girl just displayed!

I will beat to death anyone who dares to hurt my brother!

The little sister of the Wang Clan sat on the table and glared at the others with widened eyes. Even though she looked delicate and young, who would dare to underestimate her now?

Well done!

Wang Chong was delighted and he secretly shot her a thumbs up. As expected of the number one expert of the Wang Clan! It was exactly as he remembered it. Even though she often slacked off, her talent was still much too incredible for average people to match up with her.

Her blows had no techniques whatsoever behind it, but her incredible strength and reaction speed still made it easy to wreck these guards of the Yao Family.

Yao Feng, my apologies. You arent enough to deal with us siblings!

Wang Chong added oil to the fire and sneered coldly at Yao Feng.

Audacious! Take them down!

Yao Feng flew into a rage. Agitated by Wang Chong, his entire body was convulsing rapidly.

For the elites of Yao Residence to be unable to defeat a little girl, the reputation of the Yao Family would be tarnished if word were to spread out. The gathering might have been ruined, but he had to capture the Wang siblings to uphold the dignity of the Yao Clan.

After which, he would march to the Wang Clan to hold them accountable for whatever that happened.

Boom! The moment Yao Feng raised his hands, four experts from the Yao Residence made their moves at the same time. In terms of cultivation realm, these four were much stronger than the other two whom the little girl crushed previously.

If they were unable to take them down with this, the reputation of the Yao Clan would really go down the gutters.

This time, Wang Chong didnt simply wait for the attacks to come. At the same time Yao Feng dispatched the four experts, Wang Chong pointed to Yao Feng and bellowed

Little sister, grab him!

Shoot the mount before shooting the rider. Capture the leader before capturing the other bandits, Wang Chong hadnt forgotten his purpose here. What was the use of defeating mere guards of the Yao Residence? It wouldnt mean anything even if they were to defeat all of them. Taking down Yao Feng was the priority.

Since Yao Feng had already made a move on them, there was no need for Wang Chong to hold it in any longer.

Boom! Boom!

The little sister of the Wang Familys explosive strength and speed showcased its might at this instant. Peng peng peng, the four experts of the Yao Residence barely stopped her for a few seconds before they were sent flying.

The little sister of the Wang Family charged ahead. She stepped across a table and grabbed the one sitting on the main seat, Young Master Yao.

Audacious!

Yao Feng stood up and a cold gleam glimmered in his eyes. He was completely enraged by Wang Xiao Yao and her brothers actions. A ripple spread through his clothes. Boom, a shockwave could be felt within five feet of them and the sound of billowing waves crashing down onto the shore echoed in the room.

Wang Chong was standing at the entrance of the room. Despite being a table away from him, he could feel fearsome pressure from this stormlike aura. He was forced to subconsciously retreat several steps backward. He was astonished by Yao Fengs might.

Peng!

Almost instantaneously, Yao Fengs massive fist and Wang Xiao Yaos tiny fist encountered one another violently.

Boom! A gale rose within the room and the shockwave from their encounter blasted in all directions. It seemed as though a strike of lightning had suddenly blasted through the room. Deng deng deng, Yao Fengs body staggered and he swiftly retreated three steps backward.

At the same time, the body of the Wang Familys little sister swayed and she took half a step back.

Hmph! Where do you two take this place as? Do you all really think that no one would be able to suppress you two?

Yao Fengs face paled. At this moment, rage had completely clouded his mind. All that remained in this room were his people and there was no need for him to feign as a gentleman any longer

Men! Attack them along with me. If we take down this pair of siblings, everyone will be rewarded with a thousand gold and a chainmail!

Before he even finished his words, Yao Feng exerted strength onto the floor and dashed forward. Everyones attention was jolted by his words and they charged forward as well.

Even the other scions, who didnt intend to make a move at the start, charged forward along with the rest.

In the Great Tang Empire, the name of Duke Jiu was deafening. However, it was a pity that not everyone had a choice. The families behind them were closely tied together to the fate of the Yao Clan.

If Yao Feng was humiliated, their position wouldnt be any better than his!

Not good!

Wang Chongs was astonished. In an instant, his heart felt as though it was submerged in a river of ice.

He had planned out everything, taking into account every detail, but he didnt expect Yao Feng to be that strong. Even though he wasnt as prominent as he was in his previous life, Yao Feng still showcased his fearsome talent in martial arts.

His youngest sister was insufficiently capable of defeating him!

If they were unable to capture Yao Feng, his plans would go to naught.

In an instant, a strong feeling of danger struck him.

Wang Chong knew that a giant loophole had appeared in his plans. If he didnt settle it right now, everything he did would be in vain.

Yao Feng gongzi, well help you!

Such words rang from all directions and all of the scions charged forward simultaneously.

At the same time, the sound of their footsteps rang throughout the entire pavilion. All of the experts Yao Guang Yi had planted all over the Vast Crane Pavilion charged towards the source of the sound.

Brother! 

Wang Xiao Yao yelled. Facing so many experts, she panicked. She might be powerful, but regardless of how daring she was, she was only a tenyearold girl.

She had never faced such situations before!

Little sister, dont panic! Listen to me!

At that tense moment, Wang Chong suddenly calmed down. Somehow, his voice held certain calming quality to it and upon listening to his voice, Wang Xiao Yaos anxiety disappeared and confidence arose within her once more.

Tiger Traversing the Stream!

Upon hearing Wang Chongs voice, the Wang Familys little sisters body collapsed inward before charging forward abruptly.

Ah!

A scream of agony could be heard. A guard of the Yao Residence was kicked squarely by the Wang Familys little sister and he shot through the walls, breaking through the pillars and crashing into the streets.



Brother Wang, come, cheers!

At the underground level of the Vast Crane Pavilion, there was a giant space filled with gems and pearls. Two mountainlike figures sat opposite to one another and cups of wine knocked against one another.

No one could have imagined that this was where the fate of two great clans of the Great Tang Empire would be decided.

Yao Guang Yi wore a loose robe and he was seated very formally. He kept pouring more wine to Wang Yans cup, yet keeping completely silent about his motive behind this gathering.

Brother Yao, the reason for your invitation for this gathering

Hahaha, Brother Wang, lets put that aside first. You and I are generals who guard the border and its not every day that we get to meet one another. Lets not talk about anything else and just drink.

Yao Guang Yi interrupted before Wang Yan could finish his words. Even though they were both generals, Yao Guang Yis gestures made him look more like a general who depended on his wits rather than strength.

Wang Yan frowned and his lips moved, but he held back what he wanted to say.

Wang Yan was a traditional militaristic general and he had a straightforward personality. Wang Yan thought that Yao Guang Yi had invited him here to pull him over to his side.

The moment Yao Guang Yi tries to invite him over, Wang Yan was determined to reiterate his stand to reject him firmly.

Yet, after drinking a few cups, Yao Guang Yi didnt speak of anything at all, making Wang Yan at a loss of where he should bring up the topic. Logically, Yao Guang Yi should be even more anxious than he was, yet the situation occurring at this moment was proving him wrong.

This made Wang Yang doubt his judgement. Perhaps Yao Guang Yis true motive wasnt to recruit him?

He found the situation amiss, but in the depths of his heart, Wang Yan sincerely hoped that Yao Guang Yi only invited him over to chat.

Regardless of whether it was the Yao Clan or the Wang Clan, they were both pillars helping to prop up the empire. If they could work together as one to serve the empire, that would be a blessing of the Great Tang, as well as the world.

Besides, regardless of the situation, Wang Yan was without fear. A gentleman worked in the open whereas a hypocrite worked in the shadows. As long as he operated transparently and righteously, he did not fear whatever Yao Guang Yi had in plan for him!

Brother Yao, cheers!

Wang Yan lifted his wine cup.

Boom! Just when the both of them had lifted up their cups, the ground tremored and a figure smashed onto the streets
',0,'2019-05-01 23:00:01.000000');