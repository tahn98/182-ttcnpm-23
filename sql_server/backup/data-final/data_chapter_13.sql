INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(13,'chapter 1','Ryouma Takebayashi isolated himself within the Forest of Gana, training in the arts of both magic and close combat. Three years later, it was time to

 
 
 
 
 

Eat.

Leaving the forest was the last thing in his mind.

Todays breakfast is really delicious Ryouma

Ryoumas once shabby cave was now bigger and safer. By using earth magic, he was able to make more space within the cave, and by setting up a barrier by the entrance, Ryouma was able to make the cave much safer. The cave was also now furnished with furniture made from earth magic.

As for food, Ryouma got it from the forest. Thanks to the knowledge the gods had given him and the neutral magic, Identify, Ryouma could easily pick out what was edible and what was not.

Yet despite living such an uncomfortable lifestyle out here in the middle of nowhere, where one had to hunt or forage to procure food and none of the convenience of a functioning society was present, Ryouma had never once thought of leaving the forest instead, he spent his days doing hobbies that he had no time for back in his previous life.

One particular hobby Ryouma had grown fond of was Slime research.

At first, Ryouma just wanted to try out the branch of magic known as Familiar Magic, so he caught a slime from the forest. For half a year, he raised it as a pet, then suddenly, on one fine morning, he woke up to see that the slime had changed colors. At first, he thought it was sick, but after using Monster Identify, one of the spells under the branch of Familiar Magic, he found out that the slime had evolved into a sticky slime.

From then on, Ryouma became deeply interested in why the slime evolved, so he spent his days observing the slimes. The first thing Ryouma understood was that the wild slimes were at the bottom of the food chain.

The slimes had no power to prey on other creatures, so they had no choice but to live like a pauper and get by what little they had. Ryoumas own slime, however, lived like a king, as he would feed it green caterpillars everyday, ensuring that it was always full.

Incidentally, the green caterpillars themselves had the ability to spit out sticky threads made from their own bodily fluids. Because of that Ryouma hypothesized that a green caterpillar diet might have caused the evolution. To test his hypothesis, Ryouma caught a new batch of slimes and fed them green caterpillars everyday.

2 months later, the slimes all evolved into sticky slimes however, it still wasnt clear whether it was simple nourishment or the green caterpillar diet that caused the sticky slime evolution, so to clarify things, Ryouma caught a new batch of slimes and fed them something different. In the end, the second batch of slimes all evolved into a different slime.

From here on, Ryoumas interest in the slimes grew deeper and deeper, until eventually, Ryouma had a staggering number of slimes. After catching and trying out various diets on the slimes, Ryoumas current ooze of slimes had turned into this

Slime x13
A monster that can be found anywhere. It is considered the weakest monster. It is omnivorous and its body has an average diameter of 20 centimeters. Its jellylike body houses a nucleus, which when destroyed, kills the slime. Upon death, the slimes body vanishes into nothingness. it is a mysterious creature, to say the least.

Skills
Digest Lv2
Absorb Lv3
Split Lv1

Sticky Slime x153
Physically, the sticky slime is no bigger than a normal slime, but it is able to produce a highly adhesive liquid within its body. It can spit out this liquid or use it to trap other beasts to prey on them.

Skills
Sticky Liquid Lv4
Hardening Liquid Lv1
String Shot Lv1
Digest Lv3
Absorb Lv3
Split Lv3

At fist, sticky slime only knew Sticky Liquid, Digest, Absorb, and Split. It wasnt until later that it learned Hardening Liquid and Jump. Because of that Ryouma learned that monsters were able to acquire skills through maturity or training.

As for String Shot, when Ryouma found out through his experiments that he could mix hardening liquid and sticky liquid together, he had the slimes mix them within their body. When they succeeded, they learned the skill, String Shot.

 

Acid Slime x100
An evolution resulting from the need to digest food that is difficult to digest, such as the bones of beasts. This slime variation has a powerful digestive system. Their current numbers are a byproduct of Ryoumas research and reproduction through Split.

Skills
Produce Acid Lv3
Acid Resistance Lv3
Jump Lv1
Digest Lv4
Absorb Lv3
Split Lv2

Poison Slime x188
An evolution resulting from the constant consumption of poisonous herbs. A lot of slimes die during the process, but those that survive, evolve to become poison slimes. Their current numbers are a result of Split.

Skills
Produce Poison Lv3
Poison Resistance Lv3
Produce Paralyzing Agent Lv3
Jump Lv1
Digest Lv3
Absorb Lv3
Split Lv3

Cleaner Slime x11
Slimes frequently drink water, but there are some who prefer to drink the water from Ryoumas bath. Thinking it wouldnt harm them, Ryouma allowed these slimes to drink as they pleased. In the end, they evolved into cleaner slimes.

Skills
Clean Lv4
Deodorize Lv6
Deodorizing Liquid Lv4
Disease Resistance Lv5
Poison Resistance Lv5
Jump Lv1
Digest Lv3
Absorb Lv3
Split Lv1

Scavenger Slimes x457
Ryouma wasnt happy with the fumes coming out from the toilet and the garbage inside the cave, so when he remembered how some slimes showed interest toward rotting corpses, he caught a new batch of slimes and threw them at the toilet and the garbage. As a result, the slimes evolved into scavenger slimes. These slimes eat garbage and turn them into fertilizer. A special trait of this variation is that they are able to reproduce more rapidly compared to other variants.

Skills
Disease Resistance Lv5
Poison Resistance Lv5
Lead Belly Lv5
Clean Lv6
Deodorize Lv6
Deodorizing Liquid Lv4
Stench Lv4
Produce Fertilizer Lv3
Jump Lv1
Digest Lv6
Absorb Lv3
Split Lv6

As Ryouma was getting into his slime research, he decided to put an emphasis on properly rearing the slimes, causing him to only have so many variations, but as a result, their total numbers broke past 900. Ryouma had always been good at simple but repetitive tasks, so with no one to stop him, Ryouma just kept on going and going, and in the end, the slimes ended up where they are today.

That kind of lifestyle had a healing effect on Ryoumas tired soul, and despite sometimes happening upon some bandit or large beast, which Ryouma promptly took care of with his own strength and his staggering ooze of slimes, Ryouma was able to live happily.

It was in this way that Ryouma passed his days, forever and ever
Or at least Ryouma might have had if not for a change that occurred one day. While hunting in the forest, Ryouma came across five people dressed in armor.

(Thats a rare sight Their equipment are all the same, so theyre probably not bandits I think this is my first time seeing people other than bandit. It seems like someone is hurt.)

As Ryouma watched the group of men from the thickets, he noted that one of the five men had bandages on and was being carried.

U, Uu ???
Stay with me, Hyuzu! ???
Camil, hows your magic? ???
Sorry, it hasnt recovered yet Camil

(They seem to be in pretty bad shape They dont look like bandits. I should at least give them a place to rest. Even if they do turn out to be the bandits, I think Ill be fine.)

Ryouma stood up from the thickets and was about to call out to them when it suddenly occurred to him.

(How am I suppose to call out to them? Good day? No, that kind of cheerful greeting doesnt fit the situation. Hey, you bastards! No, that would just make them wary. What am I going to do!?)

Ryouma thought of helping, but after three years with no human contact, Ryouma was at a lost what to say, and he found himself standing still. Because of that the group of men noticed him before he could say anything.

Who goes there!? ???
Wait. ???

The man at the forefront of the group immediately pointed his sword at Ryouma, but the man behind him stopped him and stepped forward.

Sorry for suddenly pointing a sword at you. By the way what is a child like you doing out here? This isnt a place for children. Are you lost? ???

The man asked, but Ryouma couldnt respond well.

I was, hunting. Ryouma
Hunting? You? Importantlooking Man man

Ryouma nodded faster than he could speak.

I think this forest is a bit too dangerous, but Was there something you wanted with us? Importantlooking Man man

Ryouma pointed to the injured person.

Theres someone, injured Ryouma

And then he offered his leather bag with one of his hands. Unfortunately, the other man was wary of Ryouma, and he stepped out to protect the man Ryouma was talking to.

Ryouma finally realized his blunder. Keeping his leather bag by his waist much like the knife by his waist must have alerted the man, so Ryouma jumped back to get some distance and took out a medicine to show that he had no ill will.

Is that medicine? Swordsman

The swordwielding man asked, to which Ryouma nodded.

Medicine, for the injured. Ryouma
Youd let us use that? Swordsman
Hurry. Ryouma

The group of men looked at each other for a moment, then the man named Camil cautiously accepted Ryoumas medicine. When they noted that the injured persons countenance had turned for the better, their caution toward Ryouma finally lessened.

Thank you. This should keep Hyuzu up for a while longer. Importantlooking Man man
How about, rest, resting, at my house? (This is pathetic.) Ryouma

Though it took awhile, Ryouma successfully managed to say the words and invite the people to his house. It would take them quite a while to reach Ryoumas place, however, due to the injured person with the group of armored men.

Why is a child like that in a place like this? Importantlooking Man
Hes obviously still very young Swordsman

(Well, I am an 11 yearold child. Its only natural for people to be suspicious to see a child out here deep in the forest. Its not a good feeling though. Id appreciate it if they could stop being so suspicious already. Hmm How about I try talking with them a bit?

On second thought, I probably shouldnt. I have the background the gods gave me, but with my poor speaking skills, I might end up just digging my own grave.)

Is there really somewhere we can rest out here? Swordsman
I dont know, but the potion he gave did work, so at the very least, he doesnt seem to be hostile. Importantlooking Man
Hunters are known to make a safe place for themselves in the forest. There might be a camp out here or something. ???

(Exactly. As long as you dont attack me first, I wont hurt you. Speaking of which, how about collecting the spoils from the traps along the way? That way Ill also be able to feed that injured person. If I do that much, a decent person would surely not attack me, right?)

Ryouma suddenly stopped and called his sticky slimes over through the familiar contract. The people following him was alerted at that, however, and they asked.

Whats wrong? Importantlooking Man
Trap Caught some prey Will come soon. Ryouma

Ryouma nonchalantly said as he looked at the man that asked that question.

(This person is probably the most important one. Everyone else seems to listen to him and the other people look like guards.)

As Ryouma was thinking that, the grasses shook, and then a sticky slime came out with a dead horn rabbit. Unfortunately, the people with him didnt know that it was Ryoumas slime, and the importantlooking man drew his sword.

(Stop!)

Immediately, Ryouma jumped in front of the man and picked up the slime and the dead rabbit.

Is that slime your familiar? Importantlooking Man

The importantlooking man seems to have picked up that the slime was Ryoumas familiar. Ryouma vehemently nodded at the man, and the man sheathed his sword.

Sorry, I didnt know it was your familiar.

(As long as weve cleared it up, its all good. I should have been more specific.)

Slimes were monsters, after all, so it was only natural that they would be wary when they saw one out here in the forest. Ryouma hid the slime and the dead rabbit in his bag, and then they started moving again.

This sure brings me back. My first familiar was also a slime. Importantlooking Man
Tamer? Ryouma
Former tamer. After my monster stopped being able to fight, I havent formed a new contract since. I had a red horse and a blizzard ape. Importantlooking Man
Strong? (Im asking because I have no idea what those are) Ryouma
I come from a long line of tamers, so Ive been taught from a young age. Unfortunately, Im not that good at magic. Im quite confident in my sword arm, though. Importantlooking Man

(A family of tamers and a group of armed men for escorts, he must be a noble. If not, then at least someone influential with plenty of money or someone similar. Calm down, me. This country is supposed to be relatively lenient when it comes to the difference between noble and commoner. The gods specifically picked this country out for me, after all.

And they also havent reacted negatively to my behavior so far, so it should be fine. Right, I should just treat him like I did my bosses during parties when they would tell me not to mind rank I still feel nervous, though.)

Though somewhat panicking, Ryouma continued along, picking up the captured prey as they made their way, until eventually, Ryouma and the group of men finally arrived at his cave.

',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(13,'chapter 2','Please wait for a moment. Boy

The boy said when we reached the cliff.

There wasnt anything around, so I thought he was waiting for another of his slimes, but to my surprise, the boy instead used Break Rock on the cliff up ahead to open a path.

Here Boy

So this is the home the boy was talking about. It is indeed a sturdy cave that could shelter against beasts and monsters.

When we entered the cave, the boy put up a barrier behind us, making the cave even safer.

I didnt think hed know barrier magic. Its useful but its not something just anyone could use. In fact, Camil himself pointed that out.

Is that barrier magic? You sure know some strange magic, boy. The effect of this barrier is concealment, right? Camil
Dont worry Youll be able to run anytime. Boy
I see Thanks. Camil

The boy nodded curtly, then he passed us and went deep into the cave.

Hes pretty considerate. Reinhart
Indeed, Master Reinhart. Jill
Hey, youre going to get left behind. Zeff
Ahh, were going, were going. Jill

When we entered the cave, we immediately noted that the walls and the floor had been properly leveled. The whole place was even furnished with furniture made of stone and wood, while magic stones set onto the walls illuminated the area.

Wow Jill
This is more homely than I thought. Reinhart
The wounded, lay him down, here Boy
Sorry, Hyuzu. Were gonna have to put you down for a bit. Reinhart
RRight Hyuzu
Wait Ill bring potion Boy

After laying Hyuzu on the bed, I watched the boy as he went deeper into the cave.

Well, in any case, with this we can finally relax a bit. Reinhart
The place is a lot better than expected. Hyuzu should be able to rest well. JIll
We owe him one. Reinhart
 Zeff

Zeff had a grim look on his face as he examined the surroundings. Zeff was the most knowledgeable among the escorts when it came to stealth and traps, so hes been assigned as the scout. The fact that he was making a grim face could only mean

Zeff, is something wrong? Reinhart
Dont you think theres something odd about this place? Im not talking about traps. Im saying it feels like someone has been living in this place for a long time depsite there only being enough furniture for one person. Zeff

It was normal for a camp to have little furnishing, but a map could be seen laid out on one of the walls here. There were even drawings of the different animals and a musical instrument at the corner of the room.

Although the room looked dreary, it felt just like a kids room. Moreover, just as Zeff had mentioned, theres only enough furnishing for one person. Its strange indeed.

Is that boy living here all by himself? Reinhart
No way. Sure, I was surprised to see him use familiar magic, barrier magic, and even earth magic, but no child could normally live by himself out here in a forest like this. Theres probably someone else here. Camil
Or he could be older than he looks. For example, if he were an elf Jill

As Camil and Jill conversed, the boy came back with a slime carrying a lot of potions.

Potions. Boy
Thank you. Ill definitely repay this debt one day. Reinhart
Dont mind I can make them, so Take as much as you need. Boy

When the boy said that, we all looked at each other in silent shock. The first to exclaim out loud was Camil.

You made these!? Camil

The boy flinched for a moment at Camils sudden outburst, but he quickly nodded his head to answer. Camils reaction was a bit over the top, but the boys potion was indeed comparable to the ones sold in the stores. This boy is getting more mysterious by the second.

Water. Boy

While I was thinking to myself, the boy offered me a stone cup. It was filled with water generated through magic. It was chilled just right.

Thanks. Reinhart
Thank you. Jill
Thanks, boy. Camil
Thank you. Zeff
Oh Boy
Hmm? Something wrong? Reinhart
Name Ryouma Boy

Oh, right. We havent introduced ourselves yet.

So your name is Ryouma. Please excuse my belated introduction, I am Reinhart Jamil, the feudal lord of the Ducal Household of Jamil. I thank you for aiding me during my subordinates difficult period. Reinhart
Duca!? Please excuse my manners! Ryouma

I tried to speak as gently as possible to avoid scaring him, but the moment I named myself, he stiffened up and bowed deeply. What was surprising was that his movements were much more refined despite his difficulty in talking.

Please stand up. You are my benefactor. You dont have to be formal with me. Reinhart

It seems he didnt know I was from a dukes household. He lifted up his face just as I asked him, but unfortunately, he stopped talking.

I dont really mind, though good grief

As I was thinking that, Camil and the others chatted with him.

Umm Im Camil. Im a magician employed by Master Reinhart to protect him. Nice to meet you. And really, thank you for helping us. I was out of mana and couldnt heal, so if you hadnt come then, Hyuzu might have Ah, Hyuzu is the name of the guy sleeping.

You really dont have to worry about how you talk. Master Reinhart isnt someone whod care about that sort of stuff. Camil

In fact, he doesnt even mind people like us. Im Zeff by the way. Im the scout of the group. Nice to meet you, kid. Zeff

Im Jill. Sorry for pointing my sword at you awhile ago. Jill

Its ok Its normal to be on guard. Ryouma

Thanks for understanding. Im also a noble, and I dont think theres anything wrong with the way youve been behaving. Master Reinhart is a generous person, so you can just act normal around him. Jill

Thank you. Ryouma

Camil, Zeff, and even Jill, who wasnt used to kids, softened their voices as much as they could to not scare the boy.

The boy thought for a moment, then he spoke.

Though his face was still a little grim, his countenance has improved.

He still talks a bit stiff, but at least hes a lot more relaxed now.

Im the one who should be thanking you. After all, you gave us a place to rest and even treated my guard. Reinhart

No problem, but why? Ryouma

Is he asking me why Hyuzu was injured? Or is he asking me why we came to the forest? Come to think of it, I havent explained anything, have I?

We were supposed to go to the town of Gaunago, where my house is, but along the way, near this forest, we were attacked by some bandits. Reinhart

Suffered much? Ryouma

No, there were quite a bit of them, but they werent particularly strong. They mustve thought they could overpower us because of their numberis, but what they didnt know is that my guards werent pushovers. What did Hyuzu in was a black bear that came out during the battle. Reinhart

Hyuzus bad luck had him attacked in the middle of a melee. Jill

We managed to defeat it in the end, but the horses ran away. Hyuzus wounds were also a lot worse than expected, so we tried to get to the village as quickly as possible. Normally, we would go around the forest, but with things as they were, we figured wed just go through it. Camil

Everyone nodded as we explained our predicament. Since the conversation has progressed like this, I think Ill take this opportunity to ask my own questions.

Speaking of which, what is a boy like you doing living here? I heard you mention that you were hunting, but it seems to me that youve been living in this house for quite a while. Moreover, the fact that youre able to hunt at your age, use various magic, and even make potions is really not normal. Quite frankly, its shocking. Reinhart

I learned from grandparents Former adventurers. Ryouma

Oh? So, his grandparents were adventurers.

They passed away. Ryouma
Sorry. Reinhart
Its ok. Its been 3 years already. Ryouma
3 years!? Reinhart and Co.
How long have you been living here!? Reinhart
I left village 3 years ago I am an outsider, so they hated me. Ryouma

Was he at an exclusive village? I know some places can be really cruel, but still

Before they died grandparents told me to go to another town Ryouma

Apparently, he wasnt good at dealing with other people, so he wandered by himself, relying only on the skills he learned from his grandparents, until one day, he happened upon this forest. Since then he hasnt exited this forest once. Meaning he hasnt talked to another person for three years.

I understand the situation now, but I cant recommend this sort of lifestyle. There are strong beasts and monsters living in the forest. Even if you say you have the ability to survive, its too dangerous. Reinhart

Its ok. I survived for 3 years. Ryouma

But! Reinhart

I know! Wait just one moment! Camil

Camil suddenly interjected, then he took out a small crystal from his bag.

Found it! Look! Camil

What is that? Ryouma

This is a small crystal of evaluation! If we use this, well be able to find out your identity and your four highest skills. Moreover, a person whos committed a crime will make this crystal turn red, otherwise, the crystal will shine a blue light. After the light comes out, the name, race, and the four skills will be shown. If you have a high level combat skill, then I wont say any more. Camil

I see, so hes going the persuade him by showing him that hes lacking.

Alright. Ryouma

As he said that, he reached for the crystal ball, but before he could touch it, he suddenly asked.

I was attacked by bandits, so I killed them is that a crime? Ryouma

If those were really bandits, then there wont be any problem. Camil

At that, the boy finally touched the crystal, and a blue light shone from it.

The crystal wasnt really meant to distinguish between criminals, but seeing that the boy is innocent puts my heart at ease.

As I thought that, I glanced at Camil, but for some reason, his countenance had gone pale.

WWhat is this? Camil

Whats wro!? Jill

Jill took a peek at the crystal from behind.

As soon as he did, he gulped.

Curious, me and Zeff took a peek at the crystal ourselves.

The problem was the listed skills.

 

Shown Skills
Domestic Chores Lv10
Mental Resistance Lv9
Physical Resistance Lv8
Health Lv7

 

Whats with those levels!? The Domestic Chores skill is ok, since thereve been plenty of precedents before this one, but he actually has mental res., physical res., and health, and every one of them is at least Lv7 up? What hell of a life has this boy been living to get these so high? Hes 11 yearsold, isnt he? That would mean he was 8 yearsold when he started living here.

Something wrong? Ryouma

AAh Unfortunately, no combat skill showed up Camil

Is that the problem!? I thought, almost yelling it out.

When I glanced at Camil, I noticed that the other guards had the same reaction.

A battle of stares commenced between us, but in the end, no one was willing to pursue the subject.

The reason behind our reaction was of course because of the fact that pain resistance was something one could only learn through pain. The fact that his level of resistance was so high meant that he mustve suffered through unimaginable pain. Im sure there are a lot of things he doesnt want to remember. If we broach the subject poorly, well just cause the boy more pain.

There are a lot of questions left, but I think well stop here for now.

Sorry, can I borrow your toilet? Reinhart
Id like to go too. Jill
Me too. Zeff
The toilet is inside There are a lot of slimes Dont worry They wont attack Ryouma
Dont worry. Im a former tamer too. I wont hurt your familiars. Reinhart

Like this we left Camil to take care of Hyuzu, but who wouldve thought that by slimes he meant this?

Wow Zeff
You said it I dont think Ive seen so many slimes in one place. Jill

Countless slimes crept freely in the hallway. Ryouma had to order them to make way for us, as otherwise we wouldnt be able to pass without stepping on one.

The number of familiars one could contract varied from person to person. The stronger the monster, the less one could normally control. Slime is the weakest monster, so following that logic, it should stand to reason that one would be able to contract a respectable number of slimes, but this Did he actually contract all these?

Ryoumakun, are all of these slimes your familiars? Reinhart
Yes. Theyre for research. Ryouma
Research? Reinhart
Slime evolution. Ryouma

Come to think of it, the slimes creeping in the area arent just normal slimes. There are sticky slimes, poison slimes, and those are probably acid slimes As for those two, I dont even know what kind of slime they are. They must be a higher variation too.

Slimes live everywhere, so Im sure you could find these variants somewhere, but there has never been any reports of such sightings in this forest.

Just as Jill said, I have also never seen this many slimes in one place.

Monster evolution is an important topic to conjurers and monster tamers alike, and the fact that youre doing such research at youre age is nothing short of extraordinary, but its a pity that theyre all slimes. Reinhart
Are slimes no good? Ryouma

Personally, I hold his abilities in high esteem. The fact that he was able to gather so many advanced classes and contract them all speaks of his abilities, but unfortunately, the world doesnt look kindly to slimes.

Frankly speaking, slimes are weak even after evolution. Monster tamers and conjurers mainly use them to study the basic, and aside from that, they dont have any value.

Which is why most monster tamers throw them away after learning the basics. Usually, they would contract a horn rabbit next. At the very least, horn rabbits are cute, so they can at least be treated like a pet. Reinhart

The world is a cruel place Ryouma

Is that soemthing an 11yearold would say?

Of course, not all monster tamers think the same way. At the very least, no one can make light of a poison slimes poison or an acid slimes acid. Those are even stronger than a horn rabbit. Reinhart
Slimes Convenient useful Ryouma

I thought hed feel down after telling him how little the world thought of slimes, but he doesnt seem to mind at all. Normally, children his age want to be recognized by others.

His lineage is a mystery, but I dont think hes dangerous. If anything, hes a good kid. After all, he helped us in our time of need. Hes definitely not a normal kid, though. But in any case, I want to help him.
',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(13,'chapter 3','U! Haa, haa. Hyuzu

After Ryouma and the others came back and chatted, Hyuzus condition worsened.

Hyuzu! Reinhart
Get yourself together! Jill
The potion and healing magic has stopped the bleeding, but now he has a fever. Its pretty high too Camil
I have, medicine. Ryouma

Hyuzus face was red with fever as he groaned and perspired.

When Ryouma heard what Camil said, he immediately ran inside.

Meeting that kid was really our good fortune, huh, Boss. Zeff
Yeah, if we hadnt met him, we definitely wouldnt be able to save Hyuzu. Reinhart
Hyuzu isnt safe yet, but even if we had somehow managed to use healing magic, we still wouldnt have been able to save him. After all, healing magic doesnt work on fevers. Zeff
Well it might work normally, but certainly not when youve lost a lot of blood Reinhart

When they stopped talking about Hyuzu, the topic moved to Ryouma.

What are we going to do about that kid? We cant possibly leave him here all by himself. Camil
Hes been living here for 3 years already. Im sure he knows how dangerous it is. Jill
Hes survived all these years and he has those resistance skills He must have lived a horrible life back at his village. Who knows if we can even convince him that the town is safer. Fortunately, hes not so far gone so as to lash at people the moment he sees one. Zeff
Ah, come to think of it, there was that person who made a scene. Reinhart
Reinhartsama, as a father of one yourself, dont you have some idea?Zeff
Youre the only one with a kid here, Boss. People like us dont havent the slightest clue what to do at times like these.
Im clueless too. We cant leave him alone, but we cant force him to go with us either. In any case, we need to go back first. Ill talk to my dad and Elize about this. Reinhart

After that they ran out of topics to talk about and silence filled the room. A few minutes later, Ryouma came back with a slime carrying a vase full of water and a medicine. Ryouma himself was carrying a monster pelt under his arms.

Umm, thank you. Camil
Treatment first. Ryouma

As Ryouma said that he covered Hyuzu with the pelt he brought and Camil poured the water into a cup.

Incline his head. Make him drink. Ryouma

As Ryouma said that, Camil made Hyuzu drink.

He seems to have drank everything. Zeff

When Ryouma heard that he handed the medicine.

Medicine. Ryouma
Thank you. Reinhart

As Reinhart said that he made Hyuzu drink the medicine. After one hour, Hyuzus condition finally improved. Reinhart and his party heaved a sigh of relief.

It was getting dark, so Ryouma proposed for Reinhart and his party to stay over. Seeing that Ryouma was not hostile and after considering Hyuzus situation, Reinhart decided to accept Ryoumas proposal.

Ryouma showed off his homemade cooking for supper. It was a simple dish, nothing more than stirfried bean sprouts and rabbit meat soup, but nevertheless, Reinhart and his party were happy.

 
 
 

The next day.

Hyuzu had already recovered and could stand by himself. Reinhart and his party didnt leave immediately, however, and they stayed until the afternoon to ensure Hyuzu was up to health.

Man, I thought I was a goner for sure. Thanks a lot, kid! Hyuzu
Are you really ok? Ryouma
Why? You worried? I heard you didnt want to go to the village, so I thought you hated people. Hyuzu
Can at least, worry about the sick Ryouma
GAHAHA! I see! I see! Sorry about that! Whoops Hyuzu

As Hyuzu was laughing, he suddenly tottered. Reinhart and Camil immediately went to support him.

Hyuzu, you ok? Reinhart
YYeah Just a little dizzy, Boss. Nothing to worry about. Hyuzu
You just got better, so please be careful. Camil

When Ryouma saw that, he took out the medicine he prepared beforehand.

Drink. Ryouma
Hmm? Whats inside the bottle? Hyuzu
Medicine for blood. You lack blood. Ryouma
Is that so? Thanks. Then Ill just go and drink Ughk!? What in the blazes!? Hyuzu

The smell of the combination of herbs and green caterpillars wafted out from the bottle. Hyuzu wasnt the only one to be put off by the smell, even Jill and Zeff couldnt help but grimace at it.

It stinks, but I can guarantee, it works. Ryouma
Well, you heard the boy. Bottoms up. Reinhart
WWait a moment! Hyuzu
Wed be troubled if you suddenly fainted along the way. Jill
Were worried too, you know. Zeff

Jill and Zeff grabbed Hyuzu by the shoulders, keeping him from running.

Forgive me! Camil

Camil took the bottle and emptied its contents into Hyuzus mouth.

!!!! Hyuzu

Incomprehensible mutterings spat out of Hyuzus mouth as he convulsed several times. Hyuzus pitiful appearance as he leaned onto a wall made it seem like what he drank was not medicine but poison. For the record, that was indeed an effective medicine, it just stank really bad.

YYou guys Hyuzu
Good medicine tastes bad, Hyuzu. Reinhart
Dont worry, this kids medicines are effective. Jill
The potions he used to treat you were highclass stuff too. Zeff
Damn it. Ahh I thought I would die Uppu Hyuzu

The stench of the medicine wafting from the pith of his stomach made Hyuzu sick. He looked like he was about to throw up, so Ryouma gave him another glass of water.

Do you have armor? Ryouma
Fuu, hmm? Ah, my armor got done in by that bear, so, no, I dont got any. No weapons too. Hyuzu
I have some Wait. Ryouma
That would help a lot, but you sure you ok giving us your stuff? Hyuzu
Yes. Ryouma

Ryouma went inside and came back with some slimes carrying five spears and 3 pieces of armor allinall.

Here. Ryouma
These are some pretty good stuff for bandits. You sure you ok handing them to Hyuzu? Jill
Weapons are made, to be used If you dont take them, no one will use, them Ryouma
This spears would go for five small gold coins, you know? Hyuzu
Take them. Ryouma

When Jill and Hyuzu saw how good the equipments were, they asked Ryouma repeatedly if he was sure about giving them away. In the end, the first to fold was Hyuzu himself.

In that case, Ill happily take them off you. But I cant stand just taking stuff for free. I dont have anything to give, but if youre ever in need, just give me a call and Ill be there. You can easily contact me by giving the guards at Gaunago Town my name. Dont hold back, alright? Hyuzu
Got it. Ryouma

Like this the party of five finished their preparations and departed. After 3 years in the otherworld Ryouma has finally met the people of this world.

Ryouma was exhausted after conversing with someone for the first time in a long while, but regardless, he still hunted as usual.

What Ryouma didnt know, however, was that this simple meeting would greatly change his life.
',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(13,'chapter 4','Two weeks after the duke left, Ryouma has been spending his days hunting and taking care of the slimes. One day, four people suddenly appeared before his house.

Hey! Ryouma! Open up! Its me! Hyuzu! Were not enemies! Hyuzu

Yelling out from in front of Ryoumas house was none other than the person he took care of. Jill, Camil, and Zeff were with him.

The path to Ryoumas house was blocked by a boulder, so they had to call out in a loud voice to be heard.

Only, Ryouma was actually in a thicket behind them.

Ill, open it, now. Ryouma
Woah!? You were outside? Hyuzu
Went out to, hunt Why did you, come? Ryouma
I wanted to thank you again for your help last time. We brought some gifts with us Theres quite a bit, so we had to store them away via dimension magic. Theres a butler who can use it in the Jamil family. Hes currently waiting for us with two other maids and Reinhartsamas family. They have some business in the forest, so we went together. If youre not busy, I could bring them now. Hyuzu

Ryouma became thoughtful for a moment, and after realizing he didnt really have any pressing matters to pursue, he decided to let Hyuzu call them over. Besides, he wasnt really about to chase away some people who went out of their way to visit him.

The four guards left to call Reinhart and his family.

Meanwhile, Ryouma decided to call back his slimes that he sent hunting and prepared to receive guests.

 

30 minutes later Ryouma had finished his preparations and a horde of almost 1000 slimes could be seen sunbathing in front of his house. That horde jostled about as they waited for Ryoumas guests.

After awhile Reinhartsama and his entourage arrived.

When the slimes noticed a large group of people approaching, their bodies began to shake.

Ryouma straightened his clothes and watched the forest, and very soon he noted some people approaching.

There were 11 people all in all. There was a beautiful woman and a beautiful girl behind Reinhart, as well as a dignified and muscular old man. Behind them were two maids and a butler just as Hyuzu had mentioned. The four guards, Hyuzu and Co., walked in front.

(Judging from the way theyre dressed, those people behind Reinhartsan must be his family. The butler aside, those maid outfits sure stand out. Isnt it hard to move in them?)

Is it him? Maid 1
Thats a lot of slimes Maid 2
Ho? I heard hes tamed a lot of slimes, but I didnt think he would actually tame so many. Old Muscle Man
They might only be slimes, but its still commendable to have tamed so many. Beautiful Woman
Even though the more familiars one has the harder it is to tame new monsters. Beautiful Girl

Reinharts group approached Ryouma while he wondered about the practicality of maid uniforms.

When the maids saw the large group of slimes, they couldnt help but find their face cramping.

Reinharts whole family except for himself looked on at the large group of slimes with amazement.

When they had finally arrived at the entrance of the house, the first to talk to Ryouma was Reinhart.

Ryoumakun, its been 2 weeks since our last meeting. Weve come to thank you for your help back then. Weve brought some gifts. Reinhart
Thank you. Ryouma
Dont sweat it, theyre just some goods we had lying around. Reinhart
Dear, before that, how about introducing us? Beautiful Woman

Reinhart introduced his family.

Let me introduce you. This here is my father, Reinbach, and this beautiful woman here is my wife, Elize. And this here is our lovely young daughter, Elialia. Reinhart
Im Reinbach Jamil of the Jamil Household, the former master of the family. Its a pleasure to make your acquaintance. Reinbach
Im Elize Jamil. Thank you for helping my husband and subordinates. Elize
My name is Elialia Jamil. Im happy to make your acquaintance. Elialia
The pleasure is mine I am, Ryouma Takebayashi I come from, a distant land Its not much, but please let me offer you, my hospitality. Ryouma

Though Ryouma would pause from time to time and could not speak straight, his words were still polite. That alone was enough to stupefy Reinharts family and their entourage.

You dont need to be so formal. Just talk to us as you normally would. To begin with, were the ones troubling you for coming here so suddenly. Reinbach
Thank you. Well then, please Ah. Ryouma

Ryouma was about to lead the people into his house when it suddenly occurred to him that the entrance was being blocked by all the slimes, so he had order them to go inside first.

Seeing them in a bright place, they really are an impressive group of slimes. By the way, is it just me or are there more of them now? Reinhart
They multiplied After everyone left. Ryouma

Ryoumas slimes currently numbered as follows

Sticky Slime x 364
Poison Slime x323
Acid Slime x211
Cleaner Slime x11
Scavenger Slime x730
Heal Slime x2 (NEW)
Skills Heal Lv1 Increased Vitality Lv1 Photosynthesis Lv3 Digest Lv1 Absorb Lv1 Split Lv2

Frankly, after the slimes multiplied Ryouma found them too numerous, and he finally started having troubles feeding them.

Moreover, he also started to worry about the longterm effects of the slimes on the ecosystem. For the time being, instead of feeding them meals in order to meet evolution conditions or increase their numbers, Ryouma decided to feed the slimes just enough to sustain them.

If that didnt work Ryouma had prepared to thin out the slimes himself, but fortunately, he found a new goblin village two weeks ago, which he wiped out.

The goblin village didnt just provide Ryouma with food, it also gave him an unexpected boon, for when Ryouma tried to heal the slimes injured during the battle, two of the slimes evolved into heal slimes, a new kind of slime that could use healing magic.

Seeing a new kind of slime appear lit Ryoumas fire once more, but fortunately, he managed to hold himself back after considering the circumstances.

Can all that actually fit inside? Reinhart
Itll be fine Ryouma
What do you mean? Reinhart
This. Ryouma

As Reinhart asked that question, Ryouma gave a command to the slimes.

Space was certainly an issue after the slimes multiplied, but three days after the fact, that issue was coincidentally solved. And the solution came from a mere question.

When the slimes had grown too numerous to fit in their room, they started to spill into Ryoumas bedroom, and so, wondering what to do, Ryouma muttered to himself.

Cant these slimes fuse or something? You know like Dokue?

In that instant, the slimes vigorously shook, and those of the same type fused together to form one slime.

Panicked, Ryouma quickly checked their status.

Big Sticky Slime x1
Skills
Sticky Liquid Lv5
Hardening Liquid Lv4
Sticky String Shot Lv3
Physical Attack Resist Lv1
Enlarge Lv2
Shrink Lv4
Jump Lv2
Digest Lv3
Absorb Lv3

Big Poison Slimex1
Skills
Produce Poison Lv4
Poison Resist Lv4
Produce Paralyzing Agent Lv4
Physical Attack Resist Lv1
Enlarge Lv2
Shrink Lv4
Jump Lv2
Digest Lv3
Absorb Lv3

Big Acid Slime x1
Skills
Produce Acid Lv5
Acid Resist Lv4
Physical Attack Resist Lv1
Enlarge Lv2
Shrink Lv4
Jump Lv2
Digest Lv4
Absorb Lv3

Huge Scavenger Slime x1
Skills
Disease Resist Lv5
Poison Resist Lv5
Lead Belly Lv6
Clean Lv6
Deodorize Lv6
Deodorizing Liquid Lv4
Stench Lv5
Produce Fertilizer Lv4
Physical Attack resist Lv2
Enlarge Lv3
Shrink Lv5
JumpLv2
Digest Lv6
Absorb Lv3

To Ryoumas good fortune, the combined slimes had an ability called Shrink, which allowed them to shrink their normally humongous bodies.

The bigger slime variants ate several times more than their smaller counterparts but effectively consumed less when compared to the food consumption of a hundred slimes. To put things into perspective, the bigger variants consumed about 20 to 50 percent less than their uncombined form. In other words, the issue of space and food had been largely dealt with.

Seeing how the slimes had a solution for the very problem that was bothering him, Ryouma couldnt help but wonder if this was some sort of selfdefense mechanism that the slimes had to keep them from endangering themselves from eating up all the food or other overpopulation issues.

There needed to be at least 100 slimes to form a big slime and they all had to be of the same type. Upon fusion the slimes lose the Split skill, responsible for slime reproduction.

It was a mystery where all the mass goes upon invoking Shrink, but in any case, Ryouma was happy to see the space and food problem solved.

Ryouma figured that showing would be easier than explaining, so he ordered the slimes to fuse.

When they fused in front of Reinhart and the others, they couldnt help but open their eyes wide in shock, many of them speechless as they stared at the big slimes.

Big Slime!? Reinhart
Impossible!? Reinbach
No, theres no doubt about it. This is definitely a big slime? You managed to tame a big slime? Elize
Is that, strange? Ryouma
No one has ever managed to tame a big slime before. Elize
Eh? Ryouma

This time around the one to be shocked was Ryouma.

When Reinbach noticed that, he explained.

The contract doesnt work on big slimes. Many have tried, and some still do from time to time, but no one has ever succeeded. Reinbach
But, of course Contract Meaningless. Ryouma

All eyes gathered on Ryouma as he explained.

Big slime is a gathering of many, slimes therefore, contract conditions cant, be met.
The contract, only works on one monster. Contracting, several at the same time, is impossible.
Contracting one out of, one hundred, is also impossible.
Because there, is only one nucleus So, the contract wont, work.
I succeeded, because I contracted, many slimes individually, and then gathered them together. Ryouma
',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(13,'chapter 5','Side Ryouma

Whats wrong? For some reason, after I answered their question, everyone  especially the Jamil family  looked at me with scary eyes.

Did I do something bad?

Magnificent. Reinbach

Huh? Whats magnificent?

Thats amazing, Ryoumakun! Youve actually solved a mystery that has been plaguing monster tamers for ages! Elize
!? Ryouma

Whats wrong with these people!? Their eyes are sparkling and theyre way too enthusiastic for some reason This is actually scary!

Madam, Reinbachsama, please calm down. Youre scaring Ryoumasama. Butler
Ah! Sorry about that, dear. I didnt mean to scare you. Elize
Sorry. I got a lil too excited there. Reinbach
Its alright Ryouma
Let me explain what has these two so excited. The explanation you gave us just now regarding why monster tamers couldnt tame a big slime happens to be one of the mysteries that has been plaguing monster tamers.

Big slimes arent strong, but theyre hard to fight, so there were a lot of people who tried to tame one. In fact, you still see some people trying from time to time, but of course, no ones ever succeeded. Reinhart
Out of pride, many monster tamers tried to uncover the reason preventing the contract from working, but they couldnt get any results. In the end, their research labs were cut, and though there are still those who research them to this day, no one has managed to succeed At least, until you, anyway. Reinbach

Uwaah This has unexpectedly turned into something crazy.

Mu, what a weak reaction Let me put it this way. The research on big slimes has been going on since roughly the very same day the arts of monster taming spread through the world.

Its an unsolvable mystery so unsolvable that people dont expect anyone to get results. In fact, its treated by research labs as a waste of time. And yet that supposedly unsolvable mystery that has made many researchers surrender was solved by you! Can you still remain so calm knowing this!? Elize

Youre kidding, right? This was just a coincidence. Damn, this looks like trouble. What to do?

What should I do? Ryouma
Leave a record in the tamer guild and announce your findings! Elize

Ah So they had an organization that gathered information on this sort of stuff. Judging from the reactions of these people, it looks like itll get me a lot of attention if I announce it though. Oh, but this might be a good opportunity to leave the forest

Town, huh Ryouma

When those words slipped out of my mouth, the people of the Jamil Family and the servants reacted.

Oh, Im sorry. I forgot you dont like towns Elize
We wont force you to record and announce your findings, but this is really one hell of a find, kid. I just want you to know that. Reinbach
I, understand. Ah Ryouma

After the slimes fused and entered, the path was finally traversable.

In any, case, lets enter, first. Ryouma

There are beasts prowling outside, it wouldnt do to stay out here for too long.

I led everyone into the house, then I left to prepare black tea for everyone. The black tea I had were looted from the bandits that attacked me in the past. They had a lot of the same brand, so its probably stolen goods. I only found out I had them when I went to look for a spear to give to Hyuzusan.

I didnt know since Id already forgotten about the bandits stuff. I had no need for money and I wasnt fond of stolen goods, so I just left them alone, but If Id known there were tea among the loots, I would have brought them out sooner.

Anyway, I brought out the bestlooking tea leaves. They dont seem to have expired yet, so it should be fine. The only problem were the cups and the chairs. I didnt have enough for 12 people, so I had to quickly make some with earth magic.

I made the tea with the ingredients I got two days ago. One ingredient was the honey I got from a bees nest, and the other was a ginger I treated with lemonstyle syrup. They were all I had to substitute sugar with, so I hope the guests like them.

Sorry to keep you waiting. Please have, some black tea. Ryouma
Oh, my. Thank you. Elize
Thats a good smell. Reinhart
Hmm You seem to have some good tea leaves with you. Reinbach
Its from the bandits who attacked me. I have a lot, more of them. Ryouma
I see Its good. Reinbach
Indeed. Elize
Youve really managed to bring out the aroma of the tea leaves too. Ryoumasama, where did you learn to make your tea? Butler

From another world is not something I could obviously say.

Grandma Taught me. Ryouma

Grandparents  Omnipotent.

I really have to thank the gods for them. If I had to make up a lie on my own, I would surely be found out. I was often called stupid honest back in my previous life  not that I agree with them. But, in any case, since all Im doing now is using the setting that the gods prepared for me beforehand  a setting they prepared by summoning the souls of my grandparents and getting their permission  I dont feel like Im lying at all. Im really thankful for it.

Theres more, honey, if youd like Ryouma
Thank you. Elize
Let me have some too. Honey is a luxury, so its a pretty rare occasion to get to taste it. Hyuzu
Wait a moment, Hyuzusan! Camil
I got the honey from, a bees nest two days ago. Its free, anyway, so Camilsan, please have, some. Ryouma
Huh, really? Then in that case, Ill have a little. Camil
Heh, youre not so different from me, are ya? Hyuzu

Around that time the young lady of the Jamil Family  Elialia I believe  noticed something as she drank her tea.

Oh? It seems its not only honey. Theres something else mixed in. Elialia

The butler immediately went to check.

I wonder if they dont like the jija (ginger) and the lamon (lemonlike fruit).

I can taste the juice of lamon. Its a refreshing taste, however, thats not all. Butler

Fortunately, they dont think its poison.

I decided to reveal the mystery. After all, it wasnt like I added poison or anything.

I also added, jija root. Ryouma
Oh, so this is jija. I thought it was just a salty vegetable. Who knew it could be used to draw the flavor out like this? Elialia
Jija can be, used in cooking Can remove the odor, of meat and fish Ryouma
That is good information. Thank you, Ryoumasama. I shall inform the master chef as soon as we return. Butler
Youre welcome. Ryouma
Now then  before I completely forget  we came here to thank you for your help last time. Please accept our gifts. Sebasu. Reinhart
Yes, Reinhartsama. Item Box. Sebasu

When Reinhartsan mentioned about the gifts, the butler behind him stood up and invoked a spell. Suddenly, an empty circle appeared from thin air. The butler, Sebasu, extended his hand inside and took out something.

Item Box
As the name implies, its a dimensionaltype spell that creates a new dimension to store items in. Its a difficult spell but also one of the basics of dimension magic. I can use it too, but I cant put that many items in mine.

On the desk in front of me, upon which was a bucket full of fruits, came falling down countless rolls of paper and cloth.

Thats, a lot. Ryouma
I didnt know what would make you happy, so I brought various goods. Please accept them. Reinhart

Reinhart said as he opened the rolls.

Inside the rolls were preserved food, clothes, writing tools, mana stones for lighting, clocks that moved via mana all sorts of stuff, each and every single one of which were practical things that my house lacked.

It seems he took his time considering what my house was lacking.

I just eyeballed the clothes, so try them out first. If theyre too big Arone, Lilian. Reinhart
Yes. Arone, Lilian
You can ask these two to help you. Reinhart

I was wondering why they would bring their maids with them to a place like this, as it turns out, theyre here to ensure the clothes fit.

I felt kind of bad, so it was fortunate that the clothes didnt need much changes.

I should thank them.

Thank you, very much. You brought, so much. Ryouma
Dont sweat it. Theyre not that expensive, and besides, we also have something to do around these parts. Reinhart
Something, to do? Ryouma
Remember when I said that were a family of monster tamers? My daughter, Elia, has been studying all this time. Now that shes of age, she needs to tame her first monster, a slime. Reinhart

Her first monster, huh. Since he mentioned age that must mean that she hasnt been permitted to until now. Well, monsters are alive, so she would have to take care of it and its also a little dangerous. Its only natural her parents wouldnt allow her until shes old enough.

Congratulations. Ryouma

When I said that, the young lady drinking tea smiled and said thank you.

Since shes yet to tame her first monster, I might as well lend her a hand.

Weve been looking all this time, but weve yet to find a single slime. Elialia
Slimes are monsters too, theyre living beings. There are times when you just cant find one. Reinbach
Then, how about, here. Ryouma

I stood up and pointed on the map of the forest on the wall.

River. There should be, lots of slimes, here. Ryouma

It was a river not too far from the house. Its where I get my water from, and in fact so do the wild slimes.

If they look around it, they should be able to find a couple.

For the record, when I went to get water once, I managed to catch 14 slimes. Of course, that was only a onetime thing.

When I said that, the young ladys face beamed, and after getting her parents permission, she asked me something.

Can I call you Ryoumasan? Elialia
Please. Ryouma
Then Ryoumasan, if its no trouble, could you teach me which slime to pick. Elialia
Which, to pick? Ryouma
Yes. I only need one slime, but theres probably a lot out there. Im not so sure which one I should tame. Elialia

Ah, so thats what she meant. Theres no good or bad slime, though

In that case You should pick out your slime, according to what kind you, want it to evolve into. It will, take a lot of time, though. If you want strength, a different monster, would be preferable. If you dont have want to raise, the slime for long, you dont need to think, too much Do you, still want a slime? Ryouma
Yes, it will be my first monster, so I intend to treasure it forever. Elialia

What a pure face. Well, if its her, she probably will treasure it. I should help her.

Hmm? Why do I think she would treasure it if its her?

I dont think Ive ever thought like this before.

Am I being fooled? Could I be interested in her? A mentallywise over 40yearsold uncle like me?

Stop. Lets not think too much.

Umm, is it not possible? Elialia

I dont mind teaching her, but the way Im talking right now sure is a pain. It would be great if I could talk smoother.

If youre alright with me, sure, but Only 3 types, can be currently chosen. Ryouma
Why only 3? Elialia
One of the evolution conditions is, unknown the other one, I dont have enough food the other one, doesnt suit, women That last one would, actually be the best abilitywise, though Ryouma
Can I have a moment? Elize

While I was talking with the young lady of the Jamil Household, the madam interjected. She had a serious look on her face.

Mother, I am currently talking with Ryoumasan. This is important for my first contract, so please dont interfere. Elialia
I know that but theres something bothering me. Ryoumakun, if Ive been hearing correctly, it seems you actually know the evolution conditions for the slimes? Elize
More or less. Ryouma

As I thought, the madam muttered, then she turned to Reinhartsan, who promptly lightly shook his head.

(I didnt hear about this!)
(I only heard he was researching!)

Well, thats probably what they were saying with their gestures.

Could it be, this is another mystery, like the big slime? Ryouma
Yes. Slimes can be found anywhere, but no one actually knows anything about them. You should be more careful who you teach those conditions to.Elize

I thought it was fairly simple though.

But then again, even Japan has its own share of mysteries.

If I ever get the opportunity, I would like to talk to those slime researchers.

But thats still in the future. I need to figure out what to do with the present first.
',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(13,'chapter 6','What should I do? I wondered.

I decided to just tell them.

After all, the process was more important to me than the result. I started researching slimes because I was interested in them and because I enjoyed the process. I didnt really care about the result and besides, Ive already told them. Its a bit too late to try and be secretive now.

Its alright The conditions for evolution is, diet.
Different diets, result in different variations.
Green caterpillars, lead to sticky slimes.
Poisonous herbs, lead to poison slimes.
Slimes have, preferences. If you feed them, what they prefer, theyll eventually evolve.
If you dont give them, what they prefer, they might still evolve, but slower alternatively, they might die Ryouma

I see, so thats how slimes evolve. Elia

Seeing the young lady nod interestedly, I nodded and continued.

More food, will make the slime evolve faster.
You can pick from, poisonous herbs, green caterpillars, and the washed bones of monsters
Those three can give you, poison slime, sticky slime, or acid slime. Ryouma
Which type did you say I couldnt pick? Elialia
Cleaner slime, scavenger slime, heal slime. Their abilities are, the real thing, though. Ryouma

The four members of the ducal family did not know of the slimes I mentioned except for the heal slimes, so they couldnt help but glance at each other.

What kind of slimes are the cleaner and scavenger slimes? Elize
They have the clean, and deodorize, skills. Ryouma
Clean and deodorize? I dont believe Ive heard of those before. Elize
I can guess what deodorize does, but what about clean? Reinhart
It would be faster, if you saw it, for yourselves Please wait, a moment. Ryouma

I left the guests for a moment and went to the kitchen. There I took a cloth and dipped it in rabbit blood. I took that cloth back with me along with one cleaner slime.

Sorry to keep, you waiting This is a cleaner, slime. Please, look. Ryouma
A bloodstained cloth? What are you going to do with that? Elize
This. Ryouma

I ordered the slime mentally.

As soon as I did, it took the cloth in my hands and took it into its body.

The cloth spun around the nucleus of the slime.

Ive seen this scene countless times, but no matter how many times I see it, I cant help but be reminded of a washing machine.

10 seconds later.

The slime stretched out a part of its body and handed the cloth to me. I spread that cloth open for the others to see. When the four members of the dukes household saw it, they looked at it with inquisitive eyes. The butler and the two maids in particular where quite taken.

The blood is gone, huh. The colors changed too. Was it digested? Reinhart
It was sucked by the slime, right? Is that it? Elize
No, madam. Thats not all. Maid
Arone?

The one who answered their questions was one of the maids, a middleaged woman by the name of Arone.

Ryoumasama, I believe that slime has the ability to eat filth. Am I understanding this correctly? Arone
It is as you, say. Ryouma
What does that mean? Reinhart
That cloth wasnt just filthy with blood. There were other substances mixed in. Its current color is its original color.

When you dont wash dirty things, the filth piles up, until eventually, its no longer possible to easily remove them. In some cases, the original color is lost forever.

So, in other words, the clean skill is a skill that can clean even the filthiest things! Am I correct? Arone
You are However, its not just, nonliving things. Ryouma

I mentally ordered my slime, and in the next moment, it took my hand and the cloth into its body.

Wha!? Elize
Dont worry. Ryouma

Slimes would normally digest anything they put inside their bodies, so it was understandable why the members of the dukes household cried out when they saw my hand enter the slime. In fact, it wasnt just them. Every person in the room made a stiff face when they saw my hand enter the slime.

Five seconds later, I took out my hand.

Are you ok? Reinhart
The cleaner slime, only digests filth They wont digest human, or animal meat, unless ordered to. Ryouma
Who wouldve thought there would be a slime like that? Reinhart
Please dont do that again. Its bad for the heart. Elize
My apologies Ive already gotten used to it, and I really didnt want to touch, this cloth if possible. Ryouma
Well, it certainly wasnt very clean awhile ago. Elize
It used to be a goblins loin cloth, after all. Ryouma

Everyones face twitched when I said that, but the maids became even more interested.

Apparently, that was because a goblins loincloth was considered to be the filthiest thing in this world.

With this slime no matter how dirty or how disgusting something is you can make it as good as new Moreover, you normally cant, take a bath while traveling, right? Ryouma
Yes, at most I can just wipe myself. This was my first trip, actually. It feels so wrong not being able to take a bath at least once a day. Elialia
Rest assured then, with this slime All your problems can, be solved. Ryouma

After I said that, the young lady of the dukes household looked at me with lasers coming out of her eyes. Eek! I thought. This is really scary! For various reasons!

To make things worse, the madam and her 2 maids eyes also magnified.

Because, this slime, can eat filth, bad odor, all the dirty things. Ryouma
Thats the one! I want that slime! Please help me get a cleaner slime! Elialia

Oops. Didnt I just say she couldnt pick this? Why oh why did I pitch it to her then? Not to mention this was the worst of them all! Ahh, damn it! I lose myself too easily when it comes to slimes At the very least, I should have pitched the scavenger instead!

This is no goo Ryouma
How could you say that after showing me such a wonderful thing? How cruel! Elialia
Ryoumasama, I am a maid of the Jamil Household from a family whos served for generations. I too have studied the basics of monster taming. If its no trouble, may I also know the methods of acquiring a cleaner slime? Arone
Id like to know too Elize

The women seemed to go mad after being pitched such a wonderful slime. Their zeal was so great that the men actually faltered the guards too

Ryoumakun, please dont excite the women any more than this. Camil
The method is, hard to say to women Ryouma
You cant teach women? Camil
I dont mind teaching, its just hard, to say. ryouma
They want to know, though. Camil

Camilsan decided to help me out by leading most of the men and I to a corner of the room.

I get you. Reinhart
Who wouldve thought it would be something like that. Camil
Thats certainly not something you can say to a woman. Zeff
I think not even other women could easily say it. Jill
Well, Im sure itll sort itself out. Hyuzu

Hyuzusan, however, did not share our sentiments. He turned around without a care for the world, and

Ojousama! Madam! Arone! I know the method! Hyuzu

he proclaimed it with a huge smile on his face.

What is he doing!? Does he know of a good way to say it?

Really!? Elialia
Yeah! You just have to bathe and use the water as bait! Hyuzu

He said it!! He said it super directly!!

Ah There goes the slap.

When the women had finally calmed down, Reinhartsan explained the details.

If you put used water alongside clean water, most slimes would go to the clean one. For some reason, however, some slimes would prefer the used water.

Those are the slimes that have the inclination to become cleaner slimes.

After picking out the slimes with talent, all thats left is to feed them nothing but water and filth. Sebum and sweat would probably work best.

I cant believe such a slime actually exists. Elize
Sorry. Ryouma
Umm, its not your fault, Ryoumasan. Elialia
I think its not easy for women to catch cleaner slimes. Ryouma
Ryoumasan. Elialia
? Ryouma
I think Ill go with a cleaner slime, after all. Elialia

It seems the young lady hasnt given up just yet.

Then which of the guards Ryouma
That wont be necessary. I may only be an apprentice, but Im going to be a monster tamer from here on. I cant possibly rely on others for this. Elialia
You dont need to do everything, by yourself Ryouma
Even then, I believe I should take the first step on my own. Elialia
Your call Ryouma
II Ill do it! Can I have some water please? Elialia

Everyone teared up at the young ladys speech. Her face was beet red, but she endured it.

She doesnt really have to go so far.

Whats with the atmosphere, though? Its almost as if a protagonist has just made a lifechanging decision.

Anyway, it felt bad just bringing out some water, so I decided to suggest she use the bath. As a former Japanese, there are times when I really want to go for a dip, so I built a bath when I was building my house. Who wouldve thought itd be used for something like this, though?

Theres, a bath here. Please use it. Ryouma
You have one? Thank you so much! Elialia

I filled the bath with water magic and heated it with fire magic. When the water was at the right temperature, I called the young lady over. The entire preparation took just a few minutes. Magics seriously convenient.

After the young lady came with the two maids, I went back to where the others were.

Ouch! Man, they really glared at me. Hyuzu
You deserved it. Camil
Its because you didnt have any of that. Ryouma

By that I was obviously referring to delicacy. I was often told I didnt have any back on in my previous life, but even I wasnt as bad as him Or at least, I think I wasnt.

I mean When I grew up I had to watch my mouth, or risk getting accused of sexual harassment. It would have been social suicide if I wasnt careful.

Ah, Ryoumakun. Welcome back. Elize
Umm Madam, I Ryouma
Its fine, dont mind it. Shes made her decision and its not like you lied, right? Elize
Of course. Ryouma
Then thats that. Besides, Im happy to see shes serious about becoming a monster tamer. That being said, she couldve just gotten the cleaner slime by asking one from you, though. Elize

What did she say?

What? Ryouma
I said she could have just gotten one from you. That didnt cross your mind? Elize

Ha, ha ha ha Why didnt I realize something so simple. If Id realized that earlier, we wouldnt have had to go through all this mess.

Maybe being here in the forest all this time has finally taken its toll on me. Its been three years since I started living here, after all.

Good grief Ryouma
Oh, well it was interesting in any case. I thought thats youth for you, or something. Anyway, Im glad to see my daughters resolve. Elize
Is that so Ryouma

I feel so tired for some reason

After the young lady left the bath, she took the water and searched the river for slimes. Meanwhile, I consulted the maids on the fitting of my new clothes.

The young ladys luck seemed to be good, as she came back early, about the same time when I was done talking to the maids.

The young lady brought the slime with her and contracted it in front of everyone.

By then it was already dark, so I suggested for everyone to stay the night. I let them use the bath and cooked supper for them.

The butler and the 2 maids offered to help, but I rejected their offer.

Im happy for the offer, but the kitchen is too small. We wouldnt be able to fit with 3 more adults. Not to mention, everything was built for a kid to use, so if theyre unlucky, they might just end up breaking their hips.

Incidentally, our menu for supper was fried beast meat with grated jija, my otherworld version of pork fried with ginger. Reinhartsan seemed to enjoy himself, and the others praised me too.

It was a weird taste for a Japanese like myself, though. Of course, Ive gotten used to it by now.

Its a pity but it cant be helped since theres not a lot of rock salt to get from the cliff, and I even have to use alchemy to purify it because its a mishmash of various minerals. If it werent for alchemy, I probably wouldnt have stayed here for 3 years.

The ingredients here in the forest are enough to live off of, but theyre not enough to satiate my Japanese stomach.

Well, at least theyre happy.
',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(13,'chapter 7','After supper everyone chatted over tea.

Ryoumakun, what are you planning to do from here on? Madam
To be honest, Im not really sure, but Im, thinking of moving Ryouma

My research has already reached a point where I could take a break and the slimes are starting to get too many for the house to handle. Moreover, Ive also started to yearn for peoples company and I also need more seasonings and foodstuff.

Because of that I started considering going around the world, but I wasnt sure what to say. After all, a kid whos been isolated his whole life couldnt possibly just go and say I want to see the world! all of the sudden, right? That would be just too unnatural.

In the end, I decided to make use of my allpowerful grandparents.

My grandparents told me to live happily in town. My current lifestyle is no good I think. I dont think my grandparents would be happy to know I live here. Ryouma
Ryoumakun Madam

As the atmosphere in the room went solemn, Reinbachsama closed his eyes and pondered for a moment, then he spoke.

How about leaving with us then? Reinbach
Huh? Ryouma

Huh? What is he saying? Isnt this our first meeting?

We are a ducal family, you know? We can at least take care of one persons necessities without problem. And besides, I think its a waste for a skilled monster tamer like yourself to be secluding himself deep in the forest. I know you dont like towns, but How about it? Wont you try going out for a bit? Reinbach

I never thought he would propose something like that. The other people seemed to be in agreement too what with those eyes that seemed to say its alright Everyone here is so nice Its enough to make my heart ache.

Well be going to Gimuru starting tomorrow. Once were done with our errands, we will be going home. Well pass by here again, so How about it? Do you want to travel with us? Reinbach
Travel Ryouma

I dont know anything about this world The gods taught me some basic knowledge, but Ive never seen anything in person. In fact, I didnt even know how big of a deal my slime research was until they told me. Im sure there must be a lot more things I dont know.

RRight I think I might just get in your way But if its not too much trouble, will you let me travel with you? Ryouma
Oh! So youre coming! Reinhart
Well Ive also started thinking of leaving the forest, so Ryouma
I see, I see Youll have to prepare your belongings then. We can extend our departure time to tomorrow afternoon. Will that be enough time for you to get ready? Reinbach
Ill be ready by morning. I can bring everything with me with my Item Box, after all. Ryouma
Oh my, you can use Item Box at your age? Thats amazing! Madam

Really? I thought a lot of people could use Item Box?

Grandma said it was, convenient, so I learned, it. I heard a lot of people could, use it, though? Ryouma
Well, its certainly an elementary level magic and a lot of people could use it, but the fact that you could use it while still being so young is a feat indeed. Madam
I think itll be a pleasure studying with Ryoumasan. Elia
Thank you. Ryouma

After thanking the Jamil family, the young lady, the maids, and the guards offered to help me pack, so I decided to start with the most troublesome room.

Whoa, whats with this room? Hyuzu
Its filled to the brim with weapons and armor. Elia
Picking out what to bring from all these wont be easy. Arone
Is that a pelt at the end of the room I see? Lilian
Whats with those pile of bags at the corner that looks like trash? Hyuzu

The place I brought them to was none other than the storage room. All my loots from the bandits Ive subjugated in the past three years are gathered here. I would sometimes come here to do maintenance on the weapons, but other than that, almost everything else has been stored away.

I will be, putting everything into my, Item Box. That bag there, contains the bandits belongings. Ryouma
Specifically? Hyuzu
Dont know Ryouma
Dont know? You didnt bother to check? If you dont check your spoils properly after a battle, isnt that the same as risking your life for nothing? Hyuzu
It wasnt interested, so Ryouma

I answered Hyuzusans question curtly, but there were actually several reasons. One, most of the bandits belongings stank to high heavens, and two, there were rarely anything noteworthy. In one particularly bad case, I even ended up pulling out rotten meat literally trash loot.

The money was irrelevant to me too. After all, I never went to town. What would I need them for?

In the end, searching through the bags was just a hassle, so I stopped bothering and just threw them to the corner after having the slimes clean them up.

Then should we check the contents first? Arone
Good, lets go with that. If the contents turn out to be junk, well throw it away. Lets split the work of putting away and identifying the contents, so we can get things done faster. What do you think? Jill

I nodded.

At that, we left identifying the contents to the young lady and the two maids, while we took on the role of putting the things away.

For a while, we continued with my group just throwing things into that black hole, but along the way, they noticed that some of the equipment and the pelt could be sold for a good price.

But what most unexpected was that among the bandits belongings Ive ignored until now was a bag of coins with a large sum of money. 40 medium gold coins, to be exact.

Even I know the value of 40 medium gold coins, so it was truly a fortuitous find for someone like me whos at long last decided to go to town.

But After that matter with the slimes, Im concerned about the accuracy of my knowledge regarding this world. To be safe I should inquire the price of commodities.

I thought that to myself as we cleaned out the storage room as well as the other rooms. When wed cleared out everything, I called the cleaner slimes and scavenger slimes and had them add the finishing touches. When everything was sparkling clean, I suggested for the ducal family and the guards to use the newly cleaned rooms.

You sure? If theres work to be done, we dont mind helping.
Its alright. I can do the rest by myself Only food, and medicinal, ingredients are left, after all. Ryouma
Medicine, huh Thats certainly not something amateurs should be handling. Alright then, but if theres anything else we can do, be sure to call us. Camil
Thank You, Camilsan. Ryouma
Its fine, its fine. Thanks for the room too. Honestly, any room with four walls would suit me just fine, so this is really a big help. Camil

After chatting a little and giving my thanks I left

Now then All thats left are the starter items I got from the gods Oh, right I wonder if I can take the slimes with me? I cant just leave them here. I should ask. They should still be awake.

Reinhartsan, Reinbachsama. Ryouma
Something the matter, Ryoumakun? Reinbach
Can I bring my slimes with me? Therell be 17, slimes all in all Ryouma
Sure, go ahead. Its only natural for a monster tamer to take his monsters with him. Reinbach
The carriage has plenty of space, so its no problem at all. Reinhart

I see, thats good to hear.

Thank you very much. Ryouma

As I said that, they replied with a smile, saying, its fine. Theyre really goodhearted people. If this were in Japan, the equivalent situation would be me hitchhiking and asking, By the way, I have 17 pets with me. Can I take them too?

I dont know about others, but I definitely wouldnt agree to it. One or two maybe, but 17? In the first place, I dont even have a license, so

I really cant overstate how grateful I am to the Jamil household. Oh, come to think of it, since Ill be going out, I might as well say a word or two to them.

As I thought that, I went to the deepest room in the house. It was really nothing more than a wideopen space, but inside, at the front wall, was a hollowed out area wherein the statues of the gods were enshrined.

The religions of this world dont prohibit idol worship, so sculpting a statue of a deity isnt a problem. In fact, there are adherents who buy small idols as models, so they could sculpt their own. They usually do it while praying in an area recommended by their church.

Personally, I did just that and created the idols, allowing me to both thank the gods and practice my earth magic. After making the idols, I enshrined them at my training grounds and reported to them daily.

But first, to keep others from seeing me, Im going to block the entrance with earth magic



There. Done.

I sat in the lotus position before the idols. After a few minutes of meditation, I opened my eyes and spoke.

Today ended safely too. Youre gods, so Im sure you know, but some guests came today. Ill be traveling with them for some time, so I wont be able to visit for a while.

Im finally leaving the forest. I think Ill be able to keep that promise about visiting the church now.

Its just I dont know when Ill be able to come back, so Ive decided to take all my belongings with me.

If I decide not to come back here again, then Ill make new statues in my new home.

Anyway Until next time.

After that I stood up, opened the entrance, and left.

Speaking of which, it seems I only have a hard time when Im talking with humans. Huh Well, Ive never talked to the idols before, so I never noticed, but I guess the stammering is because of nerves, after all.

Well, whatever With this Ive settled everything that needed to be done. All thats left is for me to leave with the slimes.

Lets sleep.

Like that I went to bed, but

I wonder what the town is like.

I couldnt stop thinking of the town.

Sleeping was harder than usual.
',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(13,'chapter 8','The next day.

Hmm?

When I woke up, the room felt different. As for what, I wasnt really sure.

There was a magic stone for light by the bedside, though its brightness was lowered so it was dim, but other than that there wasnt much in the room. Huh? And then it clicked. The furniture was gone.

Oh, right. I packed my stuff yesterday, didnt I? Wait, no Somethings off.

Turning up the brightness of the magic stone with mana, I looked around me and saw that there was virtually nothing in the room. I did place some of my stuff in my Item Box, but Im sure I didnt touch the tables, chairs, or shelves.

I didnt break them either, so where are they?

I dont see them anywhere.

Actually, now that my heads a bit clearer, even my bed is gone.

Whats going on? Surely, I wasnt robbed, right? I mean if some burglar had broken in I would surely have noticed it, and the slimes would have made a ruckus too. The duke stayed over, but theres no way theyd steal my stuff.

Those things wouldnt sell for much, having been made with earth magic and all. And besides, what kind of burglar would even think theres something to be stolen from caves?

The spoils I got from the bandits could be worth a pretty penny, but Ive already stored them in my Item Box. That leaves only the slimes Hmm? The slimes? Oh no!

Uoo!?

Normally, thered be at least a cleaner slime in the room, but even that wasnt around. As I tried to get myself out of bed, all of the sudden, it felt like I was floating, and then in the next moment, pain greeted me.

Huh?

When I got up, the room became dim again. I turned up the lights, and the room was back to normal. The chairs, the desks, and the rest of the furniture were back in place. Apparently Id fallen off my bed.

I was dreaming?

After calming down, I looked up the bed. Lo and behold, the slime was there, bending down a little as it stared at me. I could confirm that the other slimes were in the cave too.

Good grief, please dont scare me like that, or so I wanted to say, but I guess its not really applicable in a situation like this.

How long did I sleep for?

If I recall correctly, it should still be on the desk

Ah, found it.

When I looked up at the desk, the magic tool clock that I received yesterday was there. The timepiece of the clock was a round, thin, gold plate with two long pins. That timepiece stood on a round pedestal that was supported by a yshaped metal stand.

All the parts of the clock from the timepiece to the stand were of high quality, but the mechanisms that normally made a clock was nowhere to be found, making the whole thing look like a mirror. But that wasnt exactly wrong since it was polished well enough that it could be used as one.

The clock had the numbers 1 to 12 just like in Earth. A full revolution was 12 hours, and two revolution was 24 hours or 1 day. In other words, it read just like a clock from Earth, so it was easy for me to use.

Looking closely though, theres a slight difference in the seconds of this world and Earths. But Ive been living on this world for three years already and Ive yet to notice any difference in the length of days, so maybe its just my internal clock being messed up.

Anyway, from looking at that clock, I could tell that it was currently half past five. The clock doesnt have an am or a pm, but obviously its am. If it were pm that would make me one hell of a sleeper.

I could sleep again, but I dont really feel like it.

As I was thinking that, I heard the sound of footsteps approaching.

Sebasusan? Ryouma

Good morning, Ryoumasama. Sebasu

When I turned to the direction the sound was coming from, I saw the ducal familys butler walking.

Whats the matter? Ryouma
I thought I heard an odd sound a while ago, and then I saw the hallway lit, so Sebasu

Oh, did I wake you up? Ryouma
Not at all. We servants are normally up around this time. Arone and Lilian are already up too, but the others are still asleep. Sebasu
I see Ryouma

Well, in any case, the suns up, so I might as well go draw some water. Itd be waste if I were to just loiter around here, and Im sure Ill just get in peoples way.

When I told Sebasu my plans, he looked puzzled. Cant we just get water with magic? he asked.

I did a little walking and some light training before leaving, then I told Sebasu and the others to use the facilities as they pleased and left.

Fuu.

When I left the house, the peaceful atmosphere of the morn and the forest caressed my skin. I immersed myself in it as I took a deep breath. The sun had already started to rise in the distant sky, so there was enough light to easily walk the road.

As I stepped on the grass wet from morning dew, I leisurely walked through the familiar road. Ive walked this road to the river countless times.

As I was starting to get sentimental, I suddenly realized that the dream this morning was about the house when I first came to this world.

Its been awhile, so it wasnt a perfect representation, but in the past, it wasnt just the furniture missing, I also didnt have that map on the wall or that path that led deeper in.

When I was just starting out, all I did was dig and procure food and water.

The dream this morning was about a time not long after Id dug enough space to live a little.

Oh, this is it.

In front of me was the river I always drew water from. The deepest part only reached up my knees, so it wasnt very deep, but it was fairly wide, so it was convenient to draw water from.

 Rock  Ryouma

I made a water jug with earth magic.

After getting used to magic Ive started to rely on it more and more, but in the past, I definitely drew water here every morning.

I also bathed, did my laundry, and trained by this river back then. Most of my time was spent here.

One day, when my house was finally a little bigger, I went here to draw water, but then I happened upon a slime that had been washed away.

It wasnt the first time that I saw a slime being carried downstream, but that time it was at an arms length, so on a whim, I picked it up and brought it back home. That was when I first learned how to tame monsters.

The basics of monster taming, also known as the contract, required one to weave a thread from mana and use that to connect ones self with a monster. Once the contract was in place, the practitioner could command the monster or understand its intention to some extent. It is also possible for the practitioner to know its position.

When I completed my first contract, the vague emotion I felt from the slime was fear. When I saw it shaking, I decided to call it Tabuchikun because it reminded me of my first meeting with a former subordinate.

At first, Tabuchikun was weak due to having been washed away, so it moved fairly slow. Even when I reached out for it, all it did was shake a little, but it didnt try to run away.

When I gave it green caterpillar to eat, they managed to run away twice out of the five times I tried to feed it. It couldnt catch up to them. It tried to drink water from the river, but the downstream took Tabuchikun away again. I was wondering why the slimes were being washed away. Apparently, that was why.

I continued feeding and training them, and after getting one to evolve, I began my research And thats basically how I got to where I am today.

How nostalgic Its a pity Tabuchikun is no longer with us, but I still have his nucleus.

 

I wonder how my subordinate, Tabuchikun, is doing.

He was a chubby otakuish man who joined our company right after graduation. I had a lot of years under my belt, so I was tasked to show him the ropes. He quivered when he saw my body in my past life. I know I shouldnt be saying this, but he got along pretty bad with others too.

He was never late though, and he always explained how to do something when asked. He would also always feel sorry whenever he did something wrong regardless of if the personinquestion knew or not. We both shared otaku hobbies, so although our direction and generation was different, we still talked some. He had some difficulties with communication, but he got a little better before I died.

I saw him working before I died. Skillwise theres no problem, but it would really be best if he just resigned and found a better company.

Thinking back, I had my fair share of troubles with him, but he was definitely one of the better subordinates I had. If not for that, I wouldnt have named a slime after him.

Ahh, what am I doing? Its no fun recalling a nogood boss and a nogood subordinate.

The slimes have gotten too many, so I dont name them anymore, but I can still distinguish them and call them individually thanks to the contract. Its really convenient.

Oops, what time is it?

Lost myself in nostalgia there. Seeing the sun reflecting over the water and how bright the surrounding area is, I dont think I have any time left for training. I should go back or Ill miss our schedule.

I filled the water jug I made with water and carried it on my shoulders back home. When I got back, Jillsan and Zeffsan were standing in front of the house. They were shocked to see me carrying a jug bigger than myself, but we exchanged greetings, and then I went into the house.

The clock was already pointing to 7 by the time I came back.

Time sure flies.

Oops.

Ryoumakun, youre back. Good morning. Reinhart

I saw Reinhart approaching.

Good morning. Ryouma
Are you ready to go? Reinhart
No, problem. Ryouma
Good to hear. Reinhart

After that I was invited to eat breakfast, and then I passed the rest of the time feeding the slimes.

When the time to leave came, I ate with the ducal family, and then I took the slimes with me and left the house.

I should block the entrance with earth magic There.

Alright, now its time to start our journey!

Shaking away all the nostalgia from these past three years, I turned around.

There, the 11 people I would be traveling with were standing.

Are you ready? Reinbach
Yes, lets go. Ryouma
Lets go then. Reinhart
Alright, lets go! Ojousama, Ryouma, if anything happens be sure to say it! Hyuzu

Hyuzu was the first to walk, and then we followed from behind.

Like this I took my first step toward a new beginning.
',0,'2019-05-01 23:00:01.000000');