INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(10,'chapter 1',' Continental Calender Year 1547, midThirdmonth

It was late in the night, the dark reigned supreme over Capital City Parnam, as three shadows running through its streets. As if to avoid the light, the shadows traveled on the back alleys without any light moss streetlamp. They repeatedly turned around to look at their back, as if they were pursued by something. And then,

swoosh

Kugh(Shadow 3)
!(Shadow 1 2)

The moment they heard the stirred air sound, one of the shadows fell forward. Sticking on his back was a Kukri Knife (a small curved blade). The pursuers will soon arrive! The moment the shadows realized this they decided that it would dangerous if they remained in this place and soon running separated to left and right direction. Every presence behind them seemed to be pursuing to the left direction. The shadow who ran to the right felt pity at his comrade while at the same time, he also felt relieved that he had able to escape. However,

(Shadow 1)

The shadow stopped. It was because there was a presence of an unusual man stood in his way. That man appearance was bizarre. He was wearing a seemingly skin tight black metallic armor and a black Sword Tiger mask.

Parnams Black Tiger(Shadow 1)

The shadow unconsciously muttered. Recently, there was a rumor amongst the weeds. There is a Black Tiger in the Friedonia Kingdom, and the weed who saw him never returns. Why, even though no witness ever returned, there were people who know about him? Because that Black Tiger only rooting out the weeds, he didnt lay hand on a certain merchant who saw his appearance by coincidence. Judging from merchants vague testimony and the information of the whereabouts of the weeds at that time, In the Capital there is a Black Tiger, became a common knowledge amongst the weeds.

And then there was this rumor. If while spying, you encountered Parnams Black Tiger, the first thing you do is to run. The Black Tiger is not someone that could be defeated. But then that rumor was closed with the remark  Well, as if you could escape, that is.When the shadow encountered the rumored Black Tiger himself, he could agree that the rumor is true. Even though the Black Tiger was just standing still, the shadow couldnt find any gaps of weakness in him. It was just like a veteran warrior that had threaded many battlefields.

Surrender. My Lord is a merciful one. If you do not resist, I will guarantee your safety.(Black Tiger)

Perhaps due to the masks effect, the Black Tiger spoke with a muffled voice. His words werent as if he had any care about his opponent. It was in all respect, no more than a very formal ultimatum. However, the shadow who should have been cornered, smiled.

 scum, we are not afraid to die!(Shadow 1)

After saying this, the shadow pulled out the two shortswords from his waist and leaped upon the Black Tiger. The two swords approached the Black Tiger. However, the Black Tiger lowered his waist, unsheathed his oodachi, and slashed down at the man, cleaving him into two.1

!?(Black Tiger)

The next moment, the cleaved body of the man was wrapped in flames. From the beginning, it seemed that he had been intending to seal his own mouth by dying and burn his own body to remove any trace of evidence. What a commendable loyalty In the past, The Black Tiger might think so. However, the current Black Tiger didnt have any thought like that. If there was any meaning of a death born out of loyalty, then it would be if the Lord grieves for him. To die for a Lord that uses his subordinates life as disposable asset, would be only a vain death.

Kagetorasama.(Black Cat Operative)

When he came back from his thoughts, the Black Tiger Kagetora was surrounded by men in black masks. They were the members of the Special Intelligence Operations Squad, the Black Cat, under King Soumas control, who protected Elfrieden from the shadow.

The other one? (Kagetora)
Its the same state like this one.(Black Cat Operative)
I see(Kagetora)

Kagetora pondered for a short moment before he gave to his subordinates, the Black Cat.

I will leave the cleaning to you. I will report this matter to His Majesty.(Kagetora)
Yes, Sir!(Black Cat Operative)

After he confirmed the Black Cat Squad had dispersed, Kagetora remembered the word that the shadow said awhile back.

 Infidel Scum!

If not for his skill, Kagetora could barely caught that mans whisper.

(Infidel, right? Then this is yet another troublesome one.)

While thinking so, Kagetora disappeared into the dark.



 A Government Office in Parnam Castle

Today, just like yesterday, I was working on the paperwork while having Liscia to help me. Then, the glass door on the balcony behind me opened all of a sudden. I was surprised and looked back, and there was the sight of Kagetora black metallic armor dyed with blood I am glad that we were the only one in the room. The chambermaids, who at times came to serve, would surely faint if they saw this scene. Well, though I believe he would wait for a time when there are no other people.

!? Blood!(Liscia)

Liscia rushed to try wiping the blood sticking on the armor with a nearby cloth, but she was stopped by Kagetoras hand.

Its the opponents blood. It is not something that should sully Princesss hands.(Kagetora)
I I see.(Liscia)
Furthermore...... this oodachi bestowed by Your Majesty, it has an amazing sharpness.(Kagetora)

Kagetora tapped the big sword carried around his waist. By the way, I did make an oodachi during my research of NineHeaded Dragons Katana. It was made to improve the range of the slashing attack  sharpness, but its length was the major flaw and it was a weapon that was hard to handle by people with mediocre body stature (they wouldnt able to unsheathe it). I granted him the sword, thinking that there wouldnt be any problem for someone as tall as Kagetora, so I am glad that it turn alright.

Well, if you fine with it, all is well.(Souma)

I was halfamazed as I gave this reply, then I heard a small laugh from inside the mask.

 Is something wrong? Have I said something strange?(Souma)
Fufu I just thought that I am blessed with a good Lord.(Kagetora)
? Is that a sarcasm?(Souma)
No, this is my true opinion.(Kagetora)

Perhaps he had thought about something, Kagetora spoke with a good mood. Well, I dont understand But, lets put this aside. The Commander of the SIO Squad Black Cat wouldnt just visit for a mere chitchat.

Then, perhaps you have something to report?(Souma)
Yes, Your Majesty. Recently, the weeds have lively movement.(Kagetora)

Weeds So, spies. In other words, recently, the foreign spies were skulking in the city.

Grand Chaos Empire?(Souma)
If it is the Empire, we will not be troubled like this. Since we have connection to some degree, we will withdraw before it becomes kill or be killed situation.(Kagetora)
By connection Do you mean trades?(Souma)
We frequently exchanged information about other countries.(Kagetora)
So, there is a shadow rule in the shadow world (Souma)

This was a realm that I shouldnt interfere foolishly, better to leave it to Kagetora.

What country did these skulking spies come from?(Souma)
There are no evidence since they had been destroyed, but probably, they came from the Orthodoxy Empire(Kagetora)
, so, its Lunaria Orthodoxy State(Souma)

Lunaria Orthodoxy State.It is a religious state that is ruled by the Pope of the Lunarian Orthodoxy and the headquarters of Lunaria Orthodoxy, one of the two humanitys religion in this continent, the counterpart of Mother Dragon Creed that worship the Mother Dragon in Star Dragon Mountains. Lunaria Orthodoxy itself is harmless as it touted a principle of helping the weak, but when it became a nation, the Orthodoxy State, there were many rumors of their shady movements. Not long ago, they incited their believers in Amidonia to stage a rebellion and then intervene by invading Amidonia on the believers behalf.

However, our country is not hostile to the Orthodoxy State, isnt it?(Souma)
Spies are not something that is only dispatched to enemy countries. Even in countries that we want to be friend with, we will dispatch spies to collect information or negotiation cards.(Kagetora)
Hmm then, their active movement means(Souma)
Perhaps, there might be some movement in the near future.(Kagetora)
How troublesome(Souma)

I remembered what I had said when I first met with Roroa.

That country hates the Star Dragon Mountains and the Grand Chaos Empire.(Roroa)
In Lunaria Orthodoxy, the recognition of Holy Man or Holy Maiden  are supposed to be the Popes exclusive decision. Because of it, for the Lunaria Orthodoxy State, Mariadono is an unpardonable wicked woman who is impersonating a Holy Maiden.(Roroa)
It is unlikely that the Orthodox State will leave Elfrieden Kingdom after its national strength increased by annexing Amidonia. They will surely make a contact someday. Then perhaps they will give Soumadono some title like Holy King, and try to drag you into a war with the Empire.(Roroa)

If what Roroa had said happened It will surely become yet another troublesome matter.


 A few days later. A report was brought to me Holy Maiden of Lunaria Orthodoxy has entered our country.
',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(10,'chapter 2','  thats why I am going to hold an audience with the Lunaria Orthodoxy States Holy Maiden tomorrow.(Souma)
I see(Maria)

In the display on the quasireceiver of the Royal Voice Broadcast, the beautiful visage of the Empress of the Gran Chaos Empire, Maria Euphoria, started to show sorrow. Even for the Empire, the strongest nation of the Humanity, the Orthodoxy States Holy Maiden would be still a troublesome opponent.

This place was the Royal Voice Room in Parnam Castle. It had been several days after I was informed that the Orthodoxy States Holy Maiden had entered our country and yesterday, there was a request for an audience from that Holy Maiden. And tomorrow, I would be scheduled to hold an audience with that Holy Maiden. In response to this situation, I asked Hakuya to inform Mariadonos imouto Jeannedono, in order to established this emergency leader summit with Mariadono.

I didnt think that the Holy Maiden would visit just for mere small talks. I didnt know yet what her purpose was, but it certainly was not a something that will benefit the Empire, who they viewed as an enemy country. This is the reason why I wanted to reinforce our cooperation as the leaders of the secret alliance. I didnt want the Empire to have any distrust when one day they know that I met with that Holy Maiden. I then asked Maria.

Even for the Empires Holy Maidendono, you have difficulty in dealing with the other Holy Maiden?(Souma)
 Please dont call me Holy Maiden. Oh, Great King of Friedonia.(Maria)
Ugh(Souma)

What a superb counter. Well, for the one who didnt desire any praise, the people who praise them would be annoying I am neither Ika1 nor a Giant Isopod2 nor Dedede3. When I was thinking about this matter, Maria sighed in a coquettish manner.

I couldnt say much about Orthodoxy States Holy Maidensama since I never met her personally But I am troubled since when the citizens called me Holy Maiden on their own accord, she threw complains on me.(Maria)
 Was there no inquiry from the Orthodoxy State? If I was someone from the Orthodoxy State, if Mariadono has acquired a fame so high that you are referred as a Holy Maiden, rather than pushing someone from my side to become Holy Maiden, wouldnt it be faster to just officially ordinate Mariadono as a Holy Maiden?(Souma)
Ah Which reminds me, there was a talk about that, wasnt there? But, I refused it.(Maria)

Mariadono replied as if that talk had nothing to do with her at all.

Did you refuse?(Souma)
If, in reverse, you are the one being asked, will Soumadono accept it?(Maria)
I absolutely refuse.(Souma)
See?(Maria)

Maria had a soft but slightly wistful smile.

What do I need to do if I become a Holy Maiden? Just to whom, what kind of order I need to give? The Orthodox State touted justification, saying to protect the weak, while driving people into conflict. I dont want to become a glorified banner of such thing.(Maria)
Mariadono(Souma)
Although my station is that of an Empress who govern over the Empire, in the end, I am just a human. Rather than being worshiped as a Holy Maiden, I want to be loved by other people, as a human, as a person.(Maria)

I want to be loved by other people, as a human, as a person.
I inscribed her words deeply into my heart.



Lunaria Orthodoxy State.

This country, the headquarters of the Lunaria Orthodoxy, is governed by the Pope of Lunaria Orthodoxy. It is a religious state whose history is about as long as Elfrieden Kingdom. It was said that during the Continents Chaotic Era, in contrast to Elfrieden Kingdom, where its Founder King, a Hero, brought together many races and unite them into one nation, Lunaria Orthodoxy State was united by the religious force. Its doctrine Save the Weak , seemed to be codified based on its experience of that era.

Originally Lunaria Orthodoxy is a religion believed by a tribe, the Moon People Lunarian. The socalled Lunarian tribe was a tribe that not originally reside on this Continent, but came from outside. Whether the outside that they referred to was some islands like the NineHeaded Dragon Archipelago or they came from a different world just like me, I dont know. However, in the legend, they came from the moon on the sky, hence their name.

The object of the Lunarian faith is their old home, the Moon, that they worshipped as the Moon Goddess Lunariasama. Since Lunariasama is shining high in the sky as the Moon, it is prohibited to make idol statue of her. Furthermore, in the Capital City of the inner sanctuary of the Lunaria Orthodoxys Central Church, there was the socalled Moon Stele Lunalith where the Moon Oracle was engraved.

Initially, it was only believed by the Lunarian, but in that Continents Chaotic Era, it penetrated even to the other Nations and Races as their mental support, therefore the number of believers increased. Thereafter, there was a sectarian conflict within the same faith. There were also disputes about which was accepted belief and which was heretical, but when the dust settled, the current Lunaria Orthodoxy had took form, a major religion that was believed by onehalf of the Humanity in the Continent together with the Mother Dragon Faith.

As for the doctrine, they touted Save the Weak and Mutual Aid. When there is trouble, everyone will help each other, perhaps this simple doctrine was the reason why it gained new believers. Just like its doctrine, the believers will do acts like fundraising for impoverished people or distribute food for poor people. It would be easy to comprehend that Lunaria Orthodoxy and its believers were harmless. However, when it comes to Orthodoxy State, something stinks.

It was said that they used the believers living in each country to intervene in the politics of that country or to encourage rebellion. I say this once more, the believers themselves are harmless, there are people who are truly dedicated to saving the weak. We mustnt control them by treating all the believers as one same entity, and in the first place, the more you suppress a religion, the more it blazes up. No matter how powerful the military power a country has, if the soldiers believe in Lunaria Orthodoxy, then the national stability will deteriorated as soon as that country becomes hostile to the Orthodoxy State. Thats why, a religion that is so intertwined with political power to such degree like that is troublesome.

Then a Holy Maiden from that troublesome country, Lunaria Orthodoxy State, was currently, stood before me. Right now, there were only a few people in the Parnam Castles Audience Room. It was because the Orthodoxy States side hoped for an audience with a small number of people. From the Orthodoxy States side, only the Holy Maiden alone requested for the audience. Thats why, on both sides of my throne, there was the First Queen Consort Candidate, Liscia, and the Second Queen Consort Candidate, Aisha, who also acted as my bodyguard and was armed. Then, the Prime Minister, Hakuya, was the only person who stood between the Holy Maiden and me.

Well, even if this girl was an assassin who pretending to be a Holy Maiden, since Aisha was here, it would be fine. But looking at the girl standing on the carpet few steps below the throne, that worry is undue. When I saw her appearance, I thought

( I see. Certainly, she is the Holy Maiden )

Well, perhaps there was no word better to describe the girl, other than Holy Maiden. Her age is about 18 years old. She has a beautiful face and alluring dark eyes. Her silver hair was tied into two bunches. She was a bishoujo who even made Aisha spoke out her admiration unintentionally. Her figure, clad in a priestess habit, could be said as the exact depiction of a Holy Maiden itself. If I hadnt built my resistance, thanks to Liscia and others, I would be overwhelmed just by meeting her eyes.



 But, I wonder why. In contrast to her high spec appearance, I didnt feel any charm from her for some reason. On the contrary, I felt a strange sense of discomfort when I saw her. Though, I thought she is a cute girl. Something was bothering me. While concealing this sense of discomfort inside my heart, I addressed the Holy Maiden.

Welcome. The Lunaria Orthodoxy States Holy Maidendono. I am, Soma Kazuya, the one who has been asked to govern over this country.(Souma)
Greeting and good tidings, Your Majesty Great King of Friedonia. My name is Mary Valenti. Today I visit as an envoy from the Lunaria Orthodoxy State. Despite the sudden request, to be granted an approval for this audience, on behalf of the Popesama, let me offer my gratitude to Your Majesty from the bottom of my heart.(Mary)

The Holy Maiden Mary bowed in a gracious manner as she spoke this. Mary The pronunciation of her name is similar to Mariadono.  Since she was titled Holy Maiden, I thought that she would say, You must believe in our God in a condescending manner. Well As expected, it wouldnt do to take a lofty attitude like that for the first meeting with a King of a country.  Mary raised her face, and then looked straight at my eyes before she spoke up.

The tales of Your Majestys accomplishment had even reached Lunaria Orthodoxy State. In less than a year after being summoned as the Hero, Your Majesty had rebuilt the Elfrieden Kingdom, defeated Amidonia Dukedom and then annexed its territory. Truly a feat of a Hero.(Mary)
You are regarding too highly of me. I had not done any Herolike deeds. That I could rebuild this Kingdom, it was all because I am blessed with good subordinates. I had repulsed Amidonia, but annexing it was just how the course of event flowed.(Souma)
How the events flow is a fate beyond human comprehension. Your Majesty might have the Gods Blessing.(Mary)

Gods blessing, huh?  What a religiouslike opinion. I couldnt agree with her, though.

No, this course of event, in the end, had been orchestrated by one person. The person who you should extol is not me, but the Amidonia Princess who made that once in a lifetime decision.(Souma)
Roroa Amidoniadono, is it? Though she still young, she was able to draw out greatest benefit for the citizen by bringing the two nations into negotiations. As fellow women, I truly admired her.(Mary)
(Souma)

But I believe Roroa will dislike you. When Roroas elder brother, Julius was still the Duke of Amidonia, the Orthodoxy roused the believers in the Dukedom and caused a rebellion. Although Julius had suppressed the rebellion, Roroa was angry that her fellow citizen was spilling each other blood. Actually, as the Third Queen Consort Candidate, I wanted Roroa, who manages this countrys finance, to be here in this place. However, considering her animosity towards the Orthodoxy State, I had her to wait with Junasan and others in the Office Room. Even if she isnt a girl who will let her feeling showed on her face I dont want her to endure this needlessly.

However, just how much truth is in this girls words? Her tone was flat, so it was hard to read any particular emotion from her. She didnt seem to be playing any tricks. If she could speak like this after knowing what her country had done, she would be a hypocrite liar, but there was also a possibility that she was just pretty flower who didnt know anything No, if either of these is true, then her emotion should be more apparent. If the former is true, then as a liar, she would give more I am sincere appeal to deceive her opponent. If the latter, then she should have a more enthusiastic feeling, as if she came here with righteousness on her side.

However, this girl was too flat. It was as if to say that her visit was normal and natural. This might be how a countrys envoy act in the first place, but in this girls case, I felt that this was too extreme The discomfort inside me became bigger.

Then, Marydono, what brings you here today?(Souma)

I tried asked straightforwardly so that I wouldnt let that sense of discomfort showed in my expression. Then, without losing any of her placidity, Mary slightly bowed her head.

Ara, thats right. As a Holy Maiden of Lunaria Orthodoxy, today I have a request to your Majesty.(Mary)
Request?(Souma)

I asked while having a bad feeling about this, and Mary replied with a completely nonmalicious smile.

I would like to ask for Lunaria Orthodoxy to be the state religion of Elfrieden Kingdom.(Mary)
',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(10,'chapter 3',' (State religion)(Souma)

A nations official religion. In modern developed countries, this concept that had become entirely obsolete. A religion that is believed and officially endorsed by the country as a whole. That religions holy days will be celebrated by the country as national event Anyway, its not all that realistic to apply this in our multiracial country.

Marydono. Do you realize just what your request entails? If we, a multiracial country, give preferential treatment to one religion, this will be a cause for disunity for our nation. Are you asking me to commit such a foolish mistake?(Souma)


I protested, albeit gently, in order to display my annoyance at her request. I may not look like it, but I am someone with an authority worthy enough to be referred as a Great King. If she was just a small fry, she would be afraid to anger me and then behave. However, the girls expression didnt change at all.

Were not requesting preferential treatment. Even though, I would be grateful if we are given preferential treatment, we simply wish for our faith to be appointed as a State Religion to begin with. Our Lunaria Orthodoxy preaches tolerance for other people. This attitude also applies to interactions with other religions. We neither wish to expel the believers of other faiths nor demand their conversion.(Mary)

Saying that, Mary held her open hands high towards the ceiling.

The Moon in the sky changes its appearances as the seasons change, and changes its face based on the time and place it can be seen. Furthermore, just like how some may see an animals silhouette on the Moon while others see a human one, the same thing applies to faith. Even if it appears differently, the belief and faith in God doesnt change. The unbelievers only have a different perspective in seeing what we see as Lunariasama.(Mary)
(Souma)

That was quite the poetic opinion. If that opinion expressed her true feeling, then surely she was really tolerant But I think that her words are nothing more than a weightless papiermch pretension.  

However, isnt it true that you people dont recognize the Mother Dragon Faith that believes in the Mother Dragon of Star Dragon Mountains.(Souma)
God is in the Heavens and inside ones heart. To worship a being that physically exists in the world, is only a fear of that being. What could the Mother Dragon do for a believer of the Mother Dragon Faith? The Star Dragon Mountains only interact with Nothun Dragon Knights Kingdom, right?(Mary)
It is natural that an awe towards a greater and stronger being becomes a faith. The Mother Dragon is a being beyond human understanding, a symbol of the nature itself.(Souma)
That way of thinking is wrong. We believe that faith is a bond between the God, who thinks of humans, and humans, who think of God. We see the Mother Dragon Faith as a misplaced affection to the Mother Dragon. We wont accept it as a religion. Of course, I could understand why there are people who have such thoughts.(Mary)

Mary replied fluently. She didnt say anything particularly strange. In fact, the impression I got from her was one of sound logic. If this was a personal opinion, I wanted to discuss this leisurely with her on another occasion However, her premises were wrong on several points.

 Since this is a good opportunity, I would like to hear your opinion on this matter. May I?(Souma)
In what matter?(Mary)
Recently, it looks like there are many spies loitering around this country(Souma)
In this country? Even though this country appears so stable, how unexpected.(Mary)

As expected, Marys expression didnt change one bit. I couldnt feel any ripple in her emotion from some time ago. This even made me believe that she really didnt know about the Orthodoxy States dark side.

 The moment that spy attacked my subordinate, he said a phrase, Infidel scum. Perhaps, he was a believer of some religion. As a person who worship a God like that Spy, does Marydono understand the meaning of that spys words?(Souma)
Since I am not that spy, it is difficult to ascertain, but(Mary)

Mary pondered briefly and replied without any particular enthusiasm.

Perhaps, he thought that his mission was ordained by God. The people who disturbed his duty were, to him, people who didnt believe in God. Thats why he saw them as Infidels.(Mary)
In other words, it doesnt matter whether or not the other people are really unbelievers?(Souma)
From your story, I feel that, yes, it doesnt matter. Though, this is only my subjective opinion.(Mary)
 No, this is helpful.(Souma)

I wonder why. Shes made me even more perplexed. I couldnt detect any agenda or motive in her words. She honestly replied to all my questions Is this really a negotiation? Shouldnt negotiations be a clash of opinions towards a mutual objective? For this reason, any fact that could become a weakness was hidden and conversation was directed towards an advantageous position. However, I completely couldnt feel anything in her speech and conduct towards such a direction This is difficult.

 Lets return to the original topic. So you want Lunaria Orthodoxy to become our countrys state religion?(Souma)
Yes.(Mary)
How will this benefit our country? Also if you talk about religious tolerance, then what the people put faith into should not matter. So, what is the reason behind the Lunaria Orthodoxys request to be made as a state religion?(Souma)
I will answer your first question. If Lunaria Orthodoxy is granted a state religion status, then we are prepared to acknowledge Your Majesty, the Great King, as a Holy Man. Your Majesty will not only be a king of a country, but a Holy King. Then, the Orthodoxy State would depute me as personal bishopess1, to serve Your Majesty, as if to serve God Himself.(Mary)

After saying this, Marie bowed It was just like what Roroa had said. But I never imagined that it would be the Holy Maiden herself to visit me.

 It sounds as if your visit is to offer yourself as a bride.(Souma)
If Your Highness Holy King desires so, you may do whatever you please to me.(Mary)
I still havent accepted nor agreed to your offer.(Souma)
Please forgive my impertinence.(Mary)

Mary once more bowed down. She wasnt shy at all rather than that, her expression was that of someone who didnt even know that she had done bad things. Aisha, who stood beside me, twitched when I mentioned the bride thing, while Liscia wasnt perturbed at all. She was just observing Mary with serious expression. Just how was Mary reflected in her eyes?

Then? After I become the Holy King and the Holy Maiden serves me, is there any benefit to this country?(Souma)
This country has grown so much that it stands on equal grounds to the Grand Chaos Empire. This is also the result of His Majestys reign. If you can acquire Lunaria Orthodoxys influence, then this country could acquire power that could surpass even the Empire.(Mary)
Sure, we have become bigger. But dont you know that in terms of both territory and national strength, the Empire still surpasses us?(Souma)

Well, at least on the aspect of technological power, though, we have already overtaken them, but lets not reveal that. However, Marie shook her head.

About 40 of the Empires population comprises Lunaria Orthodoxy believers. This was because the Mother Dragon Faith wasnt that popular in the Empire that waged expansionist wars against Star Dragon Mountains in the past. In other words, close to half of the Empires population follows the Lunaria Orthodoxy. If this country ever found itself at war with the Empire, then with our influence we could divide the Empire in half.(Mary)
Stop speaking something that outrageous so easily. Our country has no intention whatsoever to wage war against the Empire.(Souma)
This is just a whatif situation. In short, we are extending an offer to be the most powerful Humanity country that even could surpass the Empire.(Mary)

Hmm. Well, if we look at the relationship between monarchs and the Roman Catholic Church during the Middle Age and Early Modern Age2, this isnt an uncommon topic. With the combination of the political power of the Monarch and the authority of the Church, they could govern the country and crush any enemies. This might be the simplest method to achieve such result. But only if we ignore the power balance struggle between the monarch and the church that happened afterwards However, right now, we are trying to move the age forward. To regress into the older age is something that we must not do. Suddenly,

May I have a moment of your time, Your Majesty?(Hakuya)

Hakuya, who had been silently observing our course of conversations, spoke up.

Please forgive me, Holy Maiden, but I need to talk with His Majesty for a bit.(Hakuya)
Yes, of course.(Mary)

After asking permission from the Holy Maiden, Hakuya approached my throne. Then he brought his face to my ears and then whispered to me.

(You seem to be under the weather, Your Majesty)(Hakuya)
(Yeah Somehow, I cant bring the sun out as I feel a strange sense of discomfort from that girl.)(Souma)
(That may be so, but it may be necessary to consider thinking about this differently.)(Hakuya)
 (Differently?)(Souma)

When I asked, Hakuya nodded.

(I have been observing her from earlier, she seems to be lacking emotions.)(Hakuya)
(Yes, I also arrived at the same conclusion.)(Souma)
(The Orthodoxy State may have some objective in sending her here, but it looks like Marydono doesnt think anything about it. She is merely relaying the top hierarchys intention t us as a Holy Maiden, just like a postal Qui, isnt it?)(Hakuya)
(?! So she is just a messenger!)(Souma)

So she wasnt tasked to negotiate! Its no wonder Even though I was discussing with her, it didnt feel like we were negotiating. Surely, she had been guided about potential  questions with prepared answers and was negotiating with me by following this guideline. Thats probably why she answered honestly when asked questions outside those guidelines, just like the spys thoughts question.

Or perhaps she had been instructed to honestly answer such questions. Even if the other party skillfully tried to bring out important information from her, if she hadnt been instructed about it, she could just honestly answer, I dont know. This is not a negotiation. This was like a text to speech software reading a mail to another person. I took a glance at Mary. She noticed my glance and then slightly tilted her head while still being expressionless.


( I see. Indeed, a girl similar to DIVAloid3)(Souma)

Back in my previous world, there was text to speech software called DIVA  loid. It was visually represented by a cute girl, could recite text with a girls voice, and even sing, which let to big following on video sites.4 When talking to Mary, it felt like talking to that DIVA  loid.5

(In this case How should we negotiate after this?)(Souma)
(Well. Please try to get an answer to your second question. However, we should infer the answer not as her personal opinion, but the intention of the Orthodoxy State itself.)(Hakuya)
 (I understand.)(Souma)

Thank you for waiting.(Hakuya)

After I confirmed that Hakuya had returned to his original position, I spoke to Mary.

By the way, I have not yet heard the answer to the second question I asked earlier. What is the reason behind the Lunaria Orthodoxys request to be made as a state religion?(Souma)
It is for all Humanity.(Mary)

Mary answered without any hesitation.

The Demon Lord Territory suddenly appeared in the Continents Northern side. Although at present, their expansion seems to have stopped, as long as Demon Lord Territory exists, there will be no peace for Humanity. In order to obtain that peace, it is essential to invade the Demon Lord Territory and eliminate the Demon Lord. However, the demonic monsters of Demon Lord Territory are so strong that it is impossible for any country to deal with the threat individually. There is a need for all of Humanitys nations to fully cooperate and consolidate their power.(Mary)
(Souma)
Therefore, I wish for Your Majesty to become a Holy King. With Your Majestys power combined with the influence of our Lunaria Orthodoxy, together we could bring many nations together. After we uniting many nations and ensuring their cooperation, even the Empire would yield. Since 40 of the Empires population comprises Lunaria Orthodoxys believers, the Empire cannot ignore this fact. With Humanitys nations united, we will attack the Demon Lord Territory, defeat the Demon Lord, and liberate the North.(Mary)

Mary was replying grandly. It was just like a Crusade. To unite the nations with the power of religion towards liberation of lost lands.6 So, they wanted me to become their banner bearer as the Holy King.

(But that is simply what it looks like on the surface.)(Souma)

By taking into account the invisible people who stood behind Mary, their intention came to light. Mary seriously believed that it was to liberate the North, but the intention of those behind her was different. To unite Humanity, thats the hint. There is already a structure in this world which brought Humanity together into one. Its the Humanity Declaration initiated by Mariadono. Although this pact had a defect, I believed that, for the time being, it had functioned effectively.

Mariadono, who was deemed by the Orthodoxy as impersonating a Holy Maiden, was gathering fame and honor as the leader of Humanity Declaration. The Orthodoxy State could not tolerate this situation. After all, the more Mariadono gained recognition, the more their own Holy Maidens influence diminished. A religious country is one ruled by religious influence. In other words, the loss of such influence is a life and death problem for the State.

(Thats the reason why the Orthodoxy State wanted to create an international cooperation structure, different from the Humanity Declaration. A new structure that could emphasize their own influence.)(Souma)

And it seemed that they wished to have me as the figurehead. I sent a glance to Hakuya. who noticed it, closed his eyes and nodded, then he shook his head. Perhaps he had arrived at the same conclusion as I did. Furthermore, it seemed as though he wanted to say we must not accept it Well, of course. Anyway, there is something that I ought to ask.

By the way, what will happen if I refuse this Orthodoxy States proposal?(Souma)
Though it is regrettable, I guess it will be better to give up on it. We shouldnt force you too much.(Mary)

Huh? This is unexpected, she back down easily. I thought that if I didnt accept their demands, they would say that they would incite the believers inside the Kingdom and raise rebellions. While I had such thoughts, Mary continued speaking without changing her expression at all.

However, perhaps Perhaps we will wait.(Mary)
Wait?(Souma)
Yes. Your Majesty may change your mind, or perhaps a new candidate for Holy King will appear.(Mary)
!?(Souma)

Well, well so this is it, huh? In other words, if in the future a country becomes slightly stronger or if a monarch who had gained fame emerges, they would propose the same to them as well. Perhaps, if this is true then the fate of 40 of the Empires Population is believers, we could divide the country if we want to will befall us. Mary might not have noticed, but this was a very frightening threat.

(This is something that I could just immediately decide right now.)(Souma)

Either to accept it or to reject it, I couldnt decide without giving it a thorough thinking. At least, I would like to have a thorough discussion with Hakuya. I stood up.

I understand the Holy Maidens request. However, given its importance, it is impossible to give an immediate reply. I would like some time to carefully consider it. Let us continue this conversation another day.(Souma)
Certainly, Your Majesty. I pray that you will come to a good decision.(Mary)

Mary obediently exited the Audience Room. Even though the negotiation didnt reach any conclusion, there was no disappointed expression on her face. I had been observing Marys face really seriously. Even during this conversation, her expression practically didnt change at all. As if she was a dolllike girl.

(Puppet An artificial!?)(Souma)

 There I noticed. The true meaning behind the sense of discomfort I had felt from her.
',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(10,'chapter 4',' It was nightfall by the time the audience with Mary was concluded. Roroa and Junasan welcomed us when we returned to the office room. I also could see Carla in her maid uniform at the corner of the room.

Welcome back Darling Eh, did something happen?(Roroa)
Ano, what happened, Your Majesty?(Juna)

Though both welcomed us with smiling at first, they became worried the moment they noticed my expression. Ahaha, surely my face looks horrible right now May be it really does. Junasan touched my forehead with her cold and soft hand.

Theres no fever But, are you feeling unwell? Would you like to rest for a while?(Juna)
Wait, Cianee! What happened to Darling!?(Roroa)
Eeven if you ask me, I also dont know!(Liscia)

Just as Roroa pressed Liscia for an answer, I replied, No Im alright, and gently brushed Junasans hand aside, before sitting down on my desk. And then,

Sorry. Liscia, Aisha, Junasan, Roroa Come over here.

I beckoned my fiances closer. The four of them exchanged glances and then slowly approached me. When they stood close enough by my side, I wrapped them all together in a hug.

Hyah?(Liscia)
What!?(Aisha)
Aw(Juna)
Wa, Darling!(Roroa)



All four of them yelped, but I kept hugging them without minding it. From a distance, this would look like a team huddle, so there would be nothing romantic about it. Even so, I could feel their warmth I finally calmed myself down. After a minute or so, I released them. While mending her disheveled clothing, Liscia asked with a hint of anger.

 You better explain whats going on, okay?(Liscia)

I was glad that behind her angry tone, there was a heart that genuinely cared about me.

Yes. I shall explain everything.(Souma)
Souma, you became like this after the last conversation with the Holy Maiden, right? Did something happen?(Liscia)
 I felt a sense of discomfort, the whole time during the audience.(Souma)
Sense of discomfort?.(Liscia)

I nodded.

When I saw Mary for the first time, I thought that she was pretty. But at the same time, I felt that something was strange. She should have come across as a very attractive girl, yet I couldnt bring myself to see her that way.(Souma)
But, from her appearance, she looks like a beautiful girl in my eyes.(Hakuya)

Hakuya said so. Yeah, perhaps, other people wouldnt able to notice it.

I also didnt notice at first. However, the moment I thought that this emotionless girls face is Like a Doll, or perhaps I should say, seems artificial, I noticed the true cause of that sense of discomfort. She resembled someone.(Souma)
Resembled someone? Who?(Liscia)

Liscia ask me, and so I pointed my fingers at her.

You. Liscia.(Souma)
Mme!?(Liscia)
Yes. Furthermore, Aisha and Roroa too.(Souma)
Eh, really?(Aisha)
Me too?(Roroa)

Upon hearing my answer, Aisha and Roroa stared at each other face. I then turned my attention to Hakuya.

Hakuya. How you would describe Marys appearance to someone who was not in the Audience Room?(Souma)
 Lets see. A well proportioned face. Silver hair, braided into twoWh!?(Hakuya)

Hakuya widened his eyes, so he seemed to notice it too. I then let out a sigh.

For me, I will describe her like this. Her well proportioned face resembles Liscia. Her silver hair is reminiscent of Aishas Dark Elvish characteristics. Her hairstyle is similar to that of Roroa. In other words, Marys appearance seems to be a composite of features from Liscia, Aisha, and Roroa.(Souma)
Lilike us!?(Liscia)

Yeah. The reason why I did not get charmed by her at all, even though she was a such beauty, perhaps because my face perception processing gave off a red flag. If one day, Aisha suddenly had a human face, then I would be surprised. If Liscia or Roroas hair became silver then it would be natural if I felt uncomfortable. Then, Aisha tilted her head.

Wait a moment. If she has our characteristics, then where is Junasans part? Her body appearance is also average, right?(Aisha)
Thats it!(Souma)

I slapped my knee, as I realized something.

Judging from her look, Mary doesnt have any resemblance to Junasan. Though perhaps, only her big black eyes are similar to Junasan, but as a characteristic, this is too weak. Then, what is the difference between Junasan and the other three?(Souma)
I am the only candidate as a Royal Consort. And it is only my engagement which still has yet to be announced to public.(Juna)

I nodded at Junasans reply.

For Liscia, Aisha, and Roroa, their engagements have already had been announced to public, but out of consideration for Junasans activities as Song Princess, Junasans engagement hasnt been announced yet. Thats why no one knows that Junasan is my fiance. Following that, if we consider that the dispatched Holy Maiden possesses the characteristics of my fiances, excluding Junasan, and the active movement of the Orthodoxy States spies in the Capital, we could speculate that what the spies gathered was information about my fiances appearance. It was so that they could send a girl as the Holy Maiden that matched my preferences, or at least wont be disliked by me.(Souma)
Souma, then.......(Liscia)
Yeah Do you remember what Mary said, when I mentioned whether she was here to offer herself as a bride?(Souma)

If Your Highness Holy King desires so, you may do whatever you please to me.

Mary replied without any objections. To send a girl who matched my preference, and that girl said, You may do whatever you please to me. And then, as quid pro quo, in exchange for her, the Orthodoxy State could try to press on their demand. In other words, for the Orthodoxy State, the Holy Maiden was

Something similar to national scale honey trap.(Souma)
What they attempted is similar to how the nobles try to peddle their own daughters(Liscia)

Liscia spoke in astonishment. Truly, for a country governed by clergymen, this was a vulgar doing. As a nation, the Orthodoxy State is just as worldly as any other country.

After I noticed the true cause of the sense of discomfort I asked Mary about how she was selected as a Holy Maiden, and she gave a very thorough and detailed explanation.(Souma)

It seemed that the Holy Maiden of the Orthodoxy State was chosen from among the nuns who work in the Central Church by an oracle from the Moon Stele Lunaris. It seemed that most of the nuns were originally orphans. They numbered around 50 people. Perhaps, they were trying to have a diverse stock of Holy Maiden candidates for the rulers they wanted to ensnare. At the Central Church, a place separated from the secular world, the nuns received their training and were raised into obedient believers of the doctrine. And when they come to a certain age without being chosen as a Holy Maiden, it seemed that they would be dispatched as Priestesses to various churches.

This is a horrible story, isnt it?(Aisha)

Aisha spoke up.

Doesnt that mean they are like real dolls? That they have no free will at all?(Aisha)
No, no, Ainee. Ive heard worse stories.(Roroa)

Aisha found this unacceptable, but Roroa had a different opinion.

No matter what the country is, a proper orphanage is difficult to operate. After all, even when the children reached the working age, without any education, they would end up as cheap manual labor. A place that teaches reading, writing, and basic arithmetic is rare. Especially for women who come from the orphanage There are many who have no choice but to sell their own body. If they could escape from this circumstance by living in the church so that they can have enough food, clothing, and shelter, then for the person themselves, this would be happiness, right?(Roroa)
Even so! They are being raised to become tributes to rulers of other countries as Holy Maidens!(Aisha)
Calm down, Ainee. I also find it unpleasant like you do, but using children in marriage is something that is common practice for any Noble or Knight House. Even I I also use myself for political reason.(Roroa)
Roroadono.(Aisha)
Besides, in her story there are several Holy Maidens. In other words, there are other nuns who experience the same story. It would be pitiful to call the Holy Maiden as a tribute, but if they were handed to a ruler, they are marrying into wealth and status. Just like me, who used myself for political reason, am perfectly happy, whether or not they are happy or sorrowful is not something that can be decided by another person.(Roroa)

Roroa gave her honest answer. Really She is a strong girl.

I also agree with what Roroa has said. Although I dont find this method agreeable, I have no complaints with the system itself.(Souma)
Then why does your face have such a horrible look?(Liscia)

Liscia asked me and put her hands on my forehead.

What shocked me was the fact that Mary accepted that I am a Holy Maiden and completely embraced everything about it.(Souma)



At the end of the audience, I asked Mary something which had been bothering my mind.

Marydono, dont you have any problem with being treated as the Holy Maiden? To suddenly become burdened with the countrys prestige, to stand in front a king of another country, and then to assure that  king you may do whatever you please to me. This burden is too much to bear for a single person, even more so for a mere normal girl this will be utterly unbearable. Isnt this way of life too grueling?(Souma)

In response to my question, Mary answered while smiling.

I have been graciously bestowed with the position of Holy Maiden by the Lunariasamas Will. The Holy Maiden is the face of the Orthodoxy State. I, who had been bestowed with that duty, rather than my own sentiment, I would like to carry out my duty as the Holy Maiden that has been conferred to me. Not just for my country, but also for the people.(M)
Are you forsaking yourself for others sake?(Souma)
Isnt this a natural obligation for those who have received a higher honor than other people? Does Your Majesty Souma as a Great King not understand this?(M)
.........(Souma)
 To live according to other peoples expectations, I think, is an extremely magnificent and wonderful way of living. I am willing to devote myself for the sake of those who revere me as a Holy Maiden.(M)

For those who revere her as a Holy Maiden? When I saw Marys smile who believed that it is wonderful to live up to other peoples expectation, the word of the other Holy Maiden came into my mind.

Although my station is that of an Empress who governs over the Empire, in the end, I am just a human. Rather than being worshiped as a Holy Maiden, I want to be loved by other people, as a human, as a person.

One Holy Maiden takes pride to be a Holy Maiden and behaved as such.
The other Holy Maiden refuses to become a Holy Maiden and tries to be a mere person until the end.

Does Your Majesty Souma as a Great King not understand this?

 I



There are times when I had the same thinking as Mary.(Souma)

I informed my partners as if I was confessing in a church.

Perhaps Carla remembered? Our conversation during the battle with Amidonian Army.(Souma)
That time?(Carla)

Carla who was standing in the corner of the room muttered and I nodded.

I was suffering from the magnitude of responsibilities because I am merely human. As such, I was worried about the decisions I had made. Actually, I didnt want enact them, but I was obligated to. Being forced by the war at that time, I unconsciously behaved like a machine A system called a King. After all, a machine would neither suffer nor worry. (Souma)
Souma(Liscia)

I smiled wryly and shook my head to Liscia who looked worried.

At that time, thanks to Carla who pointed out to me that Wont you become broken?, I realized how warped Id become on the inside. I was barely able to stop it from progressing further However perhaps if at that time, Carla didnt point this out Just thinking about it makes me shiver. Perhaps, I may have also ended up like Mary.(Souma)

When I thought what if I, who became a system called King, stand up in front Liscia and others I was afraid. Will I, who will throw everything away for the Kingdom, be able to notice Liscia and others tears? Will I able to make Liscia and others smile?

I want to be loved by other people, as a human, as a person.
.
(Yes You are right, Mariadono)(Souma)

If I dont notice Liscia and others tears,
If I cant make Liscia and others smile,
Even if I was suffering from the heavy responsibility, even if worried about my decision,
I dont want to become just a machine, a system.

Yes, I am fine as a mere person.(Souma)
Souma?  Hyah.(Liscia)

I stood up, walked to Liscias side and hugged her.

Ano Souma? Can you release me? This is, you know embarrassing, in front of everyone(Liscia)

Liscia raised an objection, but I ignored it. If she really disliked this, with her strength, she could easily thrust me away. While still hugging Liscia, I spoke to Hakuya.

I will not become a Holy King. Furthermore, I wont let the Orthodoxy State as they please. However, all the plans that Ive thought up can only stall them and not be a permanent solution. The Lunaria Orthodoxy believers inside the Kingdom are bound to become a problem. I hope that they can be rendered powerless, or perhaps, become harmless.(Souma)
Wait, why are you having a serious discussion in this manner!?(Liscia)
Then, please leave the countermeasure to me. I have an idea for it. Therefore, please lend me Kagetoradono and the Black Cats Squad.(Hakuya)
You too, Hakuyadono? Why are you replying to him!?(Liscia)
I understand. Then lets work it out tomorrow.(Souma)
Thy will be done, Your Majesty.(Hakuya)
Am I being ignored!? Are you ignoring me!?(Liscia)
Yes, Im depending on you, Hakuya. Then,(Souma)

While scratching my cheeks, this time I turned to face the girls.

I am sorry, but please leave me with Liscia alone for tonight.(Souma)
!?(Aisha)(Juna)(Roroa)

Aisha, Juna, and Roroa were opening their eyes wide from surprise at my request. Liscia who had been protesting from the beginning,

open mouthclose mouth open mouthclose mouth.(Liscia)

There were no words coming out from her mouth anymore despite opening and closing like goldfish. To think Liscia, who was always gallant and confident, would make a face like this was quite fascinating.

DaDarling. That means(Roroa)
Roroasan.(Juna)

Roroa who was trying to ask me was stopped by Junasan who placed her hand on Roroas shoulder, whispering something to both Roroa and Aisha. She, then, turned to me and bowed with her usual elegant manner.

Then, Your Majesty, Lisciasama. We will excuse ourselves from this room.(Juna)

After saying this, Junasan gracefully left the room.

Etto Please have a good rest, Your Majesty, Princess Lisciasama. (Aisha)
Muuu Cianee! Tell me what happened later kay!(Roroa)

After that, Aisha and Roroa followed Junasan.

Until tomorrow. Please have a good rest.(Hakuya)
Ssince Ill stand outside the room as the guard Mmy Lord and Lady, take your time.(Carla)

Finally, Hakuya and Carla also exited, leaving only me and Liscia in the room. I lifted the completely stiffened Liscia into my arms.

(She is so light)(Souma)

She had some muscle, but overall her body was lean, so I was able to comfortably lift her up by our weight difference. When I put her down in the small bed that was always placed in the corner of the room, Liscia finally regained her senses.

We sat side by side on the bed, under the candlesticks light.

Aano, Souma? Is this that thing, right?(Liscia)

When the faceflushed Liscia asked me like that, I felt my cheeks warm up, too.

Ah Yeah. I intend to do so(Souma)
II see(Liscia)
I cant?(Souma)
No! You can! After all, I have been waiting for this(Liscia)

Liscia quickly shook her head. Her words became weak at the end, though.

Bbut, why this sudden? Havent you been hesitating all this time?(Liscia)
Ah, yeah. Well I had thought that after the country has become more stable, after I am able to take the responsibility, or after I took a proper steps(Souma)

Aw, geez. This is so embarrassing! I scratched my head.

However After seeing Mary and realizing that I want to be a mere person I couldnt hold back anymore.(Souma)
II see(Liscia)

There  was also some influence from Mariadonos words I want to be loved by other people, as a human, as a person. I want to love Liscia and the others as a person. I want to be loved by the Liscia and the others as a person. When I thought deeply about this, I couldnt hold back anymore Well, as expected, it would be untactful to say, I am deeply impressed by the words of another woman in this scene.

Both of us were slightly embarrassed and we began to excitedly take off our clothes while avoiding eye contact.

But, well for the First Time, an office room is not that romantic.(Liscia)

Liscia, who was folding her undressed outerwear so it wouldnt wrinkled, said this while being bashful.

I also removed my outerwear and hugged Liscia who was only wearing white underwear.

The one whos trembling. Is it me? Or Liscia?  Or perhaps both of us?

Because both of us were inexperienced, we embraced each other awkwardly.

For now, after we exchanged a kiss,

Then, should we change the location?(Souma)

I teasingly whispered into Liscias ear.

Then Liscia made a wide smile while slowly shaking her head,.

No. This place will do. After all this(Liscia)

Is the same place where you and I met.
',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(10,'chapter 5','
I was awakened by the light flowing through the window and in front of my eyes was Liscias face. Because we shared the one pillow together, she was really close. A gentle sleeping face. Every time Liscia breathed in her sleep, her fairskinned chest, that was wrapped in a blanket, moved up and down. Just by looking at this scene, an indescribable mixture emotions of joy, embarrassment, and love surfaced from my heart that made me gently placed my arms on her cheeks. Then,

Muu(Liscia)

Liscia twisted her body as if she felt ticklish and slowly opened her eyes. Perhaps she was still halfasleep. She looked around restlessly, then when she noticed me lying beside her

Ah Souma. Good morning(Liscia)

She greeted with a wide smile. Since her action was unbearably cute, I hugged Liscia and kissed her still closed left eyelid. The halfasleep Liscia laughed ticklishly.

Geez, Souma What are you doing?(Liscia)
Hmmm, I want to see more, but Sorry, Liscia, please wake up.(Souma)
Eh?(Liscia)

After gently shaking her body, this time Liscias eyes fully opened. And when our eyes met, as if accompanied by a Poof sound, Liscia face became red and she pulled the blanket until her nose and hid behind it. Perhaps, the shyness was suddenly welling up in her. Surely that includes what happened the last night. I patted Liscias head.

Good morning, Liscia.(Souma)
Gogood morning. UuuuPlease dont look at me too much.(Liscia)
Even if you say to not looking at you, yesterday I have comple Hoph.(Souma)
Even so embarrassing is still embarrassing!(Liscia)

Liscia press a pillow on my face. After I put away the pillow, I stretched my hand and body.

Ugghmmmm I dont want our second time to be in the office.(Souma)
Why?(Liscia)
Since its my workplace, I have to order for the bed to be tidied early in the morning. As much as I still want to make out, I have to get up. Its a torture.(Souma)
II see(Liscia)

I got off from the bed and began putting on the clothes that I was wearing yesterday. Since I didnt bring a change of clothes, I had to return to my bedroom. After I finished putting on the clothes, I asked Liscia.

Umm Is your body feeling alright?(Souma)
Yyeah a bit sluggish, though(Liscia)
I see Then I will ask the maids to tidy up the bed.(Souma)
Yes, please.(Liscia)

After I kissed Liscias forehead, I left the office room. When I opened the door, I found Carla, who was averting her flushed face away and Serina, who was smiling gently, standing there. Ignoring Carla who was standing outside as a guard, why was Serina here? Serina then spoke with a gentle smile the likes of which I have never seen before.

Good morning, sir. Did you enjoy last night?1(Serina)

Whoaa. Although it was a template question, when she actually said it, it was painful to hear.

 You wake early, Chief Maid.(Souma)
Because I am also the Princess personal maid.(Serina)

Uttering something that couldnt be judged as an answer, Serina bowed her head with a refined gesture. I wanted to say, Yeah, you just think that this will be absolutely amusing., but if I pointed this to her, I had a feeling that she would respond with knifestabbing words three times sharper, so I kept silent. An undisturbed SuperS2 Maid wont torment me. If we ignored her bad habit hobby of teasing cute girls, she is an extremely capable person.

 I leave Liscia and the room to you.(Souma)
I understand, Lets go, Carla.(Serina)
Yes, Chief Maid.(Carla)

After giving a bow, Serina and Carla entered the office. After a while, I could hear voices coming from inside,

SeSerina! I am still naked!(Liscia)
Since I have to tidy the bed, please get off at once. Otherwise, I will have to ask Carlasan to take you and the bed outside.(Serina)
Wait Carla! Dont lift up the bed!(Liscia)
Sorry. But if I resist the Chief Maid, I will suffer embarrassing things at her hands afterward(Carla)
Kyaaaaaaaaa! (Liscia)

 Yeah, Liscia. Please stay strong.

Now then(Souma)

Lets change the mood. My resolution is settled. My willpower is plenty. If I want to protect my beloved family, whether a Major Power or a Religious State, let me show them whos boss.

Shall I devise a sinister plan or two with Hakuya?(Souma)

I started walking down the corridor.



A few days later.

I informed the Holy Maiden Mary who was staying on the Lunaria Orthodoxy Church in the Capital City about a second audience and summoned her to the castle. Thus, in the Audience Room within the Royal Castle, having the same members and arrangement as the last audience several days ago, Mary and I faced each other.

Mary was still as beautiful as when I saw her for the first time several days ago and still felt artificial in some respects. After we exchanged formal greetings, I decided to immediately cut to the chase of the real issue.

Now then, about the issue of recognition of Lunaria Orthodoxy as the State Religion(Souma)
(Mary)
If you could accept several conditions, I could accept it.(Souma)
Severalconditions?(Mary)
What? It is not too difficult. Furthermore, it is only two. First, I wont become Lunaria Orthodoxys Holy King. I want your side to also stop nominating me without permission. I want a firm promise on this matter.(Souma)
May I ask why? If you become the Holy King, then will you not rise above all Lunaria Orthodoxy believers in every country?(Mary)

The puzzled Mary slightly tilted her head to the side, so I slowly shook my head.

Because I am not a Lunaria Orthodoxy believer. If an unbeliever like me is suddenly appointed as the Holy King, it will only provoke a backlash from the believers. So I will respectfully decline that honor.(Souma)
I see.(Mary)

Although Mary seemed to be disappointed, she promptly backed down. Of course, this was no more than an official facade. I dont want to become a Holy King and put this country back into an era when religion constrained science. The objective of this condition was to prevent the Orthodox State from arbitrarily appointing me as the Holy King without my permission and turn me into their figurehead against the Empire.

Then, for the second condition Ill have the Prime Minister Hakuya to explain it.(Souma)

After I declared so, Hakuya bowed and moved a step forward.

Then, let me explain. The other condition that we would like to present is that we wish to invite a bishop from the Lunaria Orthodoxy State who will oversee the affairs regarding Lunaria Orthodoxy believers inside the Kingdom.(Hakuya)
Of course, we will accept this condition. I will humbly accept that position.(Mary)

Hakuya waved his hand to Mary who was holding her hand in her chest.

Please, that couldnt possibly be accepted. We dont wish to trouble Holy Maidendono with this position. What we intend to do is to invite a certain person to our country.(Hakuya)
There is someone you want to invite? This might be rude, but may I know who this person is?(Mary)
Bishop Soug Lester.(Hakuya)
(Mary)

The moment she heard Hakuya mentioned Soug Lester, Marys slightly narrowed her brow. I saw it only for a few seconds, but she was obviously felt a hatred. It was the first time the dolllike Mary showed a humanlike expression. Mary then asked with a slightly stiffened expression.

Hakuyadono Do you know what kind of person that man is?(Mary)
Yes. I have heard that he is a very sharpminded person.(Hakuya)
He is only cunning. That man is a fallen sinful bishop. He is an insolent man who swindled large amounts of money from various believers, drank wine, and philandering with women. While a priest should be abstaining from the pleasure of the world, that person is tainted by worldliness and acts without selfconstraint. Even His Holiness the Pope and the Cardinals also view him as a problem. Even for me I dont like that man.(Mary)

It was a clear rejection. So he is a man who even disliked by the dolllike girl. I became a bit interested in him.

How come a man like him became a Bishop?(Souma)

When I posed this question, Mary pursed her lips briefly, after which she opened her mouth to speak up.

 Although I am embarrassed to say this, we who are adorned with the robe of the priesthood are supported by donations from the believers. Since Soug had gathered a substantial amount of money through numerous methods, there are Cardinals who are nominating him as a Bishop(Mary)

Ah, I could vaguely see it. Perhaps, that Soug guy had bought off some of the Cardinals. Excepting the Holy Maiden, it seemed that the top hierarchy of the Orthodoxy State, rather than just smelled like worldly men, they felt more corrupt and smelled fishy instead. Thats why even if they want to remove him, they couldnt. Mary turned towards Hakuya.

However, I heard that even in the Church there are rumors that the Church ought to expel him soon. I believe that it is simply a matter of time until he is excommunicated. So why would you invite a man like that?(Mary)

Despite the opposing partys glare implying major objection and disagreement, Hakuyas calm smile stood unbroken.

Isnt this a good opportunity? If he will be expelled, then lets have our nation take care of him. His Majesty Souma likes unparalleled talented people and has commanded that if there is such a Bishop like that, then His Majesty would like to meet him.(Hakuya)

Really? But no, I dont ever remember saying that. This whole plot was something that Hakuya had fabricated on his own. After all, I was not even aware of Sougs existence until now. Mary directed her eyes full of dissatisfaction at Hakuya.

To dispatch him as a Bishop means that he will stand at the top of the Lunaria Orthodoxy believers who reside in this country, wont it? I do not believe that he is fit for that position.(Mary)
If that is really the case, then it will merely be an issue of having another person replace him. If he is a person who doesnt measure up to our expectation, then we wont mind, dealing with him ourselves.(Hakuya)

Whoaa My my, Hakuya, you have a really terrifying evil smile. Since in the first place, he has a cleverlooking face, this should appropriately be called as a Freezing Smile3. Mary had nothing more to say, thanks to his impressive presence.

 That will be fine. If by any chance, he cant come to this country, then I will be replacing him instead.(Mary)
Yes. We will be depending on you at that time.(Hakuya)

These two had already maneuvered around and fathomed on each other intention. A violent confrontation between a poker face and an evil face, rather than clashing sparks, the surroundings were wrapped in a freezing atmosphere. Even Liscia and I were repelled by their mood. Aisha, who also wasnt used to this, felt sick.

At any rate, with this we could assume that the negotiation had concluded. To summarize the details that we decided this time, it would be these three points.

         Friedonia Kingdom accepted Lunaria Orthodoxy as a State Religion
         Lunaria Orthodoxy State will not appoint Souma as the King Holy.
         The Orthodoxy State will have to dispatch Soug Lester as a Bishop.

As soon as the discussion finished, Mary left the audience room. They could not trick me into becoming a Holy King, but since they were able to make the Kingdom recognize Lunaria Orthodoxy as the State Religion, this could be said to be an adequate result. With this, she should return to the Orthodoxy State peacefully. I waited until the report that she had left the castle to finally release the nervousness wrapping me.

Fuu With this, perhaps we could succeed?(Souma)
Most likely, I dare say. The Holy Maidendono too should be satisfied that she has achieved some results.(Hakuya)

I shrugged my shoulders when Hakuya grinned.

Hakuya, you still have your evil face.(Souma)
 Pardon me.(Hakuya)

When Hakuya returned to his usual calm face, I asked.

Then, tell me, have you made any preparations to invite a bishop like that Soug fellow?(Souma)
Yes, Your Majesty. The Black Cat Squad led by Kagetoradono has already infiltrated the Orthodoxy State. Perhaps they already brought Sougdono to the border. I am confident that they will escort him into our country in the next few days.(Hakuya)

That was an extreme cautionary measure. It was almost as if they were acting like bodyguards.

Is there a special reason to depute the Black Cat Squad as his bodyguards?(Souma)
By seeing the Holy Maidendonos reaction, they would hesitate to dispatch a person who would become a disgrace to the country as the Bishop. The sHoly Maidendono also accepted that conditions only so that Lunaria Orthodoxy will be recognized as a State Religion. However, even after we have their guarantee, there is a risk that they will annul it because of a reason.  For example, they may claim that Sougdono is injured in an accident, so they would have to dispatch another person. For this reason, I had to send the Black Cat Squad beforehand to accompany him.(Hakuya)

Injured in an accident yeah, right.. This was just my own mental image of them, but this is something that I believe the Orthodoxy State would do. If it would only just stop at mere injuries, it would still be fine, but nothing prevented them from things like, imprisonment (read it as His whereabouts are unknown) or in the worst case, assassination (read it as He passed away from an illness.) To think that far ahead and anticipate it, as expected from Hakuya.

It looks like the Kingdoms celebrated Black Robed Prime Minister title is not just for show. After all, your serpentine deviousness had taken it that far. (Souma)

I poked fun at Hakuya, when Liscia, who stood next to me, sent an unamused look at me.

Speaking of serpentine deviousness, wouldnt it suit Souma more? After all, you have deceived that pure and innocent HolyMaidensama.(Liscia)
I wish you dont spoke like I am the villain here. There were no lies in my words to her, you know?(Souma)

My words about recognizing Lunaria Orthodoxy as a State Religion is not a lie. However.
I grinned.

I only changed the meaning of the phrase State Religion.(Souma)

',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(10,'chapter 6','
A few days later.

The Royal Castle of the Kingdom of Friedonia announced that all faiths and religions were to be registered for certification process with the country, with all certified religions to be officially recognized as State Religions. In a nutshell, they had changed the meaning of State Religion within the Kingdom to simply means a Religious Organization.

It had been a long time since King Souma showed himself via the Royal Voice Broadcast to address the Citizens.

Until this day, in this country, every person, every household, every race, had decided for themselves which God they would worship. Other than the Mother Dragon Faith and Lunaria Orthodoxy, that have a large number of devotees, there are those such as the Dark Elves of the GodProtected Forest who worship the Divine Beast and others who worship the nature gods of the sea and the mountains. This is a very normal occurrence because every race and tribe has different living environments.

The people who lived in the inland cities, villages on the mountains, or towns on the beach, nodded at Soumas words. Because their living environment was different, it was natural that there would be difference among the things they feared and the entities they revered. Soumas reflection in the Broadcast continued his speech.

This country is a multiracial country. It is a country where the customs of various races melt and mix together to form a new culture every day. I trust that this situation also ought to apply in matter related to faith. I believe what this country needs is not a single religion for everyone, but harmony brought forth by allowing religious freedom for every faith. Just like how you have a being you believe in, other people also have beings they believe in. I wish for you, the Citizens, to acknowledge this fact and be tolerant. If you do this, then others will also be tolerant of your faith.

The citizens were half in doubt over this speech. In this country, where the mass media had not yet developed, information on other religions was limited. In other words, they didnt know what was going on in a church or temple of a different religion. Since they dont know, they were suspicious. A doubting heart would conjure up demons out of the shadows in the dark and saw withered flowers as ghosts. Even though the gathering was just a coven of Mountain worshippers, if the Citizen were suspicious of them, that coven would be seen as an evil secret society. King Souma also seemed to understand this very well.

What is necessary to gain that tolerance? It is mutual understanding. No matter how much you are tolerant to other religions, you wouldnt accept an evil religion with practices like Sacrifice a virgin to summon a demon. I will never even ask you to tolerate such a religion. However, it will be hard to see from the outside whether someones religion is a good one or an evil one.

And so King Souma said this vigorously, Hence, the current State Religious certification system.

I wish for the representatives of all faith groups to register their religion with the government. As long as they pass the evaluation process,  which will be repeated every few years, their religion will be recognized as a State Religion. The evaluation is also simple. They would only have to pledge that there will be no illegal activity under the law conducted in the name of the religion, such as killing, stealing properties, or perpetrating sexual assault, amongst other things.

Although it is basically prohibited to cause injury to other people, there might be faiths which may involve selfharming. Please confirm with the responsible officer in each district for details, such as whether or not tattooing is considered as injuring others. A religion that cannot pledge these provisions and isnt registered will be subject to government control and scrutiny. The Citizen too, will not be able to sleep in peace, if believers of a dangerous faith reside nearby. I hope that everyone can understand this point.

King Souma spoke until that point when he stopped for short pause after that, he made continued a speech directly addressing each and every Citizen.

My final word is I believe that a faith should be for the sake of those who are living right now. So that they are not dragged by sorrow, so that they could still give their best for today, so that they can properly live as a person, these are the reason why faith exists. My utmost desire is that no one will be hurt because of faith.

My utmost desire is that no one will be hurt because of faith.

Perhaps this was what the King Souma wanted to say the most. After the appearance of the Demon Lord Dominion, the Citizen couldnt cast out the uneasiness in their hearts. In this era, there was a revitalization of faiths as the mental support for the Citizens hearts. And when faiths were revitalized, religious conflict and sectarian conflict would follow. A faith that should serve as a mental support for the heart would also end up as a means to hurt fellow Citizen. The Citizens presumed that King Souma was grieving about this. Then King Souma made a single clap.

Now then, lets end the heavy formal talk here. When all registered religions have become State Religion, we plan to hold a small event. I will leave the explanation to Roroa.
HaiHai Leave this to me

King Souma stepped aside and a lovely braided girl beside him moved to the center.

Former Ducal Princess of Amidoania, currently Soumas Third Fiance Candidate, Roroa here. Everyone in the Kingdom of Friedonia, how yall doing?

At the sight of Roroas bottomless cheeriness, the Citizens jaw dropped. The serious atmosphere of the broadcast before had disappeared as if it never happened. King Souma was also amazed at her.

Roroa, are you going to continue with that mood?
Isnt this fine, Darling? After all, Im also casting in Junane...... Oneesans show Together with Oneesan, so my character is already established.
You forcefully pushed yourself into the casting, though

While putting a wornout expression, Souma dropped his shoulder. Although their interaction was more of an older brother who was dominated by the imouto rather than between fiance, the Citizens who watched them had a warm and fluffy feeling. Then Roroa faced the screen and spoke out while placing both of her hands on her waists.

Well, from now on, all of the registered faith wills be designated as State Religion, but there is a small request for when you are registering it. I hope that you will tell us if there are any interesting events from the religious festival of the god you believe in. In the world which Darling came from, religious events eventually became national festivals, since such events are a source of joy and merriment regardless of whether or not you are a believer or nonbeliever, I believe that everyone in this country to will have a great time!

The Citizens raised a cheer. They didnt really understand what Roroa was saying, but it seemed that they responded to the word Festival. Perhaps their feelings were roused by the mood that something fun would happen. By the way, not long after that, Roroa said, Furthermore, if there is a festival, there will be money moving around, uhauhaha, but since her voice was in small whisper, only Souma heard it why smiling wryly beside her.
Roroa winked at the screen.

Well since it is difficult to understand just by words, perhaps it would be better to show a specific example. For that reason, Sougocchan, could you come here and explain?
Hey, wait a minute, Roroajouchan. You shouldnt call me occhan

The person, who appeared in the broadcast while saying this line, was a 40something male of human race with a quite burly body figure. While stroking his smooth head that was tanned like his skin, the man had a wry smile while speaking.

Even if Im look like this, this ojisan is still a Bishop of Lunaria Orthodoxy, you know.

Just as he had claimed, the man was wearing a priests uniform(?) of Lunaria Orthodoxy. The reason why there is a ? mark is because his priest vestments are remodeled  which is too say it was so remodeled that its shape was already resembled a makaizou figure.1 The long sleeves were cut halfway, and both of the trousers and his robe were only kneelength. Since this style of clothing were worn by a burly suntanned man, it didnt look like a priest vestment at all. Roroa raised a protest at that man.

Occhan is occhan. Furthermore, to call me, a Third Queen Consort Candidate, as jouchan, I believe that Octopusocchan is also suitable call name for you.

To this flaming response, the man also returned tit for tat.

Dont attach an octopus there! You too, Jouchan, if you will soon become a married woman, then try to master even a single erotic trait that you can exude out.2
What are you saying. Ahan

Seeing that Roroa made a sudden coquettish gesture, the man did a muffled puph before bursting out a laugh. Since her sexy pose (?) were being laughed at, Roroa entered the angry mode, so King Souma hastily restrained her from behind.

Release me, Darling. I must boil that octopus right now!
Calm down Roroa. I think that whatever you do is cute.

While saying this, King Souma patted her head. Roroa then curled at King Soumas neck.

 Really?
Yeah.
Nnn Then, Ill let him go.

Are you fine with just that!, was what the Citizens thought as a reaction. The citizens, who still werent aware about this couples manzai3, didnt understand what they had witnessed, but seeing how they reconciled for some reason, they felt relieved. King Souma made a wry smile and spoke to the man with the remodeled priest vestment.

Well then, Sougdono, Ill leave the explanation to you.
I understand. King Souma.

After replying, the man named Soug moved forward.

How do you do, the Citizens of the Kingdom of Friedonia. This humble servant is Soug Lester. I came from the Lunaria Orthodoxy Church and was dispatched as a Bishop to administer the Lunaria Orthodoxy believers in the Kingdom. Well, lets not bother with the ceremonious titles, even so, though its slightly earlier than others, Lunaria Orthodoxy had been recognized as a State Religion. Furthermore, it seemed that the Mother Dragon Faith , and through the Second Queen Candidate, Aishadono, the Divine Beast Faith, which was followed by the Dark Elves of the GodProtected Forest, also had been registered. So, the other religions or sects should register as soon as possible.

Most of the crowd was surprised at Soug who gave his speech with an easygoing mood. Especially, for the Lunaria Orthodoxy believers. The Bishop. who stood in a position to administer the Orthodoxy believers within the Kingdom, made a statement about recognizing other faiths. As they lived in a multiracial country, the Orthodoxy believers within the Kingdom disliked a conflict with other races, so most of them was in the moderate faction camp. However, as expected, they were hesitated to be actively involved with people who believe other faith. So Sougs statement this time had removed their hesitation away.

(Ah. So it will be fine if we be friends with others)

The Orthodoxy believers within the Kingdom would have similar thoughts. Soug continued speaking.

Then, I think that the Lunaria Orthodoxy believers already know about this, but soon it will be the time for the Spring Arrival Festival.

Spring Arrival Festival was a festive event of the Lunaria Orthodoxy, a festival to celebrate the passing of winter and the arrival of spring. In this festival, the children from the Orthodoxy would dress like fairies and will walk around handing flowers in their basket to the adults. In other words, the children would become the Spring Herald. Adults would give this Spring Herald with sweets in exchange for the flowers and then pray for this year to have a good harvest. In other words, this festival was like a spring version of Halloween.

Rejoice since this Spring Arrival Festival has been recognized by Roroajouchan as a national festival event. This might be sudden, but this festival will be held next weekend. I have already sent a notification to villages and towns with Orthodoxy Churches. It is free for anyone to partake in, whether or not they are Lunaria Orthodoxy believers and the children will receive sweets regardless. I also want the other adults from other religion to cooperate if you can. You only have to accept the flowers and give sweets, if a child comes. Easy, right? Since I think this is a festival that the adults can enjoy too, please participate actively in it.

There was applause from the audience. They might have thought that this is fun. Ever since Souma made broadcast programs, there was a trend amongst the Kingdoms Citizens to take pleasure in an event like this. Then Soug stepped down and Roroa appeared once more.

This time, we will celebrate an event from Lunaria Orthodoxy, but for the other faiths too, if you have a fun festival like this then please tell us. The various religious festivals will form this countrys Citizen Festival. I believe this is a wonderful thing. Whether you are believers or unbelievers, lets have the whole country enjoy the festival!

When Roroa raised her fist, the Citizens cheered.



Continental Calendar, 1547, End of Thirdmonth

On this day, I went down to the city with Aisha and Tomoe.

Wwe brought the grace of spring desu!(Tomoe)

Tomoe, who was wearing a White Magestyle robe with fairy wings attached on the back, held out a flower to an vendor obaachan of a street stall. The obaachan smiled widely,

Oh my, my. What a very cute fairysan(Obaasan)

While saying this, she received the flower from Tomoe, and then gave her a bag with sweets inside. Tomoe who received the sweets lowered her head and thanked the obaachan, then she trotted over to us and showed the bag with candy.

Niisama, I got this!(Tomoe)
Yes, I saw her giving it to you. Good for you.(Souma)
Yeah!(Tomoe)

When I patted her head, Tomoechan was wagging her tail. Looking at Tomoechans action, Aishas expression also became softened.

Aw. Tomoedono is as cute as always.(Aisha)
Yes. Aishaneesama, lets eat the candy together.(Tomoe)
Can I!? I love you, Tomoedono  (Aisha)
Hya!(Tomoe)


Aisha hugged Tomoechan. It seemed that even though Aisha wasnt from the Faewolf Tribe4, it was as if she had a tail and now wagging it. Or rather, why are you being fed by an 11year old girl? While I was astonished at Aishas action,

Oh, isnt the one standing there is the King?(Soug)

Someone suddenly called out to me. When I turned to look back, there was Soug drinking wine at an open seat in the bar. It seemed that he had already finished drinking the large wooden mug in his hand. I wondered whether this guy, who drinks in broad daylight, really was a clergyman.

 O sir clergyman, isnt it still noontime?(Souma)
Its a festival, after all. Please dont say something such so formal, o sir politician. So you are with Aishajou and Tomoejouchan today?(Soug)
Yes Well, its like spending time with the family.(Souma)

I mustnt only give preferential treatment to Liscia. Although I already had an intercourse with Liscia, I had not yet laid my hand to my other fiances. It was to avoid situations where the out of sequence birth order of children could develop into a throne succession problem.5 Especially for Roroa who is in a delicate position as the former enemy countrys Ducal Princess. For her sake and for the children who will be born from her in the future, I must not lay my hands on her for now.

There wont be any problem for Junasan as a Royal Consort Candidate, so any child born from her wont inherit a right of succession, and for Aisha, since as a longlived race, it was hard for her to be pregnant with a child in the first place. But both of them are being considerate to Roroa, so they are waiting. As a result, Liscia, who was requested by the other Fiance Candidates, Please, quickly bear a child, complained that,  The pressure made my stomach hurt.

In any case, since I cant lay my hands on them for now, I need to pay attention to them in other aspects. After listening to my story,

Han. Its hard to have a household at such young age, eh?(Soug)

Soug said like it was none of his concern and drunk the wine in his mug as if to flaunt it at me.

Puha(Soug)
Arent you drinking too much?(Souma)
In the Lunaria Orthodoxy, wine is said to be sacred. In other words, I by pouring this liquid into my body, I will accumulate blessings.(Soug)
That is just an excuse for drinking. You are really a poor sort of a priest.(Souma)
But, isnt it convenient for the King that I am such an irresponsible one?(Soug)

Soug grinned He sure knows how to answer, this octopus old man. I shrugged my shoulders.

Oh well, it seems that Hakuya chose you because of that reason.(Souma)
Well, I will properly fulfill my role, Your Majesty.(Soug)
I hope so. Well then, I will now go back to Aisha and Tomoechan.(Souma)
I pray to God, may peace be with you and your family.(Soug)

Hearing the words of prayer with questionable seriousness from the bishop, I returned to Aisha and Tomoechan.



',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(10,'chapter 7','
 Continental Calendar Year 1547, Early Thirdmonth

On that day, a single man was being interrogated at the center of the Orthodoxy faith, Orthodoxy Central Church, which was located in the Holy City Yumuen in the Lunaria Orthodoxy State. The man was standing while being surrounded by an Investigative Committee in a dark room. For the people related with the Lunaria Orthodoxy, to stand in front of the Investigative Committee was the same as being judged whether or not one is a sinner. If they sentenced an excommunication, that person will lose all protection and rights in this country. They will be driven off to a position where even if they were killed by someone, no complaint would be taken.

However, not a hint of nervousness could be seen in the mans face. Instead, he sported a fearless smile, as if he found this situation enjoyable. The mans name is Soug Lester. His occupation is a bishop. Seeing Soug like this, one of the Investigative Committee made a complaint.

Do you know the seriousness of being called to this place?(Investigator)

However, Soug didnt break his fearless smile.

Im aware. This place is a judgment room of the Orthodoxy Church, whether or not someone needs to be excommunicated. Anyone, no matter who they are, who have committed sinful acts should be trembling in this place. However, since there I have nothing to be ashamed of, my mind is calm.(Soug)

Sorge was polite with his wording, but his attitude was as if he was scoffing at the committee.

I see that you are really brazen(Investigator)

With a sour face as if he just gulped down a bitter bug, the committee members looked over the documents on their hand.

Your sinful behavior is intolerable. Lunariasama had taught If thou are a clergyman, then thou shall assume honorable poverty, lest not the believers turneth away their ears.However, havent you done the opposite of this teaching?(Investigator)
Oh my, dont I look like someone who is in an honorable poverty?(Soug)

That being said, Soug stretched out the Makaizoumodified priests vestment, which he wore on his body.

I am wearing these shabby clothes and living in a dilapidated house in the citys outskirt. However, you gentlemen in this place, wear a very highgrade clothing, and live in a lovely house, am I right?(Soug)
 However, dont you receive a large amount of donation from the believers?(Investigator)

Again the Investigative Committee argued and Soug shrugged his shoulders.

There is no reason to refuse their gifts.(Soug)
What an excuse! What do you take the believers gratuity for!(Investigator)
If the donations that they gave are only for fattening my belly, then it will be a sin. But just look at my belly, isnt it is ripped into a beautiful sixpack like this?(Soug)

While saying this, Soug lifted up his coat to show off his welldefined abdominal muscles. For a moment, the members of the Investigative Committee were dumbfounded at Soug as he made a pose while showing off his muscular body, which didnt suit a priest at all, but immediately their face reddened from anger.

That is irrelevant to this discussion!(Investigator)

However, the Investigative Committee anger had no effect on Soug at all.

Perhaps. If it is a fat gentleman you seek, you can find him amongst the Cardinals.(Soug)
(Investigator)

Soug gave a light scowl and the committee members were at loss of words for a moment. Speaking of a fat person amongst the Cardinals, then it would be Cardinal Gordo. He has a bulging obese figure unbecoming of a clergyman. In fact, the Investigative Committee members had already knew that most of the huge sum of donations received by Soug are flowing to Cardinal Gordo. In other words, Soug was implicitly saying, the one you should accuse is not me, but that man.

However, that man is a Cardinal. In the hierarchy of Lunaria Orthodoxy, it is the rank next after the Pope. It was beyond the authority of the Investigative Committee. In order to accuse him, an approval from the Pope or a unanimous vote from other Cardinals is necessary. However, since the Pope could be forced to abdicate by a unanimous vote by the other Cardinals, neither the Pope nor the Cardinals would make the first move for censuring a Cardinal to avoid becoming the next victim. This was the reason why Cardinal Gordo, who has a poor reputation, still remained in his position.

The Investigative Committee had the intention to accuse Soug and also to press a censure request for Cardinal Gordo, who have a connection with him, to the Pope, However, that Soug fellow was under the protection of Cardinal Gordo. If the Investigative Committee tried to force their way in judging Soug, then Cardinal Gordo might denunciate them for abusing their authority. Therefore, the likelihood for them to excommunicate Soug for the donation problem was zero from the start. That was Soug didnt lose his carefree attitude.

The Investigative Committee decided to change their brunt of arguments.

Besides that, there is a report that you are wandering from one tavern to another every night.(Investigator)
Is it a sin to drink wine? I believe wine is a sacred drink, isnt it?(Soug)
Even so, there should be a limit for it.(Investigator)
I am so sorry. But the flock who come to hear my sermon are mostly drunkards, and they come to a tavern, not the church.(Soug)

By the way, the sermon Soug gave on the tavern was a brief citation from the scripture, Lunariasama once said,Give your thanks that you are living today. So, a toast people!. This alone had the same virtuous merit as listening to the long boring sermon of the Priest in the Church on Restday (or so was what he decided by himself) and this was really popular with the lazy believers. While the interrogators got their nerves bulging from irritation, they once again changed their point of attack.

Not only that! It seems that a prostitute oftentimes goes in and out of your house! Isnt this a disgraceful act for a clergyman!(Investigator)
Oh dear, do you want to say that a woman is unclean? If so, then how were you gentlemen born into this world?(Soug)
That is not the problem here! We ought to love and respect Lunariasama. Even in a wedding that follows legitimate procedure, a devout faithful will still feel guilty to Lunariasama, but to mingle with prostitutes, isnt this preposterous?(Investigator)

Although the Investigative Committee was completely angry, Soug laughed loudly.

Whats wrong about that? Lunariasama had said, A believer should be honest with others. It is normal as a living thing to want to hold a woman. Because Lunariasama had created this matters too. Between a married man who went to the flower town while hiding his position as a clergyman and me who openly beckons the prostitute, which of us is more honest?(Soug)

The Investigative Committee members were speechless. If one thinks calmly about it, both are bad, but since Soug declared it so openly and brazenly, they couldnt refute him. Afterward, Soug continuously emphasized the beauty of the female curves to shut the Investigative Committee members mouth. Since the content of his speech didnt have any relation at all to the censure, the situation inside the room didnt resemble an investigation anymore.

Eventually unable to do anything about Soug, the Investigative Committee ended the interrogation. Soug left the room and walked in the corridor of the Central Church and encountered a young nun. It was a dazzling beautiful girl with silver hair tied into twin tails

Soug was thinking about such thing while closely eyeing the girl. The silverhaired girl walked expressionlessly like a doll, but when she saw Soug, she furrowed her brows. And when they passed by each other, she turned her face away with a scornful humph and walked away in a brisk pace.

Looking at the girls reaction, Soug scratched his head with a bitter smile.

 Yareyare, it seems I am being hated.(Soug)

Soug is this kind of man. Though he had a slovenly lifestyle, he was unexpectedly popular with odd fellows and the elderly. On the other hand, young girls hated him and treated him like a pest.

(Well, a young girl like her wouldnt understand the adult charm that this ojiisan exudes.)

While stroking the beard on his chin, Soug made such poor loser kind of excuse in his mind.

  

 Continental Calendar Year 1547, Late Thirdmonth

Tonight, Soug was in a certain pleasure district in the Holy City Yumuen. Though this city could be said as the Mecca for Lunaria Orthodoxy, there are people who call the city their home, so a pleasure district like this also exists. As expected, it would be impossible to see the sight of clergymen in a pleasure district, but perhaps they slip into this place while in commoners attire. In this district, perhaps it was only Soug who loitered and played around with a (completely remodeled) priests vestment.

The reason why most of the donations received by Soug was flowing into Cardinal Gordos coffer was, honestly, to maintain this free way of living. It wasnt like Soug himself desires that large amount of money. However, in order to be free in this ceremoniously stiff city, he needed a powerful backer.

Yo, Sinful Bishopsama. Whats up?(Old Man)

As he walked down the street, an old drunkard who was drinking at the shopfront called up to him. Soug raised his hand towards the old man.

Hey gramps. Thanks for always donating(Soug)
What? I got no relatives, so if my money will be taken by the country after my death, then I better use it for my drinking friend.(Old Man)
Kakakka1. Well said. Lets have a drink next time.(Soug)

Just less than ten paces away from the old man, Soug was greeted by a well dressed old lady.

What we have here? So you havent been sacked yet? Perhaps Lunariasama is also wondering why a man like you became a Bishop(Old Lady)
So what? If we are talking about someone with a nasty tongue, then it will be you, granny. So you are still alive? Wont Lunariasama be wondering why you havent gone to her yet?.(Soug)
Humph! If I died before you, then you will be praying for me, right? If you prayed for me, then I will get lost in the Afterlife. I wont let that happen!(Old Lady)
Kakakka. Then I guess you have to do your best in staying alive for a long time!(Soug)

It was an amazing relay of abusing words, but the two people who exchanged those words were still lively. Perhaps because this was a conversation between those who are well acquainted with each other. Then, this time a sexy and beautiful lady wearing clothes with a high degree of exposure addressed Soug.

Wait Souchan. Lately, you havent come to play at all. The girls in the shop are lonely, you know?(Prostitute)
Oh, Ill visit next time.(Soug)
You are always saying that. Isnt it because of the rumored lover?(Prostitute)
Hah, as if. Do you think this head will attract a woman?(Soug)

While giving his reply, Soug tapped his shiny sunburned smooth head which caused laughter from the people on the spot. In the Central Church, Soug was a nuisance, but in the pleasure district, he is a popular person.

When Soug was walking in the pleasure district like this for some moment, he suddenly stopped. Then he turned around and looked towards the dark back alleys.

How about you stop this and show yourself. I have no interest in being followed by a man.(Soug)

A man clothed as a pilgrim with a dirtstained hooded mantle that covered his body appeared from the shadows. His face was hidden behind the hood, but he was probably a male Beastkin since a tapered nose was visible. The man made a polite bow to Soug without taking off his hood.

 When did you notice it?(Hood)
You have been wearing it since entering the pleasure district. I am a former adventurer in my party I was entrusted with looking for enemies. I am sensitive to the others presence.(Soug)
Why did someone like you become a Bishop?(Hood)
I was injured seriously when I made a blunder in a dungeon. That happened in this country, so since I was helping the old Bishop who had been taking care of me, it naturally happened. The fate that happened that day continued even after that gramps passed away.(Soug)
I see(Hood)

Then, the hooded man walked towards Soug. He was putting his hand in his pocket, trying to take something out. Soug put himself on guard, but when he looked at the object presented to him, he released his caution. What the man held in his hand was a bottle of expensive liquor.

I want you to join me in having a cup. Do you know a good place?(Hood)
 Kakka. If that is the case, then why dont we go to my house?(Soug)

And then, the two began to walk side by side.
',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(10,'chapter 8',' Sougs house was a dilapidated building located on the outskirts of Holy City Yumuen. When they entered the deserted dark room, the hooded mans nose was twitching.

This smell(Hooded Man)
Is it that smelly?(Soug)

Soug laughed Kakkakka, while lighting the candlestick.

Its because this is a home of a single man. Well, I hope you dont mind it(Soug)
No, I think that for a single man home, it is rather well ordered.(Hooded Man)

Actually, for a single mans house with a dilapidated exterior and considering Sougs easygoing personality, the room was really tidy. There were neither trash scattered around, nor dust piling up at the corner of the floor. More importantly, what made the hooded man more interested was.

Is this smell medicine?(Hooded Man)

When the hooded man pointed this out, Sougs laughter ceased. For a moment, he stared fixedly at the man before finally, he shrugged his shoulders as if he had resigned.

 It looks like you have a good nose.(Soug)
The Beastkins have a particularly keen sense of smell compared to the Humans, after all.(Hooded Man)

As he said that, the man removed his hood, and showed his wolflike appearance.

My name is Inugami. Pleased to make your acquaintance.1(Inugami)
Well, you can become my acquaintance or what not but, is that your real name?(Soug)
As you have guessed, it is in an alias.(Inugami)
Well, I guess it will be impossible for a guy who hid his identity and infiltrated this country to honestly give his real name.(Soug)

Despite saying these words, Soug had finished the preparations for drinking. He placed two glasses, bread, and cheese on the narrow table. With the desk interposing between them, they sat facetoface. After pouring the alcoholic drink he received from Inugami into the two glasses, Soug took a sip of it bit by bit and then asked.

So, which country do you come from?2(Soug)
Elfrieden Or, perhaps its better to call it the Kingdom of Friedonia now?(Inugami)
Oh, the country where the king is a hero from another world?(Soug)

Even though he was a bishop in an insular church and poorly uninformed of the worldly affairs, the rumors of the Hero King of the Elfrieden also reached Sougs ears. It was said that the throne was quickly transferred to him right after the summoning, rebuilding the slowly declining Elfrieden Kingdom, resolving the troubles both domestic and foreign, even annexing Amidonia Dukedom, the Kingdom became a force that even could compete with the Empire.

Just by listening to this, these acts could be said as great achievements in every field, but Soug didnt hear that this was the personal accomplishment of King Souma himself.  The rumor circulated in the foreign lands was about the activity of the people around him, like the SilverHaired Guards Commander and the RedHaired Assault Commander in the military, or the civil officials like the BlackRobed Prime Minister and the Food God Ishidzukasama. It would be different conclusion if the people of other lands could watch the Royal Voice Broadcast, but what the other country received about King Soumas achievement was just about his wisdom in appointing such talented retainers.

Thats why Soug wanted to discuss about this King.

It seems that the country itself is greatly prosperous, but what kind of person is the King?(Soug)
Lets see Even I dont know it well.(Inugami)
Wait, wait,  what do you mean you dont know?(Soug)
His combat skill is not different from a city dweller. Even if he trained, at best he will be at the level of a soldier. I also dont know about his generalship since he leaves it to his retainers. Nevertheless, he has quite the ingenuity which he showed in a field that is hard to be seen by the public, such as creating national institutions.(Inugami)
 Even so, its not different from what I heard before.(Soug)

When Soug made this remark, Inugami laughed.

However, the only thing I can say is that anyone who saw His Majesty Souma too lightly will be surely outmaneuvered by him. This is what I painfully experienced. Even if that person is the Lunaria Orthodoxys Holy Maiden.(Inugami)
The Holy Maiden? Have the top guys done something?(Soug)

Inugami then informed Soug about the events a few days ago. The Orthodoxy State had requested the Friedonia Kingdom to recognize Lunaria Orthodoxy as the State Religion. In exchange, King Souma will be ordained as the Holy King. Because there is a risk that they could incite the Lunaria Orthodoxy believers inside the Kingdom to rebel, it was difficult, even for the Kingdom, to refuse this request. In response to this situation, King Souma declined to assume the Holy King title and recognized Lunaria Orthodoxy as the State Religion.

And then, he made a notification towards every religion inside the Kingdom, Every religion that registered and passed the nations examination will be recognized as the State Religion, and placed every religious activity inside the Kingdom under his management. Soug was taken aback from the beginning to the end, but he finally laughed cheerfully.

Kakkakka, well done, well done! Its a splendid way to slip away from the top guys intention.(Soug)

Even though in a sense, his peers had been outwitted, Soug laughed with delight. And then, he gulped the entire glass of liquor at once, before putting the glass back and frowned.

Fumu However, I guess the top guys wont stay quiet. They are persistent bunches. Even if you stall them for now, after a while, wont they just come back with new tricks at hand?(Soug)
Yeah, His Majesty Souma was concerned about this. Therefore,(Inugami)

Then Inugami took out a letter from his pocket and presented it to Soug.

This is a letter addressed to you from Prime Minister Hakuya.(Inugami)
To me?(Soug)
I believe that you will understand once you read the letter. Prime Ministerdono hopes that you will consider to come to the Kingdom and become the Bishop who will manage the Lunaria Orthodoxy believers inside the Kingdom.(Inugami)
Me? Become the Bishop of the Kingdom Really?(Soug)

When Soug skimmed the contents of the letter, it was almost identical to Inugamis words, but written with more polite phrases. Soug placed the letter and then slammed at the table before he stood up.

I see. So you are intending to separate the Lunaria Orthodoxy believers from the Orthodoxy State by appointing the worldly me as the Bishop.(Soug)

If the Orthodoxy State decreed something to the Lunaria Orthodoxy believers in another country, that decree would reach the Bishop who was the representative of Lunaria Orthodoxy in that country first. Then the Bishop will convey that decree to each local church in his country. Thats why, if by some chance they could make this Bishop seat fall to someone with a low attachment to the State and the Church, then it will be possible to shut out any decree from the State.

 (I see What a wellplanned scheme)

Is this a plan from the King? Or perhaps, its the Prime Ministers plan?(Soug)
The one who planned to invite Sougdono is the Prime Minister.(Inugami)
I see. Both the master and the servant are shrewd No, the word should be unthinkably sly.(Soug)

Soug thought carefully with his arms folded.

(Certainly, for that condition, there is no one more suitable other than me. Furthermore, there is a rumor that Cardinal Gordo will be expulsed soon. If that greedy cardinal loses his position, then the fellows of the Investigative Committee would surely censure me at that time. In that case, I will have to pay the piper. Thats why the opportunity right now to brazenly go abroad wouldnt be a bad choice But )

However, there was one point that Sougs concerned about.

What would happen if I refuse that offer?(Soug)
I dont know if it will be a wise choice. We already conveyed to the leaders of the Orthodoxy State through the Holy Maiden, that we want Sougdono to receive the position. Surely the Orthodoxy leaders dont see Sougdono to become the representative of the Kingdoms Lunaria Orthodoxy believers as a desirable result(Inugami)
 You mean I am in danger, huh?(Soug)

Soug sighed at Inugamis opinion.

So, from the beginning, my choice doesnt matter, huh?(Soug)
I am sorry. But if you accept our offer, with our power we will safely escort you to the Kingdom without letting any harm befall you.(Inugami)
Thats not the problem here(Soug)

Soug briskly scratched his hairless head. And then he pondered about something before with a decided face, he looked straight at Inugamis eyes and spoke up to confirm something.

You said that King Souma is crazy about talented people, right?(Soug)
Crazy? That word is misleading. Isnt it normal for a ruler to desire excellently talented people?(Inugami)
Ah, sorry if that rubs you the wrong way. I am not doing that on purpose. I just want to confirm if King Souma will employ an excellent talented person for real. Even if for example, they are not from the Human Race?(Soug)

Being asked with a serious expression, Inugami thought for a moment.

Lets see. This is just my personal opinion, but I think the basis of His Majestys judgment is the presence of talent. He gathered various talented people, regardless of their race, age, gender, history, or even outward appearance.(Inugami)
Is that true? For example, even someone who is loathed by the Lunaria Orthodoxy Church, as a witch?(Soug)
Witch?(Inugami)

When Inugami heard that unsettling word from Sougs mouth, suddenly the entrance door was opened. When they turned their backs to look at it, there was a woman covered with brightly colored fabric on her head. In this country, to wear a simple colored clothing while covering the head with colorful fabric is the sign of a prostitute. Originally, prostitutes covered their head with a cloth when they entered a mans house so their face wouldnt be seen, but when this became their dress code, they covered themselves with brightly colored fabric.

The prostitute tried to enter the house, but the moment she noticed Inugamis presence,

 Ah, so you are in the middle of a conversation. Please excuse my rudeness.(Prostitute)

She spoke while making a bow, before trying to leave the room in a hurry.

Wait, Merula. You can trust this guy here.(Soug)

Soug called the prostitute to stop, who then ceased her steps and slowly turned back.

Is that true?(Merula)
Yeah. Either way, he is on the side that opposes this country.(Soug)
But arent you a person from this country......(Merula)

Why are you drinking with the enemy?, was what the prostitute wanted to say while dropping her shoulders. Then, she moved towards the table and then sat down in a vacant seat. While Inugami was confused about the situation, the prostitute took off the cloth covering her head.

!?

What appeared before him was a beautiful woman in her twenties with transparentlike fair skin, golden hair, and then pointed ears. Pointed ears are a characteristic of the Elves. However, in addition to that, she had another characteristic. Her eyes are crimson like rubies.

High Elf......(Inugami)

Crimson eyes, the unique characteristic of High Elves. Elves and Dark Elves dislike interacting with other races, but there are also some who lives among other races in multiracial countries like Elfrieden. Some marry with other races and have children with them. However, High Elves extremely dislike interacting with other races more so than the other Elves. As a result, High Elves only exist on the island northwest of the continent, the High Elf country that didnt permit any entry of other races than Elves, Gahran Spirit Kingdom. Additionally, Gahran Spirit Kingdom has its own Spirit Religion.

Thats why Inugami was surprised. It was unbelievable that in a country that was a headquarters of a different religion than the High Elves, there was a High Elf who dressed in a prostitute attire. Seeing how Inugami was at loss for words, Soug smiled wryly and placed his hands on top of the High Elfs head.

Let me introduce her. She is Merula Merlan As you might have guessed, she is a High Elf. Even if she looks like this, she is a granny who has lived twice as long as me. So be careful.(Soug)
How rude. A centuryyearsold High Elf is equivalent to a twentyyearsold in Human, you know? Even in the Spirit Kingdom, one hundred years old will only be treated like a youngster.(Merula)
Umm So what kind of relationship do you two have?(Inugami)

While both of them started to converse with each other in a familiar manner, the still dumfounded Inugami, regained his sense to ask that question. Merula and Soug looked at each other.

What kind housemate?(Merula)
No, no, since the house owner is me, shouldnt it be I took you in my care?(Soug)
Wait a moment, the one who is being looked after is you! If I dont clean this house, you will soon leave things around untidily!
You are worrying too much about petty things!(Merula)

The two somehow began to fight like an old married couple. Inugami then asked for an explanation while holding his temple.

Umm, if you can. Please, can you tell me from the beginning From why a High Elf like Meruladono is in this place?(Inugami)
Hm? Ah, sorry, sorry.(Soug)

Soug regained his composure and then crossed his arms while looking at Merula.

Can I be the one who will explain this?(Soug)
Hm Ill leave it to you(Merula)

With Merulas approval, Soug began to narrate the story.
',0,'2019-05-01 23:00:01.000000');