INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(1,'chapter 1','Translators note This is a complete retranslation effort starting from the very first chapter of Death March Web Novel version based on the latest edit by the author at the time of the translation, some or the majority of the parts may differ greatly from the initial version (mostly due to more accurate translation) and I intend to take better care as to not mistranslate a word. Rest assured, older translation will not be deleted and you can still read all chapters where they are retranslated chapters will have their own page. This has been a long time coming, please bear with me. )

(Authors note below)
 20161217 To those who read this for the first time (especially those who come here after reading the manga or watching the anime version)
The story flow of the web novel version differs greatly to that of the anime (as well as the manga and light novel version)
In addition, this web novel version has not been revised since it was first written, thus there is a lot of superfluous explanations and it may be lacking in descriptions, please be warned.
For those who want an easier to read version, have a look at the light novel version if you would.



Orbiting stars.
Innumerably, endlessly.
Have you ever seen a shooting star?
Whether its fleetingness captivates you visually, or prompts you to make a wish, I think to each their own.

However, would you ever get the chance to watch a falling meteorite as it tears through the sky before your very eyes?
Breaking the sky apart accompanied by loud thunderous roars, striking the earth with overwhelming mass and speed.

Theres probably some who have seen the scene on TV or video sites... Yet Im sure no one would wish to look at it closely in person as they fall down.

Yes, right now, around a hundred meteorites are falling down the earth one after another before my very eyes.

Nay.

I shouldnt speak like its none of my business. This cataclysm was perpetrated by none other than yours truly.

As a result of me picking a choice without thinking 10 minutes earlier, the earth is being gouged out by meteorites now.
The meteorites strike the ground a few kilometer ahead, infringing the Enemies there, while the dots displayed on the Radar in the corner of my view are disappearing as life disappears at the impact point.

And once most of the meteorites have vanished into the earth, sounds of something falling finally arrived here slightly late, then after a bit, tremor hit.

Right before a cloud of dust that looks like its crawling on the ground arrives here...

Suddenly, a pain that feels like divine punishment assaulted me.

Its as if my head has been split open.

As if my body is getting torn apart.

A moment before I parted with my consciousness due to the pain, I got swept away by the cloud of dust.







Time goes back a bit.

Im working in my day off to meet the delayed deadline of an already delayed project. Im a programmer by trade working at a subcontractor outsourcing company that develops stuff like smartphone games, and PC browser games as commissioned by large publishers.

Even for a black company, they dont usually assign one person to take care of two projects or more at once. However, a young junior programmer disappeared on us on the verge of the delivery deadline, leaving multiple bugs and spec changes behind! Darn it!
There are only two programmers in this company with its high turnover rate, Juniorshi and me. Of course the company refused to post an urgent filling, and I ended up having to take care of not only my project, but also Juniorshis.

Alright, done with inputting and filling in all the Classes, now I just need to get autodocument generate documentations and relation diagrams from the source code, then I can start the bug hunting for real.

I stretch a bit and crack my neck.
Looking around, practically all the seats are filled here, you wouldnt believe its supposed to be holiday. The person sitting next to me, a debugging subcontractor, is muttering something while working on his assignment, but no one is paying him any heed. No one has any time to do so. All designers and planners around me continue to work silently with dead vacant eyes.

When I came back to my workspace after getting a cup of coffee, my PC was already done with its job, displaying documents needed for debugging.

Still, I really cant get fired up working without documentations like this.

Guess theres no point in complaining to Juniorshi who was thrown into real world work without any OJT time. There were four programmers when Juniorshi joined half a year ago, but now theres only me, is this company going to be alright I wonder.

Sat, Suzukisan, weve got a new claim from our client, they said WW was too hard for beginners go fix it, what do you think.

This darn dude almost blurted out Satou. Weve been in the same team for half a year already, stop getting it wrong!

When I turn around, the director who doubles as a planner, Mr. Obese is walking here with his usual troubled look.
He looks gleeful somewhat even though were getting a new problem here. Why does this company have so many M anyway.

WW is an abbreviation of World War the PC browser game were working hard on. Its an orthodox strategy game with a bit of additional social functions.

Didnt they tell us not to lower the difficulty anymore because they were afraid their main target base wouldnt play it if we did?

Thats right, the current difficulty level was decided upon after several meetings with the client. Those wastes of time were really a waste huh. Blah.

Cant we just add back the canceled functions we came up with initially, the Entire Map Exploration and the three map annihilation bombs? We can give rewards like a rare title or something to those who dont use it until the end so those who are good wont use it themselves, how about it?

Weve got no more time, lets go with that Then, Suzukisan, go for it.
Mr. Obese said it nonchalantly like usual.

Hold it right there. Im currently busy debugging the MMORPG, go get the OK from our client first. We dont have time to fix it if they reject the idea after we put it in.
OK. Im gonna give em a call now.

Mr. Obese went into the smoking area with his phone in one hand while swaying his large build around.

Afterward, I continued to work while muttering to myself.
Along the way, Mr. Obese gave the GO sign, and I deceived my hunger with some junk food as the night deepened.

I corrected the countless careless mistakes that Juniorshi left behind until midnight, and then left the rest to the debug team.
Oh right, what was the name again? We always call it with MMO or RPG, I cant seem to recall the official name. Oh right, its FREEDOM FANTASY WORLD. Its easy to mix up with WWs former name, FANTASY WAR WORLD, so no one used it. That reminds me, the code name was FFW in the specification document. Later on, WW lost the FANTASY part, so its not really confusing now, but too late by then.

While working on correcting WW, I also deal with new bug reports from the debug team.... Another all nighter huh.

The check continued on until early in the next morning, and I miraculously managed to deliver the MMORPG client program.

Of course, theres still some bugs remaining, but we have this trump card called Update Patches with web distributed games, so no worries there. I can hear the users booing already, but Im sleepy. I forwarded the corrected WW executable package from the debug team to Mr. Obese with intraoffice mail, and took my first sleep in 30 hours under my desk.

Ah, bliss. Laugh all you want at this corporate slave. Right now, sleep is justice.




TLN If youre reading this novel at any other site than Sousetsuka .com you might be reading an unedited, uncorrected version of the novel.


Are you familiar with lucid dreams?
Its a dream where youre aware that youre inside your own dream.

Right now, Im in a wasteland.

I was going to say Porusans famous line, but I held myself back.

Thats right, a wasteland. Id imagine its like the Grand Canyon in the United States.

How did I know this is a dream?

One, I remember falling asleep under my desk. As for the other one, I can see four Icons in the lower right corner of my view, and a gadget written as Menu in the upper right corner.

The same as WW I just worked on.

However! This isnt the first time Im dreaming about debugging after falling asleep in the middle of a death march. Though, I have no clue why its in a wasteland, and not my workspace or my own room...

Maybe its because the room I was in was too dry or something.

I tap on the Menu just because and look at the menu screen. A nearfuturistic looking semitransparent window gets displayed in my view... Sheesh, I weep at my lack of imagination, theres really no way I can apply as a designer or a planner even by mistake.

The Menu is separated with tabs, and besides the usual INFO, MAP, UNIT MANAGEMENT, STORAGE, ALTERNATE, LOG, and SETTING entries, theres some new ones, STATUS, EQUIPMENT, MAGIC, SKILL that shouldnt exist in WW.

Did it get mixed up with FFW that I debugged together last night.

Well, no point in demanding consistency in a dream.

Looking at Status, my Level is 1, all attribute values including HP and MP are 10 points. These are the basic status values before you allocate bonus points. Come to think of it, this was the last thing I checked, the Character Making value test.

Hm? What are these Type of Occupation, Rewards and Punishments, etc, they shouldnt exist in FFW though? Whered they come from.
Age is 15 year old... Was I unconsciously wishing to reexperience school days or something?
Type of Occupation Managerial, I dont have a single subordinate even now though!
Still, the fact that theres this Affiliation None, wonder if I actually wish to change job...

These entries are truly profound... Ah, I want a vacation.

Stuff like Unique Skill (Ability), Unit Creation and Unit Arrangement are probably mixed in from WW, but is there any point for the Unique Skill (Ability) column to be listed on the Menu?
And what is this Indestructible column at the end? What a mysterious dream.

My Equipment are a polo shirt, chinos pants, and sneakers. Hey, theyre my attires from yesterday. The Storage has a wallet, a phone, and a yellow box with impressively balanced diet inside. Oh right, I was going to eat it but lost to my drowsiness so I left it on the desk.

Magic and Skill are a blank space.
I have 10 skill points on the Status Screen, but theres nothing to allocate it on, tragic.

I open Setting and add Radar and Map as basic functions. Map displays a wide area and the location Im at. Radar is almost the same as Map, but it displays all units in an explored area regardless whether theyre friends, foes or neutral parties.

Radar only shows a small white dot indicating me. Its displaying a 100 meter wide area around me, the rest is grayed out.

Umu, no enemy in sight. Boring. If only I were in a plain, I would have lay down and indulge myself in sloth.
Im not into lying down on a rugged ground.

I nonchalantly look at the four icons on the lower right. Theres one Explore Entire Map and three Meteor Showers. Its the emergency plan for beginners that I threw together haphazardly after a briefing session with Mr. Obese.

Explore Entire Map is just as the name suggests, it explores everything within the Map youre in. In addition, it enables you to browse everything about units in the map including their weakness.
Theres too much info that its useless without a certain amount of knowledge though, but Mr. Obese insisted on it so I implemented it anyway.

I tap on it like I would a smartphone to test it.
The Radar gets fully filled in, displaying countless red dots indicating enemy. I lower the Radars magnification, showing a wider area.
Theres so many enemy here, half of the Map is buried in red... Dont you think theres way too many?

Gotta make my own army Units against this many foes!
Fighting off a huge army with a small force sure fires you up!







...There were times when I thought that.

Unit Creation.... No possible unit to create.
Unit Arrangement.... No created unit.

Are you telling a level 1 character to charge at these lololol
As expected of a dream. Way to turn up the absurdity.

I glanced at the Meteor Shower icons in the lower right corner.

This must be heavens wills telling me to annihilate them with Meteor Shower!

The parameter of Meteor Shower was set while I was in high tension during an allnighter. Its got a stupid amount of power enough to defeat everything other than the last boss in the campaign scenario, and the hidden bosses.

I wanted to send a message to beginners, Please use these on the maps you cant finish.

Wanna push it?
Yes

You bet I do, Yaruttseburakkin

Somethings not quite right with that last part.
Perhaps Im still high off an allnighter, I tap one of the icons.

...The silence is deafening.
I had such a huge expectation, yet nothings happening?

I sulk right there while feeling a bit sad. The rugged ground on my back feels painful.
And then, those things come into my view as I look at the sky....




Thanks for waiting.

Were finally back at the opening scene.

Real name, Suzuki Ichirou. Character name, Satou, this is how my life in a parallel world began.




(Authors note below)
My first time writing novel
It sure is hard   



Attention
Satous Unique Skills (Special Ability) besides Menu wont have their turn until the final stage of the story.
',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(1,'chapter 2','Hello, Suzuki here.

Having weak presence, Ive been called Satou since my school days for some reason, this is Suzuki.

Even though you dont feel pain in a dream, you can still feel the signs of pain when you dream of dental caries.

No?

Well, thats good too.

TLN This is how a chapter begin whenever it feature Satous perspective, it usually dont have anything to do with the actual plot at all. Just some random musing, so dont think too much of it



I seem to have fainted for two hours following the severe pain after the Meteor Shower receded.

Time is displayed on the menu. Convenient.

My body is half buried in the ground.

Huh? I cant get up...

Feels like when you cant get up off the bed during winter.

My hands seems to move but menu operation is troublesome since it feels heavy.

If only I had ESP to operate the menu and check the log.

When I imagined such thing, it really happened. Truly a dream. Its unrestricted in strange places.

When I see the log while thinking that, the message Satou Level Up showed up and harassmentlike Chararacchara famous level up SFX rang when I scroll through the log.

Continuing for a while, I finally found a different message log.

 Skill Summoning Magic Foreign World Acquired.

 Summoning Magic Meteor Shower Acquired.

TLNWhoops, was also skill earlier. Edited

Meteor Shower is that meteorite summoning magic, right?

A magic that does not exist in FFW. Summoning magic is only supposed to summon spirits or golems. Though the world part bugs me. There is no Foreign World in either FFW or WW. It bothers me even for a dream.

The Level Up messages continue on.

Since theres a time stamp, the order of message cant be wrong. In order its Skill Acquired Magic that belongs to the Skill Acquired Level up

In other words, this is the kind of RPG that does not let you automatically learn skill and magic as your level rise! TLN They work independently, you can learn skill or magic regardless of level when you meet the required condition

No no, why am I doing analysis inside my dream...

Apparently my habit of analyzing thing from gamer era surfaced.

But its alright, even if its embarrassing inside a dream, lets continue the analysis.

Even in this situation, my real body is properly taking a rest anyway!

I close the log and decided to check the magic and skill tabs.

But, before that.

I set the Log to open by default from Setting.

Its troublesome to switch every time.

The magic are Meteor Shower and All Map Exploration.

The skills are Summoning Magic Foreign WorldNature Magic Foreign World.

Seems that All Map Exploration belongs to the nature magic category.

Are usable magic and skills registered?

But I cant use it if I dont remember it anyway, this time the exclusive use of initial bonus seems to be an irregular pattern. As a programmer I immediately deny the notion.

My status has changed considerably. After all, my level has been raised to 310 from 1. The limit in FFW should have been 50 at the time I went to sleep. Though we raised it to infinity when we ran stress tests for the server, so the memory from that time mightve affected this dream.

Experience point is lined up to astronomical 9 digit number I dont feel like reading it. I want to have a comma splice for every three digits. Lets propose it for the next update.

Strength (STR) and Intelligence (INT) attribute values are 99. FFW client stopped the value at 99 but there is no actual upper limit. During the time when we lift off level restriction the attribute value also rises accordingly... In other word, the actual value might be higher here too?

Endurance (HP) and MP (Magic Power), and Stamina (SP) became 3100 harmoniously. These were set to rise by a fixed 10 for each level. There are many approaches to reduce consumption by various skills.

Its made this way since, according to the Planner, the adjustment is troublesome if we raise each one differently.

In addition, availale skill points are also 3100.

Did it decrease from the acquired skill earlier?

When I properly look at the skill column, both skills just now are greyed out. Magic Art  Foreign World Lv0 shows up when I tap it. Tapping the Lv text shows Please Allocate Point (110) confirmation dialogue, so I tried to put 10.

Without any SFX the text becomes white and changes to Magic Art Foreign World Lv10. Skill points become 3090. 1 level  1 skill point. Easy to understand. When I try to tap on it again, Maximum Level message shows up.

On the magic column, All Map Exploration text is white while Meteor Shower remains in grey.

It seems that you cant meet the condition to use magic if you dont allocate points to the skill level. So I also put 10 point to Summoning Magic Foreign World Lv0.

There are also many things added to title column.

First Monster Slayer. Yep, I understand this. Then Scale Tribe SlayerScale Tribe CalamityScale Tribe Natural Enemy Im not sure what scale tribe mean but theyre probably reptile monsters. Furthermore, Wyvern SlayerDragon SlayerLowerDragon SlayerInfantDragon SlayerAdultDragon SlayerAncient, and other Dragon Slayer with  inside continue endlessly.

Well, with this many dragons I can understand why such an impossible levelup to the 300 range happened. The map was dyed red before the attack earlier.

Then, just like the title for Scale Tribe, Dragon Tribe Natural Enemy appeared and the last title is God Slayer.

Do you believe in god?

Wait, when did I kill one?

I tap the log and search for God.

Defeated the Dragon God AkonKagura!

There, the cause for god slayer. I see, the meteor shower also killed the god.

I see, I killed it...

Only 2 remain, god slaying technique!!

No no, why would I kill them?

However, the log doesnt show damage so I dont know how powerful it was. In FFW and WW, the last blows damage should have been displayed but... I guess theres no point questioning a dream.

Since its become easier to move, I raise my body.

I turned off the display icon for Meteor Shower in Setting since I dont want to become the enemy of humanity by carelessly clicking it.

Oh right, I havent used the the magic column yet.

I switched to the tab with All Map Exploration and Meteor Shower in it. Now then, how do I use it. I wonder if tapping it normally work.

There is no particular change when I try to use All Map Exploration.

When I check the log theres Magic All Map Exploration used message. MP become 3090 in the status. Seems like it uses 10 MP. I added HP and MP to the basic indicator since its annoying to check it every time.

Theres nothing reflected in the radar besides myself.

Its nice that theres no enemy but mass killing like this is not funny. No, wait, its normal for a game.

Since there are no victims I guess its okay? I slowly tap on Meteor Shower. It feels like something is greatly pulling out my vigor. I saw my MP continue to decrease. It stopped after consuming exactly 1000 points.

It didnt feel like this the first time I use it, guess it wasnt using my own magic power back then.

I look up at the sky. Theres no meteorite yet. Considering before, it should be soon.

And then groups of meteorites tore apart the cloud.

Huge. What the heck is that.

Its 100 times bigger than the earlier meteorites.

I instinctively run before thinking.

Of course toward the opposite side of the drop zone.

I run as fast as I can with the continuous huge meteorite impact sound in the background.

As if running through water, the air resistance is too strong. It normally feels sluggish when you run in dream, is it just me?

I rolled over to the other side of a tablelike small wall of rocks. Even though I said small, its about 100 meter in radius so it will do fine as a shield.

A tsunami of dust sweeps apart just right after I went into the shelter. Peoplesized rock occasionally roll through the dust cloud, my spine becomes a bit cold.

I wonder how long it will elapse. When the dust cloud settled I climb the rock wall to see at the fall.

Since the slope is pretty steep I tried jumping over 50 cm height.

Pon. I fly 5 meter feeling like hearing such SFX.

Landing on the ledge of a rock with slight panic.

This status rise, what a joke

I tried to deceive my trembling body by speaking to myself.

That giant meteorite earlier is probably thanks to INT increase too

I run up the wall by alternatively kicking through various ledges and rock edges like some kind of ninja in games and manga. How convenient. Even in a dream, I have pretty high adaptability if I may say so myself.

I can see the collapse of a mushroom cloud in the distance. Most likely, its sediment that got caught up on the fall. This would create crop failure and health damage disaster if this was the real world, due to reduction of sunshine.

Its a dream anyway, theres no helping it even if I think about it.

I think its bad that its too real.





TL Note I changed Spirit World to Foreign World since I realized that its actually about summoning the meteorite and dont want to confuse it with Seirei which also translate to Spirit. Im sorry for people who have read it early.
',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(1,'chapter 3','Hello, Suzuki here. My character name is Satou, but Im Suzuki.

I want to talk with somebody instead of doing this monologue!

Since the dream continued with no sign of it being over, I decided to look for human habitation.
Thankfully at the edge of the wide map, there looks to be some kind of highway.

Three days since then. Ive been been walking day and night yet am still only halfway there.
Stamina gradually decreases, but its still 28003100. It decreased about 100 each day.
If this was a game, I should be okay for 28 days, more but Im at my limit already.

Im not sure if its because of my stat or dream, but I dont feel tired. I did get sleepy but I endured it.
Right now Im walking, even if I run the stamina decrease rate isnt really different. If I jump around the stamina decreases faster than running but honestly, its still within margin of error.
Then why am I walking now? Thats because Im free.

You dont understand?
Yeah.

At the start I was singing a one man karaoke while running.
Originally my repertoire is small. I ran out of song immediately.
The surrounding scenery is magnificent but unfortunately it rarely changes.

Then, I decided to read the long log.

Im not a text mania but since its painful to silently walk an aimless road I began reading the log from the very first entry.
Of course, its hard to read text while running, so the I began to walk. Absorbed with reading the log, I forgot my original purpose of getting onto the highway.

The log started with Magic Art All Map Exploration is used, then Defeated messages for defeating Lizardmen and Dragonewts until the Dragon God.
Afterwards theres Defeated All Enemies on the Map and lastly Source Conquered Dragon God Valley. Source? Whats that? Mystery words pop out.

Then the log continue with loot and level up record.

The loot records occupy 80 of the log. Common gold and equipment. Then various dragon materials like horns, fangs and scales. Up to this I can understand the quantity and quality of the loot, but in contrast the remaining ones are strange.
Lizardmen and Dragonewts corpses are stored in the storage. Is it for Necromancers use?
The rest are probably daily goods used by the scale tribe like food and fuel. I cant lay out such items though... furthermore, theyre all named with Broken as prefix, arent these garbage?

As one would expect, reading through tens of thousands detail would be troublesome.
Thankfully, WW storage window allow you to search and classify by item type and user tag.

I designed them as a common interface to decrease working steps needed. Im a bit proud making it on the same level as the latest OS filer.
Of course theres also optional folder functionality, but when you tap the stored item inside the bag, the the subtype  will expand and you can take it out without confirming.
TLN The runon in this sentences is absolutely crazy, Ill appreciate help on this one. The trivia is not that important for the plot though so you can just interpret this whole geeky paragraph as  Its awesome.
Of course you can also take out the item by dragging it too. In addition, you can also put user registered items on custom tab. This was added because its annoying to input search word every time on a smartphone.

Well, the story wandered off for a bit. I cant blabber on program scheme too long.

I open two storage windows to begin organizing items. First, I create folder for each general type. Then I will create subfolder for subtypes inside.

In the setting screen I enabled the Automatically Stack Identical Item Type option.
Without this enabled, the total number of item would be too much.

By the way, we call overlapping position like this as stacking in FFW and WW. The term exists in most RPGs so I will omit the explanation. I wonder if the etymology comes from piling pieces in the old simulation board game?

Anyhow

Gold loot is mostly money.

The most numerous one is Ancient Empire Gold Coin. Its 1.001.2000, wow. I tried taking out one, its quite big and heavy. Its about as big as a 500 Yen coin and as heavy as five 100 Yen coins. If I remember right, its about 30 grams. Since in real world money, the coin is about 47 grams, this one is too big and heavy. If we calculate, the total is 303 Tons... an absurd weight. In the real world,  about 2500 Tons of gold is mined a year. Hoarding so much gold like this, is very Dragonish.

The next numerous one is Saga Empire Gold Coin of about 40K. Im interested whether there are many empires or if it was a new empire built after the former one was destroyed by dragon. I tried taking out this one too, its a bit smaller than 500 Yen coin but the weight is only about 7 grams.

And the third, Crimson Treasure 30K. Fantasylike money appears! Taking it out, its about half as heavy as the ancient gold coin with rubylike appearance. Sometimes it let out a flash of light from inside creating cyber (chick?) feeling. A bit mysterious.

The rest are 10K of Ancient Empire silver and copper coin. Saga Empire and Shiga Kingdom silver, small silver, big copper, copper, pennies. Drag Holy Kingdom big silver, silver, copper, and various money with Kingdom suffix. These are all in total 7000. For now, I sort it by country.

Still, WW and FFW didnt have sets of money... In WW it was called Wol, and in FFW its Kaane, theres no other currency beside it.
These various currencies may come from the quiz show I saw during my break, impacting this dream.

There are also jewelry, ornaments and works of art besides money. A simple silver ring, emerald fist, ornamental crown, lifesized gold figure, decorative short sword, and many valuable looking things numbered about 300K.

Most are just expensive looking normal items, however there are magic items mixed in around 5 of the total.

The detailed information for the magic items are long so I skimped on reading it. Think of it like reading internet slang that you dont understand the meaning of on a bulletin board. There was no mounted jewelry in WW and FFW. TLN Okay this is so abrupt, Im not sure whats what. This guy need an editor.

Understanding some of the explanations, there are some things that catch my attention. Able to produce 100 litres of water a day, Hell Water Jug (Water Bottle).

I took out the Hell Water Jug (Water Bottle) when I found it and drank the water. I wanted to wash my face and hair with it but gave up since the dust from the meteor shower is still floating around.

Other things are able to keep 1000 sheets of money, Magic Wallet, stacking 30 varied types of up to 30 items, 30 Holding Bag. I put those 3 things on favorite.

Since I have infinite storage, theres no point in a magic bag, but I like seeing a long sword slide in and out of the bag.

The second day ended with such feelings after organizing treasury.



Moving on with this and that on the third day. The clock has crossed 0. Im not sure if its really the third day since I slip off the clock every 4 hours.
Since Id get tired if I sleep on this wastelands rough ground, I keep walking instead. Theres a full moon tonight so theres no problem with visibility.

Having principle of saving the best dish the last, its time to check for the weapons and armors.

I sort the type to show materials and corpses. According to the log There are only a few lizardmen and dragonewt corpses. This is most likely because of the meteorite, having received the full blow, there wont be many remains. However, there are a lot of dragons corpse. Defeated Log shows that there are 23 times as many. This place become like a dragon version of an elephants graveyard.

Most are materials from body parts, but there are also iron ingots, medicinal plants, timber, and stone. Theyre probably from the things crushed by the meteorites. Random body parts and scales are many. Regardless of type, there are 7.9600K of them. Even an old dragon skin mold and husk exist.

Huge, if the scale is this big, wonder how huge is the body.

Losing to curiosity, I took out an adult dragons scale, its about 50 cm. While Im at it, I took out lower dragons scale to check, its palm size.

Broken item are put in specific folder. I was thinking of throwing it away but stopped since it could become a literal mountain of garbage.

Finally, its equipment.

There are around 30K things. The Scale tribes spears are especially abundant, things which begin with Scale Tribes are around 20K. Neither are magic items, theyre all normal items made of copper or iron. There are only few strange armors.


I tried taking out a scale tribes spear. 2 meter long wooden handle with sharpened bone tip comes out. It cant pierce when I tried it out so it probably isnt a war spear.

The magic items are such like Dragon Skin Armor or Dragon Scale Armor, seems like items blessed by the dominant tribe are around 100. Those are probably scale tribe armor.

The remaining 10K of half are Dragon based large swords, spears and bows. These are probably legacy from the (scale tribe) people who beat dragons.

The special effects kind are jewelry, but I dont understand the effects so lets put it off for now.

There are several interesting things, especially Holy Sword and Divine Sword
My long lost Chuuni heart is stimulated.

Why are these Holy Swords are named Excalibur, Durandal and such?
There are also katanas named Muramasa and Kotetsu

Tension up

I enjoy wielding Excalibur. It should be quite heavy but I swing it carelessly. Beautiful traces of light come out. However for physical weapons isnt sword disadvantageous? But its popular in flashy games.
Oh yeah, there is no specific name for the Divine Blade.

I expected to get sword skill when I wielded the sword, but it didnt happen. It will likely come out if I defeat an enemy with a sword.

Unusual things also exist. Cannons and ballistas That are usually placed on fort to counter flying enemy on the sky. Skimming the descriptions, it seems that it seems that it doesnt use gunpowder, but magic power instead.

Another one, its gun!
Handgun 100, Rifle 50. Magic powered gun 12. Just for the record, there are no guns in FFW. There are cannons in WW though.
I take out the smallest magic gun.
Its a Derringer sized gun with excessive ornaments. The trigger guard is attached.

In the real world, guns have safety locks, over here its a switch with 0, 1, 3, 10 scale. I set the scale to 1.

I aim at a rock nearby with one hand and pull the trigger.

A psshh sound comes out, yet no trace of the shot could be seen.
When I confirm the rock, a hole about as big as a 10 yen coin penetrated it. Its a 2 meter thick hard rock. MP consumption is 1. There is no recoil, its a laser gun like feeling. This is totally SF instead of fantasy.
With scale set at 10, the rock completely crushed. This would kill Ogre level demons in one shoot. MP consumption is 10.

Its way too efficient.

Its an item that would totally break balance of a game. No one would want to use magic.

After that, I played around with the gun while moving for 1 hour.





Finished checking items in general, I began thinking something that bugged me before.

What is Source anyway?
I mutter to myself.

Its about the Source Conquered Dragon God Valley from the log.

...this is probably not related to tax withholding. TLN SourceGensen, gensenchoushuutax withholding at the source

Besides on the log, theres no mention of Source Conquered Dragon God Valley anywhere else.
Usually I would have said Its a dream anyway but something is bugging me.
Having a change of pace by thinking while running, I feel like coming up with something, Im immersed in running while feeling good, I feel like anythings good.

Im incoherent if I do so say myself.
Spending many days without any conversation is no good after all. Im not suited for that.

Ive walked for 1 day before I knew it, and finally almost reached the highway.
There is no human habitation according to map. I can see the highway leading to west on the north west.
By the way theres no presence beside me displayed on the map.
I wonder if they ran away in fear of that catastrophe aka meteorite shower with thunderous roaring and earthquakes.
I stopped the halfbaked running and began walking.

I want to take a bath

As expected, not having one in 3 days make my head itchy.
Its not possible to boil water, but there is water. Thankfully the dust cloud has lessened, its possible to wash off the dirt.

I store my clothes and shoes into storage, find a suitable rock and pour water down from the top of my head.

Its a bit cold, but Ive come back to life.

Feeling relieved, I took out clean clothes from the loot and wipe off the water.
In order to take a refreshing nap, I take out a suitable bed with canopy from the loot and put it on the ground.

That day I slept for the first time after 3 days.

The next morning, I noticed that I could hang laundry on the canopy so I wash my clothes with bucket from storage and hang them up, side by side.

After that I lazed around on the bed while eating jerky  for half a day.
Im expecting that someone will pass on the highway, but theres no change in radar, even untill afternoon.

If this was a light novel or manga, I would have met a princess being attacked by thief

I like template ay.

The service in my dream is so bad.

I turn a blind eye to the fact that I easily get treasures and significant level up, cursing my luck.
A flag would occur if this was a dream or tale, but nothing happened.

After changing into the dry clothes, I began walking toward the highway.




TLN A chapter with a lot of geeky and trivia exposition. Please bear with me for a while.
',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(1,'chapter 4','Hello, Suzukis here. Its so lonely, I may die of loneliness.

I truly, genuinely want a companion.
After reaching the highway, I got lost on the route its been 7 days since then and I still havent reached a human habitat.

Speaking of which, somehow I got these skills  Sprint, 3 Dimensional Maneuver, Jump, Sing, Camping and Washing.
However, those skills remain grey because I havent allocated any points to them.

I seemed to have learned Sprint3D Maneuver and Jump after the second meteor shower, but overlooked it due to the confusion.

When I was playing around with the gun, the map got in the way so I changed it and the log to icon view, then I noticed something. From the log its written as place with bed and canopy but...
Isnt that a camping ground? I retorted myself.





Onto 8th day, I finally found a villagelike place on the wide area map.
I sprinted to the village. However theres no presence of people according to the map. I wonder if theyre taking refuge or got destroyed. No, they shouldve gone back if they were taking refuge, since its been one week.

Before long I reached the junction road heading to the village.

There stood a crude guidepost on the roadside. A 1.5 meter commonly made stick nailed to the ground. The destination is written with something like ink.

I cant read it...

Something is written there, but I cant read it at all. At least, its different from all the languages I recognize from the real world. In fact it looks like some sort of fictional writing from a game.

I keep the map on maximum magnification, showing wide area.
While feeling slightly irritated for not being able to read the long awaited clue, I hit the junction on the map. Theres no actual meaning to this action, but this time it seems to be a bulls eye.

Welcome to Enikei Village,Seryu City 32Km,Until Kazo Kingdom 105Km I can read it! ...Or rather, its in Japanese!

Pop up displays pile up above the map, each written in Japanese. Is this an effect of All Map Exploration? Cheers for opportunism.

For the time being, Seryuu City seems to be the closest from here, but its too plain.

After all this time, lets go to the village first. I know that theres no one there, but I feel like Im losing if I head straight on!
There might be some hidden quests there! If this is my dream, there must be one!!!

The village comes into view on the other side of the field in just about 5 minutes. Between the narrow street extends fields that looks like they have been abandoned for about a year. Its a world of weeds.

Even from a distance, I can distinguish that many house look burnt down. Every house has one thing in common the roof is completely burned down.

Judging from the position it seems to be a Village Abandoned by the Attack of the Dragons.

There are no bones lying around, but many things like harvest baskets, wooden hoes and various farming tools are scattered around as if they were left behind in hurry.

As I stared at the tools lying around, though its too exaggerated to call it a revelation, I got a little idea.

If I got washing skill from doing laundry. Maybe Ill get something if I dig some farm land?

I took the hoe and dug the farmland with it.

Cultivation Skill Acquired
Farming Skill Acquired

It appeared on the log as expected. Though I didnt expect cultivation to pop up.
In order to get better, Ill try many other things.

Suddenly, I pluck what appears to be a vegetable peeping out between the weeds.

Harvest Skill Acquired

Its a bit fun.
Even so, its easy mode, eh?
Come to think of it, what is this vegetable? Is it spinach, komatsuna? TLN Wiki it p
I stared at it and AR message saying Hisaya Grass, leaf vegetables. Its seldom eaten raw, usually processed as condiment or pickles. popped up on over the vegetables.

Yep, its convenient alright. Seems like this is a different function than the map search.

Incidentally, I tried pulling out weeds.

Weeding Skill Acquired

I didnt cut it, I just pulled it.

I went into something that looks like a windbreak, I take out an ax and chop down a suitable tree.

Lumbering Skill Acquired

I wonder what next? I feel like trying some ideas.
I write a mathematical formula on the ground with the hoe.
112, there.

Arithmetic Skill Acquired

Fumu, if this is OK then Emc

Lost Knowledge Skill Acquired

The famous theory of relativity is a lost knowledge?
Id rather get a flying skill or some such...

This time Im writing some random characters in Japanese. TLN henohenomoheji, google it P

Painting Skill Acquired

Paintersan would be mad.
Next I wrote  alternatively which made it looks like a childrens play.

Game Skill Acquired

Is anything fine?

I begin trying one thing after another.
I tried shaving the burned wood on the back of a house with knife in the shape of a stick.

Carpenter Skill Acquired
Weapon Creation Skill Acquired

In addition, I wrap the stick with a leather I took out of the storage.

Leather Craft Skill Acquired

Making a makeshift broom I sweep it under my foot.

Cleaning Skill Acquired

Im running out of material. When I tried to find more I discovered a cemetery with a native god shrine.
Since I grew up in the countryside, I inadvertently join hands as if praying for my grandmother.
I offered some jerky and poured some sake cup.

Prayer Skill Acquired
Title Devout Believer Acquired

Umu, I didnt intend to get a skill but...

The material is dry already, I began to get tired of the skill acquiring, lets get out of the village.
Its sunset just as I depart on the highway.
Im walking toward Seryuu City, according to the guidepost. After accustoming my eyes under the moonlight, in the dark, I sprint down the highway.

When its near dawn, the wide area map showed the whole Seryuu City. Of course, unlike Enikei Village it has a lot of people.
TLN Wut, its only 30 Km away and you have enhanced speed...

Finally, finally civilization

I cry while trembling with expectation of finally talking with people.

....This is such a long dream. Dont you think so?

',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(1,'chapter 5','I finally got to Seryuu city.
Lets strip it naked with All Map Exploration!

The population is around 120,000. Its a remote region within Shiga Kingdom ruled by an earl. 95 are from the human tribes, the other 5 are beast tribes, scale tribes and fairy tribes. Average level is 23. Highest level is an old man magician who is 48. The knights range from 520 with most of them on 812. Normal soldiers are around 57. There are a lot of bows as a dragon countermeasure. Male to female ratio is 56 with lots of females. Its unknown whether its because of difference in birth rate or because of a high male mortality rate.

Unexpectedly, townspeople only have a few skills. Most of them have 13 skills, some even none. Having 20 or so skills may be the exception rather than the rule.

Only a few magicians exist. There are only around 2000 people with magic skills. 90 of them have Livelihood magic while the remaining 10 seem to belong to the countrys army.

Its as expected in a way, but level 310 is truly extraordinary. In addition, I got carried away and learned a lot of useless skills which would make me conspicuous.

The map could show the name of the streets and merchants houses to some degree. Even though it could not tell you what kind of item theyre selling or their reputation, its still quite useful for a first look of the town. Unfortunately, the search function only works for people, I could not find shops with it.
Seen from reduced scale, the town is shaped like a 6 Km diameter oval, along with two 3 Km long  shaped, bowing darumalike objects attached on each side.

I enlarge the map and confirm each area...
First, closest to the gate, there seems to be a general residential area. Beside the gate are stables and inns. Joining them, the local lords castle is located in the center of the neighborhood. Something like a temple or magicsomething is at the center of the city. Far from the gate, the lords mansion stands. And a cafeteria that looks too small for city use, but too big as an exclusive for the local lord.
When I tried to search for people with Adventurer as job, I got nothing. There was no Librarian either, which probably mean theres no library. However, there were some Slaves...

After checking so many things, Ive arrived at the front of the gate. Showing up together with the sun in the horizon, perfect timing! Though the gate is closed at this time.

I thought that there would be other people in front of the gate but there was actually no one.
It would be nice if there are some villagers who are going to sell their vegetables in the morning.

Is it only in my dream that stores dont open until 10 in the morning?!

Wearing a polo shirt and chinos pant might be too noticeable. I searched the storage, found several robes and took out the only non wornout dark green robe to wear. The remaining robes are magic tools but theyre too wornout, I cant bring myself to wear them. This robe smells a bit like mold.

My body looks like the time when I was 15 with its small stature. The robe is too long and is dragging a little on the floor. Thanks to that, the sneakers are hidden. My mustache didnt even grow after one week. I was so happy and showed it off when it grew during the freshman period of university life. Though, I shaved it immediately when I got a girlfriend...

While having a recollection of the past, I continue to wait for the gate to open.





When I got near the gate an uncle soldier came out while saying some unknown words.
The long awaited conversation featuring a mysterious language!!

Shiga Language Skill Acquired!

I love Easy Mode!
God hasnt forsaken me!!

I operated the menu and acquired the Shiga language skills extremely fast! Overenthusiastically allocated 10 skill points with glee.

Whats wrong kiddo? Do you have a stomachache?

A bearded soldier follows suit. I dont think calling a man in the latter half of his twenty, kiddo, is right... Oh thats right, my outer appearance is that of 15 years old.

Good morning. Yes, Im fine.

I replied amiably with a vague smile, trademark of Japanese people. Even if the partner is a middle aged uncle, its a human after a long time. A smile is only 0 Yen!

The heavy gate opened behind us during the conversation.

Kiddo, show me your identification per the rules. If you dont have one, pay the 1 copper coin tax.

What! ID in a dream!
I thought of showing the driver license from my wallet but it looked like itd be troublesome so I stopped.

Im sorry, I dont have an ID card.

I honestly say it. If I got refused then I could just stealthily go in, if I got caught then Id just run. Im seriously acting selfimportant inside this dream! Yes, a dream! This is a dream!

Did you lose it? We could issue a new one but thatd be 1 silver coin, ya okay with that?

Oi oi, can you so easily issue a new one?
There are some Shiga silver coins in the storage if Im not mistaken.

Please allow me to get a new one! My horse, surprised by the loud roar 8 days ago, escaped while carrying my luggage. If I havent got beef jerky in my pocket and didnt find some spring water on the way here, I would have been dead!

Im a smooth talker if I do say myself I may truly have a talent for swindling.
As if responding to my inner monologue 

Excuse Skill Acquired
Deception Skill Acquired
Negotiation Skill Acquired

Cheap skills!!!

Come here kiddo!

Uncle soldier beckon me to the guard station located besides the gate.
Even after telling excuses with great pain, uncle soldier just ignored it. His ignorability powerlevel is too high!

Just in case, youre not on some wanted list or a thief, right?
Yes, of course.

Im a member of general public unrelated to crime

Then, put both of your hands on top of this Yamato stone and speak your name.

Uncle guided me to the place with a 20 wide LCD display inside the guard station. Where in Yamato this Yamato stone can take me? Can it goes to space?
TLN Yamato in yamato, not sure what the heck hes talking about here beside the reference to Space Battleship Yamato series.

For name, it should be character name right

Satou.

Right after I spoke my character name. The lithograph started to dimly shine with blue light, and some words appeared.
Its the same letter as the one on the guidepost earlier. I could properly read it thanks to Shiga language skill. I want an English language skill!

Kiddo, its fine to release your hand now.

On the lithograph, Race Human,Level 1,Class Commoner,Affiliation None,Occupation Type Management,Title None,SkillNone,Reward and Punishment None are written.

Eh, isnt the level different?

Leaving me puzzled, uncle soldier smoothly wrote the displayed writing from the lithograph onto a paper using a quill. Finally, he stamped the entry form, Person Approving Retainer of Earl Souryuu, Knight Soun, with his ring on the place where his name is written.

Here, dont lose it next time.

Uncle soldier passes me the certificate while I give him a silver coin and one big copper coin taken from my pocket.

What, you can properly put money on pocket. Arent you quite watchful! Keep your ID safe from now on.

Also, dont forget this. Residence permit is only for 10 days. If you want to stay longer, apply your request here or at the ward in the town hall. Wherever it is, the procedure costs 3 copper coin.

If youre caught in town with an expired permit during homeless hunt, the penalty will be 1 silver coin. If you cant pay it, youll be forced to become a slave.

The explanation flowed smoothly, as if hes always doing it.
Lets be careful not to forget it.
Falling from vagrant to slave is severe! I wonder if its the same with homeless hunting during Edo period?

Thank you very much.
Ou, youre welcome to the guard station for consultation if theres any trouble. Dont mind about the fee.

Ive heard something good. Thank you, knight Soun.
I go out of the guard station with a polite farewell.

I took quite a bit of time inside the station, that the main road from before to inside the gate is now filled with people with an exotic feel.





Theres a semicircular space with a radius of about 20m between the gate and the town. Whether it is there to prevent confusion or for war, I dont know.

The certificate is made with material resembling Japanese paper. Even though the cityscape is in western style, the parchment is... Is it the limit of a dream?

I fold it carefully into my pocket. Of course, I put it directly into the Favorite folder inside my pocket.

Lets look for a change of clothes while strolling the main street for now! Even after many washings, its painful to only wear one set of clothes.

I want to take a bath before changing clothes...

I tried to search for a bathhouse attendant, but it was not applicable. Im wondering if I got the occupation name wrong or if there are no bathhouses in the first place. If its the latter then I have no choice but to get hot water and take a bath inside my room in the inn later.

You there! If you havent decided on an inn then come to ours! Ill give you some service!
WhWhat...
Its fine, its fine, its cheaper than all the others Not, but it is filled with a devotion to delicious food and clean bedding!

When I was thinking about how to get into a bath, someone suddenly pulled my arm and my consciousness came back. A random high tension girl embraced my arm while pulling it. I wonder if shes a barker, its a scene you usually see in a campus festival in modern Japan.

Im dragged to a store that looks like a bar while thoroughly enjoying the soft feeling wrapped around my arm. Because I entered from the main street it feels dim.

Mom! Mom! I brought a guest!

Out of consideration for the softness transmitted to my arm, I have no more hesitation in deciding to stay in this inn. Yep, poyopoyo is justice!

Its the first customer since the stars fell, isnt it... Hm? He doesnt have any luggage, is he really a guest?

An aunty with a big body came out of the bars counterlike place. Contrary to her figure, shes a beautiful woman with a face filled with dignity. I think shes about 30? Im sorry for calling you aunty.

However, even though these motherdaughter pair are natural beauties, why are they fat! Lose 10, no 20 Kg, and they would be in my strike zone!!

Though Im out if shes a married woman. An affair will only make everyone involved unhappy!

Because of that, er, starfall? My luggage ran out with my horse due to the uproar... Fortunately my wallet is safe, and I somehow got into this city
Thats a disaster. Our inn charges 1 big copper coin for one night. If youre fine with sleeping together with other guests in a big room then itd be 2 copper coin. Meal is served at this bar for one serving. Its the limited service for an accommodation.

Fumu, I dont know the market price, so lets investigate the value of big copper coin and silver coin by paying for 10 days.
The landlady has arithmetic and cleaning skill so there wont be any mistake with calculation.

Then, its for 10 days please.
Okay, its exactly 2 silver coins then.

I take out 2 silver coins and pass it to the landlady.
It seems that 5 big copper coins equal 1 silver. It doesnt seem like she miscalculated either.

Landlady, could I have my meal now? If possible a light one.
If its something like oatmeal, black bread, or vegetables then I could get it out right now. And Im sorry but meat hasnt appeared in the market, so theres none of that.
Then black bread and a stew please.
Okay, Ill bring it immediately, so just sit there. Martha, write the guest book for our customer here.

The landlady goes to a place that looks like a kitchen, and in her place, a poster girl who looks like a head clerk from a historical drama holding a notebook tied up with string, Marthachan, skips here.
Shes cute but she looks around a high schoolers age? A bit too young for me.

Yes, Im writing for you so can you give me your name?
Its Satou.
Okay Satou. Your occupation and age please.

I almost said 29 year old programmer. According to Status screen, it should be 15 years old.

A peddler, 15 years old.
Marthachan smoothly fills the notebook. It looks to use yellow papers like the one used on Japanese calligraphy.
When Martha attempted to chat after writing the inn registry, the landlady came out and told her to clean the 2nd floor.

The crystal clear stew with consommelike soup, cabbage and carrotlike vegetables, and as announced before, without meat, is more delicious than I expected. There are 3 slices of about 2 cm thick black bread. Its harder than what Ive heard, but feels about as hard as senbei. It looks like it would fill your stomach. I bite it full of curiosity. It feels like a meal full of strange SFX.

Feeling satisfied for a meal after a long while, I pay the price. 1 copper coin. Usually its prepaid.

Come to think of it, Martha and the landlady use loanwords like service and oatmeal normally, but when I observed their lips, I know that theyre speaking different words. I wonder if its an appropriation of the Shiga language words.

Lip Reading Skill Acquired

...Yay, I can become a spy or a detective orz.

Or rather,

Landladysan, I intend to buy some groceries and a change of clothes, do you know any good places?

Although I know stores location from the map, there are too many of them, I dont feel like going around all of them.

If you want something with high quality material, then the tailor on the city center would be the best. For something reasonable then the east is good, and if you want it cheap then the west. If you have money in excess then you could tailor it on the high class shop at the inner wall, but youd need gold coin. General good stores usually open near clothing stores, so please plan your budget carefully.

Thank you very much. Ill go to the east street since tailoring seems that it would take some time.
Okay, although the public order is good on the east, be careful of pickpockets. Since sometimes there are migrant workers from the west there.

Yes, I will be careful.

I leave the inn while thanking the landlady for the advice.
Its starting to get hard to say that this is a dream. But I will work hard!



TLN Since the next few chapters are quite slow paced, I will hold off on releasing a chapter a day and will release only after Ive finished translating up to chapter 8. Please understand. )

',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(1,'chapter 6','Satous here. Its Suzuki but Im Satou. This is a dream right? Please say that this is a dream, Johny.




A slight unrest began to arise inside my mind, when I went shopping on the earls territory metropolitan, Seryuu city.
Its a few minutes walk to the east of Marthas mother inn.
Come to think of it, I havent heard the name of the inn, the map says Monzen Inn is that it?

Immediately after entering a series of food stalls, I smell something good.
Eh? Isnt this the smell of soy sauce?
Okonomiyaki and crepes are being baked on an iron plate. The seasoning seems to be soy sauce. Just as I thought, its fine to call this a dream. Theres no way soy sauce exists in a fantasy!

Even though Ive just had a meal, I buy one of them.

Yep, its delicious.

I want pork cutlet sauce rather than soy sauce. The other stalls seems to be frying something that looks like croquettes. What a high food culture for a fantasy!

Im going around various stalls, trying various foods.

How much is this gabo for 3 serving?
Its 2 Pennies.
Expensive, isnt it 1 penny?
young maam, like that, we couldnt eat then How about 2 pennies for 4 then?
Give me 5 for 2.
It cant be helped, since young maams a beauty, Ill give in.

I listened to some interesting conversation between an aunty and a food stall owner. Haggling basic eh? Since Im accustomed with just buying whats written on the price tag, it looks difficult.

Eavesdropping Skill Acquired

Anything I can help, young man?
Fumu, from the conversation earlier a gabo seems to be about 0.40.5 pennies a pop. Since this uncle has arithmetic skill, he shouldve been able to calculate the minimum amount.
I want to buy 2 gabo, is it alright with 1 penny?
Youve heard the conversation just now eh? Its alright then. Young man have arithmetic skill too right. Youre pretty good for such young man.
Its just a normal mental arithmetic though...
I got 2 gabo fruit for 1 penny. Gabo is actually a fistsized red pumpkin fruit. Incidentally, penny is an oblong shaped coin weighing about 1 gram, its a yellowish copper coin with high impurity.

Estimation Skill Acquired
Haggling Skill Acquired

But I didnt haggle it?
Is it because I saw the earlier haggle by the aunty?

While wondering what to do with the gabo fruit I looked around me. The stalls are all around the size of half a tatami yet there are many of them. Furthermore theyre lively.

While looking around the stalls I raised Estimation, Haggling, Arithmetic, Negotiation, and Scamming skills to the max level.

After strolling around for 10 minutes, I finally found my goal.

I choose a large shoulder bag from one of the stall. The price is displayed over the bag thanks to the estimation skill. Handy.

It seems that Id get hated if I suddenly bought it on market price. I understood the knack after 3 stores. I could get it to half the market price if I try to bargain for 34 times... but honestly, its annoying.

The spoils from the stalls... Large Shoulder Bag, A Bit Stylish Bag, Commoners Underwear for Top and Bottom  10 set, Hand Towel in Various Size, and Handkerchief. The towel is just two piece of clothes stacked together, Im a bit dissatisfied.
Also, even though I cant sew, 2 Meter Hemp Cord  5 Bundles, Cotton Thread  1 Roll, Sewing Needle  5, Sewing Scissors, Cotton Cloth, somehow theres a lot of them. Since I have the storage, theres no problem even if a lot of them end up unused.
In all its 4 silver coins. Unexpectedly, clothing is more expensive than foods.

I dont think I need to say this, but the first thing I bought, the shoulder bag, is a dummy. I will put things I bought into the bag and directly send it to the storage without being seen. I put some commoners clothes inside the bag so that it doesnt look flat.

I wanted to buy some some robes or cloaks but, only wornout or cheap looking things were available so I didnt.

Thats right, I want to confirm heat retention of the storage. Ill buy some properly hot things and put it inside.
Shopping is fun

Now then, I wonder what else?

Chair lined up ahead of the stalls and there are a lot of old mans are drinking sake since morning.

Drinking in daytime... Oh yea, tableware!

There shouldve been a shop that sells them among the one Ive visited so far.
Thats right, shoes! I forgot about shoes.

I want a reminder. Theres a memo field inside the Exchange tab of the Menu, lets put the shopping list there.

Its been a while since I saw the Exchange tab, over there I found the reason why I was level 1 when I entered the city.
Those status was the same as the one written in this page. The upper limit for each attributes are chosen from the drop down box here. Even the skills and title can be chosen as None.

I could give false info however I want, huh...

I buy a pair of shoes suitable for rough journey, and a robe matching the shoes, also a sandal for each foot.
I thought of buying everything since I have the storage but I cant find shoes with correct size. Ill just order it on the main streets shoes store later.

Hungry after a second round trip, I set my eyes on a store selling something that looks like shoyu ramen and went into it. Although the noodles are different, its a proper ramen.
Other was kneaded flour coiled in a stick varnished with misolike substance then baked. It felt like a mismatch, but it was properly delicious.

I also bought cups, pots, tableware that caught my attention, I bought them all. Also, a kitchen knife, hot pot and frying pan. Somehow there were no cutting boards, do people here dont use them?

I casually bought a bathtub made of metal. As one would expect, the tub is too big to place into the bag so I brought it quietly to the back of an alley and put it into the storage. And since it would look unnatural if I went back into the east avenue, I decided to exit onto the the main street.



At the east street stalls overflow, making the street there squeezed on the center. Here, the main street is about 6 meter width, horse carriage and humanpowered cart are going back and forth on it.

Just like the street earlier, different from what I imagined of a fantasy, the street here are very clean. There are no body of dead animals lying around. There are also no bum on the alleyway.

The stalls are sparse here unlike on the east street, in exchange there are a lot of stores instead. There are many people with good attire walking around the street.

Are collars popular?

The people who are pulling carts generally wear collar. When I search for it, theyre slaves. Mysteriously, all the slaves wear collars, reason unknown.

A carriage coming from the downtown drop its speed to match the walking speed of the pedestrians.

The carriage passes in front of me. There are around 10 woman slaves inside.
My eyes are glued to one of them. A girl befitting to be called Yamato Nadeshiko featuring black hair and black eye, although she looks like shes been in a long journey. Since the great majority of people looks like northern European, this is probably the first time Ive seen Asian face.
The girl set her eyes downcast, our eyes met when she lift it up and even though theres no dramatic development, somehow the little girl featuring orthodox northern European face with light violet wavy hair standing next to her looked very surprised when she saw me. No well, Its troubling when you stare so hard at me... Im not into loli.

When I took a long look at the little girl, a pop up with her name and level appeared next to her.

Arisa. Level 10. Her level is high for a little girl.

Further informations appear.

11 years old.

Title Witch of the Lost KingdomMad Princess

Skill Unknown

The carriage disappear toward the west street.

Frankly, those were some trouble inviting titles... No no, I wont approach it okay? Absolutely!



Lets go back to my original purpose.
I searched the map for tailor or shoemaker, and found them gathered near the side street, so I went there. Most of them had young women clerks which isnt fit for an old man, so I went into the shop with a kindhearted looking middleaged couple.

Pardon me, Do you have a solid merchantlike robe with calm color?
Welcome, please come over here, we will bring you sample cloths. There were 5 ready made goods on the rack there, but its been selling well recently.

The husband guide me to the lounge and take out the samples from the interior. In harmony, the wife bring out tealike drink. Its an elegant cup.

Since itll be cold from now on, how about this thick fabric? If youre going on a journey then we can also prepare a matching waterproof cloak, how about it sir?

These are excellent goods. Maybe. These are the types that the largest clothing store buy a lot with a lot of color variety, The 5 variety of the well selling goods probably also have cloak order accompanying them TLN Im sorry, this sentences is total gibberish for me too in the raw.
The tailoring will need 5 days to finish. If I only buy one then its only 3 days.
5 gold is quite a large sum, but its a money needed for necessary things.
However, I want at least 1 change of robe before the day is over. I feel like I dont know the TPO if I go the inner wall with this battered robe Im wearing.

Master, since this shop is only for tailoring, do you know shops selling ready made goods? As you can see the robe Im wearing is battered from the journey, I want to buy temporary clothes before the tailoring is done.
TLN Not sure why Satou call the shopkeeper goshujin

For ready clothes, I recommend Poel Clothing on the Teputa street. Although its my stupid sons clothes, I can assure his clothmaking skill. Its quite famous among the commoner so its quite enough for temporary clothes.

Promoting his sons shop eh... I politely refuse when hes offering to draw a map, its enough with oral instruction so I get out of the shop. I almost forgot to take the deposit receipt until I was called by the Mrs. Shopkeeper in hurry.

I bought two robes, one is a calming dark brown color and the other is a gaudy red with yellow stripes. The former is as said before, and the latter, lets omit the story behind it. My line of vision went into the valley of the female clerk...

Of course I also order shoes on the tailor. 1 for walking in the city, 2 boots for journey.


When I came back to the Marthas inn after shopping, the sun was already sinking for the day.

',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(1,'chapter 7','Satous here. The petty one who doesnt want to get involved with the dangerous looking slave, Satou.

Its a dream! My selfsuggestion is reaching the limit.

Its another world thats most likely a dream, become my compromise. Yes, however small the possibility is, I wont give up.



My heart broke when Martha said that a bathroom is not available except at the lords castle. Since the food and sanitation culture of the town were pretty high, I thought that bathrooms also mattered.

I thought of bathing in the room with the tub, but since itd get the room moldy due to the moisture, I was told to do it in the backyard since it also has a well.

The backyard is about 6 tatami wide. The well is not that far from the back door, its not equipped with a pump and is the type that uses buckets to draw water, usually used in older days. Im using one of the two wooden tubs available.

I thought that I would struggle but thanks to my high (STR) status, it was easy.

Thought they said backyard, the fences are only around my waist high. Even though there are only few pedestrians traffic, there are still some. Its already dim during the dusk but if I take a bath in place like this, isnt this like an exhibition?

When I look around, theres a partitioning screen near the back door.

I see, am I meant to use this?

After I put up the screen, I started bathing with the cold water.

Though its only about as high as my waist, its enough for shielding the view.

...Dang, I forgot to buy soap!

Theres probably no shampoo, but if its just soap it should be available. Lets look for it tomorrow.

A woman comes out of the back door. Its a blond beauty on later half of her 20. How do I say this, this town has a lot of beautiful woman.

Our eyes match.

Iyan.

...Disgusting. Im disgusted with myself.

After the woman finished drawing the water into the tub, she started bathing after putting the screen.

Theres a screen between us, it is, but!

Whenever she moves a little, the upper body is!

Well, I estimate it to be D cup, purunpurun, its asserting itself!

She did cover it by hand, but it got unexposed occasionally...

No, no, Im not a DT, I summon all my strength to avert my gaze and go back to wash my body. Restrain yourself my healthy lower body!

I glanced at the females face, shes wearing an expression full of deliberate composure!

As I thought, adult women are nice!!

I wipe my clean body with the towel... Wonder where I should put the water.

There is no drainage.

Its okay to sprinkle the water on the garden plants. Theres a drainage below the plants.

I wonder if I looked pathetic since the woman teach me. I thanked her and went back to the inn after throwing the water.

I want you to overlook my glance during my way back.

Surveillance Skill Acquired

Poker Face Skill Acquired



I changed into the new robe after bathing, feeling refreshed.

The dinner was still mainly vegetables, but the taste really came out, it was delicious. Though I prefer it to be a tad thicker.

The service was raw salad. Id be happier if it were meat...

I feel that the protein was a bit insufficient, so I took out jerky from my pocket and gnaw on it.

I wanted to drink sake, but the customers around me drink an unrefined looking local sake, causing my craving to stop. I want to drink some cold beer

After the meal, there is no light when I come back to my room. Not even light from magic. Work harder, Fantasy!

When I ask the landlady whos struggling with drunken people, Its 1 copper coin if you want a lamp, so she says. By the way, the lamp has to be returned later. It seems that the guys who are awake stay in the bar to drink the night away, only the guys who want to sleep go back to their room. TLNduh!

The toilet is shared together, scooping styled. When I think about it, doing it on the outdoor like I always did until yesterday is better than this. Theres a bundle of straw that looks like a wiping cloth. Since it seem that Id get hemorrhoid if use that, I cut a handkerchief in suitable size and use it. This isnt ecofriendly, but I dont want to save for this kind of thing!

I come back to the room after finishing the business.

The light from the lamp is dim. Theres probably some magic items able to be used as illumination among the loots, but Im afraid that the inn could be half destroyed if I take out the wrong item.

The rented room is 8 tatami wide with a single sized wooden bed, there are also chair and desk. The chair having no roller legs is a matter of course, but even the desk doesnt have a drawer.

The window is small enough that when you put your face to look outside it feels cramped. I wonder if its for ventilation, the landlady warned me to close it before I sleep to prevent crime.

Fortunately, I can still see the menu screen even in the dark.



From now on, lets fill the memo column in the menu with objectives.

Act 1. Lets really think about whether this is a dream or reality.

For the time being, as to not have anything that Id regret later if this was really a real another world, lets temporary recognize that this is really another world. Yes, temporarily. I cant give up in that regard!

Act 2. Lets be wary of the surroundings.

Even though I could escape from danger thanks to my high level, I should not act too antisocial that it would hinder me from touring another world. Also, although I dont know how strong they are, there are gods here lets not be overconfident of my own ability and act careless.

Act 3. Lets obtain means for selfdefense.

I want to obtain a method to neutralize opponents, or skills and magic to weather through a troublesome situation. I want to refrain from raining meteors every time theres trouble.

Act 4. I want to find a way back to reality.

If its a dream then a way to wake up, if its thrown into another world situation then lets find a way to go back. I dont intend to act too positive though.

Act 5. Might as well enjoy the exotic atmosphere.

With how real this all feel, sightseeing is enjoyable. My budget is plentiful anyway.

Lets ask Mr. knight if theres doitall person whos willing to guide me for a tour.

Are these all?

I added soap in the postscript before I slept.



This is the first time Ive been asked to be a tour guide.

So she says, the 20 years old jackofalltrades, Nadisan, while laughing bashfully. Shes not a beauty, but shes full of cute gestures. She must be plainly popular.

Normally shes a contractor for miscellaneous job from the worker guild, apparently, hired to become a tour guide is quite unusual.

Since walking is a pain, I borrow a roofless horsedrawn carriage to tour the town.

The west street is not too prosperous so the citizen not only deal with the front store, but also various slightly gray goods in the back.

For example?

Well, something like love potion on the alchemist and pawn shop, moneylender, even brothels exist. The slave company is also located on the same street.

I reacted to slave word. I dont want to meet that little girl I can only feel trouble from her.

Oh? Do you have an interest for slaves? Guard job is impossible, but they can be made to carry your load or chore during your journey. Most peddlers usually employ them.

Recently they closed after the stars fell before, but theyre going to resume. They will open 3 days after the day after tomorrow.

It seems that the slaves that the slave company buy are leftovers from the slave market or in training, theres usually a slave auction held once a month.

If you want to hire guards for the journey, then there are a lot of them gathered in the bar. Since its hard to find trusty ones, in case the need arise, do ask JackofAllTrades by all means!
TLN Not sure if shes talking about herself or the worker guild

The carriage proceed along the inner wall of the west street.

Around here is the craftsmens block. Woodwork, blacksmithing, leather craft and many other craftsmen are here. Most of them dont have an actual store. Generally they open small store or stall and directly sell their products to the customer in person.

Can they repair weapon and armor over here?

If you have a craftsman acquaintance there then theres no problem but generally to prevent trouble, one usually go to the arms shop as an intermediate. There are high class arms shops within the inner wall for knights, and shops for soldiers and general public on the Biso street in the eastern town. Hunters like to go to the western town since there are a lot of meat processing stores, there along with an arms shop.

Lets just roughly look at each store on the street. It probably wont end in one day.

Come to think of it, isnt the lords mansion located in the north?

Youre well informed. Do you want to look inside the inner wall for a bit? Though theres only harvest work to be seen there.

Well, Id hate to be mistaken as a food thief if I walk there on foot.

The carriage proceed along the western inner wall, toward the narrow street between the inner and outer wall.

After advancing for a while we got to the open gate, the soldiers guarding the gate are present too. The driver nods to the guard and continue to enter the northern district.

Theres a vast farm land over there. The carriage continue on the farm road. The farmers are harvesting Gabo, the same fruit I bought yesterday.

I dont know if its normal, but there are a lot of kids around elementary school age helping.

Those children are probably from the orphanage, since its currently harvest season, the children from the town might be coming here to work.

Even children work? No more neet?!

My face became strange for a little while.

If theyre not from wealthy family, then children around that age normally work.

Dont they go to school? WHO please, support Seryuu City!

School? If theyre nobles or come from wealthy family then they usually study in private, but schools only exist in the royal capital.

Furthermore, the Gabo fruits those children harvest are actually staple foods for people with light wallets.

Hmm? But the inn serves bread and thin rice porridge, sometimes even stew right?

No well, Monzen inn is relatively high level so they dont serve Gabo fruit. On the west street, most stalls are selling flat bread, gruel, and pickled vegetables. It has strong smell, bitter, and hard to chew so the wealthy rarely ever eat it. Since you can eat cheap things until your stomach full, theyre not only eaten by the orphanage children but also by about half of the general populace too.

If its like that, wont normal potatoes suffice?

The quantity of the yearly harvest differ. Although the harvest area becomes smaller, they could be harvested in one month period and rarely fail. Furthermore it has a property that enable it to fertilize fallow. Thanks to Gabo fruit, the number of orphanages has increased dramatically.

What a convenient fantasy fruit. Even opportunism has limit.

Even so, Nadisan is quite knowledgeable... she looks like a literary woman graduated from the royal capital school.

I wasnt raised in the manor inside the wall, the food situation in rural area was pretty terrible.

She grew up outside eh, I wonder if the lord has a monopoly. Its a subtle mystery.

Its goblins favorite food. If its not a place surrounded by wall like this, goblins would come in a blink of an eye and devour them greedily.

Back in the day, there was an explosive increase of goblins in the northern part of the kingdom, the scholar who investigated the cause concluded that its related to Gabo fruit. In those days, if someone found a Gabo tree they would burn it down, then it began to be used as a food source and now it has become the staple food of the lower strata people. However, even now, if someone finds a Gabo tree, they would burn it down.

The wall around the farm is quite a bit lower than the towns wall. Is it around 2 meter and a half?

I wonder what that tower standing tall 1 kilometer away is? Its been in my mind since before I entered the farm. Its around 20 meter tall and look surprisingly rugged.

Thats a self defense mechanism built to repulse Wyvern attacks. The one in the castle is for the castles defense, the one here is used when the attacks come to the farm.

About 40 years ago when the black dragon attacked, it played a very big role. Back then, although a tower half as tall as this was brought down, the records depicting the successful repelling remain.

Repel... Did it escape through the sky?

Leaving aside the Wyvern, as expected, defeating true dragon is impossible. Only someone like the ancestor king, Yamato who was a great magician or Saga empires hero are able to do something like that.

...Hero, eh?

Saga empire has this heros summoning great magic. Since the compensation is enormous, except when the 66 year Demon Kings resurrection cycle begin, its not performed. Yamatosama and Saga Empires founder were also heroes who were summoned when the world was in crisis back in the day. Its so dreamy.

As I thought, these Yamato and Sagasan... I can slightly see through why the holy swords were named Excalibur and such.

When the story was finished, the one lap tour around the farm was also finished. The carriage went back towards the inner wall.
',0,'2019-05-01 23:00:01.000000');
INSERT INTO `chapter`(`book_id`,`name`,`data`,`number_of_read`,`upload_date`) VALUES(1,'chapter 8','Satous here. Dumbfounded because the long awaited route abruptly closed.



An existence that can only be expressed as a demon descended.

Having horns like a ram, dark red glowing eyes, and a jet black gleaming body. Four arms, bat wings, and a split stinger tail. Truly a demon.

The demon mowed down the knights effortlessly.

The plaza became full of toppled carriages and dead bodies scattered around...

It suddenly appeared in the middle of a peaceful afternoon.



The inner wall is mostly occupied by mansions of the nobles and the wealthy, while the shops are only located on the side of the main street leading to the castle.

When we arrived at the inner wall, we got off the carriage and started taking a stroll around the luxury shops guided by Nadisan.

This is the shop who sells the high class armor for the knights. The shop doesnt only have metal armor. Sometimes they have magic armor available. Youd need dozens of gold coin for that

This one here is the largest jewelry shop around. Especially their service for ruby and sapphire is the best in the kingdom. As for commoners, I recommend Liz jewelry across the street.

If you want robes tailored, then this shop is good. Although it had been on the wane for a bit last year, after the son who come back from the royal capital succeeded the shop, it became awfully popular. Since 1 article cost around 23 gold coin, commoners dont come here but it may be a good idea for merchants who just got a big job to order here to increase their prestige.

Nadisan is so wikicertified knowledgeable. As expected of a jackofalltrades?
Although luxurious looking horsecarriages come and go from the castle to the plaza, the traffic is relatively sparse for our carriage to stop on the road.

I wonder if Nadisan is thirsty after that much talking?

This is the most popular open terrace shop for sweets and tea among women in Seryuu city.

Nadisans eyes are sparkling. It doesnt seem like a plea, just pure longing.

Nadisan, my throat has dried up, since were here already lets take a break there.
Yes, I understand. Please go ahead, I will wait here.

...hmm?

Suddenly, a lonely decision?!

You dont want to enter, Nadisan?

Im sorry, since this is a high class tea shop...

Please tell me stories about the castle and the plaza while we drink. Of course, I will pay for the tea and sweets.

Nadisans eyes are shining... but it quickly become cloudy. Is it that expensive?
Lets be forceful here!

Lets go.

I tenderly led her hand inside the shop.



I was a bit prepared for it, but a set of tea and sweets are only 1 silver coin. Isnt that cheap? That was what I thought, but considering that amount will let you stay for 5 days in a high class inn, its probably quite expensive for the general public.

Even though its an open terrace cafe, they put stylish marble tables there. The teacup and the pot look expensive too.
The tea tastes like an Assam tea. It doesnt seem like they put sugar and milk though.
In exchange, they serve it along with sweet baked cookies. It seems that you eat it with cottage cheese or jam spread.

Is this that desirable? When I look at the girls in the surroundings, everyone is eating something that looks like hotcakes full of honey cream.

So I called the waiter and asked for two serving of the hotcakes. Its 3 silver coin.

Delicious

Its certainly delicious... Particularly the charming happy expression from Nadisans face!

Even while enjoying the sweets, Nadisan didnt forget her professional duty, she talked about the land reclamation of the Seryuu city castle and various other things.

Such peaceful afternoon tea time did not last long...

Initially, a big shadow passes through the plaza.

Followed by deep bass screams.

Crossing the plaza, a gigantic fireball pierces into the castle.
One spire crumbles forward.

After the cloud of dust and the sound from the collapsed spire expire, the people who were frozen in the plaza restarted. Screams arise, soldiers urge them to take shelter in angry voice.

The four armed demon float in the plaza while spreading his black wings.

Is there a custom for a demon to come play in the afternoon in this town?

There is no such thing! Quickly, lets escape!

Nonchalant Skill Acquired

I said something foolish. Nadisan pulled my arm to urge me to escape but maybe her waist lost its strength, she cant get up.

Though its shameful, I look at the situation around me to incite me to act. Somehow I cant digest the acquired information very well. As if my body is moving separately from my head.

When the second fireball crosses through the rampart to the castle wall, a semitransparent blue barrier stretched in the sky, and the fireball stopped.

Just before the barrier set, groups of knights and magicians come out of the castle wall.

Besiege him! Why dont you guys utilize the castles defense?
For someone so pathetic he cant even get up off his chair, I cursed at the knights. Even though I got knowledge about battle only from manga and games...

The demon lands on a flower bed in the center of the plaza. He deliberately choose to fight on the ground instead of in the sky with its advantage.

The heavy infantries in the back let loose a rain of arrows. Sounding like violent rainfall, the plaza is pierced. Unfortunately every arrow that hit the black body of the demon is repelled.

3 cavalry knights holding lances on their left hand line up to assault the demon together. The demon exhales purple breath from his mouth toward the knights. Is that a poisonous breath? The face of the knights and the horse who got direct hits look pretty terrible. The knights who lose their vigor fell off their mounts and got kicked by the demon, flying to their companions.

From the opposite side of those knights, another 3 cavalry rush forward!
Although the first two knights got mowed down by the demons tail, the other one who was late successfully impaled the demons body with their lance.
The knights who got mowed down by the tail stand up, and strike the demon with their swords.

The demon parries the knights attack with his claws and roars!
Debris and little rocks rise and swirl around the center of the demons jet black body, the swirling speed gradually increase...

A chilling premonition crawls up my spine!
I lift my body off the chair. I thought of escaping but Nadisan, whos still weak in her waist and cant get up, entered my vision.

There is no time. Escape is impossible.

I pulled down Nadisan from the chair, and direct the thick marble desk toward the demon as a shield.
I couldnt see it from my position, but at this time, the demon unleash shockwaves of vacuum blades in all directions.

It was close but I managed to hold Nadisan behind the marble desk.
Heavy impact shock the desk. Part of the marble was blown off from the vacuum blade. The view from the open terrace becomes tragic.

The shops surrounding the plaza are either partially or completely destroyed. One is even struck with a small carriage terribly.

I took a sidelong glance at the demon who began to walk toward the soldiers who were struck with the shockwave, and carry the unconscious Nadisan to leave the plaza.

I run like the wind toward the main street while carrying Nadisan.
The people who are evacuating are hustling to the inner wall, its a dangerous situation.

I turn down a side street before arriving at a packed crowd. Since both my hands are occupied, I operated the menu with thought and raise the 3D Maneuver and Jump skill to level 10.

Approaching the inner wall, I found a big building. Jumping while alternating between the inner wall and the buildings wall like a ninja in manga, I cross over the inner wall.

Retreat Skill Acquired

When I check the map, it seems that here is the verge of the east town.
I forcibly stop a carriage that happen to pass by and ask the driver to escort Nadisan to the workers guild. The coachman is reluctant at first but he cheerfully undertook the job when I shove him a gold coin.

Persuasion Skill Acquired
Bribing Skill Acquired

This is not the time to scrimp on money.

I rely on the memory from yesterday, and take out a certain item from the storage while running.

',0,'2019-05-01 23:00:01.000000');